*FEIRA.CMD  V 2.01
*PROGRAMADOR: F�BIO A. RASCHE
*CONTR. DE ESTOQ. DE LIVROS
*FEIRA DE LIVROS CEAT/SESC 1992

SET CONSOLE ON
SET ESCAPE OFF
SET PRINT  OFF
SET TALK   OFF
*MENSAGEM DE ABERTURA 
STOR 0 TO I
? CHR(27)+"x5"
DO WHILE I<5
? CHR(27)+"J"
STOR I+1 TO I
ENDDO
? CHR(27)+"J               Aguarde !"
DO WHILE I<10
? CHR(27)+"J"
STOR I+1 TO I
ENDDO

*INICIALIZA��O
DO FEINIC
SET ESCAPE ON

*LA�O PRINCIPAL DO PROGRAMA
STORE " " TO OPCP
DO WHILE T
 STORE " >>MENU PRINCIPAL<< " TO MENUATUAL
 STORE " " TO OPCP
 CLEAR GETS
 DO FEICABEC
 @ 04,00 SAY "XWWWWWWWWWWWWWY"
 @ 05,00 SAY "V1� Cadastros V"
 @ 06,00 SAY "V2� Movimento V"
 @ 07,00 SAY "V3� Listagens V"
 @ 08,00 SAY "V4� Emiss�es  V"
 @ 09,00 SAY "V5� Relat�riosV"
 @ 10,00 SAY "V6� Sair W�DOSV"
 @ 11,00 SAY "V             V"
 @ 12,00 SAY "VOP��O:       V"
 @ 13,00 SAY "ZWWWWWWWWWWWWW["
 DO WHILE .NOT.OPCP$"123456CMLERS"
  @ 12,08 SAY CHR(27)+"y5"
  @ 12,08 GET OPCP
  READ
  @ 12,08 SAY CHR(27)+"x5"
 ENDDO
 DO CASE
  CASE OPCP="1" .OR. OPCP="C"
   IF FILE ("FEI0C.CMD")
    DO FEI0C
   ENDIF
  CASE OPCP="2" .OR. OPCP="M"
   IF FILE ("FEI0M.CMD")
    DO FEI0M
   ENDIF
  CASE OPCP="3" .OR. OPCP="L"
   IF FILE ("FEI0L.CMD")
    DO FEI0L
   ENDIF
  CASE OPCP="4" .OR. OPCP="E"
   IF FILE ("FEI0E.CMD")
    DO FEI0E
   ENDIF
  CASE OPCP="5" .OR. OPCP="R"
   IF FILE ("FEI0R.CMD")
    DO FEI0R
   ENDIF
  CASE OPCP="6" .OR. OPCP="S"
   STORE " " TO SAIR
   CLEAR GETS
   @ 22,08 SAY "CONFIRMA ? (S)im/(N)�o: "
   DO WHILE .NOT. SAIR$"12SN"
    @ 22,32 SAY CHR(27)+"y5"
    @ 22,32 GET SAIR
    READ
    @ 22,32 SAY CHR(27)+"x5"
   ENDDO
   @ 22,07 ERASE
   IF SAIR="S" .OR. SAIR="1"
    ERASE
    ? "   SISTEMA  DE  CONTROLE  DE   LIVROS"
    ? " "
    ? "             Encerrado !"
    SET CONSOLE OFF
    POKE 64683,0
    CLEAR
    QUIT
   ELSE
    RELEASE SAIR
   ENDIF
 ENDCASE
ENDDO

*EOF - FEIRA.CMD 