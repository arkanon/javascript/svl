<?php


?>
<html>

<head>

  <style>

    textarea
    {
      font-size: 12pt;
      font-family: monospace;
      font-weight: bold;
      font-style: normal;
      color: white;
      background: black;
      padding: 5px;
      width: 500px;
      height: 500px;
      border: 1px solid black;
    }

  </style>

  <script>

    window.onload = function()
    {
      document.getElementById('console').focus();
    }

  </script>

</head>

<body>

  <textarea id="console"></textarea>

</body>

</html>
