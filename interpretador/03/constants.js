/* constants.js */

    var insert      = true;

    var charWidth   = 12;
    var charHeight  = 24;
    var overwHeight =  4;
    var consWidth   = 50;
    var consHeight  = 15;

 // var prompt      = '❓'; // 2753
    var prompt      = '?'; // 2753
 // var cont        = '➜'; // 279c
    var cont        = '→'; // 2192
 // var tab         = '⥤'; // 2964
    var tab         = '⇥'; // 21e5
 // var eol         = '↵'; // 21b5
    var eol         = '↲'; // 21b2

    var cursorState;
    var cursorBlinkTime = 250;
    var cursorColors    =
    {
      'focus' : [ 'transparent' , 'white'   ],
      'blur'  : [ 'transparent' , '#808080' ]
    }



    /*
       <http://unixpapa.com/js/testkey.html>
       <http://www.ascii-code.com/>
    */

    var special_key =
    {
      /*
         Caracteres com o mesmo codigo ASCII no Firefox
            -  --  insert
            '  --  cursor right
            "  --  page down
            .  --  delete
      */
        8 : '<backspace>',
        9 : '<tabular>',
       13 : '<carriage return>',
       19 : '<pause>',
       20 : '<caps lock>',
       27 : '<escape>',
       32 : '<space>',
       33 : '<page up>',
       34 : '<page down>',
       35 : '<end>',
       36 : '<home>',
       37 : '<cursor left>',
       38 : '<cursor up>',
       39 : '<cursor right>',
       40 : '<cursor down>',
       45 : '<insert>',
       46 : '<delete>',
      144 : '<num lock>',
      145 : '<scroll lock>',
    };



    var function_key =
    {
      112 : '<f1>'  , // p
      113 : '<f2>'  , // q
      114 : '<f3>'  , // r
      115 : '<f4>'  , // s
      116 : '<f5>'  , // t
      117 : '<f6>'  , // u
      118 : '<f7>'  , // v
      119 : '<f8>'  , // w
      120 : '<f9>'  , // x
      121 : '<f10>' , // y
      122 : '<f11>' , // z
      123 : '<f12>' ,
    };



/* EOF */
