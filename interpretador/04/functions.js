/* functions.js */



    function $(elid)
    {
      return document.getElementById(elid);
    }



    function getSelectionStatus(el)
    {
      var start = end = line = col = lines = cols = 0;
      var normalizedValue, range, textInputRange, len, endRange, lines_set;
      if ( typeof el.selectionStart == 'number' && typeof el.selectionEnd == 'number' )
      {
        start = el.selectionStart;
        end   = el.selectionEnd;
      }
      else
      {
        range = document.selection.createRange();
        if ( range && range.parentElement() == el )
        {
          len = el.value.length;
          normalizedValue = el.value.replace(/\r\n/g, '\n');
          // Create a working TextRange that lives only in the input
          textInputRange = el.createTextRange();
          textInputRange.moveToBookmark(range.getBookmark());
          // Check if the start and end of the selection are at the very end
          // of the input, since moveStart/moveEnd doesn't return what we want
          // in those cases
          endRange = el.createTextRange();
          endRange.collapse(false);
          if ( textInputRange.compareEndPoints('StartToEnd', endRange) > -1 )
          {
              start = end = len;
          }
          else
          {
            start  = -textInputRange.moveStart('char', -len);
            start += normalizedValue.slice(0, start).split('\n').length - 1;
            if ( textInputRange.compareEndPoints('EndToEnd', endRange) > -1 )
            {
              end  = len;
            }
            else
            {
              end  = -textInputRange.moveEnd('char', -len);
              end += normalizedValue.slice(0, end).split('\n').length - 1;
            }
          }
        }
      }
      set1  = el.value.split('\n');
      lines = set1.length;
      set2  = el.value.substr(0,start).split('\n');
      line  = set2.length - 1;
      col   = set2[line].length;
      cols  = set1[line].length;
      return {
               'start' : start,
               'end'   : end,
               'line'  : line,
               'col'   : col,
               'lines' : lines,
               'cols'  : cols
             };
    }



    function set_cursor_pos(ctrl, pos)
    {
      if (ctrl.setSelectionRange)
      {
        ctrl.focus();
        ctrl.setSelectionRange(pos,pos);
      }
      else
      {
        if (ctrl.createTextRange)
        {
          range = ctrl.createTextRange();
          range.collapse(true);
          range.moveEnd('char', pos);
          range.moveStart('char', pos);
          range.select();
        }
      }
    }



    function show(data)
    {
      for(field in data) $(field).value = data[field];
    }



    function readKey(type,ev)
    {

      // <http://unixpapa.com/js/testkey.html>
      // <http://www.ascii-code.com/>

      var eparts = [ 'modifier', 'char', 'ischar', 'which', 'charcode', 'keycode', 'asc' ];
      var key    = new Array();

      key['modifier'] = '';
      if ( ev.ctrlKey  ) key['modifier'] += '<control>';
      if ( ev.altKey   ) key['modifier'] += '<alternate>';
      if ( ev.shiftKey ) key['modifier'] += '<shift>';
      if ( ev.metaKey  ) key['modifier'] += '<meta>';

      key['which'   ] = ev.which;
      key['charcode'] = ev.charCode;
      key['keycode' ] = ev.keyCode;
      key['asc'     ] = key['keycode']==0 ? key['which'] : key['keycode'];

      key['ischar'  ] = ev.isChar;
      key['char'    ] = String.fromCharCode(key['asc']);
      if ( special_key[key['asc']] !== undefined ) key['char'] = special_key[key['asc']];
      if ( key['keycode']!=0 && function_key[key['asc']] !== undefined ) key['char'] = function_key[key['asc']];

      for (epart in eparts) $(type+'-'+eparts[epart]).value = key[eparts[epart]];
   // $('lastKey').value = $('console').lastKey;

    }



    function interpret(el,ev)
    {

/*

      var blast, last;
      var asc, chr;
      var col, cols;

      var data = getSelectionStatus(el);

      blast = el.value.substr(-2,1);
      last  = el.value.substr(-1,1);
      asc   = $('keypress-asc').value;
      chr   = $('keypress-char').value;
      col   = data['col'];
      cols  = data['cols'];
      pos   = data['start'];

      if (last=='\n') last = '<cr>';

      data['blast'] = blast;
      data['last' ] =  last;

      if (asc==13     && last=='<cr>') el.value = el.value.substr(0,el.value.length-1)+eol+'\n'+prompt+' ';
      if (col==40     && chr!='<cursor left>' && chr!='<cursor right>' && chr!='<back space>') el.value += cont+'\n';

      if (col==2      && chr=='<cursor left>') el.value += eol+'\n';
      if (col==2      && chr=='<back space>' ) el.value += eol+'\n';

      if (col==cols   && chr!='<cursor left>' && chr!='<cursor right>') el.value += eol+'\n';
      if (col==cols+1 && chr=='<cursor left>' ) set_cursor_pos($('console'), pos-2);
      if (col==cols   && chr=='<cursor right>') set_cursor_pos($('console'), pos+2);
      $('console').scrollTop = $('console').scrollHeight - $('console').clientHeight;

      show(data);

      if ( $('keydown-char').value==='<cursor down>' ) el.scrollTop -= 24;
      if ( $('keydown-char').value==='<insert>' )
      {
        $('insert').value = $('insert').value==='false';
        insert();
      }

*/

    }



    function updateParameters(ev)
    {

// alert(ev.type);

      ev = ev==null ? window.event : ev;

      var mouse = ev.clientX!==undefined;

      $('screen-width'       ).value = screen.width;
      $('screen-height'      ).value = screen.height;
      $('screen-availWidth'  ).value = screen.availWidth;
      $('screen-availHeight' ).value = screen.availHeight;

      $('window-outerWidth'  ).value = window.outerWidth;
      $('window-outerHeight' ).value = window.outerHeight;
      $('window-innerWidth'  ).value = window.innerWidth;
      $('window-innerHeight' ).value = window.innerHeight;
      $('window-pageXOffset' ).value = window.pageXOffset;
      $('window-pageYOffset' ).value = window.pageYOffset;

      $('body-scrollLeft'    ).value = document.body.scrollLeft;
      $('body-scrollTop'     ).value = document.body.scrollTop;
      $('body-clientWidth'   ).value = document.body.clientWidth;
      $('body-clientHeight'  ).value = document.body.clientHeight;
      $('body-scrollWidth'   ).value = document.body.scrollWidth;
      $('body-scrollHeight'  ).value = document.body.scrollHeight;
      $('body-offsetLeft'    ).value = document.body.offsetLeft;
      $('body-offsetTop'     ).value = document.body.offsetTop;

      $('docEl-scrollLeft'   ).value = document.documentElement.scrollLeft;
      $('docEl-scrollTop'    ).value = document.documentElement.scrollTop;
      $('docEl-clientWidth'  ).value = document.documentElement.clientWidth;
      $('docEl-clientHeight' ).value = document.documentElement.clientHeight;
      $('docEl-scrollWidth'  ).value = document.documentElement.scrollWidth;
      $('docEl-scrollHeight' ).value = document.documentElement.scrollHeight;
      $('docEl-offsetLeft'   ).value = document.documentElement.offsetLeft;
      $('docEl-offsetTop'    ).value = document.documentElement.offsetTop;

      if (mouse) $('body-clientX').value = ev.clientX;
      if (mouse) $('body-clientY').value = ev.clientY;

      //                                   ev.layerX;
      //                                   ev.layerY;
      //                                   ev.offsetX;
      //                                   ev.offsetY;
      //                                   ev.pageX;
      //                                   ev.pageY;
      //                                   ev.screenX;
      //                                   ev.screenY;
      //                                   ev.x;
      //                                   ev.y;

      var els = ['container', 'console'];
      for (i in els)
      {
        var el = $(els[i]);
        var id = el.id;
        $(id+'-scrollLeft'  ).value = el.scrollLeft;
        $(id+'-scrollTop'   ).value = el.scrollTop;
        $(id+'-clientWidth' ).value = el.clientWidth;
        $(id+'-clientHeight').value = el.clientHeight;
        $(id+'-scrollWidth' ).value = el.scrollWidth;
        $(id+'-scrollHeight').value = el.scrollHeight;
        $(id+'-offsetLeft'  ).value = el.offsetLeft;
        $(id+'-offsetTop'   ).value = el.offsetTop;
        if (mouse) $(id+'-clientX').value = ev.clientX;
        if (mouse) $(id+'-clientY').value = ev.clientY;
        if (mouse) $(id+'-insideX').value = ev.clientX - $(id+'-offsetLeft').value;
        if (mouse) $(id+'-insideY').value = ev.clientY - $(id+'-offsetTop' ).value;
      }

      $('cursor-offsetTop'  ).value = $('cursor').offsetTop;
      $('cursor-offsetLeft' ).value = $('cursor').offsetLeft;
      $('cursor-insideTop'  ).value = $('cursor-offsetTop' ).value - $('console-offsetTop' ).value;
      $('cursor-insideLeft' ).value = $('cursor-offsetLeft').value - $('console-offsetLeft').value;
      $('cursor-line'       ).value = $('cursor-insideTop' ).value / charHeight;
      $('cursor-column'     ).value = $('cursor-insideLeft').value / charWidth;
      $('cursor-lines'      ).value = $('console-clientHeight').value / charHeight;
      $('cursor-columns'    ).value = $('console-clientWidth' ).value / charWidth;

    }



   function capLock(e)
   {
     var kc = e.keyCode  ? e.keyCode  : e.which;
     var sk = e.shiftKey ? e.shiftKey : ( (kc==16) ? true : false );
     $('divMayus').style.visibility = ( (kc>=65 && kc<=90) && !sk) || ( (kc>=97 && kc<=122) && sk) ? 'visible' : 'hidden';
   }



   function blinkCursor(i)
   {
     $('cursor').style.backgroundColor = cursorColors[cursorState][i];
     setTimeout( function() { blinkCursor((i+1)%2) } , cursorBlinkTime );
   }



   function setCursor(state)
   {
     if (state!=null) cursorState = state;
     $('cursorState').value = cursorState;
     $('cursor').style.height    = ( insert ? charHeight :              overwHeight );
     $('cursor').style.marginTop = ( insert ?          0 : charHeight - overwHeight );
   }



    function keyPress(el,ev)
    {
    }



    function keyDown(el,ev)
    {
      if ( ev.which== 9 ) setTimeout( function(){ el.focus() } , 0 );
      if ( ev.which==35 ) $('cursor').style.left = parseInt($('console-offsetLeft').value) + parseInt($('console-clientWidth').value-charWidth);
      if ( ev.which==36 ) $('cursor').style.left = $('console-offsetLeft').value;
      if ( ev.which==37 ) if ( parseInt($('cursor-column').value) > 0                                    ) $('cursor').style.left = $('cursor').offsetLeft - charWidth;
      if ( ev.which==39 ) if ( parseInt($('cursor-column').value) < parseInt($('cursor-columns').value)-1) $('cursor').style.left = $('cursor').offsetLeft + charWidth;
      if ( ev.which==45 )
      {
        insert = ! insert;
        $('insert').value = insert;
        setCursor();
      }
    }



    function keyUp(el,ev)
    {
   // if ( ev.which==40 ) el.scrollTop -= 24;
      if ( ev.which==40 ) el.scrollTop -= 40;
    }



    function textInput(el,ev)
    {
    }



    function init(ev)
    {

      $(         'cursor').style.width  = charWidth;
      $(         'cursor').style.height = charHeight;
      $(        'console').style.width  = 1 + charWidth  * (1+consWidth);
      $(        'console').style.height =     charHeight * (0+consHeight);
      $(        'console').focus();

      $(         'insert').value = 'true';
      $(        'numlock').value = 'false';
      $(       'capslock').value = 'false';
      $(     'scrolllock').value = 'false';

      $('charWidth'      ).value = charWidth;
      $('charHeight'     ).value = charHeight;
      $('overwHeight'    ).value = overwHeight;
      $('consWidth'      ).value = consWidth;
      $('consHeight'     ).value = consHeight;
      $('cursorBlinkTime').value = cursorBlinkTime;

      updateParameters(ev);

      setCursor('focus');
      blinkCursor(0);

    }



/* EOF */
