}  P-IMPRES                                                                                                                 } .pl 68
.sr 2
.pc 70
.rm 70
.tb 11
.pm 11
.op










 	�
�
�
 COL�GIO EVANG�LICO ALBERTO TORRES - CEAT


 	���
 �REA DE INFORM�TICA

















 	�
  E� Ĵ � e  MANIPULA��O DE SPRITES EM LOGO � � e E� � 

















 	���
 PAULO ROBERTO BAGATINI


 	�
 Segundo Grau - Terceiro Ano


 	���
 Inform�tica







 	,,�
 Lajeado, outubro de 1991  � 9   �3                     �.mt 8.5"
                         "Se voc� j� realizou tudo o que planejou,

                                   voc� n�o planejou o suficiente."



                                                 Martial
.mt .5"
.pa  �.   o5                     �.pg








 	&&�
 SUM�RIO







   INTRODU��O  . . . . . . . . . . . . . . . . . . . . . . . .  4

   1 - DEFINI��O DO PROBLEMA . . . . . . . . . . . . . . . . .  5
   2 - JUSTIFICATIVA . . . . . . . . . . . . . . . . . . . . .  6
   3 - OBJETIVOS . . . . . . . . . . . . . . . . . . . . . . .  7
   4 - HIP�TESES . . . . . . . . . . . . . . . . . . . . . . .  8
   5 - MATERIAL USADO  . . . . . . . . . . . . . . . . . . . .  9
   6 - DEFINI��ES  . . . . . . . . . . . . . . . . . . . . . . 10
   7 - NO��ES DE NUMERA��O BIN�RIA . . . . . . . . . . . . . . 13
   7.1 - Passagem da base bin�ria para a decimal . . . . . . . 15
   7.2 - Passagem da base decimal para a bin�ria . . . . . . . 17
   8 - USO DO SISTEMA BIN�RIO EM SPRITES . . . . . . . . . . . 19
   9 - INTERPRETA��O DE PRIMITIVAS . . . . . . . . . . . . . . 21
   10 - ESTRUTURAS L�GICAS UTILIZADAS  . . . . . . . . . . . . 24
   11 - FIGSTAR: MANIPULADOR DE SPRITES  . . . . . . . . . . . 26
   11.1 - Organograma  . . . . . . . . . . . . . . . . . . . . 26
   11.2 - Controle de edi��o . . . . . . . . . . . . . . . . . 27
   11.3 - Controle de figura . . . . . . . . . . . . . . . . . 28
   11.4 - Controle de vari�vel . . . . . . . . . . . . . . . . 29
   11.5 - Controle de arquivos . . . . . . . . . . . . . . . . 30
   12 - FILOSOFIA DE TRABALHO: O SISTEMA LOGO  . . . . . . . . 31
   13 - O PROCESSO: DIFICULDADES, APERFEI�OAMENTO, SOLU��ES  . 33

   CONCLUS�O . . . . . . . . . . . . . . . . . . . . . . . . . 34

   ANEXOS

   1 - Imagem de algumas telas do programa . . . . . . . . . . 36
   2 - Fluxogramas . . . . . . . . . . . . . . . . . . . . . . 49
   2.1 - Transforma��es bin-dec  . . . . . . . . . . . . . . . 50
   2.2 - Transforma��es dec-bin  . . . . . . . . . . . . . . . 50
   2.3 - Programa��o de sprites via "listafig" . . . . . . . . 51
   3 - Listagem do programa  . . . . . . . . . . . . . . . . . 56
   3.1 - Primeira vers�o do projeto  . . . . . . . . . . . . . 57
   3.2 - �ltima vers�o do projeto  . . . . . . . . . . . . . . 61

   REFER�NCIAS BIBLIOGR�FICAS  . . . . . . . . . . . . . . . . 71
.pa  �5   o5                     �







 	�
 INTRODU��O








 	�

 Este trabalho � a s�ntese de oito meses de pesquisa, �expe�
ri�ncias e muita programa��o em torno de um tema ordinariamente desco�
nhecido �pela �maioria dos que trabalham com LOGO: �a �manipula��o �de �
sprites.

 	�

 Em poucas palavras, pode-se dizer que, para o LOGO, �sprites �
s�o pequenas telas de 16x16 pontos nas quais o programador pode �criar �
uma figura. Entre os comandos primitivos integrados na linguagem est�o �
os �que permitem utilizar o recurso riqu�ssimo que os sprites �repre�
sentam. �Contudo, �se �analizarmos com cuidado, �notaremos �que �essas �
primitivas deixam a desejar no momento em que nos dispomos a �explorar �
com vontade as figuras dos sprites. A programa��o � lenta e cansativa. �
� �freq�ente �perdemos o controle sobre o conte�do de cada �sprite. �A �
id�ia �ent�o �foi produzir um programa que nos �desse �total �controle �
sobre �os sprites. O procedimento adotado foi o de criar e �juntar �em �
menus as primitivas que faltassem para a programa��o.

 	�

 Foi �um �trabalho �duro no qual teve imenso �valor �a �ajuda �
prestada �pelo �colega �Marcelo Bihre, a quem sou �muito �grato. �Meus �
agradecimentos tamb�m ao professor Hugo Landmeier, pelas manifesta��es �
positivas �em rela��o ao trabalho, e ao CEAT, como entidade, �por �ter �
permitido desenvolver em seu Laborat�rio de Inform�tica o projeto e �o �
presente relat�rio, alvos desta Mostra de Ci�ncias.
.pa  �%   o5                     �







 	���
 1 - DEFINI��O DO PROBLEMA








 	�

 � �poss�vel �ampliar e desenvolver os m�todos �de �edi��o �e �
programa��o de sprites em LOGO? Se for, o qu� deve ser desenvolvido �e �
como faz�-lo?
.pa  �$   o5                     �







 	���
 2 - JUSTIFICATIVA








 	�

 Considerando:


 	�

 - �que �o LOGO � relativamente pobre no que diz �respeito �� �
programa��o de sprites;


 	�

 - que o assunto "sprite" � mais abrangente do que �simples�
mente "desenho";


 	�

 - que at� agora esse campo foi pouco explorado em LOGO;


 	�

 - �que � interessante abordar em LOGO um assunto que �outras �
Linguagens j� abordaram;


 	�

 - �que �desenvolvendo programas �nessa ��rea, �indiretamente �
desenvolve-se programas �teis em outras �reas;


 	�

 - �que nessa �rea pode-se aplicar, com vantagens, �programas �
desenvolvidos em outras �reas;


 	�

 - �que o processo de elabora��o de algo completamente �novo, �
beneficia �visivelmente nossa capacidade de racioc�nio l�gico e �abs�
trato;


 	�

 - �que as id�ias desenvolvidas com o projeto ser�o �bastante �
�teis aos que realmente se interessarem pelo assunto;


 	�

 justifica-se a realiza��o do projeto.
.pa  +   o5                     �







 	

�
 3 - OBJETIVOS








 	VV�
 3.1 - PRIM�RIOS




 	�

 - �desenvolver �rotinas que possam �rolar, �rotar, �deletar, �
inverter, editar, ampliar e reduzir sprites;


 	�

 - �desenvolver �um �procedimento pelo qual �se �programe �um �
sprite �atrav�s do comando primitivo "listafig", com entrada de �dados �
em bin�rio ou decimal;


 	�

 - �entender o processo pelo qual passa o sprite durante �sua �
programa��o no editor ou no comando "listafig".




 	���
 3.2 - SECUND�RIOS




 	�

 - entender o que significam e para que servem as primitivas: �
"edfig", "criafigl", "listafig", e "copiafig";


 	�

 - aprender a utilizar o sistema de nota��o bin�ria e a fazer �
transforma��es do tipo bin-dec e dec-bin;


 	�

 - �desenvolver �fun��es recursivas �e �subprocedimentos �que �
sejam �teis em outras �reas do LOGO;


 	�

 - utilizar procedimentos desenvolvidos noutros projetos;


 	�

 - �produzir um programa bem apresent�vel esteticamente, �que �
re�na o produto dos objetivos prim�rios e que permita manter um r�gido �
controle sobre as figuras dos sprites;


 	�

 - generalizar ao m�ximo o programa final.
.pa  �4   o5                     �







 	

�
 4 - HIP�TESES








 	�

 - �a �m�quina com a qual o projeto est� �sendo �desenvolvido �
talvez n�o tenha mem�ria e/ou processamento r�pido o suficiente para a �
execu��o de certos procedimentos, como por exemplo "rolar", devido ��s �
transforma��es matem�ticas necess�rias, que consomem demasiado tempo e �
mem�ria;


 	�

 - conforme o n�mero total final de procedimentos do projeto, �
o �programa �pode �se tornar invi�vel, uma vez que a �maior �parte �da �
mem�ria ser� ocupada pelos procedimentos. Caso isso aconte�a, � �pro�
v�vel ter que separar os programas em dois ou mais arquivos de disco;


 	�

 - falta de l�gica em procedimentos poder� impedir o bom an�
damento do trabalho, fazendo o programa "empacar" no meio do caminho;


 	�

 - �� poss�vel que o tempo dispon�vel para a programa��o �n�o �
seja �suficiente para que se possa concluir de modo �satisfat�rio �uma �
parte �significativa do projeto at� o momento de edi��o da VII �MOSTRA �
DE TRABALHOS PR�TICOS DO CEAT.
.pa  "   o5                     �







 	HH�
 5 - MATERIAL USADO








 	���
 5.1 - MATERIAL PARA O DESENVOLVIMENTO DO PROJETO




 	�

 - computador HB8000;

 	�

 - aparelho televisor colorido PANASONIC;

 	�

 - drive 5�;

 	�

 - disco 5� face dupla;

 	�

 - fonte transformadora para o drive;

 	�

 - cartucho HOT-LOGO vers�o 1.1;

 	�

 - editor de textos MSXWORD vers�o 3.0;

 	�

 - impressora EPSON FX 100+;

 	�

 - folhas tamanho of�cio;

 	�

 - papel pr�prio para impressora.




 	���
 5.2 - MATERIAL PARA A MOSTRA




 	�

 - computador HB8000;

 	�

 - aparelho televisor colorido PANASONIC;

 	�

 - drive 5�;

 	�

 - disco 5� face dupla com o projeto desenvolvido;

 	�

 - fonte transformadora para o drive;

 	�

 - cartucho HOT-LOGO vers�o 1.1;

 	�

 - relat�rio do projeto.
.pa  ,   o5                     �







 	���
 6 - DEFINI��ES








 	�

 Devido �� �proposta do projeto, este �relat�rio �apresentar� �
express�es �pr�prias da linguagem LOGO. Com o intuito de �facilitar �a �
compreens�o da leitura e evitar futuras confus�es, ser�o �apresentadas �
abaixo tr�s defini��es b�sicas. Foram formuladas por mim e por isso �� �
poss�vel que nem todos concordem com elas. N�o devemos nos esquecer no �
entanto, que o certo ou errado em trabalhos inovadores � muitas �vezes �
relativo j� que neles defini��es pr�-estabelecidas freq�entemente �n�o �
s�o capazes de abranger de modo satisfat�rio o novo contexto em que se �
inserem. �No LOGO isso acontece principalmente porque � uma �linguagem �
com �fins �imediatos educacionais. Por tratar com �crian�as, �tomou-se �
cuidado �ao �escolher �as express�es que �a �linguagem �usaria. �Elas �
deveriam �dar essencialmente uma id�ia de concretude, uma vez �que �a �
capacidade de abstra��o das crian�as � relativamente reduzida.

 	�

 Para �dar essa apar�ncia palp�vel aos programas gr�ficos, �o �
LOGO reconhece tr�s entidades b�sicas: a TARTARUGA, o SPRITE (pronun�
cia-se �"spraite") �e a FIGURA. Apesar de para muitos �serem �a �mesma �
coisa, s�o distintas. Distintas, mas de tal modo entrela�adas que �uma �
depende da outra para existir. Pode-se comparar a rela��o TARTARUGA �- �
SPRITE �- �FIGURA � rela��o PESSOA (invis�vel) - ROUPA - �ESTAMPA �(da �
roupa).  Ambas as rela��es podem ser esquematizadas segundo a fig 6.1, �
onde �a entidade mais externa abrange a mais interna e onde �as �setas �
indicam a �rea de a��o de cada primitiva.


 	88�
 ����������������������������������������������������������

 	88�
 � atat      mudepos     pass�vel de dele��o,invers�o,etc �

 	88�
 �                                                        �

 	88�
 �                                                        �

 	88�
 �                                                        �

 	88�
 �    TART  SPR  FIGURA  ITE  ARUGA      giga             �

 	88�
 �                                                        �

 	88�
 �                                                        �

 	88�
 �                                                        �

 	88�
 � mudect                                                 �

 	88�
 �                                       kilo             �

 	88�
 �          edfig         at/dt                           � 

 	88�
 �                                                        � 

 	88�
 �               mudefig                                  �

 	88�
 ����������������������������������������������������������
                                                           fig 6.1


  � 9   o5                     �
 	�

 TARTARUGA


 	�

 Se o LOGO � t�o conhecido, devemos isso principalmente � sua �
famosa tartaruguinha. Apesar de para n�s essa tartaruga ser o �desenho �
que aparece na tela sempre que ligamos o computador, para o LOGO ela � �
uma �entidade, �quase um ser vivo, que dirige os desenhos na �tela. �� �
chamada pelo interpretador LOGO de TARTARUGA, n�o porque o desenho que �
aparece � o de uma tartaruga, mas porque a entidade que recebe �nossas �
ordens �para fazer os desenhos tem por nome TARTARUGA. Essa �diferen�a �
sutil justifica-se pelo fato da tartaruga (letra min�scula) poder �ser �
trocada �por um cachorro ou helic�ptero, continuando no entanto a �ser �
tratada pelo LOGO como TARTARUGA (letra mai�scula).

 	�

 Podemos �acessar em LOGO, pelo comando primitivo "atat" �at� �
30 �TARTARUGAs, 30 entidades independentes entre si, numeradas de 0 �a �
29, �capazes �de �realizar suas pr�prias fun��es. �A �que �normalmente �
usamos � a de n�mero 0.




 	�

 SPRITE


 	�

 Al�m disso, se usarmos a compara��o, a TARTARUGA pode trocar �
de roupa. Pode tirar sua roupa de tartaruga e colocar uma de cachorro, �
por exemplo. � poss�vel escolher entre 60 roupas, rotuladas de 0 a 59. �
Essas �roupas �s�o �chamadas �em �LOGO �de �SPRITEs. �Com ��dimens�es �
quadrangulares �de �16x16 pixels (pontos na tela), �quando �deletados �
(apagados), �t�m �a forma de um quadrado cheio. �O �comando �"mudefig" �
permite trocarmos de SPRITE, ou, de acordo com a compara��o, mudarmos �
a roupa da TARTARUGA. O par�metro do comando � um n�mero inteiro de �0 �
a 59, correspondente ao SPRITE desejado.




 	�

 FIGURA


 	�

 Sabemos �que �cada SPRITE tem uma FIGURA. O SPRITE �36, �por �
exemplo, �cont�m a FIGURA de uma tartaruga voltada para cima na �tela. �
Acontece que podemos modificar a FIGURA desse SPRITE. Podemos edit�-lo �
atrav�s �da �primitiva "edfig" e modificar seus 256 pontos �(bits). �O �
fato �do desenho do SPRITE poder ser mudado refor�a a id�ia �da �nossa �
compara��o: podemos dizer que a FIGURA do SPRITE � a estampa da �roupa �
da TARTARUGA, pois pode ser mudada segundo a nossa vontade.


 	�

 Agora �que foram definidas as 3 entidades do �LOGO, �podemos �
fazer uma s�ntese da rela��o entre elas.




 	�

 TARTARUGA - SPRITE - FIGURA


 	�

 A TARTARUGA � invis�vel, n�o tem forma. Ela � o esp�rito �do �
SPRITE �assim �como o SPRITE � o esp�rito da FIGURA. Esta �ltima �� �a �
�nica �das tr�s entidades que tem forma. Ainda assim, s� �porque �est� �
contida �no SPRITE. Por causa disso, ela d� forma ao SPRITE, �que �por �
sua �vez �d� �forma � TARTARUGA. � por isso que �comandando �"dt" �n�o �
provocamos �o �desaparecimento da TARTARUGA (embora nos d� �essa �im�
press�o), �mas �sim �o desligamento do SPRITE. Acontece �o �mesmo �que   � 9   o5                     ��acontece �� �uma TV ligada, quando subitamente falta �luz. �Segundo �a �
compara��o, seria como se tir�ssemos a roupa da TARTARUGA.

 	�

 Da �mesma forma, n�o dev�amos dizer que editar um �SPRITE �� �
editar �uma �figura (apesar do formato do comando "edfig"), j� �que �a �
FIGURA � resultado da edi��o e modifica��o do conte�do do �SPRITE.Veja �
bem, 1 SPRITE (existem 60) � uma seq��ncia de 32 bytes, onde cada byte �
� uma seq��ncia de 8 bits, ou seja, num SPRITE existem 32x8=256 �bits. �
Cada bit pode estar aceso ou apagado. Essa combina��o aceso/apagado, � �
que �forma �a FIGURA. Com um pouco de �an�lise �combinat�ria, �podemos �
deduzir que em 1 SPRITE de 256 bits podem ser formadas at� 2256 FIGU�
RAs diferentes. Ou, se tomarmos os bytes teremos 25632 FIGURAs distin�
tas,uma vez que 1 byte apresenta 256 combina��es diferentes. Note �que �
2256 = 25632 = 1157920892373161954235709850086879078532699846656405640 �
39457584007913129639936.

 	�

 Sendo �assim, �se FIGURA fosse o mesmo �que �SPRITE, �SPRITE �
seria o mesmo que FIGURA e existiriam em LOGO n�o 60, mas 2256 SPRITEs �
independentes, �o que � um absurdo. Logo, est� mais do que �claro �que �
FIGURA n�o � o mesmo que SPRITE.
.pa  �$   o5                     �







 	���
 7 - NO��ES DE NUMERA��O BIN�RIA








 	�

 O trabalho com sprites exige, al�m de certo ponto, �conhecer �
e saber operar com n�mero bin�rios. Normalmente, para executar �nossos �
c�lculos, �utilizamos �o sistema de numera��o decimal, que �� �formado �
atrav�s da combina��o de dez s�mbolos diferentes (0 a 9). Por que �dez �
e �n�o �oito �ou vinte s�mbolos diferentes? �Provavelmente �devido �ao �
n�mero �de �dedos �de nossas m�os, pois nada impede que �se �adote �um �
n�mero �maior �ou �menor de s�mbolos para um �determinado �sistema �de �
numera��o.

 	�

 O sistema utilizado em computadores � o sistema de numera��o �
bin�ria, �j� conhecido na �poca de Newton. Os n�meros �bin�rios �foram �
desenvolvidos por Boole, e podem ser encontrados em "�lgebra �Boolea�
na". Utilizam apenas dois s�mbolos, ou seja, trabalham na base �bin��
ria �ao �inv�s �da decimal. Os dois s�mbolos s�o 1 e �0, �podendo �ser �
representados �respectivamente por SIM e N�O, uma l�mpada ACESA e �uma �
APAGADA, uma chave LIGADA e uma DESLIGADA, tens�o de 5 e de 0 volts.

 	�

 O motivo da utiliza��o de n�meros bin�rios em computadores � �
que, �para �eles, � muito mais f�cil trabalhar com chaves �abertas �ou �
fechadas, �presen�a ou n�o de tens�o, l�mpadas acesas ou apagadas, �do �
que com dez diferentes s�mbolos. Abaixo, a tabela da fig 7.1 compara a �
numera��o bin�ria com a decimal:

.lh 6

 	���	
 ���������������������������������������������������

 	���	
 ������������������������   ������������������������

 	���	
 �� base bin � base dec �   � base bin � base dec ��

 	���	
 ������������������������   ������������������������

 	���	
 ��   0000   �    00    �   �   0110   �    06    ��

 	���	
 ��   0001   �    01    �   �   0111   �    07    ��

 	���	
 ��   0010   �    02    �   �   1000   �    08    ��

 	���	
 ��   0011   �    03    �   �   1001   �    09    ��

 	���	
 ��   0100   �    04    �   �   1010   �    10    ��

 	���	
 ��   0101   �    05    �   �   1011   �    11    ��

 	���	
 ������������������������   ������������������������

 	���	
 ���������������������������������������������������
.lh 8
                                                       fig 7.1


 	�

 Note �que a tabela pode continuar indefinidamente, tanto �na �
numera��o �decimal, quanto na bin�ria. � importante salientar �tamb�m, �
que �os 00 � esquerda do n�mero, tanto decimal quanto bin�rio, �servem �
apenas para caracterizar o n�mero em termos de quantidade de �d�gitos, �
podendo �perfeitamente �serem ignorados. Se �us�ssemos �l�mpadas �para �
representar �n�meros bin�rios, as apagadas seriam os 00, mas �nem �por �
isso as que estivessem � esquerda dos n�meros seriam retiradas, j� que �
poderiam ser reutilizadas em outras situa��es, exemplo:

  � <   o5                     �
 	���
 ��������������

 	���
 �O O O O => 1�

 	���
 �O O O O => 9�

 	���
 �O O O O => 9�

 	���
 �O O O O => 1�

 	���
 ��������������
                                     fig 7.2


 	�

 A �numera��o bin�ria n�o se restringe aos n�meros �inteiros, �
mas se estende at� os reais. Foge, por�m, ao objetivo desse �relat�rio �
a explora��o de n�meros irracionais ou fracion�rios bin�rios, uma �vez �
que nos sprites utiliza-se apenas n�meros inteiros. N�o deixa contudo, �
de �ser uma sugest�o, que um trabalho futuro explore tamb�m este �lado �
dos n�meros bin�rios. Veja ainda, que a base bin�ria permite os mesmos �
operadores que a base decimal, por exemplo: 010+100=110 <=> 2+4=6.

 	�

 Existem dois termos muito usados em computa��o: bit e �byte. �
O �bit � definido como um elemento da mem�ria que pode estar �em �duas �
situa��es: 1 ou 0, sim ou n�o, etc. Na tela, um bit em 1 � chamado �de �
bit �aceso, �e �� um ponto ligado (em qualquer cor) �cuja �unidade �de �
medida �� o pixel. Por defini��o, um ponto (bit aceso) mede um �pixel. �
J� o bit em zero (0) � um ponto apagado, que n�o aparece na tela. �Nos �
n�meros bin�rios, os algarismos s�o denominados bits, sendo represen�
tados tamb�m por 1 ou 0. Byte � um n�mero bin�rio de 8 bits.

 	�

 Do mesmo modo que bit � elemento de byte, byte � elemento de �
muitos �sistemas em computa��o: a matriz das letras de um �computador, �
por exemplo, � um quadrado de 8x8 bits, logo, uma pilha de 8 bytes. Um �
n�mero decimal � considerado 1 byte, quando seu correspondente bin�rio �
for �um n�mero de 8 bits. A fig 7.3 mostra a estrutura��o de 1 byte �e �
seus 8 bits:

.lh 6

 	���
 �����������������������������������������������������������������

 	���
 �                            B Y T E                            �

 	���
 �����������������������������������������������������������������

 	���
 � bit 7 � bit 6 � bit 5 � bit 4 � bit 3 � bit 2 � bit 1 � bit 0 �

 	���
 �����������������������������������������������������������������

 	���
 �  0/1  �  0/1  �  0/1  �  0/1  �  0/1  �  0/1  �  0/1  �  0/1  �

 	���
 �����������������������������������������������������������������
.lh 8
  Obs: 0/1 => 0 ou 1                                          fig 7.3



 	�

 S�o �comuns �tamb�m, e cada vez mais, devido ao �aumento �da �
mem�ria �RAM dos computadores, o uso dos m�ltiplos de �byte: �kilo(K), �
mega(M), giga(G) e tera(T). Contudo, nos bin�rios n�o se usa, como �na �
base decimal,


 	�
 1x103  para Kb, mas sim 1x210;

 	�
 1x106  para Mb, mas sim 1x220;

 	�
 1x109  para Gb, mas sim 1x230;

 	�
 1x1012 para Tb, mas sim 1x240.

                  Obs: Kb => kilo byte, ou simplesmente K.


 	�

 Um �computador de 64 Kb (ou simplesmente 64 K) de �RAM, �por �
exemplo, �permite armazenar 64x210 = 64x1024 = 65536 bytes �(caracte�
res), da seguinte forma.

 	�

 Cada �caracter da mem�ria � armazenado no padr�o �ASCII, �em �
base bin�ria. O c�digo ASCII � a sigla de "American Standard Code �for �
Information �Interchange", �ou seja, C�digo �Standard �Americano �para �
Interc�mbio de Informa��es. Sabemos que o computador opera somente �em   � ;   o5                     ��base �bin�ria, apesar da sa�da ser sempre em decimal. Podemos �repre�
sentar �caracteres alfab�ticos (a,b,c,...), num�ricos �(0,1,2,...) �ou �
simb�licos (+,-,*,...) usados no computador por meio de c�digos bin��
rios, numerando cada um com um valor diferente. Poder�amos associar de �
v�rias ��formas ��estes �caracteres �a �n�meros �bin�rios, ��por�m ��a �
padroniza��o �dessa �associa��o � muito conveniente, �pois �permite �a �
compatibilidade entre v�rios produtos, tanto em termos de �"software", �
quanto �de "hardware". Por esse motivo, o c�digo ASCII � um �dos �mais �
populares. �A �letra "A" em ASCII � representada pelo �n�mero �bin�rio �
1000001, que � 65 em decimal. Assim subseq�entemente, "B" =66, etc.

 	�

 Dizer �que a mem�ria de um computador pode �armazenar �esses �
65536 caracteres, significa dizer que estes s�o guardados na forma �de �
bytes �bin�rios, �atrav�s �do c�digo ASCII. O caracter �da �letra �"A" �
(mai�scula), �por �exemplo, �� guardado sob a forma �65, �01000001 �em �
bin�rio (note que � um byte: 8 bits).






 	���
 7.1 - PASSAGEM DA BASE BIN�RIA PARA A DECIMAL




 	�

 Um �byte � uma combina��o de 8 bits, que podem estar �acesos �
ou �apagados. Em An�lise Combinat�ria, isso � um caso de �Arranjo �com �
repeti��o �de �elementos �(A'), cuja fun��o � �calcular �o �n�mero �de �
combina��es �que �se �pode formar com n elementos, cada �um �tendo �p �
possibilidades de estar representado. Nos n�meros bin�rios, a �l�gica �
� a seguinte (fig 7.4):
.lh 6

 	::�
 ���������������������

 	::�
 � 1� � 2� � 3� � 4� �

 	::�
 ���������������������

 	::�
 �                �0 �

 	::�
 �           �0�     �

 	::�
 �                �1 �

 	::�
 �      �0�          �

 	::�
 �                �0 �

 	::�
 �           �1�     �

 	::�
 �                �1 �

 	::�
 �  0�               �

 	::�
 �                �0 �

 	::�
 �           �0�     �

 	::�
 �                �1 �

 	::�
 �      �1�          �

 	::�
 �                �0 �

 	::�
 �           �1�     �

 	::�
 �                �1 �

 	::�
 �                   �

 	::�
 �                �0 �

 	::�
 �           �0�     �

 	::�
 �                �1 �

 	::�
 �      �0�          �

 	::�
 �                �0 �

 	::�
 �           �1�     �

 	::�
 �                �1 �

 	::�
 �  1�               �

 	::�
 �                �0 �

 	::�
 �           �0�     �

 	::�
 �                �1 �

 	::�
 �      �1�          �

 	::�
 �                �0 �

 	::�
 �           �1�     �

 	::�
 �                �1 �

 	::�
 ���������������������
.lh 8
                                        fig 7.4  � B   o5                     �

 	�

 O primeiro d�gito tem duas possibilidades de acontecer: 0 ou �
1. O segundo, tem quatro: 0 ou 1 caso o primeiro tenha sido 0, e 0 �ou �
1 �caso �o primeiro tenha sido 1. A mesma id�ia � usada para �o �ter�
ceiro, quarto, etc.

 	�

 Pela ��rvore de possibilidades nota-se que para �o �primeiro �
bit, com duas possibilidades de combina��o, pode-se formar dois n�me�
ros bin�rios diferentes: 0 ou 1. At� o segundo bit, pode-se formar 00, �
01, 10 e 11, quatro n�meros diferentes. At� o terceiro bit, forma-se 8 �
n�meros distintos.

 	�

 O algoritmo, ent�o, para se calcular o n�mero de combina��es �
poss�veis que pode se fazer com n elementos, cada um com p possibili�
dades �de ocorr�ncia, � dada pela lei: A' = pn. Restringindo, �para �o �
caso �dos n�meros bin�rios tem-se p=2 (0 ou 1) e n igual � �quantidade �
de �bits �do n�mero. Exemplificando, com 10 bits existem �210 �n�meros �
bin�rios diferentes.

 	�

 No �caso espec�fico do byte, conclu�mos que com seus 8 �bits �
podem �se formar 28 = 256 n�meros bin�rios distintos. � esse o �motivo �
do c�digo ASCII estar restringido a 256 caracteres (0 a 255). Com os 8 �
bits �do �byte �tem-se 256 combina��es, que �permitem �que �a �rela��o �
homem-m�quina seja no m�nimo intelig�vel.

 	�

 Ampliando �a �rvore de posibilidades, notaremos que um �byte �
localiza-se �entre 00000000 (=0) e 11111111 (=255). Mas �como �podemos �
calcular �o �valor �decimal de um n�mero bin�rio sem ter �que �usar �a �
�rvore?

 	�

 Nos n�meros bin�rios cada bit tem um n�mero que o caracteri�
za segundo sua posi��o. A numera��o cardinal dos bits varia de 0 a �n, �
a �contar da direita, como j� foi visto na fig 7.3. Al�m �disso, �cada �
bit tem um valor, que � igual a 2 elevado ao n�mero do bit em quest�o. �
Veja: o s�timo bit � o de n�mero 6, logo seu valor � 26=64. Note que o �
valor �do bit � quantidade de n�meros bin�rios distintos que se �pode �
formar at� o bit anterior. Deve-se ainda multiplicar cada valor �pelo �
seu �bit correspondente. A transforma��o do n�mero bin�rio em �decimal �
se �d� somando os resultados de todas as multiplica��es �feitas. �Note �
ent�o, �que na soma usa-se apenas os valores dos bits em 1, �uma �vez �
que o 0 dos bits apagados anula seu valor.

 	�

 Confira todas as informa��es do par�grafo acima na tabela da �
fig �7.5, �uma amplia��o da fig 7.3. Depois tente transformar �o �byte �
bin�rio 01001101 em decimal. O resultado deve ser 77.

.lh 6.5

 	**�
 �������������������������������������������������������������

 	**�
 �         ���������������������������������                 �

 	**�
 � ... 256 �128� 64� 32� 16�  8�  4�  2�  1� valor decimal   �

 	**�
 �         ���������������������������������                 �

 	**�
 �         ���������������������������������                 �

 	**�
 � ...  28 � 27� 26� 25� 24� 23� 22� 21� 20� valor na base 2 �

 	**�
 �         ���������������������������������                 �

 	**�
 �         ���������������������������������                 �

 	**�
 � ...  8  � 7 � 6 � 5 � 4 � 3 � 2 � 1 � 0 � numera��o       �

 	**�
 �         ���������������������������������                 �

 	**�
 �         ���������������������������������                 �

 	**�
 � ...  9� � 8�� 7�� 6�� 5�� 4�� 3�� 2�� 1�� posi��o do bit  �

 	**�
 �         ���������������������������������                 �

 	**�
 �         ���������������������������������                 �

 	**�
 � ... 0/1 �0/1�0/1�0/1�0/1�0/1�0/1�0/1�0/1� possibilidades  �

 	**�
 �         ���������������������������������                 �

 	**�
 �         ���������������������������������                 �

 	**�
 �         �            B Y T E            �                 �

 	**�
 �         ���������������������������������                 �

 	**�
 �������������������������������������������������������������
.lh 8
                                                            fig 7.5  � =   o5                     �





 	���
 7.2 - PASSAGEM DA BASE DECIMAL PARA A BIN�RIA




 	�

 Assim como � poss�vel transformar n�meros bin�rios em deci�
mais, � poss�vel fazer o caminho de volta ou seja, transformar n�meros �
decimais �em �bin�rios. Para entendermos esse caminho, �devemos �antes �
entender o processo bin-dec visto antes, uma vez que dec-bin � conse�
q��ncia desse processo.

 	�

 No ��ltimo �par�grafo do cap�tulo anterior, dissemos �que �a �
numera��o �cardinal dos bits varia de 0 a n. Vemos que cada bit �� �um �
d�gito �do �n�mero bin�rio (bit � uma contra��o �da �express�o �BInari �
digIT, �d�gito �bin�rio), como cada d�gito do �n�mero �decimal. �Vimos �
ainda que o valor do bit � dado por 0/1x2n. A base 2 indica que exis�
tem �dois s�mbolos a combinar para formar n�meros bin�rios. Se isso �� �
verdade, �ent�o o valor de um d�gito de um n�mero decimal � �dado �por �
0/1/2/3/4/5/6/7/8/9x10n, pois existem dez s�mbolos a combinar. �Exem�
plo:


 	�

    143 = 1.102 + 4.101 + 3.100 = 100 + 40 + 3.


 	�

 Generalizando, �D.bn �<=> valor do (n+1)�simo �d�gito �D �do �
n�mero na base b. Ainda, conforme a tabela da fig 7.5 temos:


 	�

 n� na base b:

.lh 7

 	�

   �������������

 	�

   �b3�b2�b1�b0�

 	�

   �������������

 	�

   �D4 D3 D2 D1�

 	�

   �������������
.lh 8


 	�

 o mesmo em dec:


 	�

    d = D4.b3+ D3.b2+ D2.b1 + D1.b0


 	�

 Expandimos d colocando b em evid�ncia. Temos:


 	�

    D  = Db0

 	�

    Db = Db1


 	�

     d = D1 + D2b + D3b2 + D4b3


 	�

     d = D1 + b[D2 + D3b + D4b2]


 	�

     d = D1 + b[D2 + b[D3 + D4b]]


 	�

 O �desenvolvimento �acima �nos mostra outra �maneira �de �se �
transformar �um n�mero numa base b em decimal: multiplica-se o ��ltimo �
d�gito �pela �base. Soma-se o resultado ao pen�ltimo �d�gito. �O �novo �
resultado �� multiplicado pela base. Assim sucessivamente at� �que �se �
some o primeiro d�gito.

 	�

 Note que os d�gitos do n�mero na base b est�o escondidos �em   � :   o5                     ��seu correspondente decimal. A transforma��o do decimal para o da �base �
consiste �em tirar os d�gitos do decimal, separando-os. A forma��o �do �
n�mero se d� ent�o, concatenando os d�gitos tirados.

 	�

 Analizemos a �ltima parte da demonstra��o:


 	�

    d = D1 + b[D2 + b[D3 + D4b]]


 	�

    d = D1 + c


 	�

 Note que c � divis�vel por b:


 	�

    d   D1   b[D2 + b[D3 + D4b]]

 	�

    � = �� + �������������������

 	�

    b    b            b


 	�

 Logo, D1 � o resto da raz�o d/b.

 	�

 Recursivamente, d passa a ser o inteiro de d/b, veja:  


 	�

    d/b = D1/b + [D2 + b[D3 + D4b]]


 	�

    * resto de d/b -> D1


 	�

    * int de d/b -> D2 + b[D3 + D4b] = d


 	�

 Repetindo �o �processo at� d = 0, separamos �os �d�gitos �do �
n�mero na base b, escondidos no n�mero decimal d.

 	�

 Falta agora juntar os d�gitos. Observe que na �transforma��o �
de �uma base para decimal, o n�mero � tratado apenas �matematicamente. �
No �caminho inverso, primeiro � tratado matematicamente e �depois, �na �
concatena��o, como palavra.

 	�

 Veja �que �na expans�o invertemos a ordem �das �parcelas. �O �
d�gito D1 passou da �ltima para a primeira posi��o. O pen�ltimo assu�
miu �a �segunda e assim sucessivamente. Veja tamb�m �que �na �primeira �
divis�o do processo dec-base, o primeiro d�gito separado � o �primeiro �
d�gito �do n�mero na base, o segundo o segundo, etc. �Na �concatena��o �
eles �devem �ser �posicionados na mesma ordem para que �n�o �haja �uma �
mudan�a �no seu valor. Em outras palavras, cada novo d�gito �obtido �� �
colocado � esquerda do d�gito anterior.

 	�

 No �caso �espec�fico �dos bin�rios, o seu �valor �decimal �� �
obtido atrav�s de sucessivas divis�es por 2.

 	�

 Com isso sabemos transformar um n�mero numa base qualquer em �
decimal, e vice-versa. Nos cap�tulos 2.1 e 2.2 do item ANEXOS est�o os �
fluxogramas desses dois algoritmos.
.pa  ,   o5                     �







 	@@�
 8 - USO DO SISTEMA BIN�RIO NOS SPRITES








 	�

 Como j� foi visto, o sprite � uma tela (grade) de 256 �bits, �
ou �32 �bytes. Em termos de bit, est�o dispostos de modo a �formar �um �
quadrado de 16x16 pixels. Em termos de byte, est�o dispostos de modo a �
formar duas pilhas de 16 bytes cada.

 	�

 Na �tela da TV, cada ponto do sprite que aparece est�, �como �
j� �foi �dito, aceso. Os pontos em 0 n�o aparecem. No modo �de �edi��o �
ocorre �uma invers�o total dos bytes, de modo que o que est� em �0 �na �
tela, est� em 1 no editor. Isso por�m, n�o � problema, uma vez que �ao �
sairmos do editor ocorre uma reinvers�o dos bytes, voltando a ficar em �
0 o que era 0 e em 1 o que era 1. Por isso, quando no editor, �devemos �
formar as figuras que queremos invertidas, pois ficar�o como desejamos �
na tela (fig 8.2).

 	�

 A �numera��o dos bytes no sprite � vertical e de �cima �para �
baixo, a contar da esquerda.

 	�

 Al�m �do �editor, podemos programar um sprite �pelo �comando �
"criafigl". O formato da primitiva � <criafigl (n) (l)>, onde (n) � �o �
n�mero �do �sprite �que se deseja programar e (l) � uma �lista �de �32 �
n�meros decimais, correspondentes aos bytes do sprite.

 	�

 Essa �lista �pode �ser entendida �pela �an�lise �do �comando �
"listafig", fun��o que envia uma lista contendo 32 elementos �(bytes), �
onde cada byte � um n�mero entre 0 e 255 (l�gico!). Exemplificando, se �
ao �editarmos �um sprite seus 8 primeiros pontos na �horizontal �forem �
"OOOOOOOO", teremos as seguintes transforma��es:

.lh 6.5
.pm 3

 	���
 �����������������������������������������������������������������

 	���
 �                                                               �

 	���
 � invers�o   [][][][][][][][] modo edi��o [] apagado [] aceso   �

 	���
 �                                                               �

 	���
 � ao trocar                            (CODIFICA��O LUMINOSA)   �

 	���
 �                                                               �

 	���
 � de modo    [][][][][][][][] modo tela   [] aceso   [] apagado �

 	hh�
 �                                                               �

 	���
 �                                                               �

 	���
 �             0 0 0 1 1 0 0 1          (CODIFICA��O BIN�RIA)    �

 	���
 �                                                               �

 	���
 �                                                               �

 	���
 �                  2423    20                                   �

 	���
 �                                                               �

 	���
 �                                 ����                          �

 	���
 �                  18+8  +  1  =  �25� (CODIFICA��O DECIMAL)    �

 	���
 �                                 ����                          �

 	���
 �                                                               �

 	���
 �                                                               �

 	���
 �   [25 ...           ] -> listafig - 32 elementos (bytes)      �

 	���
 �                                                               �

 	hh�
 �����������������������������������������������������������������
.lh 8
                                                              fig 8.1   >   o5                     �


.pm 14
.lh 6

 	$	$	�
 ��������������������������������������������

 	$	$	�
 �                                          �

 	$	$	�
 �    ����������������������������������    �

 	$	$	�
 �  1�� � � � � � � � �BYTE            �17� �

 	$	$	�
 �    �����������������                �    �

 	$	$	�
 �    �               �                �    �

 	$	$	�
 �    �bit         bit�                �    �

 	$	$	�
 �    �apagado   aceso�                �    �

 	$	$	�
 �    �               �                �    �

 	$	$	�
 �    �               �                �    �

 	$	$	�
 �    �               �                �    �

 	$	$	�
 �    �               �                �    �

 	$	$	�
 �    ����������������������������������    �

 	$	$	�
 �    �               �                �    �

 	$	$	�
 �    �               �                �    �

 	$	$	�
 �    �               �                �    �

 	$	$	�
 �    �               �                �    �

 	$	$	�
 �    �               �                �    �

 	$	$	�
 �    �               �                �    �

 	$	$	�
 �    �               �                �    �

 	$	$	�
 �    �               �                �    �

 	$	$	�
 � 16��               �                �32� �

 	$	$	�
 �    ����������������������������������    �

 	$	$	�
 �                                          �

 	$	$	�
 �                                          �

 	$	$	�
 �                  �����                   �

 	$	$	�
 �                  �   � 1 aceso           �

 	$	$	�
 �                  �   � 0 apagado         �

 	$	$	�
 �                  �����                   �

 	$	$	�
 �                                          �

 	$	$	�
 ��������������������������������������������
                                                    fig 8.2
.pm 11
.lh 8
.pa  $   o5                     �







 	���
 9 - INTERPRETA��O DE PRIMITIVAS








 	�

 N�o �� raro usarmos comandos, quando trabalhamos com �alguma �
linguagem de programa��o, sem ao menos termos uma id�ia clara de �como �
funcionam. �Quando pensamos em tentar entender um comando, n�o � �raro �
tamb�m nos apavorarmos, pensando que isso est� acima do nosso alcance. �
Por�m nem sempre � verdade. Se tivermos uma m�nima no��o do que �trata �
certo �comando, �sua �rea de atua��o, com um pouco de trabalho �e �boa �
vontade �� f�cil entender seu funcionamento, podendo �inclusive �criar �
procedimentos �com fun��o similar. Abaixo ser�o �interpretadas �quatro �
primitivas diretamente relacionadas com a edi��o de sprites: �"edfig", �
"listafig", "criafigl" e "copiafig".


 	�

 "edfig": ao p� da letra, significa "edite figura". Como �foi �
definido anteriormente por�m, ser� tratado como "edite sprite". O �que �
esse �comando faz � simples: ele pega os bytes do sprite �em �quest�o, �
inverte-os e ent�o entra um programa fechado, em linguagem de m�quina, �
que �edita �os �bytes. �Nessa situa��o �eles �s�o �colocados �� �nossa �
disposi��o �para contato via teclado. As teclas abaixo s�o �as ��nicas �
com alguma fun��o no editor:

 	�

 * <CTRL>+<K> (ou <CLS/HOME>) deleta a figura em 0, ou �seja, �
apaga todos os pontos na tela (fora do editor), No editor eles apare�
cem �brancos. �Al�m disso, posiciona o cursor na posi��o �HOME �(canto �
superior esquerdo);

 	�

 * �<CTRL>+<Y> �retorna com a figura editada �inicialmente �e �
posiciona o cursor em HOME;

 	�

 * [ ][ ][ ][ ] o teclado do cursor tem as fun��es indicadas;

 	�

 * �<SPACE> a barra de espa�o acende ou apaga o ponto �abaixo �
do cursor, conforme esteja apagado ou aceso;

 	�

 * �<ESC> (ou <CTRL>+<]/[>) tem a fun��o de sair �do �editor, �
retornando �ao modo direto. Nesse momento a figura antiga do sprite �� �
trocada pela nova formada;

 	�

 * <CTRL>+<STOP> tamb�m sai do editor, mas a nova figura �n�o �
substitui a antiga. Apresenta na tela a mensagem "Parei!".


 	�

 "listafig": �significa "liste figura". O comando �envia �uma �
lista �de �32 n�meros decimais inteiros entre 0 e 255, �que �pode �ser �
armazenada �numa �vari�vel para uso imediato ou �futuro. �Cada �n�mero �
equivale ao byte correspondente do sprite;


 	�

 "criafigl": �por �extenso �seria "crie figura �a �partir �da �
lista". �O que esse comando faz � transformar de decimal para �bin�rio �
todos �os elementos de uma listafig, transportando cada um �para �suas �
respectivas posi��es no sprites, recriando a figura armazenada;  � 9   o5                     �

 	�

 "copiafig": significa "copie figura". O que ele faz � copiar �
o conte�do do sprite determinado (a figura) para outro, tamb�m deter�
minado. �A rigor, este comando n�o precisaria existir como �primitiva, �
uma vez que pode ser facilmente definido num procedimento. Simplesmen�
te manda-se criar uma figura com a listafig de outra.


 	�

 Agora, uma breve an�lise das citadas primitivas: 


 	�

 "edfig": � um comando deveras �til, mas tamb�m substitu�vel. �
Pode ser criado um procedimento de fun��o similar, que apesar de �mais �
lento �pode �oferecer �muito mais em termos �de �versatilidade. �Mais �
adiante �ser �apresentada id�ia do procedimento, que ainda �n�o �est� �
pronto;


 	�

 "listafig" �e �"criafigl" �s�o �insubstitu�veis �a �primeira �
vista. �Fora �do editor s�o a �nica forma de se acessar �e �introduzir �
dados no sprite. Talvez haja uma forma de criar procedimentos an�logos �
em m�quina, mas isso foge ao tema do projeto. Fica como sugest�o;


 	�

 "copiafig": como foi visto, pode ser substitu�do.


 	�

 Vimos �o que existe. Vejamos agora o que falta e o que �pode �
ser �criado. �Abaixo temos a seq��ncia de alguns �questionamentos �que �
deram origem ao projeto:


 	�

 1) � poss�vel ampliar os m�todos de edi��o e programa��o �de �
sprites em LOGO?

 	�

 2) Se � poss�vel, porque o far�amos?

 	�

 3) Qual o melhor m�todo de trabalho a ser adotado?

 	�

 4) Onde poder�amos encontrar apoio bibliogr�fico?

 	�

 5) O que desenvolver, para facilitar essa programa��o?

 	�

 6) Que dificuldades encontrar�amos pelo caminho?

 	�

 7) Ent�o, o que incrementar na linguagem LOGO para �permitir �
a cria��o dos manipuladores de sprites?

 	�

 8) Finalmente, depois de desenvolvido o projeto, qual o �seu �
destino?


 	�

 Analizando �o que falta e porqu� faz falta, cheguei �s �se�
guintes conclus�es:


 	�

 - No editor � muito dif�cil passar uma figura de uma posi��o �
para �outra dentro do sprite. Para moviment�-la � necess�rio �copi�-la �
ponto a ponto no local exato. Seria interessante se houvesse um m�todo �
para ROLAR (scroll) a figura no sprite;


 	�

 - Caso quis�ssemos inverter uma figura para que ela �apare�
cesse na tela como no editor (ou vice-versa), seria necess�rio inver�
t�-la �ponto a ponto no editor. � preciso um comando para �INVERTER �a �
figura;


 	�

 - �As teclas <CTRL>+<K> no edito deletam apenas em �negativo �
(0). Seria interessante um comando para deletar em positivo (1);


 	�

 - Uma boa id�ia seria um programa que fizesse a figura rotar �
no sprite.Assim seria poss�vel trocar todas as tartarugas que vestem a �
TARTARUGA �por �setas, por exemplo, a partir de uma �nica. �As �outras   � 9   o5                     ��seriam obtidas por rota��es da seta matriz;


 	�

 - �E �outras, como espelhamento, �interfer�ncia, �redu��o �e �
amplia��o. �Todos esses procedimentos podem ser criados, com um �pouco �
de �pesquisa �em cima da Linguagem. J� foram criadas as �fun��es �para �
rolar, �deletar, �inverter �e programar o sprite �via �"listafig", �em �
bin�rio �ou decimal. Imaginou-se os processos para �espelhar, �editar, �
interferir �e �rotar. Os maiores problemas s�o ampliar �e �reduzir. �A �
estrutura �desses �procedimentos pode ser encontrada �na �listagem �do �
programa, no item ANEXOS.
.pa  ,   o5                     �







 	���
 10 - ESTRUTURAS L�GICAS UTILIZADAS








 	�

 Apesar �do �tema inovador, a estrutura �b�sica �do �programa �
firma-se quase unicamente em cima de conhecimentos pr�-adquiridos �com �
outros projetos. Ao dizer isso, estamos entrando num cap�tulo �t�cnico �
do projeto e � aconselh�vel para a melhor compreens�o dos procedimen�
tos que a listagem dos programas acompanhe a leitura.

 	�

 A �estrutura �que concatena os menus foi aproveitada �de �um �
projeto anterior, ou por que n�o dizer, paralelo: o SISTEMA.

 	�

 Iniciado �em �90, o SISTEMA � um projeto que re�ne �em �seus �
menus �todas as primitivas do LOGO que atuam em programas, �vari�veis, �
propriedades, �observadores �e arquivos. Devido ao �tema �permite �uma �
fant�stica possibilidade de generaliza��o. Isso significa poder �fazer �
o m�ximo com o m�nimo. Nele foi desenvolvido o esquema de menu �gen��
rico, �escolha �gen�rica �(depois substitu�da pela �seta, �de �escolha �
tamb�m �gen�rica) e execu��o gen�rica de primitivas. Com isso, �de �60 �
blocos o programa passou a ocupar 14, realizando mais opera��es do que �
antes.

 	�

 O menu gen�rico funciona baseado nas tr�s entradas: a coluna �
da margem esquerda, uma lista na qual s�o colocadas as op��es do menu, �
e o t�tulo do menu. Primeiramente o t�tulo � centralizado na tela, �na �
linha �0. Depois o menu � apresentado. Centraliza-se �verticalmente �o �
menu �na �tela e finalmente s�o apresentadas algumas �mensagens �(Veja �
programa "m", em Anexo 3).

 	�

 Pelo programa da seta a escolha � feita movendo-se o carac�
ter �0 �(->) com as teclas [ ] e [ ] do cursor. �Pressionada �a �tecla �
<RETURN>, �o programa usa a linha em que est� a seta para �calcular �a �
posi��o �do �tem do menu na lista de entrada do menu gen�rico. A �cada �
elemento �da lista corresponde um elemento noutra �lista �(pr�viamente �
definida), que � o nome do programa que deve ser executado caso esco�
lha-se aquela op��o.Com o comando "fa�a" o programa � executado.

 	�

 Agora �j� h� uma estrutura de v�deo reverso �que �desenvolvi �
para �substituir a seta. A principal vantagem � que permite uma �total �
demarca��o �da op��o pela barra, facilitando a leitura da linha. �Al�m �
disso, n�o deixa de ser mais interessante visualmente.

 	�

 A �execu��o gen�rica de primitivas � especialmente �simples: �
uma �das entradas do programa define se a primitiva precisa ou n�o �de �
alguma entrada e outra define a primitiva. Caso a primitiva precise de �
entrada, �� apresentada uma linha de entrada na qual ser� �colocada �a �
entrada da primitiva. Depois, ou caso o comando n�o precise de entra�
da, � executada a primitiva.

 	�

 Al�m �desses �recursos b�sicos, pois sem eles a �mem�ria �do �
computador certamente n�o poderia conter o projeto (que assim mesmo j� �
ocupa �63 blocos), foram usadas v�rias rotinas em m�quina, �operadores   � 9   o5                     ��(fun��es �que utilizam a primitiva "envie") e subprocedimentos. �Ainda �
durante o desenvolvimento do projeto, sentiu-se necessidade de algumas �
rotinas para ajudar a realizar fun��es como rolar e espelhar: var, bd, �
db e dlt. Foram criadas com o objetivo de agirem como ferramentas �que �
facilitassem a forma��o do programa maior: o FIGSTAR.

 	�

 Abaixo �est�o listados os suprocedimentos mais �importantes, �
classificados segundo sua estrutura��o, juntamente com sua utilidade:


 	�

 * rotinas em m�quina:

 	�

   - aum: aumenta duas vezes as dimens�es da figura da TAT se �
inicialmente ela estiver no estado normal;

 	�

   - dim: se a figura da TAT tiver sido pr�viamente aumenta�
da, esta rotina lhe devolve o tamanho original;


 	�

 * operadores:

 	�

   - �impar: envia verd se � e falso se n�o;

 	�

   - �inteiro: envia verd se � e falso se n�o;

 	�

   - pot�ncia: realiza potencia��o;

 	�

   - var: envia mensagem de cor e n�mero da figura da TAT;

 	�

   - bd: transforma bin�rio em decimal;

 	�

   - db: transforma decimal em bin�rio;

 	�

   - dlt: divide uma lista ao meio;

 	�

   - invert: inverte a ordem dos elementos de uma lista;

 	�

 * subprocedimentos:

 	�

   - ctl2: centraliza uma palavra;

 	�

   - f: indica a figura da TARTARUGA;

 	�

   - c: indica a cor da figura da TARTARUGA;

 	�

   - .: muda a posi��o do cursor;

 	�

   - ,: muda a posi��o da TARTARUGA.


 	�

 Desses, �apenas dois merecem uma aten��o especial: db e �bd. �
Significam respectivamente decimal-bin�rio e bin�rio-decimal e reali�
zam as transforma��es indicadas. Os cap�tulos 7.1 e 7.2 do cap�tulo �7 �
explicam �como �funcionam essas rotinas. Acompanhe � �sua �leitura �os �
fluxogramas 2.1 e 2.2 do item ANEXOS.
.pa  �$   o5                     �







 	���
 11 - FIGSTAR: MANIPULADOR DE SPRITES








 	HH�
 11.1 - ORGANOGRAMA




 	�

 O �menu principal re�ne quatro campos de �controle: �edi��o, �
figura, �vari�vel e arquivos. A op��o escolhida pelo usu�rio ir� �agir �
sempre sobre a figura do sprite em quest�o, cuja imagem e n�mero podem �
ser visualizados nos dois lados do menu.

 	�

 Cada campo ramifica-se segundo o esquema abaixo:


 	���
 ��������������������������������������������������������

 	���
 �                                �editar               �

 	���
 �                                �rolar                �

 	���
 �                                �rotar                �

 	���
 �                 �edi��o  �     �deletar              �

 	���
 �                                �inverter             �

 	���
 �                                �reduzir              �

 	���
 �                                �ampliar              �

 	���
 �                                                      �

 	���
 �                                �colorir              �

 	���
 �                                �mostrar              �

 	���
 �                 �figura  �     �copiar               �

 	���
 �                                �diminuir             �

 	���
 �                                �aumentar             �

 	���
 �                                �criar                �

 	���
 � sprite�                                              �

 	���
 �                                �listar               �

 	���
 �                                �nomear      �bin�rio �

 	���
 �                                �programar�           �

 	���
 �                 �vari�vel�     �editar      �decimal �

 	���
 �                                �eliminar             �

 	���
 �                                �mostrar              �

 	���
 �                                                      �

 	���
 �                                �dir                  �

 	���
 �                                �type                 �

 	���
 �                 �arquivo �     �load                 �

 	���
 �                                �kill                 �

 	���
 �                                �save                 �

 	���
 ��������������������������������������������������������
                                                         Fig 11.1

  � 9   o5                     �


 	���
 11.2 - CONTROLE DE EDI��O




 	�

 Este menu re�ne algumas opera��es com as quais pode-se atuar �
diretamente �sobre a figura do sprite, mudando sua forma, �tamanho �ou �
posi��o no mesmo.


 	�

 A op��o EDITAR coloca o sprite no modo de edi��o, atrav�s da �
primitiva �"edfig". Como j� foi explicada e analizada no �cap�tulo �9, �
agora ser� explicada a id�ia de como criar um procedimento em LOGO com �
fun��o similar, que n�o p�de se conclu�do.

 	�

 Existem dois tipos de editor: o que utiliza pontos luminosos �
para �representar �os bit acesos do sprite e o que utiliza �o �sistema �
bin�rio. �Nesse, os pontos em 1 s�o representados pelo n�mero 1 �e �os �
pontos em 0 pelo n�mero 0.

 	�

 A �id�ia �� inicialmente definir a dimens�o do �sprite, �que �
pode variar de 1x1 a 180x180. No caso da dimens�o m�xima do sprite, os �
pontos n�o seriam ampliados, e programar esse sprite seria praticamen�
te �programar uma tela. A medida que a dimens�o fosse �diminuindo, �o �
tamanho do ponto que representaria o bit aumentaria gradualmente.

 	�

 No momento de edi��o, os bytes do sprite seriam transforma�
dos �em bytes bin�rios que uma rotina leria bit a bit. Caso �a �edi��o �
fosse em pontos, os bits em 1 seriam representados por um quadrado �na �
tela �e os bits em 0, n�o teriam representa��o vis�vel. Caso o �editor �
fosse bin�rio, os bits seriam simplesmente passados para a tela.

 	�

 Para �o editor luminoso h� dois tipos de cursor: �um �vazio, �
que envolve o ponto e outro cheio, que pisca sobre ele. Para o �editor �
bin�rio pode-se escolher entre o cursor de v�deo reverso, que faz �com �
que a imagem do n�mero sob ele apare�a invertida e o cursor que �pisca �
sobre o bit.

 	�

 Para �manipular �os �bits, a id�ia � relacionar �o �nome �da �
vari�vel �que �cont�m �o bit � posi��o atual do �cursor. �Assim, �caso �
apag�ssemos �o �ponto, a rotina trocaria o conte�do da �vari�vel �cujo �
nome fosse a posi��o do cursor.

 	�

 Al�m �disso, poder�amos incluir nesse editor mais teclas �de �
fun��o, �para inverter, rolar e deletar. H� ainda a �possibilidade �de �
recriar a figura a cada mudan�a de bit. Assim, quando acend�ssemos �um �
ponto �no �editor, acender-se-ia o ponto correspondente na �figura �do �
sprite da TARTARUGA.

 	�

 Certamente �que �a lentid�o desse editor tornaria �a �rotina �
desagrad�vel de ser usada, contudo est� longe de ser uma id�ia invi��
vel. Caso houvesse um compilador para programas em LOGO, provavelmente �
se tornaria t�o �gil quanto o editor da linguagem.


 	�

 A op��o ROLAR (scroll) permite que a figura mude de �posi��o �
no �sprite. Com o teclado do cursor, comandamos o rolamento. Quando �a �
figura chega nas extremidades do sprite, automaticamente essa parte da �
figura passa para a extremidade oposta do sprite. Os procedimentos que �
realizam essa fun��o s�o role (principal), scr1 (cima), scr2 �(baixo), �
scr3 (esquerda), scr4 (direita) e scro (teste).


 	�

 A op��o ROTAR tamb�m n�o p�de ser conclu�da. Ela �permitiria �
que a figura girasse no sprite, como se gira a m�o no ar. A id�ia �que �
se tem � a seguinte:  � 9   o5                     �
 	�

 Transforma-se �os 32 bytes do sprite em bin�rio. Uma �rotina �
l� bit a bit e quando ler 1, junta no fim do texto de um �procedimento �
especial, �a ordem [ul pf 1]. Quando ler 0, junta a ordem [un �pf �1]. �
Ap�s �ter �lido os oito bits que formam o primeiro �byte, �juntaria �a �
ordem �[un pe 90 pt 1 pd 90 pt 8], e come�aria a leitura �do �seguinte �
byte, reiniciando o processo. Quando terminasse a leitura do 16� byte, �
juntaria a ordem [un pe 90 pf 16 pd 90], e reiniciaria a seq��ncia. Ao �
terminar de ler os 256 bits do sprite, ter�amos um programa que repro�
duziria �com �"pf" �e "pd" toda a figura do sprite. �Com �isso, �outra �
rotina �desenharia a figura com a rota��o desejada. Por �exemplo, �com �
uma �rota��o �de �90 graus, primeiro a TARTARUGA daria um �[pd �90] �e �
desenharia a figura. A TARTARUGA ent�o se posicionaria sobre o desenho �
e com o comando "inverta", assumiria a figura.

 	�

 Esse processo j� foi criado por mim, por�m n�o foi �poss�vel �
incorpor�-lo �ao �projeto, pois ainda est� imperfeito. �� �lento, �mas �
permite �rota��es �de grau �nfimo, coisa que �nenhum �outro �algoritmo �
permite �fazer. �Na melhor das hip�teses, os algoritmos �dos �editores �
comuns, em BASIC ou M�quina, permitem rota��es de 90 graus.


 	�

 A �op��o DELETAR deleta a figura do sprite em 1. Na �tela �a �
figura �aparece �como um quadrado cheio e no editor �n�o �aparecem �os �
pontos, devido � invers�o que se d� automaticamente.


 	�

 A op��o INVERTER inverte o estado dos bits. O que est� em �1 �
passa �a estar em 0 e vice-versa. Ao contr�rio da invers�o do �editor, �
essa se mant�m.


 	�

 As �rotinas �das op��es REDUZIR e AMPLIAR �ainda �n�o �foram �
criadas, �nem �se �possui �uma id�ia clara �de �como �faz�-las. �Elas �
serviriam �para que a figura fizesse exatamente o que o nome diz. �Um �
"P" pequeno por exemplo, poderia ser ampliado at� que sumisse, �saindo �
do sprite. Com o inverso, poderia se reduzir at� virar um ponto.






 	���
 11.3 - CONTROLE DE FIGURA




 	�

 Este �menu �atua sobre a apar�ncia da �TARTARUGA, �SPRITE �e �
FIGURA.

 	�

 A �op��o �COLORIR permite mudar a cor da figura. �A �rela��o �
abaixo indica as teclas com alguma fun��o:


 	�

 - [ ] incrementa em 1 o n� da cor da figura;

 	�

 - [ ] decrementa em 1 o n� da cor da figura;

 	�

 - <ESC> volta ao menu.


 	�

 A op��o MOSTRAR permite mostrar a figura de cada sprite.

 	�

 Abaixo est�o as teclas de fun��o:


 	�

 - Barra de espa�o: edita a figura em quest�o;

 	�

 - [ ] realiza a fun��o "aum";

 	�

 - [ ] realiza a fun��o "dim";

 	�

 - [ ] incrementa em 1 o n� da figura em quest�o;  � 9   o5                     �
 	�

 - [ ] decrementa em 1 o n� da figura em quest�o;

 	�

 - <ESC> volta ao menu.


 	�

 A op��o COPIAR ainda n�o foi conclu�da. Ela servir� para que �
se possa copiar o conte�do de um sprite para outro.


 	�

 As �op��es DIMINUIR e AUMENTAR realizam �respectivamente �as �
fun��es "dim" e "aum".


 	�

 A op��o CRIAR servir� para que se possa passar o conte�do de �
um �sprite guardado em uma vari�vel para a figura em quest�o, �criando �
uma nova. Apesar de n�o apresentar dificuldade, tamb�m n�o est� �con�
clu�da.






 	�
 11.4 - CONTROLE DE VARI�VEL




 	�

 Este menu re�ne todas as fun��es que manipulam as �vari�veis �
de figuras.


 	�

 LISTAR � a op��o que mostra a listafig da figura em quest�o, �
a j� citada lista de 32 bytes decimais.


 	�

 NOMEAR � a op��o que atribui a um nome que o usu�rio define, �
a listagem da figura em quest�o (ainda n�o conclu�da).


 	�

 PROGRAMAR �� �uma rotina interessante. Ela �permite �que �se �
programe uma figura atrav�s do comando "listafig". Os bytes s�o �pro�
gramados �em �base �decimal ou bin�ria. Caso se �deseje �programar �em �
bin�rio, cada um dos 32 bytes da figura em quest�o ser� programado por �
um byte bin�rio, que � digitado na linha de entrada que se �apresenta. �
Com �isso, pode-se fazer a figura que se quer em papel quadriculado �e �
copiar direto para a figura da tela. Os pontos acesos representados no �
papel �s�o �1 �em bin�rio, e os apagados s�o 0. �Assim �que �se �tecla �
<RETURN>, a linha � interpretada e os bytes bin�rios transformados �em �
decimal. ��Caso �se �tenha �escolhido �programar �em ��decimal, ��essa �
transforma��o n�o � feita.

 	�

 Cada byte que o programa recebe � passado para a figura, que �
se �muda �instantaneamente. O cursor que acompanha serve �para �que �o �
programador tenha no��o de qual � o byte que est� programando.

 	�

 H� ainda alguns comandos, que est�o listados abaixo: 

 	�

 - s  : permite sair do modo de programa��o e voltar ao menu;

 	�

 - v  : volta um byte atr�s, para corrig�-lo;

 	�

 - p  : pula o pr�ximo byte, para n�o mud�-lo;

 	�

 - e  : espelha o byte sim�trico;

 	�

 - ca : copia o byte anterior;

 	�

 - cs : copia o byte seguinte.

 	�

 Esses comandos devem ser digitados no lugar do byte.


 	�

 A �op��o �EDITAR coloca todas as vari�veis �que �existem �em �
edi��o, para que se possa manipul�-las diretamente.
  � 9   o5                     �
 	�

 ELIMINAR acaba com todas as vari�veis.


 	�

 MOSTRAR exibe todas as vari�veis.






 	�
 11.5 - CONTROLE DE ARQUIVOS




 	�

 Abaixo est�o relacionadas as op��es e suas fun��es:


 	�

 - DIR  : mostra os arquivos de disco;

 	�

 - TYPE : mostra o conte�do de um arquivo;

 	�

 - LOAD : carrega do disco um arquivo;

 	�

 - KILL : elimina do disco um arquivo;

 	�

 - SAVE : grava em disco as vari�veis da mem�ria.
.pa  �#   o5                     �







 	�	�	�
 12 - FILOSOFIA DE TRABALHO: O SISTEMA LOGO








 	�

 Podemos comparar o LOGO com uma ferramenta com a qual abri�
mos �uma �parede, o conhecimento de programa��o. No in�cio, �antes �de �
conhecermos �a Linguagem, temos a nossa frente uma parede �que �impede �
que a luz do sol nos ilumine. Tateando no escuro descobrimos um orif��
cio �que �nos �permite ver ao longe o sol, brilhando com �toda �a �sua �
impon�ncia. Por esse orif�cio passa um filete de luz, que mostra a �um �
canto �uma �ferramenta: o LOGO, a picareta com a qual �comandaremos �o �
trabalho de ampliar o buraco da parede. Assim, � pelo buraco, a �parte �
gr�fica �do �LOGO, que come�amos o trabalho. Seria �bem �mais �dif�cil �
come�ar um novo buraco, sem saber o que haveria atr�s da parede.

 	�

 Os �comandos �"pf", �"pd", "pt" e "pe" �s�o �nossa �primeira �
rela��o com a L�ngua e com ela come�amos a raspar a parede com a ponta �
da �picareta. �Ent�o, num dado momento, ap�s refletir no �que �estamos �
fazendo, �encontramos uma l�gica na seq��ncia [pf 10 pd 90] [pf 10 �pd �
90] [pf 10 pd 90] [pf 10 pd 90] que faz um quadrado. Com isso �desco�
brimos que usando o comando "repita" nosso trabalho fica menor e �mais �
r�pido. �E �come�amos a dar "porradas" na parede com �a �picareta, �ao �
inv�s �de �apenas rasp�-la. O nosso buraco se torna cada vez �maior �e �
cada vez mais se abrem nossos horizontes. A medida que vamos avan�ando �
na �programa��o �vamos encontrando partes mais s�lidas �da �parede, �e �
abandonamos �o �buraco, �come�ando outro. Repetimos �isso �sempre �que �
necess�rio. �No fim, a parede j� est� t�o esburacada que �seu �pr�prio �
peso �� �suficiente para que uma parte de si desmorone. �O �buraco �se �
alarga �e �cada vez mais f�cil se torna passar por ele, �penetrar �nos �
mist�rios do outro lado.

 	�

 Quando o buraco j� est� largo o suficiente para que passemos �
por ele, j� n�o nos contentamos com apenas isso. Queremos agora �des�
truir a barreira que nos impede de pular livremente de uma lado para o �
outro. �A �programa��o se torna cada vez mais r�pida e �f�cil. �Usamos �
"ap", "ed", "envie". A parede cai gradualmente. No outro lado �encon�
tramos novos materiais. O LOGO n�o admite viseiras nem prende ningu�m. �
A �picareta �� UM dos meios, n�o O meio. A� mesclamos �LOGO, �BASIC �e �
Linguagem de M�quina. Entra o WRITE, que como uma britadeira arrasa �a �
parede. �As �dificuldades tornam-se menores, mas o �trabalho �aumenta. �
Surge o GRAPHOS, HELLO, SISTEMA OPERACIONAL, verdadeiras dinamites que �
arrombam a parede.

 	�

 Por�m ela � imensa, ela se perde num ponto ao infinito fundo �
e toma caracter�sticas dessa dist�ncia. Por isso, por mais que traba�
lhemos, �sempre encontramos mais. Imaginem um lenhador numa �floresta. �
Inicialmente �ele �precisa derrubar apenas uma ��rvore. �Depois, �caso �
deseje aumentar todo o per�metro da clareira, dever� derrubar duas �ou �
tr�s. �No �dia seguinte seriam necess�rias dez ou mais. Cada �vez �ele   � 9   o5                     ��teria mais trabalho para completar o mesmo ato do dia anterior. Por�m, �
cada dia apresentaria mais pr�tica e tiraria mais lenha.

 	�

 Assim �� �o conhecimento: infinito. Por �mais �recursos �que �
usemos, por mais que saibamos programar, nunca se esgotam as possibi�
lidades, combina��es, id�ias. E o trabalho de avan�ar na programa��o � �
nosso. �Cada um de n�s deve quebrar a sua pr�pria parede. N�o �adianta �
deixar o trabalho para os outros. A for�a exata para esse fim � �pro�
priedade de cada um. Os outros a tem a mais, ou a menos.





 	�
 �����������������������������������������������

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �                                             �

 	�
 �����������������������������������������������
.pa  ,   o5                     �







 	���
 13 - O PROCESSO: DIFICULDADES, APERFEI�OAMENTO, SOLU��ES








 	�

 O �processo de desenvolvimento de um projeto inovador, �mais �
do que o de qualquer outro, � lento, trabalhoso e n�o raro �cansativo. �
Aparecem �in�meras �dificuldades. Algumas parecem t�o �imposs�veis �de �
serem �solucionadas �que realmente, quase nos �fazem �desistir. �Nesse �
momento � hora de parar e de repensar o que estamos fazendo.

 	�

 O melhor m�todo sem d�vida (e n�o me refiro apenas � infor�
m�tica), �� come�ar analizando o que se quer, os �objetivos. �Parte-se �
ent�o �para �o que se tem, os fatos, e finalmente, define-se �clara �e �
suscintamente �o �que falta. Tra�a-se da�, caminhos a �serem �seguidos �
(agora �sim � inform�tica), que nem sempre precisam seguir a linha �do �
projeto. �Basta �que �enquadrem �o �assunto �em �que �se �tem �alguma �
dificuldade. O trabalho nem precisa ser feito na mesma linguagem. �Um �
exemplo �� �o procedimento usado para programar sprites �pelo �comando �
"listafig". �Foram necess�rios tr�s meses de reflex�o sobre o �assunto �
para �que �a id�ia se esclarecesse na minha cabe�a. Por �isso �prefiro �
acreditar que por mais que demore para a solu��o aparecer, ela existe, �
apenas �minha cabe�a ainda n�o est pronta para v�-la. Afinal, �h� �uma �
hora e um momento certo para tudo na vida.

 	�

 Portanto, �geralmente n�o � do meu interesse imediato �con�
cluir �projetos. �Interessa-me �muito mais as id�ias �que �um �projeto �
desenvolve. �� �certo que quanto mais um assunto �for �ampliado, �mais �
possibilidades �haver� de a��o. Cada projeto avan�a em um �determinado �
campo. Acontece que esses campos t�m muitos pontos em comum, que podem �
n�o �ser �o �tema �central do assunto, �mas �podem �ser �usados �como �
ferramenta nesse assunto. Por isso n�o vejo como grande problema �ter �
v�rios �projetos �inacabados. Cada vez que exploro um, �descubro �algo �
novo. Com o tempo todos v�o se concluindo, paralelamente.

 	�

 Nesse �projeto, �a principal fonte �de �aperfei�oamento �dos �
procedimentos que apresentavam alguma falha foi sua reestrutura��o �em �
fluxogramas. Al�m disso grande parte das dificuldades se deu por falta �
de �comandos �no LOGO que realizassem fun��es como as �dos �operadores �
apresentados no cap�tulo 10. Nessas situa��es, atrav�s de pesquizas �e �
debates com colegas, chegou-se � solu��o desejada em 100% dos casos.
.pa  o1   o5                     �







 	rr�
 CONCLUS�O








 	�

 � �poss�vel concluir um trabalho como esse? Penso �que �n�o. �
Fazer isso seria afirmar que j� se entrou, quando na verdade ainda �se �
est� procurando a chave.

 	�

 Mas dessa fase inicial, podemos tirar pelo menos duas �con�
clus�es importantes:

 	�

 Primeiro: �� �realmente �poss�vel �ampliar �os �m�todos ��de �
programa��o �de �sprites �em LOGO. Se os processos �obtidos �s�o �mais �
lentos �do �que os de outras linguagens, isso n�o diminui o �valor �do �
LOGO. �O m�rito de uma linguagem n�o precisa estar relacionado �com �a �
quantidade �de informa��es que d� pronta para o usu�rio ou �programa�
dor. Em muitos casos, � mais valiosa a linguagem que desafia a �pessoa �
a raciocinar, pensar, imaginar. Isso torna a pessoa criativa e vers��
til, torna-a capaz de enfrentar as mais diferentes situa��es, �encon�
trando �com �seus pr�prios recursos as solu��es �dos �seus �problemas. �
Talvez �os �comandos �do LOGO por si s�, n�o fa�am tudo o �que �os �de �
outras �linguagens fazem, mas ofere�em condi��es para que VOC� fa�a �o �
resto.

 	�

 Segundo: a pesquisa � o m�todo por excel�ncia da boa apren�
dizagem. O assunto desse trabalho n�o est� esgotado e nem ser� facil�
mente esquecido. Id�ias evoluiram nesse per�odo e muito se pode fazer, �
tendo esse projeto como base.

 	�

 Gostaria tamb�m de deixar registrado que, a meu ver, o �LOGO �
� �uma linguagem que promete. Com tempo e afinco � �poss�vel �alcan�ar �
tudo �o �que se aspira em rela��o a ela. No in�cio, �concretizar �esse �
projeto �parecia inconceb�vel e no entanto, aqui est� ele. O �fato �de �
ser lento e em pequena escala n�o � culpa do LOGO, mas da m�quina e da �
estrutura t�cnica da vers�o que foi utilizada. Tudo o que falta para o �
LOGO se tornar uma das linguagens mais poderosas dispon�veis � bastan�
te pesquisa t�cnica em cima dele, assim como se fez com o BASIC, COBOL �
e tantas outras linguagens.

 	�

 Como sugest�o para pr�ximas fases deixo as opera��es l�gicas �
com n�meros bin�rios (and, or, not, xor), que podem ter �interessantes �
aplica��es �sobre �o �bytes �dos sprites. �Junto �a �isso �a �nota��o �
fracion�ria dos bin�rios, o desenvolvimento de comandos semelhantes a �
"edfig", "listafig" e "criafigl" e a conclus�o do que n�o foi alcan�a�
do �neste �projeto, como rota��o, amplia��o, redu��o, �espelhamento �e �
interfer�ncia de sprites.

 	�

 E �ainda uma sensa��o final: foi gratificante desenvolver �o �
projeto.
.pa  �7   o5                     �.mt 5"
.op

 	dd�
  E� Ĵ � e  ANEXOS � � e E� � 
.mt .33"
.pa  t5   o5                     �.mt 5"
.op

 	66�
  E� Ĵ � e  1 - IMAGEM DE ALGUMAS TELAS


 	���
 DO PROGRAMA � � e E� � 
.mt .33"
.pa  �3   d6                     �.mt 5"
.op

 	LL�
  E� Ĵ � e  2 - FLUXOGRAMAS � � e E� � 
.mt .33"
.pa  t5   d6                     �.mt 5"
.op

 	pp�
  E� Ĵ � e  3 - LISTAGEM DO PROGRAMA � � e E� � 
.mt .33"
.pa  t5   d6                     �.pn 57
.pg



 	\\�
 3.1 - Primeira vers�o do projeto




 	���
  E� Ĵ � e  T�TULOS � � e E� � 



 	�

 aprenda , :X :Y

 	�

 aprenda . :C :L

 	�

 aprenda a

 	�

 aprenda apague

 	�

 aprenda atrifig :n� :nome

 	�

 aprenda ctlz :L :list

 	�

 aprenda dados

 	�

 aprenda dados1.0

 	�

 aprenda dados1.1

 	�

 aprenda dados1.2

 	�

 aprenda dados1.3

 	�

 aprenda dados1.4

 	�

 aprenda del :type :nfig

 	�

 aprenda esco1.0

 	�

 aprenda esco1.1

 	�

 aprenda esco1.2

 	�

 aprenda esco1.3

 	�

 aprenda esco1.4

 	�

 aprenda giga

 	�

 aprenda grave :arquivo

 	�

 aprenda help

 	�

 aprenda input :var :string

 	�

 aprenda inv :nfig

 	�

 aprenda inv2 :nfig

 	�

 aprenda kilo



 	���
  E� Ĵ � e  PROCEDIMENTOS � � e E� � 


ap , :X :Y
   mudepos ( lista :X :Y )
fim

ap . :C :L
   mudecursor ( lista :C :L )
fim

ap a
   tat
   rg
   dt
   kilo
   mudecf 1
fim

ap apague
   tat
   eltudo
   liberemem   ;   d6                     �fim

ap atrifig :n� :nome
   att
   at
   mudefig :n�
   edfig   :n�
   atr :nome listafig :n�
fim

ap ctlz :L :list
   atr "elementos 0
   atr "n�        1
   repita nel :list [atr "sublist   nel elemento :n� :list
                     atr "elementos :elementos + :sublist
                     se :n� = ult :list [pare]
                                        [atr "n� :n� + 1]]
   atr "elementos :elementos + nel :list - 1
   se  :elementos > 27 [pare]
   atr "C int ( 29 - :elementos ) / 2
   mudecursor ( sn :C :L ) esc :list
fim

ap dados
   atr "c 7
   atr "l 9
   atr "esco [[] [] [] [] [] []]
   atr "menu [[] [] [] [] [] []]
fim

ap dados1.0
   atr "c 7
   atr "l 9
   atr "esco    [? [] 1 2 3 4]
   atr "menu    [HELP [] EDITOR FIGURAS VARI�VEIS ARQUIVOS]
   atr "titulo1 [CONTROLADOR]
   atr "titulo2 [D E]
   atr "titulo3 [SPRITES]
fim

ap dados1.1
   atr "c 7
   atr "l 9
   atr "esco    [1 2 3 4 5 6]
   atr "menu    [EDITAR ROLAR DELETAR AMPLIAR REDUZIR INVERTER]
   atr "titulo3 [EDI��O]
fim

ap dados1.2
   atr "c 7
   atr "l 9
   atr "esco    [1 2 3 4 5 6]
   atr "menu    [COLORIR MOSTRAR REPRODUZIR AUMENTAR DIMINUIR CRIAR]
   atr "titulo3 [FIGURAS]
fim

ap dados1.3
   atr "c 7
   atr "l 9   ;   d6                     �   atr "esco    [1 2 3 4 5 6]
   atr "menu    [LISTAR NOMEAR PROGRAMAR EDITAR ELIMINAR MOSTRAR
   atr "titulo3 [VARI�VEIS]
fim

ap dados1.4
   atr "c 7
   atr "l 9
   atr "esco    [1 2 3 4 5]
   atr "menu    [MOARQ ARQS ELARQ LOAD SAVE]
   atr "titulo3 [ARQUIVOS]
fim

ap del :type :nfig
   atr "limpo :type
   repita 31 [atr "limpo ( sn :type :limpo )]
   criafigl :nfig :limpo
fim

ap esco1.0
   atr "x ascii care
   se  :x = 27 [att n�velinicial]
   se  :x = 49 [p1.1]
   se  :x = 50 [p1.2]
   se  :x = 51 [p1.3]
   se  :x = 52 [p1.4]
   se  :x = 63 [help]
   esco1.0
fim

ap esco1.1
   atr "x ascii care
   se  :x = 27 [p1.0]
   se  :x = 49 [editar]
   se  :x = 50 [rolar]
   se  :x = 51 [deletar]
   se  :x = 52 [ampliar]
   se  :x = 53 [reduzir]
   se  :x = 54 [inverter]
   esco1.0
fim

ap esco1.2
   atr "x ascii care
   se  :x = 27 [p1.0]
   se  :x = 49 [colorir]
   se  :x = 50 [mostrar]
   se  :x = 51 [reproduzir]
   se  :x = 52 [aumentar]
   se  :x = 53 [diminuir]
   se  :x = 54 [criar]
   esco1.0
fim

ap esco1.3
   atr "x ascii care
   se  :x = 27 [p1.0]
   se  :x = 49 [listar]
   se  :x = 50 [nomear]   ;   d6                     �   se  :x = 51 [programar]
   se  :x = 52 [editar]
   se  :x = 53 [eliminar]
   se  :x = 54 [mostrar]
   esco1.0
fim

ap esco1.4
   atr "x ascii care
   se  :x = 27 [p1.0]
   se  :x = 49 [moarq]
   se  :x = 50 [arqs]
   se  :x = 51 [elarq]
   se  :x = 52 [load]
   se  :x = 53 [save]
   esco1.0
fim

ap giga
   .deposite 62432 227
   .chame 68
fim

ap grave :arq
   eliminearq :arq
   gravetudo  :arq
fim

ap help
fim

ap input :var :string
   ( ponha :string )
   atr :var ( pri line )
fim

ap inv :nfig
   criafigl :nfig tp listafig :nfig
fim

ap inv2 :nfig
   atr "var listafig :nfig
   repita 32 [atr "byte 255 - ( pri :var )
              atr "var  ( jf :byte ( sp :var ) )]
   criafigl :nfig :var
fim

ap kilo
   .deposite 62432 226
   .chame 68
fim
.pa  �4   d6                     �


 	�
 3.2 - �ltima vers�o do projeto




 	���
  E� Ĵ � e  T�TULOS � � e E� � 



 	�

 aprenda , :x :y

 	�

 aprenda . :c :l

 	�

 aprenda a

 	�

 aprenda aum

 	�

 aprenda b

 	�

 aprenda bd :bin

 	�

 aprenda c

 	�

 aprenda can

 	�

 aprenda cap

 	�

 aprenda cop :u

 	�

 aprenda copy

 	�

 aprenda cor

 	�

 aprenda cri

 	�

 aprenda cse

 	�

 aprenda ctl2 :l :e

 	�

 aprenda db :a :n

 	�

 aprenda del :fig

 	�

 aprenda dim

 	�

 aprenda dlt :l1 :l2

 	�

 aprenda eimpar :m

 	�

 aprenda esp

 	�

 aprenda f

 	�

 aprenda fyg

 	�

 aprenda grave

 	�

 aprenda inv :l

 	�

 aprenda invert :ls

 	�

 aprenda k :t :c :m :f :v

 	�

 aprenda l :v :c :l :i :z

 	�

 aprenda liste :fig

 	�

 aprenda m :c :l :t

 	�

 aprenda most

 	�

 aprenda n

 	�

 aprenda o :p :m

 	�

 aprenda o0

 	�

 aprenda o1

 	�

 aprenda o2

 	�

 aprenda o3

 	�

 aprenda o4

 	�

 aprenda p1

 	�

 aprenda p2

 	�

 aprenda p3

 	�

 aprenda pdb

 	�

 aprenda pow :b :e

 	�

 aprenda pro

 	�

 aprenda pul

 	�

 aprenda q

 	�

 aprenda role :fig

 	�

 aprenda sai

 	�

 aprenda scr1

 	�

 aprenda scr2   ;   d6                     �
 	�

 aprenda scr3 :1 :2

 	�

 aprenda scr4 :1 :2

 	�

 aprenda scro

 	�

 aprenda tudo

 	�

 aprenda var

 	�

 aprenda vol

 	�

 aprenda z :z

 	�

 aprenda �int :m



 	���
  E� Ĵ � e  PROCEDIMENTOS � � e E� � 


ap , :x :y
   mudepos sn :x :y
fim

ap . :c :l
   mudecursor sn int :c int :l
fim

ap a
fim

ap aum
   q
   dt
   .deposite 62432 227
   .chame 68
   s�tat 0 [, -102 8]
   s�tat 1 [,   83 8]
   at
fim

ap b
   . 5 19 ponha pal var "\  :n "�\ BYTE\
fim

ap bd :bin
   se �vazia  :bin [envie 0]
   se 0 = pri :bin [envie bd sp :bin]
   se 1 = pri :bin [envie ( pow 2 -1 + nel :bin ) + bd sp :bin]
                   [envie -300]
fim

ap c
   . 12 17 ( ponha ".. var 0 cortat ".. )
fim

ap can
   atr "des sn :des ult :des
   atr "fon sp :fon
   atr "y   :y - 1,8
   atr "n   :n + 1
fim

ap cap
   .  8  0 esc   [VII \ MOSTRA DE
   .  6  1 esc   [TRABALHOS PR�TICOS   ;   d6                     �   .  6  3 esc   [Col�gio Evang�lico
   .  8  4 esc   [Alberto Torres
   ctl2  7       "�REA\ DE\ INFORM�TICA
   ctl2 10       "\>\>\>\ FIGSTAR\ \<\<\<
   .  3 13 esc   [REALIZA��O \ PROGRAM�TICA
   ctl2 15       "\*\ Paulo\ Roberto\ Bagatini\ \*
   .  2 18 ponha [3� Ano A do 2� Grau Diurno
   ctl2 20       "\>\>\>\ Inform�tica\ \<\<\<
   .  3 22 esc   [Lajeado, outubro de 1991
fim

ap cop :u
   muded� 0
   mudecl 15
   un
   , -31 60 fa�a sn :u []
   ,  26 60
   ,  26 43
   , -31 43
   pf 17 pd 90
   pf 19 pd 90
   pf 17 pe 90
   pf 19 pe 90
   pf 17 pd 90
   pf 19 pd 90
   pf 17
   un
   mudecl :c
   mudefig :f
   , -40 48
   repita 3 [un
             , coorx + 19 52 fa�a sn :u []
             carimbe]
fim

ap copy
   atr "c cortat
   atr "f fig
   atat 2
   dt
   cop "ul
   atr "x care
   cop "ub
   att
   atat [0 1]
   o2
fim

ap cor
   c
   atr "c ascii care
   se e cortat =  0 :c = 29 [mudect 15 cor]
   se e cortat = 15 :c = 28 [mudect  0 cor]
   se               :c = 27 [o2]
   se               :c = 28 [mudect cortat + 1]
   se               :c = 29 [mudect cortat - 1]
   cor
fim
   ;   d6                     �ap cri
   atr "des sn :des :bt
   atr "fon sp :fon
   atr "y   :y - 1,8
   atr "n   :n + 1
fim

ap cse
   atr "des sn :des pri sp :fon
   atr "fon sp :fon
   atr "y   :y - 1,8
   atr "n   :n + 1
fim

ap ctl2 :l :e
   . ( 30 - nel :e ) / 2 :l ponha :e
fim

ap db :a :n
   se :a < 1 [envie :b]
   se :n = 0 [atr "b pal :b 0 envie db :a / 2 :n]
   se e :a * 2 > :n :a - 1 < :n
    [atr "b pal :b 1 envie db :a / 2 :n - :a]
    [atr "b pal :b 0 envie db :a / 2 :n]
fim

ap del :fig
   criafigl :fig [255 255 255 255 255 255 255 255
                  255 255 255 255 255 255 255 255
                  255 255 255 255 255 255 255 255
                  255 255 255 255 255 255 255 255]
fim

 ap dim
   atat [0 1]
   dt
   .deposite 62432 226
   .chame 68
   s�tat 0 [, -94 1]
   s�tat 1 [,  91 1]
   at
fim

ap dlt :l1 :l2
   se ( nel :l1 ) = nel :l2 [envie lista :l1 :l2]
                            [envie dlt sn :l1 pri :l2 sp :l2]
fim

ap eimpar :m
   se 0 = resto :m 2 [envie falso]
                     [envie  verd]
fim

ap esp
   atr "b "
   se :n < 17
    [atr "des sn :des bd invert db 128 elemento 17 :fon]
    [atr "des sn :des bd invert db 128 elemento :n - 16 :des]
   atr "fon sp :fon   ;   d6                     �   atr "y   :y - 1,8
   atr "n   :n + 1
fim

ap f
   .  2 15 ponha pal ". var 0 fig
   . 25 15 ponha pal ". var 0 fig
fim

ap fyg
   atat [0 1]
   criafigl fig sn :des :fon pdb
fim

ap grave
   elns
   eliminearq "figs
   gravetudo  "figs
fim

ap inv :l
   se :l = [] [envie []]
              [envie sn 255 - pri :l inv sp :l]
fim

ap invert :ls
   se �vazia :ls [envie "]
                 [envie pal ult :ls invert su :ls]
fim

ap k :t :c :m :f :v
   atr "l ( 22 - nel :m ) / 2
   at
   m :c :l :t
   . 9 19 esc [\<ESC\> VOLTA]
   l :v :c + 1 :l :l :l - 1 + nel :m
fim

ap l :v :c :l :i :z
   . :c :l ponha car 0
   .chame 342
   atr :v ascii care
   se 27 = conte�do :v [se :v = "a [tudo]
                                   [o0  ]]
   se 13 = conte�do :v
    [atr "n :l - :i + 1 atr :v :n
     se :v = "g [fa�a elemento :n :f]
                [se :v = "o [fa�a sn "o elemento :n :f]
                            [fa�a sn pal "o :a []]]]
    . :c :l ponha "\
   se 30 = conte�do :v [se :l = :i [l :v :c :z     :i :z]
                                   [l :v :c :l - 1 :i :z]]
   se 31 = conte�do :v [se :l = :z [l :v :c :i     :i :z]
                                   [l :v :c :l + 1 :i :z]]
   l :v :c :l :i :z
fim

ap liste :fig
   . 0 19 mo listafig :fig   ;   d6                     �   atr 1 care
   . 0 19
   att
   o3
fim

ap m :c :l :t
   atr "x 0
   .  0 19 esc []
   . 11  0 esc "CONTROLE
   . 14  1 esc "DE
   . 11  2 esc :t
   repita nel :m [atr "x :x + 1
                  . :c :l + :x - 1 esc sn "\ \ elemento :x :m]
   . 10 21 esc "Baga!Soft"
   . 10 22 esc "---------"
   .  2  8 ponha "FIG
   . 25  8 ponha "FIG
   . 12 16 esc [COR N�]
   f
   c
fim

ap most
   f
   atr "c ascii care
   se e fig =  0 :c = 29 [mudefig 59 most]
   se e fig = 59 :c = 28 [mudefig  0 most]
   se            :c = 28 [mudefig fig + 1]
   se            :c = 29 [mudefig fig - 1]
   se            :c = 27 [o2]
   se            :c = 30 [aum]
   se            :c = 31 [dim]
   se            :c = 32 [edfig fig]
   most
fim

ap n
   se ( algum "m = pri :p "a = pri :p "p = pri :p ) [atr "x care]
fim

ap o :p :m
   ad
   att
   dt
   se :m = 9 [n fa�a sn :p []]
             [atr "e line
              se �vazia :e
               [fa�a sn pal "o :a []]
               [se :m = 0 [fa�a lista :p :e]
                          [fa�a lista :p pal "" pri :e]]]
   n
   att
   fa�a sn pal "o :a []
fim

ap o0
   att
   k "\ SPRITE 8   ;   d6                     �     [\ EDI��O \ FIGURA VARI�VEL ARQUIVOS]
     []
     "a
fim

ap o1
   k "\ EDI��O 8
     [EDITAR ROLAR ROTAR DELETAR INVERTER REDUZIR AMPLIAR]
     [[edfig fig] [role fig] [a] [del fig]
      [criafigl fig inv listafig fig] [a] [a]]
     "g
fim

ap o2
   k "\ FIGURA 8
     [COLORIR MOSTRAR COPIAR DIMINUIR AUMENTAR CRIAR]
     [[cor] [most] [copy] [dim] [aum] [a]]
     "g
fim

ap o3
   q
   k "VARI�VEL 8
     [LISTAR NOMEAR PROGRAMAR EDITAR ELIMINAR MOSTRAR]
     [[liste fig] [a] [pro] [o "edns 9]
      [o "eln 0] [o "mons 9]]
     "g
fim

ap o4
   att q
   k "ARQUIVOS 10
     [DIR TYPE LOAD KILL SAVE]
     [["arquivos   9] ["mostrearq 1] ["carregue 1]
      ["eliminearq 1] ["gravetudo 1]]
     "o
fim

ap p1
   atat [2 3]
   un
   criafigl 10 [112 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
   atr "x2 -112
   atr "x3   73
   atr "y     8
   s�tat 2 [, :x2 :y]
   s�tat 3 [, :x3 :y]
   mudefig 10
fim

ap p2
   se :n = 17 [atr "x2 -70
               atr "x3 115
               atr "y    8]
   s�tat 2 [, :x2 :y]
   s�tat 3 [, :x3 :y]
   at
fim   ;   d6                     �
ap p3
   se e :n = 17 :bt = "v [atr "x2 -112
                          atr "x3   73
                          atr "y   -20,8]
fim

ap pdb
   b
   p2
   atr "bt line
   se �vazia :bt            [pdb]
                            [atr "bt pri :bt p3]
   se e :z = "b 8 < nel :bt [pdb]
   se :bt  = "s             [sai]
   se :bt  = "v             [vol]
   se �vazia :fon           [fyg]
   se :bt  = "p             [pul]
   se :bt  = "e             [esp]
   se :bt  = "ca            [can]
   se :bt  = "cs            [cse]
   se n�o �n�mero :bt       [fyg]
   se n�o �int :bt          [fyg]
   se :z   = "b             [atr "bt bd :bt]
   se e :bt > -1 :bt < 256  [cri]
   fyg
fim

ap pow :b :e
   se :e = 0 [envie 1]
             [envie :b * pow :b :e - 1]
fim

ap pro
   atr "des []
   atr "fon listafig fig
   atr "bt  []
   atr "n   1
   . 7 19 esc [bIN�RIO##dECIMAL]
   p1
   atr "z care
   se algum :z = "b :z = "d [pdb]
                            [pro]
fim

ap pul
   atr "des sn :des pri :fon
   atr "fon sp :fon
   atr "y   :y - 1,8
   atr "n   :n + 1
fim

ap q
   atat 2
   mudecl 15
   un , -112 16 ul , -77 16 , -77 -15 , -112 -15 , -112 16
   un ,   73 16 ul , 108 16 , 108 -15 ,   73 -15 ,   73 16
   atat [0 1]
fim   ;   d6                     �
ap role :fig
   atr "lfig dlt [] listafig :fig
   atr "p1   pri :lfig
   atr "p2   ult :lfig scro
fim

ap sai
   atat [2 3]
   dt
   atat [0 1]
   o3
fim

ap scr1
   atr "p1 sn sp :p1 pri :p1
   atr "p2 sn sp :p2 pri :p2
fim

ap scr2
   atr "p1 sn ult :p1 su :p1
   atr "p2 sn ult :p2 su :p2
fim

ap scr3 :1 :2
   atr "a1 2 * pri :1
   atr "a2 2 * pri :2
   se :a1 > 255 [atr "a1 :a1 - 256
                 atr "k1 1]
                [atr "k1 0]
   se :a2 > 255 [atr "a2 :a2 - 256
                 atr "k2 1]
                [atr "k2 0]
   atr "a1 :a1 + :k2
   atr "a2 :a2 + :k1
   atr "b1 sn :b1 :a1
   atr "b2 sn :b2 :a2
   criafigl :fig ( sn :b1 sp :1 :b2 sp :2 )
   se �vazia sp :1 [atr "p1 :b1
                    atr "p2 :b2
                    pare]
                   [scr3 sp :1 sp :2]
fim

ap scr4 :1 :2
   atr "a1 pri :1
   atr "a2 pri :2
   se eimpar :a1 [atr "a1 :a1 - 1
                  atr "k1 1]
                 [atr "k1 0]
   se eimpar :a2 [atr "a2 :a2 - 1
                  atr "k2 1]
                 [atr "k2 0]
   atr "b1 sn :b1 :a1 / 2 + 128 * :k2
   atr "b2 sn :b2 :a2 / 2 + 128 * :k1
   criafigl :fig ( sn :b1 sp :1 :b2 sp :2 )
   se �vazia sp :1 [atr "p1 :b1
                    atr "p2 :b2
                    pare]   ;   d6                     �                   [scr4 sp :1 sp :2]
fim

ap scro
   atr "b1 []
   atr "b2 []
   atr "x ascii care
   se :x = 27 [o1]
   se :x = 30 [scr1]
   se :x = 31 [scr2]
   se :x = 29 [scr3 :p1 :p2]
   se :x = 28 [scr4 :p1 :p2]
   criafigl :fig sn :p1 :p2
   scro
fim

ap tudo
   atat todas
   dt
   tat
   elns
   mudecf 0
   cap
   liberemem
   un
   att
   aum
   o0
fim

ap var :o :prim
   se :prim < 10 [envie pal :o :prim]
                 [envie        :prim]
fim

ap vol
   se n�o �vazia :des [atr "fon sn ult :des :fon
                       atr "des su :des
                       atr "y   :y + 1,8
                       atr "n   :n - 1]
fim

ap z :z
   att
   liberemem
   fa�a sn pal "o :z []
fim

ap �int :m
   se s�oiguais :m int :m [envie  verd]
                          [envie falso]
fim
.pa  �5   d6                     �.op








 	xx�
 REFER�NCIAS BIBLIOGR�FICAS







   1 - AVALON Software. O livro vermelho do MSX.

 	�

 S�o Paulo, McGraw-Hill, 1988. 323 p.

   2 - BECKER, Fernando et alii. Apresenta��o de trabalhos escolares.

 	�

 11 ed. Porto Alegre, Multilivro, 1990. 67 p.

   3 - Manual de opera��o do TK2000 Color.

 	�

 S�o Paulo, Microdigital Eletr�nica Ltda. 210 p.

   4 - VALENTE, Jos� Armando & VALENTE, Ann Berger. Logo: conceitos,

 	�

 aplica��es e projetos.

 	�

 S�o Paulo, McGraw-Hill, 1988. 292 p.