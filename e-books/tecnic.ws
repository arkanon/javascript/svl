}  P-IMPRES                                                                                                                 } .pl  68
.aw off
.po   4
.cw  12
.pc  75
.op
.rm 80

















 E� Ĵ � e  

 	��� 
 DETALHES


 	��� 
 T�CNICOS


 	���&
 DA


 	NN�
 LINGUAGEM HOT LOGO  V1.1
 � � g@E� � 

















 	ii�
 7� edi��o.  Revista e ampliada.


 	�%
 * * *


 	���
 by Paulo Roberto Bagatini


 	''�
 22 de mar�o de 94
 � � e � � g@ 






 	���
 TECNIC.LGO : 152.064  bytes

 	���
 Tempo de impress�o :    min  � 9   �3                     �.pa
.rm 300
.op
#############################################################################
#                                   INDEX                                   #
#############################################################################






     1 - PRIMITIVAS . . . . . . . . . . . . . . . . . . . . . . . . . . .  4
         1.1 - Listagem
         1.2 - Classifica��o
         1.3 - Dicion�rio
         1.4 - S�ntaxe
         1.5 - Convers�o

     2 - MENSAGENS  . . . . . . . . . . . . . . . . . . . . . . . . . . . 27
         2.1 - Listagem
         2.2 - Classifica��o
         2.3 - Convers�o

     3 - DEFINI��ES . . . . . . . . . . . . . . . . . . . . . . . . . . . 28

     4 - ABREVIA��ES  . . . . . . . . . . . . . . . . . . . . . . . . . . 30

     5 - POT�NCIAS DE BASE 2  . . . . . . . . . . . . . . . . . . . . . . 32

     6 - TECLADO  . . . . . . . . . . . . . . . . . . . . . . . . . . . . 33
         6.1 - Computador HOTBIT 8000
         6.2 - Computador EXPERT DD+

     7 - TECLAS DE CONTROLE . . . . . . . . . . . . . . . . . . . . . . . 34

     8 - JOYSTICK . . . . . . . . . . . . . . . . . . . . . . . . . . . . 35

     9 - TABELA ASCII . . . . . . . . . . . . . . . . . . . . . . . . . . 36

    10 - M�XIMOS E M�NIMOS  . . . . . . . . . . . . . . . . . . . . . . . 37

    11 - DEFICI�NCIAS DO HOT LOGO 1.1 . . . . . . . . . . . . . . . . . . 42

    12 - REFER�NCIAS BIBLIOGR�FICAS . . . . . . . . . . . . . . . . . . . 43

    13 - LOGO DSK . . . . . . . . . . . . . . . . . . . . . . . . . . . . 44
         13.1 - M�dulos
         13.2 - Software Auxiliar
         13.3 - Tools
         13.4 - Projetos
         13.5 - Desenhos

    14 - ALGEBRAI . . . . . . . . . . . . . . . . . . . . . . . . . . . . 48

    15 - OPERA��ES COM N�MEROS B-N�RIOS . . . . . . . . . . . . . . . . . 49

    16 - COMPLEXOS  . . . . . . . . . . . . . . . . . . . . . . . . . . . 51

    17 - FUN��ES  . . . . . . . . . . . . . . . . . . . . . . . . . . . . 52  � 9   o5                     �
    18 - HELPS  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 53

    19 - PROGRAMA��O DE IMPRESSORAS PADR�O EPSON  . . . . . . . . . . . . 54

    20 - LETRAS . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 56

    21 - LISTAS . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 57

    22 - PROGRAMA��O EM LINGUAGEM DE M�QUINA  . . . . . . . . . . . . . . 58

    23 - MATRIZES . . . . . . . . . . . . . . . . . . . . . . . . . . . . 63

    24 - MENUS  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 64

    25 - PROGRAMA��O DE M�SICAS . . . . . . . . . . . . . . . . . . . . . 65

    26 - PANEL�O  . . . . . . . . . . . . . . . . . . . . . . . . . . . . 70

    27 - PMTG . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 71

    28 - FIGSTAR II . . . . . . . . . . . . . . . . . . . . . . . . . . . 72

    29 - PRIMITIVAS PARA DESENHOS TRIDIMENSIONAIS . . . . . . . . . . . . 73

    30 - TRIGONO  . . . . . . . . . . . . . . . . . . . . . . . . . . . . 74

    31 - 2D 3D PEGES  . . . . . . . . . . . . . . . . . . . . . . . . . . 75

    32 - CONVERSOR DE TELAS GR�FICAS : Graphos III - LOGO . . . . . . . . 76
.pa  O   o5                     �.pg
#############################################################################
#                                PRIMITIVAS                                 #
#############################################################################
5
> (
> )
> *
> +
> -
5
> .chame
> .deposite
> .entra
> .examine
> .sai
4
> /
> <
> =
> >
19
> ad
> al
> algum
> ap
> apaguedesenho
> apaguetexto
> apare�atat
> aprenda
> arctan
> arquivos
> arredonde
> ascii
> at
> atat
> aten��otat
> atr
> atribua
> att
> aumentelimite
31
> cada
> car
> car.entrada
> caractere
> care
> carimbe
> carimbetudo
> carregue
> carreguec
> carreguedes
> cb
> cf
> cl
> col
> coloque
> coloqueprop  � 9   o5                     �> comimpressora
> congele
> conte�do
> coorx
> coory
> copiafig
> copie
> cordebaixo
> cordofundo
> cordol�pis
> cortat
> cos
> cp
> criafigl
> cursor
9
> defina
> desapare�atat
> descongele
> diferen�a
> dire��o
> dire��opara
> dist�ncia
> dt
> d�
19
> e
> ed
> edfig
> edite
> edns
> el
> elemento
> elimine
> eliminearq
> eln
> elns
> elobs
> elps
> eltudo
> em.colis�o
> envie
> esc
> escreva
> espere
4
> falso
> fa�a
> fig
> fim
3
> gravedes
> gravetudo
> gravetudoc
2
> int
> inverta  � 9   o5                     �5
> jf
> ji
> joy
> juntenofim
> juntenoin�cio
5
> liberemem
> lin.entrada
> line
> lista
> listafig
26
> memlivre
> mo
> mons
> moobs
> mop
> moprop
> mops
> mostra
> mostrearq
> mots
> motudo
> mudecf
> mudecl
> mudecor
> mudect
> mudecursor
> muded�
> mudefig
> mudepos
> mudepropor��o
> mudeteto
> mudevel
> mudevelx
> mudevely
> mudex
> mudey
4
> nel
> num.elem
> n�velinicial
> n�o
28
> pal
> palavra
> paracentro
> paracentroa
> paradireita
> paraesquerda
> parafrente
> paratr�s
> pare
> pc
> pca
> pd  � 9   o5                     �> pe
> pf
> pinte
> ponha
> ponhaponto
> pos
> posi��o
> pp
> pri
> primeiro
> primitivas
> produto
> prop
> propor��o
> propriedade
> pt
3
> quando
> quem
> quociente
10
> raizq
> repita
> reproduza
> resto
> retireprop
> retiretodasprop
> rg
> rp
> rq
> rtp
14
> se
> semimpressora
> semprimeiro
> sem�ltimo
> sen
> senten�a
> sn
> som
> soma
> sorteieat�
> sp
> su
> s�tat
> s�oiguais
11
> tartaruga
> tat
> temcar
> temvalor
> texto
> tirelimite
> tl
> todas
> todasprop
> toque  � 9   o5                     �> troquesinal
9
> ub
> ui
> ul
> ult
> un
> useborracha
> useinversor
> usel�pis
> usenada
5
> vel
> velx
> vely
> verd
> vers�o
1
> �ltimo
9
> �bot�o
> �joy
> �lista
> �n�mero
> �palavra
> �primitiva
> �procedimento
> �vazia
> �vis�vel

#############################################################################
#                            1.2 - CLASSIFICA��O                            #
#############################################################################

               1.2.1 - Quanto � fun��o
                       1.2.1.1  -  48 Primitivas com abrevia��o
                       1.2.1.2  - 135 Primitivas sem abrevia��o

               1.2.2 - Quanto � �rea de a��o
                       1.2.2.1  -   9 Primitivas para arquivos
                       1.2.2.2  -  23 Primitivas para desenhos
                       1.2.2.3  -   2 Primitivas para impressora
                       1.2.2.4  -  13 Primitivas para inform�tica
                       1.2.2.5  -   3 Primitivas para joystick
                       1.2.2.6  -  14 Primitivas para listas
                       1.2.2.7  -   7 Primitivas para l�gica
                       1.2.2.8  -  24 Primitivas para matem�tica
                       1.2.2.9  -   2 Primitivas para m�sica
                       1.2.2.10 -   4 Primitivas para observadores
                       1.2.2.11 -  16 Primitivas para procedimentos
                       1.2.2.12 -   6 Primitivas para propriedades
                       1.2.2.13 -   9 Primitivas para sprites
                       1.2.2.14 -  28 Primitivas para tartarugas
                       1.2.2.15 -   9 Primitivas para texto
                       1.2.2.16 -   8 Primitivas para vari�veis
                       1.2.2.17 -   6 Primitivas para uso geral
  � 9   o5                     �               1.2.3 - Quanto � construtibilidade
                       1.2.3.1  -  76 Primitivas construt�veis
                       1.2.3.2  - 107 Primitivas inconstrut�veis

               1.2.4 - Quanto � s�ntaxe
                       1.2.4.1  -  42 Comandos sem par�metros
                       1.2.4.2  -  62 Comandos com par�metros
                       1.2.4.3  -  19 Fun��es  sem par�metros
                       1.2.4.4  -  35 Fun��es  com par�metros
                       1.2.4.5  -   7 Operadores
                       1.2.4.6  -  15 Predicados
                       1.2.4.7  -   3 Constantes

#############################################################################
#                         PRIMITIVAS COM ABREVIA��O                         #
#############################################################################
7
> apaguedesenho ___ ad
> apaguetexto _____ att
> apare�atat ______ at
> aprenda _________ ap
> aten��otat ______ atat
> atribua _________ atr
> aumentelimite ___ al
7
> car.entrada _____ care
> caractere _______ car
> coloque _________ col
> coloqueprop _____ cp
> cordebaixo ______ cb
> cordofundo ______ cf
> cordol�pis ______ cl
2
> desapare�atat ___ dt
> dire��o _________ d�
3
> edite ___________ ed
> elimine _________ el
> escreva _________ esc
2
> juntenofim ______ jf
> juntenoin�cio ___ ji
1
> lin.entrada _____ line
1
> mostra __________ mo
1
> num.elem ________ nel
11
> palavra _________ pal
> paracentro ______ pc
> paracentroa _____ pca
> paradireita _____ pd
> paraesquerda ____ pe
> parafrente ______ pf
> paratr�s ________ pt
> ponhaponto ______ pp  � 9   o5                     �> posi��o _________ pos
> primeiro ________ pri
> propriedade _____ prop
3
> raizq ___________ rq
> retireprop ______ rp
> retiretodasprop _ rtp
3
> semprimeiro _____ sp
> sem�ltimo _______ su
> senten�a ________ sn
2
> tartaruga _______ tat
> tirelimite ______ tl
4
> useborracha _____ ub
> useinversor _____ ui
> usel�pis ________ ul
> usenada _________ un
1
> �ltimo __________ ult

#############################################################################
#                         PRIMITIVAS SEM ABREVIA��O                         #
#############################################################################
5
> (
> )
> *
> +
> -
5
> .chame
> .deposite
> .entra
> .examine
> .sai
4
> /
> <
> =
> >
5
> algum
> arctan
> arquivos
> arredonde
> ascii
17
> cada
> carimbe
> carimbetudo
> carregue
> carreguec
> carreguedes
> comimpressora
> congele  � 9   o5                     �> conte�do
> coorx
> coory
> copiafig
> copie
> cortat
> cos
> criafigl
> cursor
5
> defina
> descongele
> diferen�a
> dire��opara
> dist�ncia
13
> e
> edfig
> edns
> elemento
> eliminearq
> eln
> elns
> elobs
> elps
> eltudo
> em.colis�o
> envie
> espere
4
> falso
> fa�a
> fig
> fim
3
> gravedes
> gravetudo
> gravetudoc
2
> int
> inverta
1
> joy
3
> liberemem
> lista
> listafig
24
> memlivre
> mons
> moobs
> mop
> moprop
> mops
> mostrearq
> mots
> motudo  � 9   o5                     �> mudecf
> mudecl
> mudecor
> mudect
> mudecursor
> muded�
> mudefig
> mudepos
> mudepropor��o
> mudeteto
> mudevel
> mudevelx
> mudevely
> mudex
> mudey
2
> n�velinicial
> n�o
6
> pare
> pinte
> ponha
> primitivas
> produto
> propor��o
3
> quando
> quem
> quociente
4
> repita
> reproduza
> resto
> rg
8
> se
> semimpressora
> sen
> som
> soma
> sorteieat�
> s�tat
> s�oiguais
7
> temcar
> temvalor
> texto
> todas
> todasprop
> toque
> troquesinal
5
> vel
> velx
> vely
> verd
> vers�o  � 9   o5                     �9
> �bot�o
> �joy
> �lista
> �n�mero
> �palavra
> �primitiva
> �procedimento
> �vazia
> �vis�vel

#############################################################################
#                     CLASSIFICA��O QUANTO � �REA DE A��O                   #
#############################################################################

> ARQUIVOS        arquivos
                  carregue
                  carreguec
                  carreguedes
                  eliminearq
                  gravedes
                  gravetudo
                  gravetudoc
                  mostrearq

> DESENHOS        ad
                  al
                  carimbe
                  carimbetudo
                  cb
                  cf
                  cl
                  mudecf
                  mudecl
                  mudepropor��o
                  pd
                  pe
                  pf
                  pinte
                  pp
                  propor��o
                  pt
                  rg
                  tl
                  ub
                  ui
                  ul
                  un

> IMPRESSORA      comimpressora
                  semimpressora

> INFORM�TICA     .chame
                  .deposite
                  .entra
                  .examine
                  .sai  � 9   o5                     �                  ascii
                  car
                  liberemem
                  memlivre
                  primitivas
                  temcar
                  vers�o
                  �primitiva

> JOYSTICK        joy
                  �bot�o
                  �joy

> LISTAS          elemento
                  jf
                  ji
                  lista
                  nel
                  pal
                  pri
                  sn
                  sp
                  su
                  ult
                  �lista
                  �palavra
                  �vazia

> L�GICA          algum
                  e
                  falso
                  n�o
                  se
                  s�oiguais
                  verd

> MATEM�TICA      (
                  )
                  *
                  +
                  -
                  /
                  <
                  =
                  >
                  arctan
                  arredonde
                  cos
                  diferen�a
                  int
                  produto
                  quociente
                  reproduza
                  resto
                  rq
                  sen
                  soma  � 9   o5                     �                  sorteieat�
                  troquesinal
                  �n�mero

> M�SICA          toque
                  som

> OBSERVADORES    elobs
                  em.colis�o
                  moobs
                  quando

> PROCEDIMENTOS   ap
                  copie
                  defina
                  ed
                  el
                  elps
                  envie
                  fa�a
                  fim
                  mop
                  mops
                  mots
                  n�velinicial
                  pare
                  texto
                  �procedimento

> PROPRIEDADES    cp
                  moprop
                  prop
                  rp
                  rtp
                  todasprop

> SPRITES         copiafig
                  cortat
                  criafigl
                  edfig
                  fig
                  inverta
                  listafig
                  mudect
                  mudefig

> TARTARUGAS      at
                  atat
                  cada
                  congele
                  coorx
                  coory
                  descongele
                  dire��opara
                  dist�ncia
                  dt
                  d�  � 9   o5                     �                  muded�
                  mudepos
                  mudevel
                  mudevelx
                  mudevely
                  mudex
                  mudey
                  pc
                  pca
                  pos
                  quem
                  s�tat
                  todas
                  vel
                  velx
                  vely
                  �viz�vel

> TEXTO           att
                  care
                  cursor
                  esc
                  line
                  mo
                  mudecursor
                  mudeteto
                  ponha

> VARI�VEIS       atr
                  col
                  conte�do
                  edns
                  eln
                  elns
                  mons
                  temvalor

> USO GERAL       eltudo
                  espere
                  motudo
                  mudecor
                  repita
                  tat

#############################################################################
#                          PRIMITIVAS CONSTRUT�VEIS                         #
#############################################################################
5
> algum
> ap
> arctan
> arredonde
> att
10
> care
> carreguec
> col  � 9   o5                     �> comimpressora
> congele
> coorx
> coory
> copiafig
> copie
> cos
4
> descongele
> diferen�a
> dire��opara
> dist�ncia
7
> ed
> edfig
> elemento
> elobs
> eltudo
> esc
> espere
1
> int
2
> jf
> ji
1
> gravetudoc
2
> line
> lista
7
> moobs
> motudo
> mudepropor��o
> mudevelx
> mudevely
> mudex
> mudey
1
> nel
12
> pc
> pca
> pd
> pe
> pf
> pinte
> pp
> pri
> primitivas
> produto
> propor��o
> pt
1
> quociente
5
> repita  � 9   o5                     �> reproduza
> resto
> rg
> rq
4
> semimpressora
> sen
> soma
> s�oiguais
4
> tat
> temcar
> todas
> troquesinal
1
> ult
3
> velx
> vely
> vers�o
6
> �bot�o
> �lista
> �n�mero
> �palavra
> �primitiva
> �vazia

#############################################################################
#                         PRIMITIVAS INCONSTRUT�VEIS                        #
#############################################################################
5
> (
> )
> *
> +
> -
5
> .chame
> .deposite
> .entra
> .examine
> .sai
4
> /
> <
> =
> >
7
> ad
> al
> arquivos
> ascii
> at
> atat
> atr
14  � 9   o5                     �> cada
> car
> carimbe
> carimbetudo
> carregue
> carreguedes
> cb
> cf
> cl
> conte�do
> cortat
> cp
> criafigl
> cursor
3
> defina
> dt
> d�
9
> e
> edns
> el
> eliminearq
> eln
> elns
> elps
> em.colis�o
> envie
4
> falso
> fa�a
> fig
> fim
2
> gravedes
> gravetudo
1
> inverta
1
> joy
2
> liberemem
> listafig
18
> memlivre
> mo
> mons
> mop
> moprop
> mops
> mostrearq
> mots
> mudecf
> mudecl
> mudecor
> mudect
> mudecursor  � 9   o5                     �> muded�
> mudefig
> mudepos
> mudeteto
> mudevel
2
> n�velinicial
> n�o
5
> pal
> pare
> ponha
> pos
> prop
2
> quando
> quem
2
> rp
> rtp
7
> se
> sn
> som
> sorteieat�
> sp
> su
> s�tat
5
> temvalor
> texto
> tl
> todasprop
> toque
4
> ub
> ui
> ul
> un
2
> vel
> verd
3
> �joy
> �procedimento
> �vis�vel

#############################################################################
#                          COMANDOS SEM PAR�METROS                          #
#############################################################################
5
> ad
> al
> arquivos
> at
> att
4  � 9   o5                     �> carimbe
> carimbetudo
> comimpressora
> congele
2
> descongele
> dt
5
> edns
> elns
> elobs
> elps
> eltudo
1
> fim
1
> inverta
1
> liberemem
6
> mons
> moobs
> moprop
> mops
> mots
> motudo
1
> n�velinicial
5
> pare
> pc
> pca
> pinte
> primitivas
3
> reproduza
> rg
> rtp
1
> semimpressora
2
> tat
> tl
4
> ub
> ui
> ul
> un
1
> vers�o


#############################################################################
#                          COMANDOS COM PAR�METROS                          #
#############################################################################
2
> (  � 9   o5                     �> )
3
> .chame
> .deposite
> .sai
3
> ap
> atat
> atr
9
> cada
> carregue
> carreguec
> carreguedes
> col
> copiafig
> copie
> cp
> criafigl
1
> defina
9
> ed
> edfig
> el
> eliminearq
> eln
> em.colis�o
> envie
> esc
> espere
1
> fa�a
3
> gravedes
> gravetudo
> gravetudoc
18
> mo
> mop
> mostrearq
> mudecf
> mudecl
> mudecor
> mudect
> mudecursor
> muded�
> mudefig
> mudepos
> mudepropor��o
> mudeteto
> mudevel
> mudevelx
> mudevely
> mudex
> mudey
6  � 9   o5                     �> pd
> pe
> pf
> ponha
> pp
> pt
1
> quando
2
> repita
> rp
3
> se
> som
> s�tat
1
> toque

#############################################################################
#                          FUN��ES SEM PAR�METROS                           #
#############################################################################
8
> care
> cb
> cf
> cl
> coorx
> coory
> cortat
> cursor
1
> d�
1
> fig
1
> line
1
> memlivre
2
> pos
> propor��o
1
> quem
1
> todasprop
3
> vel
> velx
> vely

#############################################################################
#                          FUN��ES COM PAR�METROS                           #
#############################################################################
2
> .entra
> .examine
3  � 9   o5                     �> arctan
> arredonde
> ascii
3
> car
> conte�do
> cos
3
> diferen�a
> dire��opara
> dist�ncia
1
> elemento
1
> int
3
> jf
> ji
> joy
2
> lista
> listafig
1
> nel
4
> pal
> pri
> produto
> prop
1
> quociente
2
> resto
> rq
6
> sen
> sn
> soma
> sorteieat�
> sp
> su
2
> texto
> troquesinal
1
> ult

#############################################################################
#                                OPERADORES                                 #
#############################################################################
7
> *
> +
> -
> /
> <
> =  � 9   o5                     �> >

#############################################################################
#                                PREDICADOS                                 #
#############################################################################
1
> algum
1
> e
1
> n�o
1
> s�oiguais
2
> temcar
> temvalor
9
> �bot�o
> �joy
> �lista
> �n�mero
> �palavra
> �primitiva
> �procedimento
> �vazia
> �viz�vel

#############################################################################
#                                CONSTANTES                                 #
#############################################################################
1
> falso
1
> todas
1
> verd

#############################################################################
#                                DICION�RIO                                 #
#############################################################################
32
> CANAL ____________ n�mero inteiro de 0 a 2
> CARACTER _________ caracter do padr�o ASCII
> COLUNA ___________ n�mero inteiro de 0 a 28
> CORN�MERO ________ n�mero inteiro maior que 0
> DEC ______________ um byte
> ENDERE�O _________ n�mero inteiro de 0 a 65535
> FIGURALISTA ______ lista de 32 bytes
> FREQ��NCIA _______ n�mero de 28 a 4095
> JOYSTICKN�MERO ___ n�mero 1 ou 2
> LINHA ____________ n�mero inteiro de 0 a 23
> LISTA ____________ dados contidos entre colchetes
> LISTAINTRU��ES ___ LISTA de prc execut�veis pelo LOGO
> LISTANOMES _______ LISTA de PALAVRAs que nomeiam prc, var ou LISTA de prop
> LISTAN�MEROTAT ___ LISTA de n�meros inteiros de 0 a 29
> NOME _____________ PALAVRA que denomina um prc, var ou LISTA de prop
> NOMEARQUIVO ______ PALAVRA que denomina um arquivo  � 9   o5                     �> NOVONOME _________ PALAVRA que vai denominar um novo prc
> N�MERO ___________ um n�mero qualquer
> N�MEROFIGURA _____ n�mero inteiro de 0 a 59
> N�MERONOVAFIGURA _ n�mero inteiro de 0 a 59
> OBJETO ___________ PALAVRA, N�MERO ou LISTA
> OCORR�NCIAN�MERO _ n�mero inteiro de 0 a 5
> PALAVRA __________ seq��ncia de caracteres ASCII sem espa�os
> PORTA ____________ um byte
> PRED _____________ predicado
> PROP _____________ PALAVRA que denomina uma propriedade
> TATN�MERO ________ n�mero inteiro de 0 a 29
> TEMPO ____________ um byte
> SOM ______________ n�mero inteiro de 0 a 31
> VOLUME ___________ n�mero inteiro de 0 a 15
> X ________________ N�MERO que representa a coordenada x
> Y ________________ N�MERO que representa a coordenada y

#############################################################################
#                                  S�NTAXE                                  #
#############################################################################

#############################################################################
#                                 CONVERS�O                                 #
#############################################################################
.pa  �   o5                     �#############################################################################
#                                 MENSAGENS                                 #
#############################################################################
41
> HOT\-LOGO\ vers�o\ 1.1
> EPCOM\ 1986
> Benvindo\ ao\ Logo
> HOT\-LOGO\ vers�o\ 1.1\ \ \ \ \ \ \
> aprenda\
> coloqueprop\ "
> fim
> \ em\
> quando\
> em.colis�o\
> atribua\ "
> \ :
> \ aprendido
> \ \ Blocos\ dispon�veis:\
> \ \ Blocos\ ocupados\ \ \ :\
> N�o\ posso\ dividir\ por\ zero
> N�mero\ demasiadamente\ grande
> Muito\ complexo\ para\ pintar\ oucarimbar
> Estou\ tendo\ problemas\ com\ \ o\ dispositivo
> Muitos\ observadores
> *\ n�o\ aceita\ *\ como\ entrada\ \
> *\ �\ uma\ primitiva
> Elementos\ insuficientes\ em\ \ \ *
> N�o\ posso\ editar\ pelo\ editor
> A\ tartaruga\ n�o\ est� \ na\ tela
> Par�nteses\ mal\ colocados
> Est� \ em\ n�vel\ alto
> N�o\ \ disse\ \ o\ que\ \ fazer\ \ com*
> *\ n�o\ �\ entrada\ de\ *
> Ainda\ n�o\ aprendi\ *
> N�o\ encontrei\ a\ coisa\ chamada\ *
> N�o\ h� \ entradas\ suficientes\ \ para\ *
> N�o\ h� \ espa�o
> Entradas\ em\ excesso
> N�o\ h� \ espa�o\ suficiente\ para\ editar
> O\ arquivo\ j� \ existe
> N�o\ encontrei\ o\ arquivo
> N�o\ posso\ usar\ o\ disco
> O\ disco\ est�\ cheio
> Parei!
> *\ n�o\ �\ verdadeiro\ ou\ falso

#############################################################################
#                            2.2 - CLASSIFICA��O                            #
#############################################################################

#############################################################################
#                                 CONVERS�O                                 #
#############################################################################
.pa  �6   o5                     �#############################################################################
#                                DEFINI��ES                                 #
#############################################################################
37
> argumento - vari�vel usada na defini��o do programa

> BIOS
> bit
> bloco
> boot
> byte

> cluster
> comando
> contador

> dado
> deletar

> editar

> figura
> fractal
> fun��o
> fun��o recursiva

> �cone
> inc�gnita
> instru��o

> n�veis de procedimento
> n�mero bin�rio
> n�mero decimal
> n�mero hexadecimal
> n�mero octal

> objeto
> opera��o
> operador

> par�metro - valor efetivo, usado como entrada na execu��o do programa
> pixel - picture cell (pic cel, ks -> x)
> PPI - interface de perif�rico program�vel
> predicado
> primitiva
> procedimento
> procedimento de estado opaco
> procedimento de estado transparente
> propriedade
> PSG - gerador de som program�vel

> RAM
> recurs�o
> reset
> ROM

> set  � 9   o5                     �> setor
> slot - conector
> sprite - �cone
> subprocedimento

> trilha

> vari�vel
> vari�vel global
> vari�vel local
> VDP - processador de apresenta��o de v�deo
> voxel - volumetric pixel
.pa  /*   o5                     �#############################################################################
#                               ABREVIA��ES                                 #
#############################################################################
57
> ASC _ ASCii        arquivos gravados em ASCII (7 bits)
> ASM _ ASseMbly     programa-fonte em ASSEMBLY
> BAK _ BAcK-up      arquivo-c�pia de um arquivo-fonte
> BAS _ BASic        programa em BASIC
> BAT _ BATch        arquivo de comandos do DOS
> BGS _ BaGa!Soft
> BIN _ BIN�rio      programa em linguagem de m�quina (formato bin�rio)
> CAR _ CARacter
> CHR _ CHaRacter
> CLN _ CoLuNa
> CMD _ CoMaNdo
> COB _ COBol        programa-fonte em COBOL
> COM _ COMmand      programa em linguagem de m�quina que roda pelo DOS / CP/M
> CSR _ CurSoR
> CTE _ ConsTantE
> DAT _ DATa         dados diversos
> DEC _ DECimal
> DSK _ DiSK         (DiSKette)
> EXE _ EXEcutable   programa execut�vel pelo DOS (reloc�vel)
> FNC _ FuNCtion     fun��o
> FOR _ FORtran      programa-fonte em FORTRAN
> GAM _ GAMe         arquivo de jogos
> HEX _ HEXadecimal
> HLP _ HeLP         arquivo de informa��es sobre algum programa
> IMG _ IMaGem
> KBD _ KeyBorD
> LGO _ LoGO         programa em LOGO
> LNH _ LiNHa
> LST _ LiSTa
> LTR _ LeTRa
> MQN _ MaQuiNa
> MSG _ MenSaGem
> NUM _ NUMero
> OBJ _ OBJeto
> OBS _ OBServa��o
> OCT _ OCTal
> OUT _ OUTput       dados de sa�da
> PAS _ PAScal       programa-fonte em PASCAL
> PIC _ PICture
> PIX _ PIXel
> PMT _ PriMiTiva
> PRC _ PRoCedimento
> PRG _ PRoGrama
> PRJ _ PRoJeto
> PRN _ PRiNter      dados para a impressora
> PRP _ PRoPriedade
> RPT _ RePeTir
> RTN _ RoTiNa
> SCR _ SCReen       arquivo de tela
> SPC _ SPaCe
> SPR _ SPRite
> TAT _ TArTaruga
> TCL _ TeCLado      (TeCLa)  � 9   o5                     �> TIT _ TITle
> TOL _ TOoL
> TRC _ TRoCa
> TXT _ TeXT         textos diversos
.pa  �1   o5                     �#############################################################################
#                            POT�NCIAS DE BASE 2                            #
#############################################################################

           byte ( b) => 1.20  b = 1
      kilo byte (Kb) => 1.210 b = 1024
      mega byte (Mb) => 1.220 b = 1048576
      giga byte (Gb) => 1.230 b = 1073741824
      tera byte (Tb) => 1.240 b = 1099511627776
      peta byte (Pb) => 1.250 b = 1125899906842624
       exa byte (Eb) => 1.260 b = 1152921504606846976
           byte ( b) => 1.270 b = 1180591620717411303424
     yotta byte (Yb) => 1.280 b = 1208925819614629174706176

                          264   = 18446744073709551616

                          2256  = 115792089237316195423570985008687907853
                                  269984665640564039457584007913129639936
.pa  �$   o5                     �#############################################################################
#                       TECLADO COMPUTADOR HOTBIT 8000                      #
#############################################################################

.lh 6
.po .25"
��������������������������������������������������������������������������������
���������    ��������������������������������������������������    �������������
�� Z � e � � e  STOP � � e Z � e  �� Z � e � � e  SLCT � � e Z � e  �    �F6      ��F7      ��F8      ��F9      ��F10     �    � Z � e � � e  CLS  � � e Z � e  �� Z � e � � e  INS  � � e Z � e  �� Z � e � � e  DEL  � � e Z � e  ��
��  ��  �    �F1      ��F2      ��F3      ��F4      ��F5      �    � Z � e � � e  HOME � � e Z � e  ��  ��  ��
���������    ��������������������������������������������������    �������������
�                                                                              �
�                                                                              �
�������������������������������������������������������������  �����������������
�� Z � e � � e  ESC  � � e Z � e  �� !�� @�� #�� $�� %�� "�� &�� *�� (�� )�� _�� +�� ^�����  �             ���
��  ��1 ��2 ��3 ��4 ��5 ��6 ��7 ��8 ��9 ��0 ��- ��= ��\ ��  �  �              ��
�������������������������������������������������������������  �              ��
�������������������������������������������������������������  �����������������
�� Z � e � � e  TAB  � � e Z � e    ��Q ��W ��E ��R ��T ��Y ��U ��I ��O ��P �� `�� '�� Z � e � � e  RETURN   � � e Z � e  �  �����������������
��    ��  ��  ��  ��  ��  ��  ��  ��  ��  ��  ��' ��..��    �  ��     ��      ��
���������������������������������������������������������   �  �      ��      ��
���������������������������������������������������������   �  �      ��      ��
�� Z � e � � e  CTRL   � � e Z � e    ��A ��S ��D ��F ��G ��H ��J ��K ��L ��� �� ^�� ]��   �  �      ��     ���
��     ��  ��  ��  ��  ��  ��  ��  ��  ��  ��  ��~ ��[ ��   �  �����������������
�������������������������������������������������������������  �����������������
�  ����������������������������������������������������������  �              ��
�  � Z � e � � e  SHIFT  � � e Z � e    ��Z ��X ��C ��V ��B ��N ��M �� <�� >�� :�� ?�� Z � e � � e  SHIFT  � � e Z � e    �  �              ��
�  �     ��  ��  ��  ��  ��  ��  ��  ��, ��. ��; ��/ ��     �  ��             ��
�  ����������������������������������������������������������  �����������������
�          ���������������������������������������������                       �
�          � Z � e � � e  CAPS � � e Z � e  ��                               �� Z � e � � e  GRAP � � e Z � e  �� Z � e � � e  CODE � � e Z � e  �                       �
�          �  ��                               ��  ��  �                       �
�          ���������������������������������������������                       �
��������������������������������������������������������������������������������
.po 4
.lh 8

#############################################################################
#                      TECLADO COMPUTADOR EXPERT DD+                        #
#############################################################################

.lh 6
.po .25"
��������������������������������������������������������������������������������
��������������������������          �������������������������                  �
��   ��   ��   ��   ��   �          �   �� Z � e � � e  HOME   � � e Z � e  ��   ��   ��   �                  �
�� Z � e � � e  F1/F6  � � e Z � e  �� Z � e � � e  F2/F7  � � e Z � e  �� Z � e � � e  F3/F8  � � e Z � e  �� Z � e � � e  F4/F9  � � e Z � e  �� Z � e � � e  F5/F10 � � e Z � e  �          � Z � e � � e  STOP   � � e Z � e  �� Z � e � � e  CLS  � � e Z � e   �� Z � e � � e  SELECT � � e Z � e  �� Z � e � � e  INSERT � � e Z � e  �� Z � e � � e  DELETE � � e Z � e  �                  �
��������������������������          �������������������������                  �
�                                                              �����������������
�                                                              �  ��  ��  ��  ��
�                                                              �7 ��8 ��9 ��/ ��
�������������������������������������������������������������  �����������������
��  �� !�� "�� #�� $�� %�� ^�� &�� '�� (�� )�� _�� +�� }��BS�  �����������������
�� Z � e � � e  ESC  � � e Z � e  ��1 ��2 ��3 ��4 ��5 ��6 ��7 ��8 ��9 ��0 ��- ��= ��{ ��  �  �  ��  ��  ��  ��
�������������������������������������������������������������  �4 ��5 ��6 ��* ��
�������������������������������������������������������������  �����������������
��    ��  ��  ��  ��  ��  ��  ��  ��  ��  ��  �� `�� ]��   ��  �����������������
�� Z � e � � e  TAB  � � e Z � e    ��Q ��W ��E ��R ��T ��Y ��U ��I ��O ��P ��' ��[ ��   ��  �  ��  ��  ��  ��
���������������������������������������������������������  ��  �1 ��2 ��3 ��- ��
���������������������������������������������������������  ��  �����������������
��     ��  ��  ��  ��  ��  ��  ��  ��  ��  ��  �� ^�� @��  ��  �����������������
�� Z � e � � e  CONTROL  � � e Z � e   ��A ��S ��D ��F ��G ��H ��J ��K ��L ��� ��~ ��* ������  �  ��  ��  ��  ��
�������������������������������������������������������������  �0 ��. ��= ��+ ��
�  ����������������������������������������������������������  �����������������
�  �     ��  ��  ��  ��  ��  ��  ��  �� <�� >�� :�� ?��     �  ���� ������ �����
�  � Z � e � � e  SHIFT  � � e Z � e    ��Z ��X ��C ��V ��B ��N ��M ��, ��. ��; ��/ �� Z � e � � e  SHIFT  � � e Z � e    �  �       Z � e � � e    � � e Z � e  � Z � e � � e    Z � e Z � e    � � e Z � e       ��
�  ����������������������������������������������������������  �              ��
�      �������������������������������������������������       ��   ������   ���
�      � Z � e � � e  CAPS � � e Z � e  �� Z � e � � e   L  � � e Z � e  ��               SPACE               �� Z � e � � e   R  � � e Z � e  �       �              ��
�      � Z � e � � e  LOCK � � e Z � e  �� Z � e � � e  GRA  � � e Z � e  ��                                   �� Z � e � � e  GRA  � � e Z � e  �       �       Z � e � � e    � � e Z � e  � Z � e � � e    Z � e Z � e    � � e Z � e       ��
�      �������������������������������������������������       ���� ������ �����
��������������������������������������������������������������������������������
.po 4
.lh 8
.pa  �C   o5                     �#############################################################################
#                            TECLAS DE CONTROLE                             #
#############################################################################
52
� � � � _______ MOVIMENTA��O DO CURSOR

CLS / HOME ____ (clear screen / home) DELETA E ARMAZENA LINHAS L�GICAS
BS (��) _______ (back space)          DELETA CARACTER ANTERIOR AO CURSOR
DEL ___________ (delete)              DELETA CARACTER SOB O CURSOR
ESC ___________ (escape)              SAI DO EDITOR
INS ___________ (insert)              QUEBRA A LINHA F�SICA
TAB ___________ (tabulate)            ABRE LINHA L�GICA
CAPS __________ (caps lock)           TRANCA O TECLADO EM LETRAS MAI�SCULAS
CTRL __________ (control)             TECLA DE C�DIGOS DE CONTROLE
SLCT __________ (select)              SEM FUN��O PARA O LOGO
GRAPH (LGRA) __ (graphic)             TECLA DE MUDAN�A
CODE  (RGRA) __                       TECLA DE MUDAN�A
SHIFT _________                       TECLA DE MUDAN�A
SPACE _________ BARRA DE ESPA�OS
RETURN (�� CR)_ ABRE LINHA F�SICA - EXECUTA LINHA DE INSTRU��ES
F6  / F1 ______ SEM FUN��O PARA O LOGO
F7  / F2 ______ SEM FUN��O PARA O LOGO
F8  / F3 ______ SEM FUN��O PARA O LOGO
F9  / F4 ______ SEM FUN��O PARA O LOGO
F10 / F5 ______ SEM FUN��O PARA O LOGO

        STOP __ CONGELA O PROCESSAMENTO
 CTRL + STOP __ CANCELA O PROCESSAMENTO
 CTRL + A _____ VAI PARA O IN�CIO DA LINHA L�GICA
 CTRL + B _____ VAI PARA O FIM DO TEXTO - LINHA F�SICA
 CTRL + E _____ VAI PARA O FIM DA LINHA L�GICA
 CTRL + H _____ BS
 CTRL + I _____ TAB
 CTRL + K _____ CLS / HOME
 CTRL + M _____ RETURN
 CTRL + R _____ INS
 CTRL + T _____ VAI PARA O IN�CIO DO TEXTO - LINHA F�SICA
 CTRL + Y _____ RETORNA A �LTIMA LNH CMD MODO DIRETO - CLS / HOME
 CTRL + ] / [ _ ESC
 CTRL + ^ / \ _ MOVE O CURSOR PARA A DIREITA

        'C' ___ SA�DA: CARACTER INFERIOR DA TECLA
SHIFT + 'C' ___ SA�DA: CARACTER SUPERIOR DA TECLA
        'L' ___ SA�DA: LETRA MIN�SCULA
SHIFT + 'L' ___ SA�DA: LETRA MAI�SCULA

        GRAPH + 'C-L' _ SA�DA: CARACTER ASCII
SHIFT + GRAPH + 'C-L' _ SA�DA: CARACTER ASCII
        RGRA  + 'C-L' _ SA�DA: CARACTER ASCII (M�QUINAS EXPERT)
SHIFT + CODE  + 'C-L' _ SA�DA: CARACTER ASCII

 CTRL + SHIFT + ^ / \ _ MOVE O CURSOR PARA CIMA
 CTRL + SHIFT + STOP __ RESETA O COMPUTADOR   (M�QUINAS EXPERT)
.pa  �6   o5                     �#############################################################################
#                                   JOYSTICK                                #
#############################################################################

.po 0.54"
.cw 15
.rm 3.48"    �1   o5                     
.co 2
����������������������������
�                          �
�  ANALISE DO CONECTOR MSX �
�  ������� �� �������� ��� �
� * E/S  4 bits de entrada �
�       16 bits de sa�da   �
�        2 bits bidirecio- �
�          nais por porta  �
�                          �
� * Sinal conforme posi��o �
�         f�sica dos pinos �
�                          �
� * PSG      >> AY-3-8910A �
�                          �
� * L�gica   >> positiva   �
�                          �
� * N�vel    >> TTL        �
�                          �
� * Conector >> de 9 pinos �
�                          �
����������������������������
�            �             �
� PINO SINAL �   POSICAO   �
� ���������� �   �������   �
� > 1  FWD   � FISICA  DOS �
� > 2  BACK  � ������  ��� �
� > 3  LEFT  �    PINOS    �
� > 4  RIGHT �    �����    �
� > 5  + 5 V �             �
� > 6  TRG 1 �  1 2 3 4 5  �
� > 7  TRG 2 �             �
� > 8  OUT   �             �
� > 9  GND   �   6 7 8 9   �
�            �             �
����������������������������
.cb  �(  �1                     �����������������������������
�                          �
�        CONECTORES        �
�        ����������        �
�                          �
� joy1> conector direito   �
� joy2> conector esquerdo  �
�                          �
����������������������������
�                          �
�         COMANDOS         �
�         ��������         �
�                          �
� joy    in(1-2)  out(0-8) �
� �bot�o in(1-2)  out(V-F) �
� �joy   in(1-2)  out(V-F) �
� quando in(0-5 [cmd])     �
� moobs                    �
� elobs                    �
�                          �
����������������������������
�         �                �
� DIRECAO �   Researched   �
� ������� �                �
�    1    �      and       �
�  8   2  �                �
� 7  0  3 �  Developed by  �
�  6   4  �                �
�    5    �   E� �� � e   Baga!Soft  Z � g@E� �    � � e Z � g@ �
�         �                �
����������������������������
.po 4
.pa  �$ �1                     �.co 1
.cw 12
.rm 300
#############################################################################
#                               TABELA ASCII                                #
#############################################################################

      �����������������������������������������������������������������
      �             Tabela de Caracteres ASCII Padrao MSX             �
      �����������������������������������������������������������������
      �  0�NUL� 32�SPC� 64� @ � 96� ` �128� � �160� � �192�   �224� � �
      �  1�   � 33� ! � 65� A � 97� a �129� � �161� � �193�   �225� � �
      �  2�   � 34� " � 66� B � 98� b �130� � �162� � �194� � �226�   �
      �  3�   � 35� # � 67� C � 99� c �131� � �163� � �195�   �227� � �
      �  4�   � 36� $ � 68� D �100� d �132� � �164� � �196�   �228� � �
      �  5�   � 37� % � 69� E �101� e �133� � �165� � �197� � �229�   �
      �  6�   � 38� & � 70� F �102� f �134�   �166� � �198�   �230�   �
      �  7�   � 39� ' � 71� G �103� g �135� � �167� � �199�   �231�   �
      �  8�   � 40� ( � 72� H �104� h �136� � �168� � �200�   �232�   �
      �  9�   � 41� ) � 73� I �105� i �137� � �169� � �201�   �233�   �
      � 10�   � 42� * � 74� J �106� j �138� � �170� � �202�   �234� � �
      � 11�   � 43� + � 75� K �107� k �139� � �171� � �203�   �235�   �
      � 12�   � 44� , � 76� L �108� l �140� � �172� � �204�   �236�   �
      � 13�   � 45� - � 77� M �109� m �141� � �173� � �205�   �237� � �
      � 14�   � 46� . � 78� N �110� n �142� � �174� � �206�   �238�   �
      � 15�   � 47� / � 79� O �111� o �143� � �175� � �207�   �239�   �
      � 16�   � 48� 0 � 80� P �112� p �144� � �176� � �208�   �240� � �
      � 17� � � 49� 1 � 81� Q �113� q �145� � �177� � �209�   �241� � �
      � 18� � � 50� 2 � 82� R �114� r �146� � �178�   �210�   �242� � �
      � 19� � � 51� 3 � 83� S �115� s �147� � �179�   �211�   �243� � �
      � 20� � � 52� 4 � 84� T �116� t �148� � �180� � �212�   �244� � �
      � 21� � � 53� 5 � 85� U �117� u �149� � �181� � �213�   �245� � �
      � 22� � � 54� 6 � 86� V �118� v �150� � �182�   �214�   �246� � �
      � 23� � � 55� 7 � 87� W �119� w �151� � �183�   �215� � �247� � �
      � 24� � � 56� 8 � 88� X �120� x �152� � �184�   �216�   �248�   �
      � 25� � � 57� 9 � 89� Y �121� y �153� � �185�   �217�   �249�   �
      � 26� � � 58� : � 90� Z �122� z �154� � �186�   �218�   �250� � �
      � 27� � � 59� ; � 91� [ �123� { �155� � �187�   �219� � �251� � �
      � 28�   � 60� < � 92� \ �124� | �156� � �188�   �220�   �252� � �
      � 29�   � 61� = � 93� ] �125� } �157� � �189�   �221� � �253� � �
      � 30�   � 62� > � 94� ^ �126� ~ �158� � �190�   �222� � �254� � �
      � 31�   � 63� ? � 95� _ �127�DEL�159� � �191�  �223�   �255�CSR�
      �����������������������������������������������������������������


       OBS: os caracteres que n�o constam na tabela n�o s�o acess�veis
            pelo WordStar 5.0.
.pa  /-   o5                     �#############################################################################
#                             M�XIMOS E M�NIMOS                             #
#############################################################################

#cln : 29 (0-28) aceitas
       28 (0-27) �teis (sem acionar a seta)
       31 (0-30) dispon�veis na tela

#lnh : 24 (0-23)

#car armazen�veis no CTRL-K :  256 (  8 lnh de 29 car + 24 car)
#car armazen�veis no editor : 3072 (105 lnh de 29 car + 27 car)

#tat : 30 (0-29)
#spt : 60 (0-59)
#cor : 16 (0-32767 mod 16)                        _
#endere�os de mem�ria : 65536 (0-65535) (-> 65535,9)

#m�x obs � em.colis�o : 10                _
         � quando     : 14 (0-13) (-> 13,49)
         Caso ultrapasse (em.colis�o), msg : Muitos observadores
                         (quando)    , msg : Quando n�o aceita * como entrada
         motudo/eltudo n�o inclui moobs/elobs

#m�x arg num prc : 255
         Caso ultrapasse, msg : Entradas em excesso

#m�x recorr�ncias recursivas : 82

#m�x car em pal, nome prc ou var : 8828
         Caso ultrapasse, msg : N�o h� espa�o

#m�x mem livre : [6417 13688]

#m�n mem livre : [37 936] ([cmd var])
         Caso menor, ocorre liberemem

#m�x arq : 5�  SD - 0,18 Mb ->  56 arq  -  limpo,      blocos (       b)
           5�  DD - 0,36 Mb -> 112 arq  -  limpo, 2832 blocos (       b)
           5�  HD - 1,20 Mb ->     arq  -  limpo,      blocos (       b)
           3�  DD - 0,72 Mb -> 224 arq  -  limpo, 5704 blocos (       b)
           3�  HD - 1,44 Mb -> 448 arq  -  limpo,      blocos (       b)
           3� DHD - 2,88 Mb -> 896 arq  -  limpo,      blocos (       b)
         Caso #arq maior, msg : Estou tendo problemas com o dispositivo

         1 bloco  �  3 clusters  �  3 setores  �  128 bytes

         Arquivos gravados em blocos, o tamanho � m�ltiplo de 128 bytes.

         Prote��o de 5� : fechar o orif�cio
         Prote��o de 3� : abrir  o orif�cio

         Tentar apagar arquivo em disco protegido, msg :
                                Estou tendo problemas com o dispositivo

         gravetudo com a mem�ria limpa : 1 {^M}, 126 {b} e 1 {^Z}
  � 9   o5                     �         Editor de textos WRITE (arquivo WRITE.COM)
           - capacidade : 30199 caracteres
           - para ser usado como um "edtudo":
              a) carregar o arquivo LOGO       : ( 3 2 {S} D nomearq ESC )
              b) entrar no modo de edi��o      : ( 1 )
              c) alterar a apresenta��o        :

                 F1+I     vai para o in�cio do texto
                 SLCT F3  troca/substitui ( T )

                 1 - ^I                 ->  ^I^M
                 2 - ^M                 ->  ^M^Jbbb
                 3 - bbbaprenda         ->  ap           (+ trc 1� aprenda )
                 4 - bbbfim^M^Jbbb^M^J  ->  fim^M^J^M^J  (+ tirar ult ^M^J )

              d) modificar os programas como quiser
              e) se quiser imprimir            : ( F1+I ESC 2 CR CR CR CR )
              f) voltar � apresenta��o inicial :

                 1 - ^M^Jbbb  ->  ^M
                 2 - ^I^M     ->  ^I
                 3 - ^M^J     ->  ^M

              g) gravar                        : ( ESC 3 4 nomearq 1 nomearq )

           Caracteres de controle da impressora:

             00 ^@
             01 ^A     11 ^K     21 ^U
             02 ^B     12 ^L     22 ^V
             03 ^C     13 ^M     23 ^W
             04 ^D     14 ^N     24 ^X
             05 ^E     15 ^O     25 ^Y
             06 ^F     16 ^P     26 ^Z
             07 ^G     17 ^Q     27 ^[ -> � (RGRA-4)
             08 ^H     18 ^R     28 ^\
             09 ^I     19 ^S     29 ^]
             10 ^J     20 ^T     30 ^^

 m�n vel tat : -128
 m�x vel tat :  128                     Delta : 256

 m�n x vis�vel : -120,5
 m�x x vis�vel :  127,5 (-> 127,49999)  Delta : 248 (-> 247,99999)

 m�n y vis�vel : - 86,81818
 m�x y vis�vel :   87,72727             Delta : 174,54545

 m�n x : -32896 (-> -32895,999)                                      �
 m�x x :  32640 (->  32639,999)         Delta : 65536 (-> 65535,998) � mudepos
                                                                     �
 m�n y : -29701,818                                                  � aceita
 m�x y :  29876,363                     Delta : 59578,181            �
                                                _
 m�x pf, pd, pt, pe : 215 - 1 = 32767 (-> 32767,9) de [-,+]
                               _
     repita : 32767 (-> 32767,49) de 0 a *  � 9   o5                     �     som    : tom (0-31) volume (0-15) tempo (0-255)
     toque  : canal (0-2) freq��ncia (28-4095) volume (0-15) tempo (0-255)

 joy    envia a dir (0-8) do bast�o do joy (1-2)
 �bot�o envia verd se o bot�o  do joy (1-2) foi pressionado
 �joy   envia verd se o bast�o do joy (1-2) foi inclinado em alguma dir (0-8)

 em.colis�o tat1 tat2 [cmd]

 quando 0 a cada segundo
 quando 1 a cada tecla pressionada
 quando 2 a cada press�o no bot�o do joy 1
 quando 3 a cada press�o no bot�o do joy 2
 quando 4 aciona e define fun��o para o joy 1
 quando 5 aciona e define fun��o para o joy 2

 abrir a tampa do slot reseta o Expert em LOGO

   -M   -D                     -d   -m  m    d                      D    M
 ���������������������������������������������������������������������������
                            -1        0        1
 � overflow
 � nota��o de ponto flutuante (nota��o cient�fica)
 � nota��o de ponto fixo
 � underflow

 -M = -1e38         (-> -9,9999999e37                               8 9's
                        -99999999999999999999999999999999999999)   38 9's
  M =  9,9999999e37 (->  99999999999999999999999999999999999999)   38 9's

 -D = -9999999      7 9's
  D =  99999999     8 9's

 -d = -0,000000001  8 0's
  d =  0,000000001  8 0's

 -m = -1n38         (-> -0,00000000000000000000000000000000000001) 38 0's
  m =  1n38         (->  0,00000000000000000000000000000000000001) 38 0's

   � esc * (* em regi�o de overflow ou underflow)
   �     Msg : Ainda n�o aprendi *
   �
   � esc +1
   �     Msg : N�o h� entradas suficientes para +
   �
   � esc -:var
   �     Msg : N�o h� entradas suficientes para rp
   �
   � esc  0,000...01 (39 0's)   ->  0
   � esc -0,000...01 (39 0's)   ->  0
   �
   � esc -9e37                  -> -0,9e38
   � esc  1e7                   ->  10000000
   �
   � esc  1e16                  ->  1e16
   � esc  1e17                  ->  9,9999999e16
   � esc  2e37                  ->  2e37  � 9   o5                     �   �
   �   soma 1 2                 ->  3
   � ( soma 1 2 3 )             ->  6
   �
   �   produto 3 2              ->  6
   � ( produto 3 2 2 )          ->  12
   �
   �   diferen�a 2 3            ->  -1
   � ( diferen�a 2 3 -1 )       ->  Par�nteses mal colocados
   �
   �   quociente 3 2            ->  1,5
   � ( quociente 3 2 2 )        ->  Par�nteses mal colocados
   �
   �   s�oiguais 1 2            ->  falso
   � ( s�oiguais 1 1 1 )        ->  Par�nteses mal colocados

 digse do LOGO : 15
 mantissa      : 8

 arredondamento : 0,50 - 1,49 -> 1
                  1,50 - 2,49 -> 2

 correspond�ncia entre `espere' e tempo de espera : 0,6 _ 1 cent�simo de s
                                                    1 ___ 1/60 de s
                                                    6 ___ 1 d�cimo de s
                                                   60 ___ 1 s
              (a princ�pio)                     3.600 ___ 1 min
                                              216.000 ___ 1 hora
                                            5.184.000 ___ 1 dia

 correspond�ncia entre o n�mero do spt, a d� da TAT e o �ngulo em que ocorre a
    troca de spt:

                       F36
                        0
          F59           .            F37
          345                         15
           .      trc       trc       .
                 352,5      7,5
                   .         .




                                       (24 sprites)




                        .

 Cores :    0 transparente - incolor
            1 preto
            2 verde
            3 verde claro
            4 azul escuro
            5 azul claro  � 9   o5                     �            6 vermelho escuro
            7 azul celeste - azul anil - ciano
            8 vermelho
            9 vermelho claro
           10 amarelo - amarelo escuro - amarelo ouro
           11 amarelo claro
           12 verde escuro
           13 p�rpura - magenta - roxo - rosa
           14 cinza
           15 branco

 Figuras :  0 c�rculo
            1 cora��o
            2 gato
            3 cachorro
            4 caminh�o
            5 foguete
            6 tijolos
            7 helic�ptero
            8 locomotiva
            9 vag�o
           10 quadrado cheio
              .
              .
              .
           35 quadrado cheio
           36 tartaruga d�   0
           37 tartaruga d�  15
              .
              .
              .
           59 tartaruga d� 345
.pa  o!   o5                     �#############################################################################
#                       DEFICI�NCIAS DO HOT LOGO 1.1                        #
#############################################################################
.pa  �2   o5                     �.op
#############################################################################
#                        REFER�NCIAS BIBLIOGR�FICAS                         #
#############################################################################


                             LOGO:


    [1] AXT, Margarete. Explorando listas em LOGO.
          S�o Paulo: McGraw-Hill, 1989.

    [2] GOODYEAR, Peter. LOGO:  � � e � � e  introdu��o ao poder do ensino atrav�s da programa��o � � e � � e  .
          2. ed. Rio de Janeiro: Campus, 1987.

    [3] LACERDA, Ana Lu�sa (et al.). HOT LOGO: primeiros passos.
          1. ed. S�o Paulo: Aleph, 1986.

    [4] MENDON�A, Fernanda de V. S. de. LOGO: projetos.
          S�o Paulo: McGraw-Hill, 1989.

    [5] MENDON�A, Fernanda de V. S. de. LOGO II: palavras e listas.
          S�o Paulo: McGraw-Hill, 1989.

    [6] NININ, Maria Ot�lia G. LOGO I: geometria.
          S�o Paulo: McGraw-Hill, 1989.

    [7] PAPERT, Seymour. LOGO: computadores e educa��o.
          2. ed. S�o Paulo: Brasiliense, 1986.

    [8] VALENTE, Jos� Armando & VALENTE, Ann Berger. LOGO:  Z � e � � e  conceitos, aplica��es e projetos � � e Z � e  .
          S�o Paulo: McGraw-Hill, 1988.


                             DIVERSOS:


    [1] AVALON Software. O livro vermelho do MSX.
          S�o Paulo: McGraw-Hill, 1988.

    [2] CARVALHO J�NIOR, Luiz Tarc�sio de (et al.). Como usar seu HOTBIT.
          S�o Paulo: Aleph, 1987.

    [3] PIAZZI, Pierluigi. Expert DD PLUS: manual de instru��es.
          S�o Paulo: Aleph, 1989.

    [4] SILVA OLIVEIRA, Renato da. Operando o editor de textos Gradiente.
          S�o Paulo: Aleph, 1989.

    [5] Epson user's manual.
          V1-2. Torrance: Epson America, 1984.

    [6] HB-8000: manual do usu�rio.
          2. ed. S�o Paulo: EPCOM, 1987.
.pa  �6   o5                     �.pg
#############################################################################
#                                  M�DULOS                                  #
#############################################################################
21
     Arquivos (write, msxword, hello)
     Auto-programa��o (fa�a)
     Bidimensional
     Desenhos Recursivos
     Fractais
     Jogos
     Linguagem de M�quina
     Listas
     Matem�tica � matrizes
                � simb�lica
                � operadores
                � n�meros complexos
                � gr�ficos
     Menus
     M�sica
     Par�metros, vari�veis
     Perif�ricos (joystick, impressora)
     Primitivas -> defina, ap, copie, fa�a, envie, inverta, em.colis�o,
                   temcar, repita, atat, quando, ji, jf, reproduza, ui,
                   temvalor
     Programas Gen�ricos
     Reconstru��o de Primitivas
     Recurs�o
     Sprites
     Tabula��o
     Tartarugas
     Tridimensional

#############################################################################
#                             SOFTWARE AUXILIAR                             #
#############################################################################

                            1 - LINGUAGENS

                                1.1  - Assembler
                                1.2  - C
                                1.3  - CoBOL
                                1.4  - DBase
                                1.5  - ForTran
                                1.6  - PASCAL

                             2 - PROGRAMAS

                                2.1  - Aquarella
                                2.2  - Cls
                                2.3  - Color
                                2.4  - Edmus
                                2.5  - Graphos
                                2.6  - Hello
                                2.7  - MsxWord
                                2.8  - MsxDOS
                                2.9  - Recover
                                2.10 - Sort  � 9   o5                     �                                2.11 - Write
                                2.12 - Zapper
                                2.13 - Zzelexec

#############################################################################
#                                  TOOLS                                    #
#############################################################################

                                 1 - algebrai
                                 2 - bases
                                 3 - complexos
                                 4 - fun��es
                                 5 - helps
                                 6 - impres
                                 7 - letras
                                 8 - listas
                                 9 - m�qui
                                10 - matrizes
                                11 - menus
                                12 - music
                                13 - panel�o
                                14 - pmtg
                                15 - figstar II
                                16 - projetri
                                17 - trigono

#############################################################################
#                                 PROJETOS                                  #
#############################################################################

1 - PROGRAMAS EDUCACIONAIS

    1.1 - �rea de Educa��o Art�stica

          1.1.1 - M�sica
                  1.1.1.1  - M�sicas


    1.2 - �rea de Ci�ncias Exatas

          1.2.1 - Biologia
                  1.2.1.1  - Gen�tipos

          1.2.2 - F�sica
                  1.2.2.1  - Figuras de Lissajous
                  1.2.2.2  - For�as El�tricas
                  1.2.2.3  - Movimento Retil�neo Uniforme
                  1.2.2.4  - Vetores

          1.2.3 - Matem�tica
                  1.2.3.1  - Calculadora Cient�fica
                  1.2.3.2  - Circunfer�ncia Anal�tica
                  1.2.3.3  - Figuras Planas
                  1.2.3.4  - Figuras Tridimensionais
                  1.2.3.5  - F�rmula de B�skara
                  1.2.3.6  - Fun��es e Gr�ficos
                  1.2.3.7  - Fun��es Trigonom�tricas  � 9   o5                     �                  1.2.3.8  - Incentro e Circuncentro de Tri�ngulos
                  1.2.3.9  - Manipula��o Simb�lica de Fra��es
                  1.2.3.10 - Opera��es com Bases Num�ricas
                  1.2.3.11 - Opera��es com Matrizes
                  1.2.3.12 - Opera��es com N�meros Complexos
                  1.2.3.13 - Operadores Matem�ticos
                  1.2.3.14 - Progress�es Geom�tricas Convergentes
                  1.2.3.15 - Resolu��o de Tri�ngulos

          1.2.4 - Qu�mica
                  1.2.4.1  - Quimiograma
                  1.2.4.2  - Tabela Peri�dica


    1.3 - �rea de Estudos Sociais

          1.3.1 - Geografia
                  1.3.1.1  - Temperatura


    1.4 - �rea de Comunica��o

          1.4.1 - Portugu�s
                  1.4.1.1  - Conjuga��o de Verbos


    1.5 - �rea de Inform�tica

          1.5.1 - Linguagen LOGO
                  1.5.1.1  - DESIGNER  : Gerenciador de Desenhos
                  1.5.1.2  - EDWORD    : Gerenciador de Textos
                  1.5.1.3  - FIGSTAR I : Gerenciador de Sprites
                  1.5.1.4  - LOGMUS    : Gerenciador de M�sicas
                  1.5.1.5  - PRINTRON  : Gerenciador de Letreiros
                  1.5.1.6  - SISGRA    : Gerenciador de Gr�ficos Estat�sticos
                  1.5.1.7  - SISTEMA   : Gerenciador de Programa��o
                  1.5.1.8  - BASIC
                  1.5.1.9  - Calend�rio Permanente
                  1.5.1.10 - FIETEC
                  1.5.1.11 - Frases Aleat�rias
                  1.5.1.12 - Gincana

          1.5.2 - Linguagem BASIC
                  1.5.2.1  - ExpLOGO
                  1.5.2.2  - Gerador de Telas de Mandelbrot
                  1.5.2.3  - Reconhecimento de N�meros Primos

          1.5.3 - Linguagem DBASE
                  1.5.3.1  - Feira do Livro

          1.5.4 - Linguagem PASCAL
                  1.5.4.1  - Conversor de Telas Gr�ficas : Graphos III - LOGO
                  1.5.4.2  - Reconhecimento de N�meros Primos


2 - PROGRAMAS RECREATIVOS
  � 9   o5                     �    2.1 - Adventure
    2.1 - Batalha Espacial
    2.2 - Biorritmo
    2.3 - Crack Head
    2.4 - Di�logo
    2.5 - Free Way
    2.6 - Hor�scopo
    2.7 - Jogo da Velha
    2.8 - Piadas

#############################################################################
#                                 DESENHOS                                  #
#############################################################################

                                 1 - anel
                                 2 - baga soft
                                 3 - boca
                                 4 - bs
                                 5 - caos
                                 6 - casa 3D
                                 7 - clock
                                 8 - cruz 3D
                                 9 - figuras recursivas
                                10 - flor-de-liz
                                11 - fractais
                                12 - gata
                                13 - geat
                                14 - guns
                                15 - mixa
                                16 - monster
                                17 - paisagem
                                18 - palha�o
                                19 - parab�ns
                                20 - pega-pega
                                21 - rb
                                22 - space ship
                                23 - su�stica
                                24 - tp
.pa  �'   o5                     �#############################################################################
#                      ALGEBRAI - FRA��ES - SIMB�LICA                       #
#############################################################################
37

.po 1.5"
.rm 30  �0   o5                     
.co 2
per�odo
geratriz
geredecimal
simplifique
some
subtraia
divida
eleve
tireraiz
multiplique
raz�o
maior
menor
ordenecrescente
m�dia
mmc
mdc
fatore
represente
nota��ofra
divisores
.cb    �0                     ��maior
�inteiro
�fra��o (�racional)
�decimalexato
�decimalperi�dico
�decimal
�irracional
�primo
�par
�divis�vel
�perfeito
�positivo (�natural)
s�oamigos
s�oequivalentes
s�oprimosentresi
s�oiguaisentresi
.po 4
.pa  �! �0                     �.co 1
.rm 300
#############################################################################
#                      OPERA��ES COM N�MEROS B-N�RIOS                       #
#############################################################################
29
Legenda:   B   - base   B-n�ria
           Bit - d�gito B-n�rio
           Bte - n�mero B-n�rio
           bte - n�mero decimal
           N   - #Bit's do Bte

       and
       or
       not
       xor

       bitn
       invertabitn

       msb
       lsb
       resultante

       bindec
       decbin

       octdec
       decoct

       hexdec
       dechex             �
                          � -log10(101-P + 2�1-xi/xi+1�)
       digse10(xi,xi+1) = �
                          � D <=> �xi-xi+1� < 5.10-(D+1)
                          �
 x � e � � e  
.lh 6
                                                     bte
n-�simo Bit de um bte ____________________ Bitn   = ������ mod B
                                                    �Bn-1�
                                                    �    �
.lh 8
invers�o de um Bit _______________________ Bit    = B - Bit - 1
complemento para B de um Bit _____________ Bit    = B - Bit
invers�o do n-�simo Bit de um bte ________ bte    = bte + Bn-1(-1)Bitn
.lh 6
                                           �
#Bits significativos de um bte ___________ �bte�  = 1 + �logB bte�
                                               �B       �        �
.lh 8
invers�o de um bte _______________________ bte    = BN - bte - 1
complemento para B de um bte _____________ bte    = BN - bte      N
                                                  = {B - Biti - 1}    + 1
.lh 6
                                                           N      i=1
                                                          � �
                                                          �2�
                                                          � �
espelhamento de um bte ___________________ bte    = bte +  h� e x � e  � x � e h� e  (Biti - BitN-i+1) (BN-i - Bi-1)
.lh 8
                                                          Z � e x � e    i=1 x � e Z � e  
rolamento � esquerda de n Bits num bte ___ bte    = (bte.BN.B n mod N) mod (BN-1)
rolamento � direita  de n Bits num bte ___ bte    = (bte.BN.B-n mod N) mod (BN-1)
.lh 6
                                                       bte                         �  bte   �
                                                  = �        � mod (BN-1) + BN.man �        �
                                                    �Bn mod N�                     �Bn mod N�
                                                    �        �                     �        �
.lh 8
rota��o de 90 graus numa pilha de M btes _  � =   o5                     �
Bdec {B Bte}
     {
      se     " = Bte [envie  0]
      se �n� ult Bte [atr "p 0][atr "p 7]
      envie B * Bdec {B su Bte} + 48 - p + ascii ult :Bte
     }

decB {N B bte}
     {
      se  0 = N         [envie  "]
      se 10 > bte mod B [atr "p 0][atr "p 7]
      envie pal decB {N-1 B int bte/B} car 48 + p + bte mod B
     }
                          ����������� ����������� ����������� ��������� �����������
      00011101 =  29      �0   0 � 0� �0   0 � 0� �0   0 � 0� �  0 � 1� �1 � T � V�
  E   10001110 = 142      �1   1 � 1� �1   1 � 1� �1   1 � 0� �  1 � 0� �0 � � � F�
  � � 01000111 =  71      �0   1 � 0� �0   1 � 1� �0   1 � 1� ��������� �����������
  � � 10100011 = 163      �1   0 � 0� �1   0 � 1� �1   0 � 1�
  � � 11010001 = 209      ����������� ����������� �����������
  � � 11101000 = 232
    D 01110100 = 116      ��������� a   b = ab                 (a mod b) = b.man a/b
      00111010 =  58      � OR    � a   b = a + b - ab
                          �AND    � a   b = �a - b�            rolar n Bits � direita
 E =     2.bte mod   255  �NOT    �     b = 1 - b              equivale a rolar N - n
 D =   128.bte mod   255  �XOR    �                            Bits � esquerda.
                          �NOR    � a + b = a   b + a   b
 E =     2.bte mod 65535  �NAND   � a   b = a   b - a   b
 D = 32768.bte mod 65535  �NXOR   � a   b =   (  a     b)
                          ���������     b = BN - b - 1

                          a   b -  a    b = a + b - 255
.cw 12
.pa  o!   o5                     �#############################################################################
#                                 COMPLEXOS                                 #
#############################################################################
20

.po 2.5"
.rm 30  �0   o5                     
.co 2
re _________ (a,b)
im _________ (a,b)
arg ________ (a,b)
mod ________ (a,b)
algebraic __ (r,�)
ln _________ (a,b)
exp ________ (a,b)
inverso ____ (a,b)
norma ______ (a,b)
oposto _____ (a,b)
sim�trico __ (a,b)
conjugado __ (a,b)
represente _ (a,b)
pot�ncia ___ (a1,b1,a2,b2)
log ________ (a1,b1,a2,b2)
diferen�a __ (a1,b1,a2,b2)
quociente __ (a1,b1,a2,b2)
raiz _______ (a1,b1,a2,b2)
soma _______ (a1,b1,a2,b2,...,an,bn)
produto ____ (a1,b1,a2,b2,...,an,bn)
.po 4
.pa  �  �0                     �.co 1
.rm 300
#############################################################################
#                                  FUN��ES                                  #
#############################################################################
66

.po 1.5"
.rm 30  �0   o5                     
.co 2
digse
fatorial       (!)
porcentagem    (%)
%deretorno
#casasdecimais
absoluto       (abs)
calcule
gms
h�divis�vel
insord
integre1 (ret � esq)
integre2 (ret � dir)
integre3 (ret pelo pto m�dio)
integre4 (trap�zio)
integre5 (Simpson)
integre6 (Boole)
raiz1    (ln)
raiz2    (tentativa)
raiz3    (Newton)
ln1      (s�rie)
ln2      (integral)
log
binomial
triang.pascal
divisorescomuns
m�ltiploscomunsat�
divisores
m�ltiplosat�
sinal
PA
PG
�
�
.cb  �'  �0                     �biriale
dmmc
intersec��o
cabide
maior
menor
mantissa
m�dia
ordcresc
deg.rad.mil.grad
newton
pot�ncia   (inteiros)
eleve      (reais)
extenso
constantes (Au �   e c g)
primosat�  (crivo)
produt�rio
somat�rio
separeunidades
solo
teto
somadosinversos
gama
s�oamigos
s�oprimosentresi
�divis�vel
�primo
�inteiro
�par
�perfeito
�positivo
s�oiguaisentresi
�elemento (pertence)
.po 4
.pa  �' �0                     �.co 1
.rm 300
#############################################################################
#                                   HELPS                                   #
#############################################################################
8

.po 3.3"
.rm 30  �0   o5                     
.co 2
asc tabela ascii
joy joystyck
msg mensagens  LOGO
pmt primitivas LOGO
gtz geratriz
lis
mus m�sica
maq m�quina
.po 4
.pa  ?)  �0                     �.co 1
.rm 300
#############################################################################
#                  PROGRAMA��O DE IMPRESSORAS PADR�O EPSOM                  #
#############################################################################
67
nul        car   0
bel        car   7
bs         car   8
ht         car   9 (= car 137)
lf         car  10
vt         car  11
ff         car  12
cr         car  13
so         car  14 (expon para 1 lnh)
can        car  24
esc        car  27
del        car 127

agudo      car  39 bs
cedi       car  44 bs
cflex      car  94 bs
grave      car  96 bs
til        car 126 bs
trema      car  34 bs

carton     dblon  enfon  union
cartoff    dbloff enfoff unioff
compon     car  15 (si)
compoff    car  18
dblon      esc G
dbloff     esc H
eliton     esc M
elitoff    esc P
enfon      esc E
enfoff     esc F
expon      esc W1
expoff     esc W0
italon     esc 4
italoff    esc 5
lentaon    esc s1
lentaoff   esc s0
micron     linespc  5 eliton  compon  supon     union
microff    linespc 12 elitoff compoff scriptoff unioff
negron     dblon  enfon
negroff    dbloff enfoff
pron       comimpressora
proff      semimpressora
propon     esc p1
propoff    esc p0
senson     esc 8
sensoff    esc 9
skipon     esc N car n   (n = 1-127) n linhas antes e n depois
skipoff    esc O
sublon     esc -1
subloff    esc -0
union      esc U1
unioff     esc U0
  � 9   o5                     �subon      esc S1
supon      esc S0
scriptoff  esc T

margemdir  esc Q car n       FX-80+  FX-100+  MODE
                             2..80   2..136   Pica
                             3..96   3..163   Elite
                             4..137  4..233   Compressed
                             4..160  4..272   Compressed Elite

margemesq  esc l car n       FX-80+  FX-100+  MODE
                             0..78   0..134   Pica
                             0..93   0..160   Elite
                             0..133  0..229   Compressed
                             0..156  0..270   Compressed Elite

           esc 0             line spc = 1/8   inch ( 9 dots)
           esc 1             line spc = 7/72  inch ( 7 dots)
           esc 2             line spc = 1/6   inch (12 dots)
           esc 3 car n       line spc = n/216 inch (1/216 inch = 1/3 dot)
linespc    esc A car n       line spc = n/72  inch ( n dots)
                              (n = 0-255) default = 12 pontos de dist�ncia
#linhas    esc C car n        (n = 1-127) default = 66 linhas por p�gina


#colunas   margemesq (�-n)/2 FX-80+  FX-100+  MODE
                             0..80   0..136   Pica
                             0..96   0..163   Elite
                             0..137  0..233   Compressed
                             0..160  0..272   Compressed Elite
                                (�)     (�)
preset     esc @

poke
esci
rep
spc
dir
mots
centralize
folhasolta
   _                                               _
0,16 = 1/6 inch � 12 dots � altura de 1 linha � 4,23 mm
.pa  ,   o5                     �#############################################################################
#                                   LETRAS                                  #
#############################################################################
9

.po 3.7"
.rm 30  �0   o5                     
.co 2
data
irineu
sbara
normal
digital
xor
TPC
�tomos
auxi
.po 4
.pa  O(  �0                     �.co 1
.rm 300
#############################################################################
#                                   LISTAS                                  #
#############################################################################
36

.po 1.5"
.rm 30  �0   o5                     
.co 2
dividalista  (pal)
espelhelista (pal)
mid$
intersec��o
uni�o
diferen�a
s�oiguaisentresi
upcase
downcase
updown
suprima
substitua
insira
rolepd
rolepe
reproduza
gerepal
gerelista
.cb  �  �0                     �posi��ode
posi��esde
quantasvezes
maiorpal
menorpal
letramai
letramin
formate
cabide
maior
menor
ordcresc
insiraord
lincare
centralize
�letra
�mai�scula
�elemento (pertence)
.po 4
.pa  � �0                     �.co 1
.rm 300
#############################################################################
#                    PROGRAMA��O EM LINGUAGEM DE M�QUINA                    #
#############################################################################

  I) PRIMITIVAS

     .chame    (end)
     .deposite (end) (dad)
     .entra    (por) (dad)
     .examine  (end)
     .sai      (por) (dad)

 II) ROTINAS EM LM
40
     .rtn              envia uma lista com os nomes das rotinas em LM
     .kilbuf           tranca o teclado para mais de um caracter no buffer
     .giga    .kilo    aumenta/diminui a TAT
     .�giga   .�kilo   substitui a TAT pelo 1� quadrante aumentado/diminuido
     .lma     .lmi     tranca o teclado em letras mai�sculas/min�sculas
     .disscr  .enascr  desliga/liga a tela
     .acenda  .apague  acende/apaga a luz do CAPS
     .CAPS             liga/desliga o CAPS l�gico
     .capst   .aceso   envia o status l�gico/f�sico do CAPS
     .cason            envia verd se o motor do cassete est� ligado
     .cas              liga/desliga o motor do cassete
     .pisc             faz a luz do CAPS piscar
     .click            provoca o click do teclado
     .zum     .beep    provoca um zumbido/beep
     .chgclr           altera cf=4 sem o LOGO saber
     .clrspt           limpa os sprites dos caracteres e desativa a tela
     .trq              tranca o LOGO numa rotina sem sa�da
     .chkram           reseta o computador via soft
     .dump             examina a programa��o da mem�ria ROM - RAM
     .time             envia o valor do time da mem�ria
     .vers�o           inicializa o LOGO
     .kbl     .kbf     constru��o da rtn .kilbuf (end l�gicos e f�sicos)
     .sbl     .sbf     insere o conte�do do buffer no pr�prio (.savbuf)
     .mobuf            mostra o conte�do do buffer do teclado
     .newv             salva a vers�o atualizada do arquivo maqui
     .�pmtmqn          envia verd se � primitiva de m�quina (pertence � .rtn)
     .img              envia a matriz dos caracteres ascii
     .poke             imprime o conte�do da tela
     .data             envia os car correspondentes aos dados de .datapoke
     .datapoke         envia a lista de dados utilizada pelo .poke
     .insbuf           insere caracteres no buffer do teclado

III) PROCEDIMENTOS

     .rtn      envie [todos os t�tulos dos prc abaixo, inclusive este]
     .kilbuf   .chame 342 esc care
     .giga     .deposite 62432 227 .chame 68
     .kilo     .deposite 62432 226 .chame 68
     .�giga    .deposite 62432 225 .chame 68
     .�kilo    .deposite 62432 228 .chame 68
     .lma      .deposite 64683   1 elobs
     .lmi      quando 0 [.CAPS 0]
     .disacr   .chame 65  � 9   o5                     �     .enascr   .kilo
     .acenda   .sai 170 23 (= .sai 171 12)
     .apague   .sai 170 87 (= .sai 171 13 = .chame 306)
     .CAPS     .deposite 64683 :a * 255
     .capst    se 255 = .examine 64683   [envie verd][envie falso]
     .aceso    se   0 = bit 7 .entra 170 [envie verd][envie falso]
     .cason    se   0 = bit 6 .entra 170 [envie verd][envie falso]
     .cas      .sai 170 invertabit 6 .entra 170
     .pisc     .sai 170 invertabit 7 .entra 170 .pisc
     .click    .sai 170 invertabit 8 .entra 170 .click
     .zum      .sai 170 225 .sai 170 127        .zum
     .beep     .chame 192 (= toque 0 1316 7 0,14 ... 40 �s)
     .chgclr   .chame  98
     .clrspr   .chame 105
     .trq      .chame 125
     .chkram   .chame   0
     .dump     (esc :e car .examine :e)
     .time     envie .examine 64670
     .vers�o   .chame 19880

     .kbl      � .deposite 62458 .examine 62456
               � .deposite 62459 .examine 62457

     .kbf      � .deposite 62456 240
               � .deposite 62457 251
               � .deposite 62458 240
               � .deposite 62459 251

     .sbl      � atr "escr resultante .examine 62459 -1+.examine 62458
               � atr "car  car .examine :escr
               � atr "msb  msb :escr
               � atr "lsb  lsb :escr
               � .deposite 62456 :lsb
               � .deposite 62457 :msb
               � esc :car

     .sbf      � atr "car .examine 64535
               � .deposite 62456  23
               � .deposite 62457 252
               � .deposite 62458 240
               � .deposite 62459 251
               � esc :car

     .mobuf    � att
               � atr "e 64496
               � atr "c 6
               � mudecursor [8 0]
               � esc [... Buffer ...]
               � ponha "\ \ ��������������������������   (26 � )
               � repita 2
               �  [mudecursor sn :c 2
               �   repita 20
               �    [atr "a .examine :e
               �     atr "e :e + 1
               �     mudecursor  sn :c ult cursor
               �     se   1 = :a [esc [�  ^A]] [
               �     se   2 = :a [esc [�  ^B]] [  � 9   o5                     �               �     se   5 = :a [esc [�  ^E]] [
               �     se   8 = :a [esc [�  BS]] [
               �     se   9 = :a [esc [� TAB]] [
               �     se  11 = :a [esc [�  ^K]] [
               �     se  13 = :a [esc [�  ^M]] [
               �     se  20 = :a [esc [�  ^T]] [
               �     se  25 = :a [esc [�  ^Y]] [
               �     se  27 = :a [esc [� ESC]] [
               �     se  28 = :a [esc [�   �]] [
               �     se  29 = :a [esc [�   �]] [
               �     se  30 = :a [esc [�   �]] [
               �     se  31 = :a [esc [�   �]] [
               �     se 127 = :a [esc [� DEL]] [
               �      esc car :a]]]]]]]]]]]]]]]]
               �   atr "c 21]
               � ponha "\ \ ��������������������������
               � mudecursor sn 0 1 + ult cursor

     .newv     � tat
               � esc [\ \ \ \ \ \ \ \ \ \ \ � MAQUI]
               � esc []
               � esc [\ \ \ �atualizando o arquivo�]
               � repita 5 [esc []]
               � esc [\ \ \ Revis�o e amplia��o by]
               � esc []
               � esc [\ \ \ \ \ \ \ \ Baga!Soft]
               � esc []
               � elns
               � eliminearq "MAQUI
               � gravetudo  "MAQUI

     .�pmtmqn  � {:pmt}
               � se n�o temvalor "&pmt [atr "&pmt .rtn]
               � se �vazia       :&pmt [eln "&pmt          envie falso]
               � se :pmt = pri   :&pmt [eln "&pmt          envie  verd]
               �                       [atr "&pmt sp :&pmt envie .�pmtmqn :pmt]

     .img      � atr "e (8*ascii :car) + (256*.examine 5) + (.examine 4)  Z � e � � e  (= 7103) � � e Z � e  
               � atr "& []
               � repita 8 [atr "& sn :& .examine :e atr "e :e + 1]
               � envie :&

     .poke     � atr "e 1
               � repita 162 [.deposite (50161 + :e) (ascii elemento :e .data)
               �             atr "e :e + 1]
               � .chame 50000

     .data

     .datapoke � envie
               �  [205 168   0 200  60 192  62   0   8   1   1   0  62  27
               �   205 165   0  62  65 205 165   0  62   8 205 165   0  62
               �    27 205 165   0  62  75 205 165   0  62 128 205 165   0
               �    62   1 205 165   0  33  24   0 197 229 203  33 203  33
               �   203  33  33   7  23   9  30   8 243 125 211 153 124 211
               �   153  62   3  61  32 253 219 152 251  71   8 203  71  40
               �     8 203  32 203  32 203  32 203  32   8 175  14   4 203  � 9   o5                     �               �    96  40   2 246 192  13  40   8 203  63 203  63 203  56
               �    24 239 205 165   0 205 165   0  43  29  32 196  17 248
               �     0 175 237  82 227  45 227  32 183 193 193  62  10 205
               �   165   0 205 183   0  56  14   8  60 111   8 203  69  32
               �   128  12 121 254  32  32 248 201]

     .insbuf   ��������������������������������������������������������
               �HEX � DEC �       BIN       �           OBS           �
               �    �     ���������������������������������������������
               �    �     � bt+sig � bt-sig � n=#caracteres a inserir �
               ������������������������������                         �
               �F3F8�62456�11110011�11111000�end ap escr (bt-sig ei+n)�
               �F3F9�62457�11110011�11111001�end ap escr (bt+sig ei+n)�
               �F3FA�62458�11110011�11111010�end ap leit (bt+sig ei)  �
               �F3FB�62459�11110011�11111011�end ap leit (bt-sig ei)  �
               �FBF0�64496�11111011�11110000�end inicial              �
               �FC18�64535�11111100�00010111�end final m�x (n=40)     �
               ��������������������������������������������������������
               �.deposite 62456 (bd ult db 64496 + n)                 �
               �.deposite 62457 (bd pri db 64496 + n)                 �
               �.deposite 62458 (bd pri db 64496) ou (251)            �
               �.deposite 62459 (bd ult db 64496) ou (240)            �
               �.deposite 64496 (ascii "A) ou (65)                    �
               �    .                                                 �
               �    .                                                 �
               �    .                                                 �
               �.deposite 64535 (ascii :car)                          �
               ��������������������������������������������������������

               � {:lt :lc}
               � atr "a 1
               � atr "b 1
               � atr "n soma nel :lt nel :lc
               � atr "m lsb 64496 + :n
               � atr "M msb 64496 + :n
               � .deposite 62456 :m
               � .deposite 62457 :M
               � .deposite 62458 251
               � .deposite 62459 240
               � repita nel :lt
               �  [.deposite (64495 + :a) (ascii elemento :a :lt)
               �   atr "a :a + 1]
               � repita nel :lc
               �  [.deposite (64495 + :a) (      elemento :b :lc)
               �   atr "a :a + 1
               �   atr "b :b + 1]

 IV) ENDERE�OS DA ROM & RAM

     ������������������������������
     �      CAMPO     �BEGIN� END �
     ������������������������������
     �BASIC           �    0�16383�
     �LOGO            �16384�65535�
     ������������������������������
     �d�gitos 0-9     � 3493� 3502�
     �ltr a-z         � 3515� 3540�  � 9   o5                     �     �ltr A-Z         � 3563� 3588�
     �ltr minus acent � 4195� 4244�
     �ltr maius acent � 4245� 4264�
     �tcl fnc (16 p/t)� 5008� 5167�
     �msg BASIC       �15734�16264�
     �msg BASIC       �16388�16353�
     ������������������������������
     �msg LOGO        �45722�46729�
     �pmt LOGO        �47423�48823�
     �^K              �51773�52028�
     �buffer do editor�54568�57639�
     �peda�os do dir  �60309�60770�
     �peda�os de prg  �60821�61298�
     �portas para prn �61943�61962�
     �buffer do tcl   �64496�64535�
     �memram -> obs   �     �     �
     �       -> prg   �     �     �
     �       -> var   �     �     �
     �       -> prp   �     �     �
     ������������������������������

  V) MICRO TABELA ASCII

     ����������������������������������������������
     �  8 BACK SPACE  �  0  � 43 + � 62 > �122 z �
     �  9 TABULATE    � 28 � � 44 , � 65 A �126 ~ �
     � 11 HOME        � 29 � � 45 - � 90 Z �      �
     � 12 CLEAR SCREEN� 30 � � 46 . � 91 [ �      �
     � 13 RETURN      � 31 � � 47 / � 92 \ �      �
     � 18 INSERT      � 34 " � 48 0 � 93 ] �      �
     � 24 SELECT      � 39 ' � 57 9 � 94 ^ �      �
     � 27 ESCAPE      � 40 ( � 58 : � 95 _ �      �
     � 32 SPACE       � 41 ) � 60 < � 96 ` �      �
     �127 DELETE      � 42 * � 61 = � 97 a �      �
     ����������������������������������������������
     ���������������������������������
     �  0 ^STOP      � 13 ^M      CR �
     �  1 ^A      <- � 18 ^R     INS �
     �  2 ^B      => � 20 ^T      <= �
     �  4 ^D         � 24 ^X    SLCT �
     �  5 ^E      -> � 25 ^Y rpt cmd �
     �  6 ^F         � 26 ^Z     eof �
     �  8 ^H      BS � 27 ^[     ESC �
     �  9 ^I     TAB � 28 ^\   csr � �
     � 11 ^K del lnh � 30 ^^   csr � �
     ���������������������������������
.pa  O
/   o5                     �#############################################################################
#                                  MATRIZES                                 #
#############################################################################
33

.lh 12
.po 2.3"
transposta ____ At
�sim�trica ____ A=At
nula __________ On
identidade ____ In=A.A-1
inversa _______ A-1=A.det-1A
oposta ________ -A=(-1).A
incompleta ____ A'
adjunta _______ A=(A')t
reduzida ______ Aij
determinante __ detA
cofator _______ cofaij = (-1)i+j.detAij
s�ocomut�veis _ AB=BA


.lh 8
achecomut�vel
diagprincipal
diagsecund�ria
tra�o
linha
coluna
tipo
#l
#c
nota��omat
nota��odet
nota��osis

adi��o
subtra��o
multiplica��o
multiplica��oporcte

�matriz
�num�rica
�linha
�coluna
�quadrada
.po 4
.pa  +   o5                     �#############################################################################
#                                    MENUS                                  #
#############################################################################
15

.po 2.5"
letra��o
numera��o
primeira letra
letra mai�scula da palavra
letra min�scula da palavra
seta
invers�o mai�scula -> min�scula
invers�o min�scula -> mai�scula
janela de uma palavra
barra branca   com fundo preto
barra colorida com fundo colorido
figurinha piscando
menu voador
menu suspenso
barra com caracter 219 (�)


.po 1.2"
.rm 10  �    o5                     
.co 5
���������
�A) DIR �
�B) LOAD�
�C) KILL�
�D) SAVE�
���������

���������
�1) DIR �
�2) LOAD�
�3) KILL�
�4) SAVE�
���������

������
�Dir �
�Load�
�Kill�
�Save�
������
.cb  +  �                      �������
�Dir �
�loAd�
�kIll�
�savE�
������

������
�dIR �
�LOaD�
�KiLL�
�SAVe�
������

���������
�   DIR �
�-> LOAD�
�   KILL�
�   SAVE�
���������
.cb  + �                      �������
�DIR �
�load�
�KILL�
�SAVE�
������

������
�dir �
�LOAD�
�kill�
�save�
������

�����������
�LOAD�LOAD�
�KILL������
�SAVE� �  �
�DIR �  � �
�����������
.cb  + �                      �������
�DIR �
�LOAD�
�KILL�
�SAVE�
������

������
�DIR �
�LOAD�
�KILL�
�SAVE�
������

���������
�   DIR �
�   LOAD�
�   KILL�
�   SAVE�
���������
.cb  + �                      �


voador



����������������
�FILE�EDIT�VIEW�
����������������
�DIR �
�LOAD�
�KILL�
�SAVE�
������


car 219
.po 4
.pa  �) �                      �.co 1
.rm 300
#############################################################################
#                          PROGRAMA��O DE M�SICAS                           #
#############################################################################

                                 DURA��ES

.lh 4
        ������������������������������������������������������������
        �      �  B  �       � SB  �       �  M   �        �  SM   �
        ������������������������������������������������������������
        �      �     �       �     �       �      �        �       �
        �      �     �       �     �       �      �        �       �
        �      �     �       �     �       �      �        �       �
        ������������������������������������������������������������
        � 360  � 240 �  180  � 120 �  90   �  60  �   45   �   30  �
        ������������������������������������������������������������
        �      �     �       �     �       �      �   3    �       �
        � 12t  �  8t �   6t  �  4t �  3t   �  2t  �   -t   �   1t  �
        �      �     �       �     �       �      �   2    �       �
        ������������������������������������������������������������


        ������������������������������������������������������������
        �      �  C  �       � SC  �       �  F   �        �  SF   �
        ������������������������������������������������������������
        �      �     �       �     �       �      �        �       �
        �      �     �       �     �       �      �        �       �
        �      �     �       �     �       �      �        �       �
        ������������������������������������������������������������
        � 22,5 �  15 � 11,25 � 7,5 � 5,625 � 3,75 � 2,8125 � 1,875 �
        ������������������������������������������������������������
        �  3   �  1  �   3   �  1  �   3   �  1   �   3    �   1   �
        �  -t  �  -t �   -t  �  -t �  --t  �  -t  �  --t   �  --t  �
        �  4   �  2  �   8   �  4  �  16   �  8   �  32    �  16   �
        ������������������������������������������������������������
.lh 8


                                   PAUSAS

                    BREVE             SEMIBREVE
                    M�NIMA            SEM�NIMA
                    COLCHEIA          SEMICOLCHEIA
                    FUSA              SEMIFUSA


.lh 4
               ��������                   ��������                   ��������
               ��������                   ��������                   ��������
 CLAVE DE SOL  ��������     CLAVE DE FA   ��������     CLAVE DE DO   ��������
               ��������                   ��������                   ��������
               ��������                   ��������                   ��������
.lh 8


                     COLCHETE
                                          infra-som         : f <    20 Hz
                   HASTE                  ultra-som         : f > 20000 Hz
                                          vsom no ar (20oC) : 340 m/s

              CABE�A


       COMPASSOS SIMPLES                        COMPASSOS COMPOSTOS

.lh 4
       ����������                               ����������
       ����������                               ����������
       ���������� bin�rio                       ����������
       ����������                               ����������
       ����������                               ����������


       ����������                               ����������
       ����������                               ����������
       ���������� tern�rio                      ����������
       ����������                               ����������
       ����������                               ����������


       ����������                               ����������
       ����������                               ����������
       ���������� quatern�rio                   ����������
       ����������                               ����������
       ����������                               ����������
.lh 8


  � S   o5                     �                 #       #               #       #       #
                 ^       ^               ^       ^       ^
                 �       �               �       �  27,5 �  30
    S -3    32������������������������������������������������
             �   �       �               �       �       �   �
    S -2     �   �       �               �       �       �   �
             �   �       �               �       �       �   �
    S -1     �   �       �               �       �       �   �
             �   �       �               �       �       �   �
    S  0    256  �  294  �  320     350  �  384  �  440  �  480
             �  -->     -->     -->     -->     -->     -->  �  -->
    S  1     �  38      26      30      34      56      40   �  32
             �                                               �
    S  2     �                                               �
             �                                               �
.lh 0
    S  3     ������������������������������������������������3840
.lh 8
                 _
           4095,49

                               FREQ��NCIAS TE�RICAS

    ��������������������������������������������������������������������������
    �  DO  �     RE      �   MI   �     FA      � SOL  �    LA     �   SI    �
    ��������������������������������������������������������������������������
    �    2 �    2,296875 �    2,5 �    2,734375 �    3 �    3,4375 �    3,75 �
    �    4 �    4,59375  �    5   �    5,46875  �    6 �    6,875  �    7,5  �
    �    8 �    9,1875   �   10   �   10,9375   �   12 �   13,75   �   15    �
    �   16 �   18,375    �   20   �   21,875    �   24 �   27,5    �   30    �
 -3 �   32 �   36,75     �   40   �   43,75     �   48 �   55      �   60    �
 -2 �   64 �   73,5      �   80   �   87,5      �   96 �  110      �  120    �
 -1 �  128 �  174        �  160   �  175        �  192 �  220      �  240    �
  0 �  256 �  294        �  320   �  350        �  384 �  440      �  480    �
  1 �  512 �  588        �  640   �  700        �  768 �  880      �  960    �
  2 � 1024 � 1176        � 1280   � 1400        � 1536 � 1760      � 1920    �
  3 � 2048 � 2352        � 2560   � 2800        � 3072 � 3520      � 3840    �
    � 4096 � 4704        � 5120   � 5600        � 6144 � 7040      � 7680    �
    ��������������������������������������������������������������������������


                 TOMb = (TOM + TOManterior )/2

                 TOM# = (TOM + TOMposterior)/2


       122 tons e semitons acess�veis pelo LOGO
           (119 em 6 escalas completas + 3 tons (LA   27,5
                                                 SI   30          _
                                                 DO 4096 -> 4095,49)




                                 ESCALAS
.pa  �6   o5                     �.mt 0
.mb 0
.pl 65
.lh 4.8
           ��������������������������������������������
 ��������������� �����       �     �������������������� limite do
           ��������������������������������������������
  �������������� �����       �     �������������������� teclado do piano
           ��������������������������������������������
  �������������� �����       �     �                  �
           ��������������������������������������������
   ������������� �����       �������                  �
           ��������������������������������������������
   ������������� �����       �������                  �� 3520
           ��������������������������������������������
    ������������ �����       �������                  �
           ��������������������������������������������
    ������������ �����       � + 3 �                  �
           ��������������������������������������������
     ����������� �����       �������                  �
           ��������������������������������������������
     ����������� �����       �������                  �
           ��������������������������������������������
      ���������� �����       �������                  �
           ��������������������������������������������
      ���������� �����       �     �                  �
           ��������������������������������������������
       ��������� �����       �     �                  �� 1760
           ��������������������������������������������
       ��������� �����       �     �                  �
           ��������������������������������������������
        �������� �����       � + 2 �                  �
           ��������������������������������������������
        �������� �����       �     �                  �
           ��������������������������������������������
         ������� �����       �     �                  �
           ��������������������������������������������
         ������� �����       �     �                  �
           ��������������������������������������������
          ������ �����       �������                  �
           ��������������������������������������������
          ������ �����       �������                  �� 880
           ��������������������������������������������
           ����� �����       �������                  �
           ��������������������������������������������
           ����� �����       � + 1 �                  �
           ��������������������������������������������
           ����� �����       �������                  �
           ��������������������������������������������
           ����� �����       �������                  �
           ��������������������������������������������
           ����� �����       �������                  �
           ��������������������������������������������
           ����� �����  SI   �     �                  �
           ��������������������������������������������
           ����� �����  LA   �     �                  �� 440
           ��������������������������������������������
           ����� �����  SOL  �     �                  �
           ��������������������������������������������
           ����� �����  FA   �  0  �                  �
           ��������������������������������������������
           ����� �����  MI   �     �                  �
           ��������������������������������������������
           ����� �����  RE   �     �                  �
           ��������������������������������������������
           �����������  DO   �     �                  �
           ��������������������������������������������
           ����� �����       �������                  �
           ��������������������������������������������
           ����� �����       �������                  �� 220
           ��������������������������������������������
           ����� �����       �������                  �
           ��������������������������������������������
           ����� �����       � - 1 �                  �
           ��������������������������������������������
           ����� �����       �������                  �
           ��������������������������������������������
           ����� �����       �������                  �
           ��������������������������������������������
           ����� �����       �������                  �
           ��������������������������������������������
           ����� �����       �     �                  �
           ��������������������������������������������
           ����� �����       �     �                  �� 110
           ��������������������������������������������
           ����� �����       �     �                  �
           ��������������������������������������������
           ����� �����       � - 2 �                  �
           ��������������������������������������������
           ����� ������      �     �                  �
           ��������������������������������������������
           ����� ������      �     �                  �
           ��������������������������������������������
           ����� �������     �     �                  �
           ��������������������������������������������
           ����� �������     �������                  �
           ��������������������������������������������
           ����� ��������    �������                  �� 55
           ��������������������������������������������
           ����� ��������    �������                  �
           ��������������������������������������������
           ����� ���������   � - 3 �                  �
           ��������������������������������������������
           ����� ���������   �������                  �
           ��������������������������������������������
           ����� ����������  �������                  �
           ��������������������������������������������
           ����� ����������  �������                  �                   67
           ��������������������������������������������
           ����� ����������� �     �                  �
           ��������������������������������������������
           ����� ����������� �     �                  �� 27,5
           ��������������������������������������������  / m   o5                     �.mt .50"
.mb  1.33"
.pl 68
.lh  8
.po   .6"
                   Rela��o entre os tons, bem�is e sustenidos

      ������������������������������������������������������������������������
      �     REb       MIb      (FAb)      SOLb      LAb       SIb      (DOb) �
      �                                                                      �
      � DO  DO#   RE  RE#   MI (MI#)  FA  FA#  SOL  SOL#  LA  LA#   SI (SI#) �
      ������������������������������������������������������������������������



                              FREQ��NCIAS EFETIVAS

.lh 4
                                     175  185  196  208  220  223  247

����������������������������������������������������������������������
����������������������������������������������������������������������
����������������������������������������������������������������������
����������������������������������������������������������������������
����������������������������������������������������������������������
                                     ���  ���  ���  ���  ���  ���  ���
                                     ���  ���  ���  ���  ���  ���
                                     ���  ���


       262  277  294  311  330       349  370  392  415  440  466  494

����������������������������������������������������������������������
����������������������������������������������������������������������
����������������������������������������������������������������������
����������������������������������������������������������������������
����������������������������������������������������������������������
       ���  ���


       523  554  587  622  659       698  740  784  831  880  932  988

                                                         ���  ���  ���
����������������������������������������������������������������������
����������������������������������������������������������������������
����������������������������������������������������������������������
����������������������������������������������������������������������
����������������������������������������������������������������������


      1047      1174
                           ���
       ���  ���  ���  ���  ���
       ���  ���  ���  ���  ���
����������������������������������������������������������������������
����������������������������������������������������������������������
����������������������������������������������������������������������
����������������������������������������������������������������������
����������������������������������������������������������������������
.lh 8
.po 4



19
  compile1   passa os dados para listas de canal, freq��ncia, volume e dura��o
  compile2   passa os dados para um programa de comandos toque
  interprete executa os dados
  interrupt  separa a liga��o do som das notas
  liga       liga o som das notas
  mix1       toca a m�sica de tr�s para frente
  mix2       toca a m�sica correspondente � da partitura de cabe�a para baixo
  mix3       toca as notas na seq��ncia {1 2 3} {2 3 4} ...
  m�sicas    envia os nomes das m�sicas programadas
  parapauta  desenha a pauta a partir dos dados
  pausa      produz uma pausa com a dura��o desejada
  play       produz um tom/semitom no canal, volume, oitava e dura��o desejados
  protom     entrada de dados
  propau     entrada de dados a partir da cria��o da pauta
  sintonize  advert�ncia para sintonizar o volume da m�quina
  troqueoit  sobe/desce uma oitava
  troquetom  sobe desce um  tom
  troquecla  troca de clave
  �m�sica    envia verd se a entrada for o nome de uma m�sica j� programada

  | N   �<                     �
   [                                                     �
    [                                        �           �
     [                              �        �           �
      [[T][D][O][V]]  ] 1� compasso �        �           �
      [[ ][ ][ ][ ]]  ] 2� compasso �        �           �
      .                             � 1� voz �           �
      .                             �        �           �
      .                             �        �           �
      [[ ][ ][ ][ ]]  ] n� compasso �        �           �
     ]                              �        �           �
     [                              �        �           �
      [[ ][ ][ ][ ]]                �        �           �
      [[ ][ ][ ][ ]]                �        �           �
      .                             � 2� voz � 1� m�sica �
      .                             �        �           �
      .                             �        �           �
      [[ ][ ][ ][ ]]                �        �           �
     ]                              �        �           � m�sicas
     [                              �        �           �
      [[ ][ ][ ][ ]]                �        �           �
      [[ ][ ][ ][ ]]                �        �           �
      .                             � 3� voz �           �
      .                             �        �           �
      .                             �        �           �
      [[ ][ ][ ][ ]]                �        �           �
     ]                              �        �           �
    ]                                        �           �
    [                                        �           �
                                             � 2� m�sica �
    ]                                        �           �
    .                                                    �
    .                                                    �
    .                                                    �
    [                                        �           �
                                             � m� m�sica �
    ]                                        �           �
   ]                                                     �

   T tons do compasso
   D dura��es dos tons, respectivamente
   O oitavas  dos tons, respectivamente
   V volumes  dos tons, respectivamente
.pa  $,   t5                     �#############################################################################
#                                  PANEL�O                                  #
#############################################################################
21

.po 3.5"
 ,
 .
 sometempo
 idade
 timer
 compress
 ren
 rem
 swap
 desenterre
 enterre
 destruabur
 buracos
 calper
 diadasem
 cappau
 for
 msgmvt
 �bissexto
 grave
 newv
.po 4
.pa     t5                     �#############################################################################
#                                   PMTG                                    #
#############################################################################
19

.po 3.5"
 ,
 .
 .img2
 print (?)
 arco
 cir.
 cir..
 clng
 lnhg
 escg
 mudecg
 cor
 cpfig
 criafig
 most
 pde
 pee
 reverso
 usa
.po 4
.pa  �   t5                     �#############################################################################
#                            FIGSTAR II                                     #
#############################################################################
34

.po 3"
 rolar
 rolardel
 rotar
 espelhar
 trocar
 copiar
 deletar (pos/neg)

 and
 or
 not
 xor
 .giga   .kilo
 .�giga  .�kilo
 .img1   (listafig 8)
 .img2   (gradeb   8)
 .img3   (spt     16)
 gradel
 gradeb/o/d/h
 spr1    (pf a)
 spr2    (pp pos pf a)
 spr3    (usa lst de btes bin)
 spr4    (direto)
 programar
 cores
 sprites
 editar
 trocartat
 carimbar
 reduzir
 ampliar
 copiarfig

.po 2"
�������
�     �                  rolar
�     �                  trocar
�     �  
 	�  
                espelhar
�������
.rm 10  $,   t5                     
.co 4
�������
�  �  �
�  �  �
�  �  �
�������
�������
�  �  �
�������
�  �  �
�������
.cb  �6  $,                     ��������
�  �  �
�  �  �
�  �  �
�������
�������
�  �  �
�������
�  �  �
�������
.cb  �6 $,                     ��������
�     �
�������
�     �
�������
�������
�  �  �
�������
�  �  �
�������
.cb  �6 $,                     ��������
�     �
�������
�     �
�������
�������
�  �  �
�������
�  �  �
�������
.po 4
.pa  �6 $,                     �.co 1
.rm 300
#############################################################################
#                 PRIMITIVAS PARA DESENHOS TRIDIMENSIONAIS                  #
#############################################################################

.po 3"
eram ______ 80 (ti+fi+tr)
tira ______ 16
ficam _____ 30
trocam ____ 34
inclui ____ 14
ctrl ______ 11
total pmt _ 78 (fi+tr+in)
PROJETRI __ 48 (   tr+in)



.po  0.7"
.rm 10  d'   t5                     
.co 5
TIRA

.cw 10
al
carimbetudo
coorx
coory
copiafig
criafigl
edfig
fig
inverta
listafig
mudefig
mudex
mudey
pca
tat
tl
.cb  �"  d'                     �.cw 12
FICAM

.cw 10
ad
arquivos
at
atat
cada
carimbe
carreguedes
cb
cf
cl
congele
cortat
descongele
dt
eliminearq
gravedes
carregue
mudecf
mudecl
mudecor
mudect
pinte
quem
s�tat
todas
ub
ui
ul
un
�viz�vel
.cb  d	0 d'                     �.cw 12
INCLUI

.cw 10
cpb
cpc
escondida
grave3d
logo3d
mudevelz
newv
poke
rpd
rpe
usa
vartri
velz
�vartri
.cb  d  d'                     �.cw 12
CTRL

.cw 10
ande
cabeceie
role
seg
sqr
troque
vire
datapoke
&enterre
&desenterre
&destruabur
.cb  4 d'                     �.cw 12
TROCAM

.cw 10
dire��opara ___
dist�ncia _____ dist�ncia3d
d� ____________ d�3d
el ____________ el3d
eln ___________ eln3d
elns __________ elns3d
elps __________ elps3d
eltudo ________ eltudo3d
em.colis�o ____ em.colis�o3d
gravetudo _____ gravetudo3d
mons __________ mons3d
mops __________ mops3d
mots __________ mots3d
muded� ________ muded�3d
mudepos _______ mudepos3d
mudepropor��o _
mudevel _______
mudevelx ______
mudevely ______
pc ____________ pc3d
pd ____________ vpd
pe ____________ vpe
pf ____________ apf
pos ___________ pos3d
pp ____________ pp3d
primitivas ____ primitivas3d
propor��o _____
pt ____________ apt
rg ____________ rg3d
vel ___________
velx __________
vely __________
vers�o ________ vers�o3d
�primitiva ____ �primitiva3d

25 prontas
 9 faltam
.cw 12
.po 4
.pa  �7 d'                     �.co 1
.rm 300
#############################################################################
#                                  TRIGONO                                  #
#############################################################################
28

.po 2"
.rm 8
�ngulo
nota��o
redu��o
argumento
  -
   t5                     
.co 4
sen
cos
tan
ctg
sec
csc
.cb  t&  ,                     �arcsen
arccos
arctan
arcctg
arcsec
arccsc
.cb  t& ,                     �senh
cosh
tanh
ctgh
sech
csch
.cb  t& ,                     �arcsenh
arccosh
arctanh
arcctgh
arcsech
arccsch
.po 4
.pa  t& ,                     �.co 1
.rm 300
#############################################################################
#                              2D - 3D  - PEGES                             #
#############################################################################
23

.po 2.5"
arco
elipse1   (eq)
elipse2   (mudepropor��o)
circunf1  (eq)
circunf2  (anda o raio e pp pos)
circunf3  (pf pd)
circunf4  (eq otimizada)
pol�gono
ret�ngulo

esfera1     (circunfer�ncias)
esfera2     (espiral)
elips�ide1  (elipses)
elips�ide2  (espiral)
prisma
poliedro
pir�mide
paralelep�pedo

gold
star
pol�gono :circunf :n :l
espiral  (de Arquimedes)
circulouco
pol�gonoradial
.po 4
.pa  T   t5                     �.co 1
.rm 300
#############################################################################
#              CONVERSOR DE TELAS GR�FICAS : Graphos III - LOGO             #
#############################################################################

program GRAPHOSIII_LOGO;

 {

   Programa em linguagem PASCAL v3.0 para ser rodado em MSX.

   Desenvolvido por Pedro Schuh.

 }

 var Arq         : file;
     Buf         : array  [1..12416] of byte absolute $9000;
     Nome1,Nome2 : string [15];
     Exec        : boolean;
     c           : char;

 procedure Cls;
  var i : integer;
  begin
   gotoxy(1,12) ; for i := 1 to 13 do insline;
   gotoxy(1, 1) ; for i := 1 to 11 do delline;
  end;

 begin

  repeat

   Cls;
   Exec   := false;
   Nome1  := '';
   Nome2  := '';

   gotoxy  ( 1,8) ; writeln ('Conversor : Graphos III  -    LOGO    ');
                    write   ('          :             : :          :');

   buflen  := 11;
   gotoxy  (13,9) ; read    (Nome1);
   if Nome1 = '' then Halt;

   buflen  :=  8;
   gotoxy  (29,9) ; read    (Nome2);
   if Nome2 = '' then begin
                       Nome2 := Nome1;
                       writeln (Nome2)
                      end
                 else writeln;

   writeln;
   writeln ('Carregando ',Nome1);

 {$I-}
   assign (Arq,Nome1);
   reset  (Arq      );
   if ioresult <> 0 then begin   :   t5                     �                          writeln;
                          writeln('Arquivo ',Nome1,' nao existe');
                          halt
                         end;
 {$I+}
   blockread  (Arq,Buf,97);
   close      (Arq       );
   move       (Buf[121],Buf,12288);
   writeln    ('Gravando ',Nome2);
   assign     (Arq,Nome2 );
   rewrite    (Arq       );
   blockwrite (Arq,Buf,96);
   close      (Arq       );

   writeln;
   write      ('Outra vez (S/N) ? ');
   read       (kbd,c);
   c := upcase(c);
   if c = 'S' then Exec := true

  until not Exec

 end.

.rm 77
#############################################################################

 	���
 Researched & Developed by  E� Ĵ � e  Baga!Soft � � e E� � 
#############################################################################
.pa  $   t5                     �.op

 	�
  E� Ĵ � e  PASSAR CANETA � � e E� � 


.po 2.5"
33 tcl HB8000 - bs
              - tcl csr

33 tcl EXPERT - return
              - tcl csr (setas abertas)

34 tcl ctrl   - cima baixo esquerda direita
              - bs
              - return

   minim�x 39 - 0 da reta
           40 - d� da tat

   bases   49 - N/2
              - bte/B^(n mod N)
           50 - 5 caixas
              - scroll
              - eq

52 fun��es    - pi,gama (constantes)

   m�quina 59 - .beep 40 micro s
           60 - direita MAQUI
           62 - micro tabela ascii 0 28 29 30 31
                                     28    30

63 matrizes   - inversa A.det
              - adjunta A=

64 menus      - janela de 1 pal (cima baixo)
              - barra branca
              - barra colorida - menu colorido
              - m�ozinha
              - barra do s�spen

   m�sicas 65 - pausas
              - tempos
              - claves
              - nota
              - tipos de compassos
           67 - escalas
           68 - freq��ncias efetivas

72 figstar II - 9 spr
.po 4
.pa  d	0   t5                     �
 	

�
  E� Ĵ � e  ALTERA��ES � � e E� � 


.po .8"
ANTES DE IMPRIMIR : - PASSAR O N�MERO DA P�GINA PARA O CANTO SUP. DIR.
                    - FAZER A LEITURA AUTOM�TICA DO # DA P�GINA
                    - CONFERIR A DATA E O N�MERO DA EDI��O
                    - CONFERIR NO "INDEX" E NO "PASSAR CANETA" OS #P�G
                    - TIRAR DO TEXTO (b^M^J) E (^@)
                    - ATUALIZAR O TAMANHO DO TEXTO
                    - POSICIONAR A BORDA DIREITA DO PAPEL JUNTO AO �LTIMO
                    - LIGAR NA IMPRESSORA O MODO UNIDIRECIONAL PARA TEXTO
                    - CONTROLAR O TEMPO DE IMPRESS�O