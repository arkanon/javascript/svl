<?php

  include('common.inc.php');

  $created = "11/07/2009 S�b 21:50:50";
  $counted = "11/07/2009 S�b 21:50:52";

# header("Content-Type: text/css");

?>

   html, body, #contents
   {
     min-height: 100%;
     width: 100%;
     height: 100%;
   }

   /* <http://scott.sauyet.com/CSS/Demo/FooterDemo1.html>               */
   /* The 'height' above is a hack for IE5+/Win.                        */
   /* Below we adjust it using the child selector to hide from IE5+/Win */
   /* Without this, Moz1.0 adds a vertical scrollbar                    */

   html>body, html>body #contents
   {
     height: auto;
   }

   body
   {
     font-family: sans-serif;
     font-size: 10pt;
     background: white;
     margin: 0;
   }

   #contents
   {
     position: absolute;
     top: 0;
     left: 0;
   }

   a
   {
     text-decoration: none;
     color: blue;
   }

   a:hover
   {
     text-decoration: underline;
   }

   img
   {
     border: 0;
   }

   table
   {
     margin: auto;
   }

   th
   {
     text-align: left;
   }

   td
   {
     padding: 0 10px;
     vertical-align: middle;
   }

   #what
   {
     display: block;
     font-size: 200%;
     font-weight: bold;
     margin: 15px 0 0 0;
     text-align: center;
     border: 0px solid red;
   }

   #where
   {
     display: block;
     font-size: 150%;
     font-weight: normal;
     margin: 0 0 15px 0;
     text-align: center;
     border: 0px solid blue;
   }

   #main
   {
     margin-bottom: 3em;
     height: auto;
   }

   #top
   {
     position: absolute;
     left: 0;
     bottom: 0;
     font-size: 150%;
     font-weight: normal;
     font-style: italic;
     margin: 15px 0 0 0;
     padding: 3px 9px;
     color: white;
   }

   #top a
   {
     color: white;
   }

   #footer
   {
     position: absolute;
     right: 0;
     bottom: 0;
     width: 100%; /* see hack below */
     background: black;
     font-size: 150%;
     font-weight: normal;
     font-style: italic;
     margin: 15px 0 0 0;
     padding: 3px 9px;
     text-align: right;
     border: 0px solid green;
   }

   /* SBMH -- see http://css-discuss.incutio.com/?page=BoxModelHack */
   #footer
   {
     \width: 100%; /* stupid hack lets IE see this */
     w\idth: 100%; /* others see this */
   }

   #footer a
   {
     color: white;
   }

