<?php

  $id      = ($_SERVER['argv'][0]) ? $_SERVER['argv'][0] : 'all';
  $scale   = 4;
  $border  = $scale;
  $opacity = 1;

  $path = "/auto/vu_sb1/home/informatica/arkanon/public_html/svl";
  set_include_path(get_include_path() . PATH_SEPARATOR . $path);

  include('common.inc.php');

  $created = "14/07/2009 Ter 15:43:59";
  $counted = "14/07/2009 Ter 15:44:01";

  $transform_prop = ' transform="translate('.$border.','.$border.') scale('.$scale.')"';
  $opacity_prop   = ' opacity="'.$opacity.'"';
  $xlink_prop     = ' xlink:href="http://svl.lsd.org.br/image/button/buttons-obj.svg#'.$id.'"';

  header( "Content-Type: image/svg+xml" );

  $encoding = "ISO-8859-1";
  echo '<?xml version="1.0" encoding="'.$encoding.'" standalone="no"?>';

?>


<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

<svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink">

<use<?php print $transform_prop.$opacity_prop.$xlink_prop ?> />

</svg>
