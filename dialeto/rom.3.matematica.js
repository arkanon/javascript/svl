/*

  $modifed = 2014/02/26 (Wed) 02:50:17 (BRS)
  $created = 2009/06/15 (Seg) 21:14:16
  $counted = 2009/07/04 (Sáb) 14:56:39

*/



   svl.arctan = function(ARG_segmento_a,ARG_segmento_b=1)
   // arco cuja tangente é ARG_segmento_a / ARG_segmento_b
   // esc(arctan( 1, 1)); //  45
   // esc(arctan( 1,-1)); // 135
   // esc(arctan(-1,-1)); // 225
   // esc(arctan(-1, 1)); // 315
   {
     if ( ARG_segmento_a == 0 && ARG_segmento_b == 0 ) return(90);
     var TMP_ângulo = Math.atan2(ARG_segmento_a,ARG_segmento_b) * (180/$pi);
     return( svl.resto( 360+TMP_ângulo, 360 ) );
   }

   svl.arcsen =function(ARG_segmento_a,ARG_segmento_b=1)
   {
     return svl.arctan( svl.quociente( ARG_segmento_a, svl.rq(1-ARG_segmento_a*ARG_segmento_a) ) , ARG_segmento_b );
   }



   svl.sen = function(ARG_ângulo_número)
   // seno de ARG_ângulo_número
   {
     return( Math.sin( ARG_ângulo_número*($pi/180) ) );
   }

   svl.cos = function(ARG_ângulo_número)
   // co-seno de ARG_ângulo_número
   {
     return( Math.cos( ARG_ângulo_número*($pi/180) ) );
   }

   svl.tg = function(ARG_ângulo_número)
   // tangente de ARG_ângulo_número
   {
     return( Math.tan( ARG_ângulo_número*($pi/180) ) );
   }



   svl.eleve = function(b, e)
   {
     if ( e == 0 ) return 1;
     if ( e <  0 ) return 1/(b*eleve(b,-e-1)); else return b*eleve(b,e-1);
   }

   svl.pot = function(ARG_base_número, ARG_expoente_número)
   // potência de ARG_base_número elevada a ARG_expoente_número
   {
     return( Math.pow( ARG_base_número, ARG_expoente_número ) );
   }

   svl.raiz = function(ARG_índice_número, ARG_número)
   // raiz ARG_índice_número de ARG_número
   {
     return( Math.pow( ARG_número, 1 / ARG_índice_número ) );
   }

   svl.log = function(ARG_base_número, ARG_número)
   // logaritmo de ARG_número na ARG_base_número
   {
     return( Math.log(ARG_número) / Math.log(ARG_base_número) );
   }



   svl.resto = function(ARG_dividendo, ARG_divisor)
   // resto de ARG_dividendo dividido por ARG_divisor
   {
     return( ARG_dividendo % ARG_divisor );
   }



   svl.rq = function(ARG_número)
   // raiz quadrada de ARG_número
   {
     return( Math.sqrt( ARG_número ) );
   }



   svl.int = function(ARG_número)
   // inteiro de ARG_número
   {
     return( parseInt(ARG_número) );
   }



   svl.arredonde = function(ARG_número)
   // ARG_número arredondado para o inteiro mais próximo
   //
   // IMPASSE: arredonde(-5.5) retorna -5 em JS e -6 no HL
   //          nos outros casos retornam valores iguais
   //          deixar assim ou implementar como no HL?
   {
     return( Math.round(ARG_número) );
   }



   svl.soma = function()
   // somatório dos argumentos
   {
     var TMP_soma = 0;
     for(var TMP_i=0; TMP_i<arguments.length; TMP_i++) TMP_soma += arguments[TMP_i];
     return( TMP_soma );
   }



   svl.produto = function()
   // produtório dos argumentos
   {
     var TMP_produto = 1;
     for(var TMP_i=0; TMP_i<arguments.length; TMP_i++) TMP_produto *= arguments[TMP_i];
     return( TMP_produto );
   }



   svl.diferença = function(ARG_número1, ARG_número2)
   // diferença entre ARG_número1 e ARG_número2
   {
     return( ARG_número1 - ARG_número2 );
   }



   svl.quociente = function(ARG_dividendo, ARG_divisor)
   // quociente entre ARG_dividendo e ARG_divisor
   {
     return( ARG_dividendo / ARG_divisor );
   }



   svl.troquesinal = function(ARG_número)
   // troque o sinal de ARG_número
   {
     return( - ARG_número );
   }



   svl.sorteieaté = function(ARG_número_natural)
   // sorteie um número natural até ARG_número_natural
   {
     return( svl.arredonde(ARG_número_natural*Math.random()) );
   }



   $pi = Math.PI;
   $e  = Math.E;
   $fi = (svl.rq(5)+1)/2;

   $grego =
   {

     'alfa'    : [ 'α' , 'Α' , 'a'  ] ,
     'beta'    : [ 'β' , 'Β' , 'b'  ] ,
     'gama'    : [ 'γ' , 'Γ' , 'g'  ] ,
     'delta'   : [ 'δ' , 'Δ' , 'd'  ] ,
     'épsilon' : [ 'ε' , 'Ε' , 'e'  ] ,
     'zeta'    : [ 'ζ' , 'Ζ' , 'z'  ] ,
     'eta'     : [ 'η' , 'Η' , 'e'  ] ,
     'teta'    : [ 'θ' , 'Θ' , 'th' ] ,
     'iota'    : [ 'ι' , 'Ι' , 'i'  ] ,
     'capa'    : [ 'κ' , 'Κ' , 'k'  ] ,
     'lambda'  : [ 'λ' , 'Λ' , 'l'  ] ,
     'mi'      : [ 'μ' , 'Μ' , 'm'  ] ,
     'ni'      : [ 'ν' , 'Ν' , 'n'  ] ,
     'csi'     : [ 'ξ' , 'Ξ' , 'x'  ] ,
     'ômicron' : [ 'ο' , 'Ο' , 'o'  ] ,
     'pi'      : [ 'π' , 'Π' , 'p'  ] ,
     'rô'      : [ 'ρ' , 'Ρ' , 'r'  ] ,
     'sigma'   : [ 'σ' , 'Σ' , 's'  ] ,
     'tau'     : [ 'τ' , 'Τ' , 't'  ] ,
     'upsilon' : [ 'υ' , 'Υ' , 'u'  ] ,
     'fi'      : [ 'φ' , 'Φ' , 'f'  ] ,
     'qui'     : [ 'χ' , 'Χ' , 'qu' ] ,
     'psi'     : [ 'ψ' , 'Ψ' , 'ps' ] ,
     'ômega'   : [ 'ω' , 'Ω' , 'ô ' ] ,

     'digama'  : [ 'ϝ' , 'Ϝ' , ''   ] ,
     'ηeta'    : [ 'ͱ' , 'Ͱ' , ''   ] ,
     'san'     : [ 'ϻ' , 'Ϻ' , ''   ] ,
     'sho'     : [ 'ϸ' , 'Ϸ' , ''   ] ,

     'stigma'  : [ 'ϛ' , 'Ϛ' , ''   ] ,
     'qoppa'   : [ 'ϙ' , 'Ϙ' , ''   ] ,
     'sampi'   : [ 'ͳ' , 'Ͳ' , ''   ] ,

   }



// EOF
