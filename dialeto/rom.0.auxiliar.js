/*

  $modifed = 2014/02/26 (Wed) 03:04:26 (BRS)
  $created = 2009/06/15 (Seg) 21:14:16
  $counted = 2009/07/04 (Sáb) 14:56:39

*/



   var svl = {};
   var svg = {};
   var def = {};
   var aux = {};

// for (var item in window) { console.log(item) }

   aux.emerja = function(namespace)
   // copia todas as primitivas do namespace para o primeiro nivel
   {
     window[namespace].primitivas().forEach( function(primitiva) { eval( primitiva + '=' + window[namespace][primitiva] ) } )
   }

   aux.submerja = function(namespace)
   // remove todas as primitivas do namespace copiadas para o primeiro nivel
   {
     window[namespace].primitivas().forEach( function(primitiva) { delete window[primitiva] } )
   }

   aux.ltrim     = function(ARG_palavra) { return ( ARG_palavra.replace( /^\s+/ , ''    ) ) } // left     trim
   aux.rtrim     = function(ARG_palavra) { return ( ARG_palavra.replace( /\s+$/ , ''    ) ) } // right    trim
   aux.itrim     = function(ARG_palavra) { return ( ARG_palavra.replace( /\s+/g , ' '   ) ) } // internal trim
   aux.trim      = function(ARG_palavra) { return ( aux.ltrim( aux.rtrim( ARG_palavra ) ) ) } //          trim
   aux.gtrim     = function(ARG_palavra) { return ( aux.trim ( aux.itrim( ARG_palavra ) ) ) } // global   trim


// aux.logo = function()
// // logotipo svl
// {
//   svg.logo.setAttribute( "width"     , "1000" );
//   svg.logo.setAttribute( "height"    , "1000" );
//   svg.logo.setAttribute( "transform" , "translate("+(svl.min_coorx()+aux.logo_margin)+","
//                                                    +(svl.max_coory()-aux.logo_margin)+") scale("+aux.logo_scale+") scale(1,-1)" );
//   svg.logo.setAttribute( "opacity"   , aux.logo_opacity );
//   svg.logo.setAttribute( "style"     , "visibility:"+aux.logo_visibility );
//   svg.logo.setAttribute( "href"      , aux.logo_source );
// }



// aux.validate = function()
// // botão para validação do código svg
// {
//   svg.validate.setAttribute( "width"     , "1000" );
//   svg.validate.setAttribute( "height"    , "1000" );
//   svg.validate.setAttribute( "transform" , "scale("+aux.validate_scale
//                                                    +") translate("+((svl.max_coorx()-aux.validate_margin)/aux.validate_scale-80)+","
//                                                                   +((svl.min_coory()+aux.validate_margin)/aux.validate_scale+15)+") scale(1,-1)" );
//   svg.validate.setAttribute( "opacity"   , aux.validate_opacity );
//   svg.validate.setAttribute( "style"     , "visibility:"+aux.validate_visibility );
//   svg.validate.setAttribute( null , "href"      , aux.validate_source );
// }



   aux.dump = function(ARG_data)
   // <http://www.openjs.com/scripts/others/dump_function_php_print_r.php>
   // ARG_data  array, hash (array simbólico) ou object
   // SAÍDA     representação hierárquica de ARG_data
   {
     var TMP_nível = (arguments[1]) ? arguments[1] : 0;
     var TMP_pad   = "";
     for(var TMP_j=0; TMP_j<TMP_nível+1; TMP_j++) TMP_pad += '  ';
     var TMP_out = "{\n" + TMP_pad;
     if ( svl.élista(ARG_data) )
     {
       for (var TMP_item in ARG_data)
       {
         var TMP_aspas = svl.épalavra(ARG_data[TMP_item]) ? '"' : '';
             TMP_out  += "'" + TMP_item + "' =>" + ( svl.élista(ARG_data[TMP_item])
                                                     ? "\n" + TMP_pad + aux.dump(ARG_data[TMP_item],TMP_nível+1)
                                                     : " "  + TMP_aspas + ARG_data[TMP_item] + TMP_aspas + "\n" + TMP_pad );
       }
       TMP_out = TMP_out.substr(0,TMP_out.length-2);
     }
     else
     {
       var TMP_aspas = svl.épalavra(ARG_data) ? '"' : '';
           TMP_out   = TMP_aspas + ARG_data + TMP_aspas + " (" + typeof(ARG_data) + ")";
     }
     if (svl.élista(ARG_data)) TMP_out += "}\n" + TMP_pad.substr(0,TMP_pad.length-2);
     console.log( "      " + TMP_out );
   }



   aux.compare = function(ARG_lista_comandos_resultados)
   // ARG_lista_comandos_resultados =
   //    [
   // //   [ ' COMANDO____ ' , 'SAÍDA PREVISTA' ],
   //      [ ' mo(todas()) ' , '[ 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 ]' ],
   //      [ ' mo(quem ()) ' , '[ 1 2 5 10 ]' ],
   //    ];
   {
     esc("Foram executados e apresentados " + ARG_lista_comandos_resultados.length + " comandos com os respectivos resultados esperados e obtidos:");
     for ( var TMP_posição in ARG_lista_comandos_resultados )
     {
       var TMP_comando    = aux.gtrim(ARG_lista_comandos_resultados[TMP_posição][0]);
       var TMP_esperado   = aux.trim (ARG_lista_comandos_resultados[TMP_posição][1]);
       var TMP_boolean_mo = svl.nel(ARG_lista_comandos_resultados[TMP_posição]) == 2 ? svl.verd() : ARG_lista_comandos_resultados[TMP_posição][2];
           TMP_esperado   = TMP_esperado.replace(/§/g,'');
           TMP_posição    = 1+svl.int(TMP_posição);
       if (TMP_posição<10) TMP_posição = svl.pal(0,TMP_posição);
       TMP_comando_pal    = TMP_boolean_mo ? "mo("+TMP_comando+")" : TMP_comando;
       TMP_texto          = TMP_posição + ". " + TMP_comando_pal + "\n";
       if (svl.não(svl.évazia(TMP_esperado))) TMP_texto += "    # " + TMP_esperado    + "\n";
       console.log(TMP_texto);
       if ( svl.évazia(TMP_esperado) || svl.não(TMP_boolean_mo) ) eval(TMP_comando); else mo( eval(TMP_comando), "      " );
     }
   }



   aux.paracima = function( ARG_nodo_svg )
   // nodo svg para cima de todos os outros nodos
   {
     ARG_nodo_svg.parentNode.appendChild( ARG_nodo_svg );
   }



   aux.parabaixo = function( ARG_nodo_svg )
   // nodo svg para baixo de todos os outros nodos
   {
     ARG_nodo_svg.parentNode.insertBefore( ARG_nodo_svg, ARG_nodo_svg.parentNode.firstChild );
   }



   aux.posicione = function()
   // posicione tartaruga
   {
     for(var TMP_i in svl.v_quem)
     {
       var TMP_ind       = svl.v_quem[TMP_i];

       var TMP_dç_cx     = svl.elemento( 1, svl.elemento( 1, svl.v_dç[TMP_ind] ) );
       var TMP_dç_cy     = svl.elemento( 2, svl.elemento( 1, svl.v_dç[TMP_ind] ) );
       var TMP_dç        = TMP_dç_cx <= 90 ? TMP_dç_cy : (360-TMP_dç_cy);

       var TMP_svg_x     = svl.svg_x      [TMP_ind];
       var TMP_svg_y     = svl.svg_y      [TMP_ind];
       var TMP_tt        = svl.v_tt       [TMP_ind];
       var TMP_proporção = svl.v_proporção[TMP_ind];
       var TMP_tat       = svg.tat        [TMP_ind];
           TMP_tat.setAttribute
           (
             "transform",
             "translate("+TMP_svg_x+","+TMP_svg_y+") rotate("+(-TMP_dç)+") scale("+TMP_tt+","+(TMP_tt*TMP_proporção)+")"
           );
     }
   }



   aux.cor = function(ARG_cor_número, ARG_elemento)
   {
     if (énúmero(ARG_cor_número))
     {
       TMP_cor = svl.resto( ARG_cor_número, svl.nel(svl.paleta()) );
       if (temvalor(ARG_elemento)) ARG_elemento.setAttribute( "fill", svl.elemento( TMP_cor+1, svl.paleta() ) );
     }
     else
     {
       TMP_cor = ARG_cor_número;
       if (temvalor(ARG_elemento)) ARG_elemento.setAttribute( "fill", TMP_cor );
     }
     return TMP_cor;
   }



   aux.cosdç = function(ARG_dimensão, ARG_eixo)
   // cosseno do ângulo entre o eixo ARG_eixo e o vetor suporte da dimensão ARG_dimensão
   {
     return( svl.cos( svl.elemento( { 'x':1, 'y':2, 'z':3 }[ARG_eixo], svl.elemento( { 'c':1, 'l':2, 'a':3 }[ARG_dimensão], svl.dç() ) ) ) );
   }



   aux.dçtan = function(ARG_cos_eixo_dimensão)
   // direção cuja tangente é dada em função do cosseno ARG_cos_eixo_dimensão
   {
     var TMP_cos = ARG_cos_eixo_dimensão;
     var TMP_sen = svl.rq( Math.abs( 1 - TMP_cos*TMP_cos ) ); // uso da função abs porque, devido aos contínuos arredondamentos,
                                                          // o cos^2 pode acabar maior que 1 e, nesse caso, rq devolve NaN...
     return( svl.arctan( TMP_sen, TMP_cos ) );
   }



   aux.log = function(ARG_texto)
   {
     window.parent.document.getElementById('saida').innerHTML += ARG_texto + '<br>';
   }



   aux.$ = function(ARG_palavra_id)
   {
     return document.getElementById(ARG_palavra_id);
   }



   aux.modç = function()
   //
   {

     var TMP__cx =    ( svl.elemento( 1, svl.elemento( 1, svl.dç() ) ) ).toFixed(svl.digse)*1;
     var TMP__cy =    ( svl.elemento( 2, svl.elemento( 1, svl.dç() ) ) ).toFixed(svl.digse)*1;
     var TMP__cz =    ( svl.elemento( 3, svl.elemento( 1, svl.dç() ) ) ).toFixed(svl.digse)*1;

     var TMP__lx =    ( svl.elemento( 1, svl.elemento( 2, svl.dç() ) ) ).toFixed(svl.digse)*1;
     var TMP__ly =    ( svl.elemento( 2, svl.elemento( 2, svl.dç() ) ) ).toFixed(svl.digse)*1;
     var TMP__lz =    ( svl.elemento( 3, svl.elemento( 2, svl.dç() ) ) ).toFixed(svl.digse)*1;

     var TMP__ax =    ( svl.elemento( 1, svl.elemento( 3, svl.dç() ) ) ).toFixed(svl.digse)*1;
     var TMP__ay =    ( svl.elemento( 2, svl.elemento( 3, svl.dç() ) ) ).toFixed(svl.digse)*1;
     var TMP__az =    ( svl.elemento( 3, svl.elemento( 3, svl.dç() ) ) ).toFixed(svl.digse)*1;

     var TMP_ccx = cos( svl.elemento( 1, svl.elemento( 1, svl.dç() ) ) ).toFixed(svl.digse)*1;
     var TMP_ccy = cos( svl.elemento( 2, svl.elemento( 1, svl.dç() ) ) ).toFixed(svl.digse)*1;
     var TMP_ccz = cos( svl.elemento( 3, svl.elemento( 1, svl.dç() ) ) ).toFixed(svl.digse)*1;

     var TMP_clx = cos( svl.elemento( 1, svl.elemento( 2, svl.dç() ) ) ).toFixed(svl.digse)*1;
     var TMP_cly = cos( svl.elemento( 2, svl.elemento( 2, svl.dç() ) ) ).toFixed(svl.digse)*1;
     var TMP_clz = cos( svl.elemento( 3, svl.elemento( 2, svl.dç() ) ) ).toFixed(svl.digse)*1;

     var TMP_cax = cos( svl.elemento( 1, svl.elemento( 3, svl.dç() ) ) ).toFixed(svl.digse)*1;
     var TMP_cay = cos( svl.elemento( 2, svl.elemento( 3, svl.dç() ) ) ).toFixed(svl.digse)*1;
     var TMP_caz = cos( svl.elemento( 3, svl.elemento( 3, svl.dç() ) ) ).toFixed(svl.digse)*1;

     var TMP_texto = '     //   [ '+TMP_ccx+' '+TMP_ccy+' '+TMP_ccz+' ] [ '+TMP_clx+' '+TMP_cly+' '+TMP_clz+' ] [ '+TMP_cax+' '+TMP_cay+' '+TMP_caz+' ]\n'
                   + '     //   [ '+TMP__cx+' '+TMP__cy+' '+TMP__cz+' ] [ '+TMP__lx+' '+TMP__ly+' '+TMP__lz+' ] [ '+TMP__ax+' '+TMP__ay+' '+TMP__az+' ]';
     esc(TMP_texto);
   }



   aux.caminho = function(ARG_início)
   {
     return( (ARG_início==null) ? Array() : svl.sn( aux.caminho(ARG_início.caller), ARG_início.name ) );
   }



   aux.execute = function(ARG_função_comandos, ARG_useretardo)
   {
     ARG_função_comandos();
   }



/*

   aux.execute = function(ARG_função_comandos, ARG_useretardo)
   {

     var full  = ARG_função_comandos.toString();
     var body  = full.slice( 1+full.indexOf('{'), full.lastIndexOf('}') );
     var lines = body.split(';');
         lines.pop();

//   esc(body);
//   esc(lines);

     var line = 0;
     var loop = setInterval
     (
       function()
       {
         var F = new Function (lines[line]);
         F();
         line++;
         if ( line >= lines.length ) clearInterval(loop);
       }
       , 1000
     );

   }

*/



/*

   aux.execute = function(ARG_função_comandos, ARG_useretardo)
   {
     if (ARG_useretardo==na())
     {
       ARG_função_comandos();
     }
     else
     {
       if (ARG_useretardo==null) ARG_useretardo = def.useretardo;
       svl.timeout += ARG_useretardo*retardo();
       setTimeout( ARG_função_comandos, svl.timeout );
     }
   }



   aux.execute = function(ARG_algoritmo)
   {
     var TMP_obj           = new Object();
         TMP_obj.algoritmo = ARG_algoritmo();
     var TMP_caminho       = aux.caminho(arguments.callee.caller);
     esc(svl.mo(TMP_caminho));
     if (svl.éprimitiva(pri(TMP_caminho)))
     {
       TMP_obj.algoritmo.apply(TMP_obj,[]);
     }
     else
     {
       svl.timeout += retardo();
       setTimeout( TMP_obj.algoritmo.apply(TMP_obj,[]), svl.timeout );
     }
   }

*/



   // <http://davidwalsh.name/detect-native-function>
   aux.énativa = function(value)
   {

     var toString   = Object.prototype.toString;     // Used to resolve the internal `[[Class]]` of values
     var fnToString = Function.prototype.toString;   // Used to resolve the decompiled source of functions
     var reHostCtor = /^\[object .+?Constructor\]$/; // Used to detect host constructors (Safari > 4; really typed array specific)

     // Compile a regexp using a common native method as a template.
     // We chose `Object#toString` because there's a good chance it is not being mucked with.
     var reNative = RegExp
     (
       '^' +
       // Coerce `Object#toString` to a string
       String(toString)
       // Escape any special regexp characters
       .replace(/[.*+?^${}()|[\]\/\\]/g, '\\$&')
       // Replace mentions of `toString` with `.*?` to keep the template generic.
       // Replace thing like `for ...` to support environments like Rhino which add extra info
       // such as method arity.
       .replace(/toString|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
     );

     var type = typeof value;

     return type == 'function'
       // Use `Function#toString` to bypass the value's own `toString` method
       // and avoid being faked out.
       ? reNative.test(fnToString.call(value))
       // Fallback to a host object check because some environments will represent
       // things like typed arrays as DOM methods which may not conform to the
       // normal native pattern.
       : (value && type == 'object' && reHostCtor.test(toString.call(value))) || false;

   }



   // How do I disable firefox console from grouping duplicate output? <http://stackoverflow.com/a/52029982>

   aux.prefixconsole = function(key, fnc)
   {
     var c = window.console[key], i = 0;
     window.console[key] = function(str) { c.call( window.console, fnc(i++) + str ) }
   }

   // zero padding for linenumber
   aux.pad = function(s, n, c) { s=s+''; while (s.length<n) { s=c+s }; return s }

   // just choose any of these, or make your own
   aux.blankspace = function(i) { return i%2 ? '\u200B' : '' } // be careful copying a log message with zero-width blankspace
   aux.linenumber = function(i) { return aux.pad(i, 2, '0') + ' ' }

   var semana = [ "Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb" ];

   aux.timestamp  = function(s=undefined)
   {
     var date = s === undefined ? new Date() : new Date(s);
     var tz   = date.getTimezoneOffset()/-60;
     var a    = Math.abs(tz).toString();
     var s    = tz >= 0 ? '+' : '-';
         tz   = s + ( tz > -10 && tz < 10 ? '0' + a : a );
         date = new Date ( date.getTime() - 60000*date.getTimezoneOffset() );
     return date.toISOString().replace(/-/g,'/').replace(/T/,' '+ semana[date.getDay()] +' ').split('.')[0] + ' ' + tz
   }

   // apply custom console (maybe also add warn, error, info)
   aux.prefixconsole('log', aux.blankspace); // or aux.linenumber, aux.timestamp, etc



   aux.tempo = function(this_current_time, this_start_time, this_end_time, this_time_difference)
   // Elapsed Time Calculator
   // by David Tam <http://www.ComputationalNeuralSystems.com/>
   //
   // Usage:
   //    var delay = new aux.timestamp(0,0,0,0);
   //    delay.StartTiming();
   // // { javascript code }
   //    esc(delay.EndTiming());
   {
     this.this_current_time    = this_current_time;
     this.this_start_time      = this_start_time;
     this.this_end_time        = this_end_time;
     this.this_time_difference = this_time_difference;

     this.GetCurrentTime = function()
     // get current time from date timestamp
     {
       var my_current_timestamp;
           my_current_timestamp = new Date();						// stamp current date & time
       return my_current_timestamp.getTime();
     }

     this.StartTiming = function()
     // stamp current time as start time and reset display textbox
     {
       this.this_start_time = this.GetCurrentTime();					// stamp current time
     }

     this.EndTiming = function()
     // stamp current time as stop time, compute elapsed time difference and display in textbox
     {
       this.this_end_time = this.GetCurrentTime();					// stamp   current time
       this.this_time_difference = (this.this_end_time - this.this_start_time) / 1000;	// compute elapsed time
       return this.this_time_difference;						// return  elapsed time
     }

   }



   // How to simulate a mouse click using JavaScript? <http://stackoverflow.com/a/6158050>
   aux.simule = function(element, eventName)
   {

     function extend(destination, source)
     {
       for (var property in source) destination[property] = source[property];
       return destination;
     }

     var eventMatchers =
     {
       'HTMLEvents'  : /^(?:load|unload|abort|error|select|change|submit|reset|focus|blur|resize|scroll)$/,
       'MouseEvents' : /^(?:click|dblclick|mouse(?:down|up|over|move|out))$/
     }

     var defaultOptions =
     {
       pointerX   : 0,
       pointerY   : 0,
       button     : 0,
       ctrlKey    : false,
       altKey     : false,
       shiftKey   : false,
       metaKey    : false,
       bubbles    : true,
       cancelable : true
     }

     var options = extend(defaultOptions, arguments[2] || {});
     var oEvent, eventType = null;

     for (var name in eventMatchers) if (eventMatchers[name].test(eventName)) { eventType = name; break; }

     if (!eventType) throw new SyntaxError('Only HTMLEvents and MouseEvents interfaces are supported');

     if (document.createEvent)
     {
       oEvent = document.createEvent(eventType);
       if (eventType == 'HTMLEvents')
       {
         oEvent.initEvent(eventName, options.bubbles, options.cancelable);
       }
       else
       {
         oEvent.initMouseEvent
         (
           eventName,
           options.bubbles  , options.cancelable , document.defaultView ,
           options.button   ,
           options.pointerX , options.pointerY ,
           options.pointerX , options.pointerY ,
           options.ctrlKey  , options.altKey   , options.shiftKey , options.metaKey ,
           options.button   , element
         );
       }
       element.dispatchEvent(oEvent);
     }
     else
     {
       options.clientX = options.pointerX;
       options.clientY = options.pointerY;
       var evt = document.createEventObject();
       oEvent = extend(evt, options);
       element.fireEvent('on' + eventName, oEvent);
     }
     return element;

   }



   aux.d2b = function(ARG_número_decimal)
   // byte decimal para byte binário
   {
     return ARG_número_decimal.toString(2).padStart(8, 0);
   }



// EOF
