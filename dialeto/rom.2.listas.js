/*

  $modifed = 2014/02/26 (Wed) 02:53:33 (BRS)
  $created = 2009/06/15 (Seg) 21:14:16
  $counted = 2009/07/04 (Sáb) 14:56:39

*/

   svl.nel = function(ARG_lista_palavra_ou_número)
   // número de elementos de ARG_lista_palavra_ou_número
   {
     if (svl.énúmero(ARG_lista_palavra_ou_número)) ARG_lista_palavra_ou_número = ARG_lista_palavra_ou_número.toString();
     return( ARG_lista_palavra_ou_número.length );
   }



   svl.elemento = function(ARG_número, ARG_lista_palavra_ou_número)
   // elemento ARG_número de ARG_lista_palavra_ou_número
   {
     if (svl.énúmero(ARG_lista_palavra_ou_número)) ARG_lista_palavra_ou_número = ARG_lista_palavra_ou_número.toString();
     return( ARG_lista_palavra_ou_número[ARG_número-1] );
   }



   svl.pri = function(ARG_lista_palavra_ou_número)
   // primeiro elemento de ARG_lista_palavra_ou_número
   {
     if (svl.énúmero(ARG_lista_palavra_ou_número)) ARG_lista_palavra_ou_número = ARG_lista_palavra_ou_número.toString();
     return( ARG_lista_palavra_ou_número[0] );
   }



   svl.ult = function(ARG_lista_palavra_ou_número)
   // último elemento de ARG_lista_palavra_ou_número
   {
     if (svl.énúmero(ARG_lista_palavra_ou_número)) ARG_lista_palavra_ou_número = ARG_lista_palavra_ou_número.toString();
     return( ARG_lista_palavra_ou_número.slice(-1)[0] );
   }



   svl.sp = function(ARG_lista_palavra_ou_número)
   // ARG_lista_palavra_ou_número sem o primeiro elemento
   {
     if (svl.énúmero(ARG_lista_palavra_ou_número)) ARG_lista_palavra_ou_número = ARG_lista_palavra_ou_número.toString();
     return( ARG_lista_palavra_ou_número.slice(1) );
   }



   svl.su = function(ARG_lista_palavra_ou_número)
   // ARG_lista_palavra_ou_número sem o último elemento
   {
     if (svl.énúmero(ARG_lista_palavra_ou_número)) ARG_lista_palavra_ou_número = ARG_lista_palavra_ou_número.toString();
     return( ARG_lista_palavra_ou_número.slice(0,-1) );
   }



   svl.ji = function(ARG_lista_palavra_ou_número, ARG_lista)
   // junte ARG_lista_palavra_ou_número no início de ARG_lista
   {
     return( svl.sn( [ ARG_lista_palavra_ou_número ], ARG_lista ) );
   }



   svl.jf = function(ARG_lista_palavra_ou_número, ARG_lista)
   // junte ARG_lista_palavra_ou_número no fim de ARG_lista
   {
     return( svl.sn( ARG_lista, [ ARG_lista_palavra_ou_número ] ) );
   }



   svl.pal = function()
   // coloque os argumentos (palavras) numa palavra
   {
     var TMP_lista = new Array;
     for(var TMP_i=0; TMP_i<arguments.length; TMP_i++) TMP_lista[TMP_i] = arguments[TMP_i];
     return( TMP_lista.join('') );
   }



   svl.sn = function()
   // coloque os elementos dos argumentos (listas ou palavras) numa lista
   {
     var TMP_sn = new Array;
     for(var TMP_i=0; TMP_i<arguments.length; TMP_i++) TMP_sn = TMP_sn.concat(arguments[TMP_i]);
     return( TMP_sn );
   }



   svl.lista = function()
   // coloque os argumentos (listas ou palavra) numa lista
   {
     var TMP_lista = new Array;
     for(var TMP_i=0; TMP_i<arguments.length; TMP_i++) TMP_lista[TMP_i] = arguments[TMP_i];
     return( TMP_lista );
   }



   svl.car = function(ARG_byte)
   // caracter ascii código ARG_byte
   {
     return( String.fromCharCode(ARG_byte) );
   }



   svl.ascii = function(ARG_caracter)
   // código ascii de ARG_caracter
   {
     return( ARG_caracter.charCodeAt(0) );
   }



// EOF
