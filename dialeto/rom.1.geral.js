/*

  $modifed = 2014/02/26 (Wed) 02:58:46 (BRS)
  $created = 2009/06/15 (Seg) 21:14:16
  $counted = 2009/07/04 (Sáb) 14:56:39

*/



   svl.versão = function()
   // versão do svl
   {
     return( "Scalable Vector Logo versão " + svl.número_versão );
   }



   svl.primitivas = function(namespace=svl)
   // primitivas no namespace svl
   {
     // cat rom.* | grep -E "^ *function " | grep -v " aux." | sed "s/ \+/ /g" | awk -F"[ (]" '{print$3}' | sort | tr "\n" , | sed "s/,/','/g;s/','$//"
     if (svl.épalavra(namespace)) namespace = window[namespace];
     var TMP_primitivas = [];
     for (var TML_f in namespace)
       if ( typeof namespace[TML_f] === 'function' )
         TMP_primitivas.push(TML_f);
     return TMP_primitivas.sort();
   }

   svl.éprimitiva = function(ARG_palavra)
   {
     if (svl.não(svl.épalavra(ARG_palavra))) ARG_palavra = ARG_palavra.name;
     return( svl.primitivas().indexOf(ARG_palavra)>=0 );
   }



   svl.procedimentos = function()
   // procedimentos definidos pelo usuário
   // <http://stackoverflow.com/q/493833>
   {
     var TMP_títulos = [];
     for (var TML_f in window)
       if (
               window.hasOwnProperty(TML_f)
            && typeof window[TML_f] === 'function'
            && svl.não(svl.éprimitiva(TML_f))
            && eval( 'svl.não(aux.énativa(' + TML_f + '))' )
          )
         TMP_títulos.push(TML_f);
     return TMP_títulos.sort();
   }

   svl.mots = function()
   // mostre títulos dos procedimentos
   {
     for (var TML_p in svl.procedimentos())
       svl.esc(svl.procedimentos()[TML_p])
   }

   svl.mop = function(ARG_palavra)
   // mostre procedimento
   {
     svl.esc( ( typeof(ARG_palavra)=="string" ? eval('window.'+ARG_palavra) : ARG_palavra ).toString() );
   }

   svl.mops = function()
   // mostre todos procedimentos
   {
     for( var TMP_procedimento of svl.procedimentos() )
     {
       if ( svl.não( eval('window.'+TMP_procedimento).toString().match(/^(async )?function/) ) ) svl.esc(TMP_procedimento);
       svl.mop(TMP_procedimento);
     }
   }

   svl.texto = function(ARG_palavra)
   // texto do procedimento
   // BUG: tratamento dos sinalizadores de nova linha (\n), novo comando (;) e comentário (//) pode ser mal interpretado
   {
     var t = ARG_palavra.toString();
     return t.substring( t.indexOf("{") + 1 , t.lastIndexOf("}") )
             .split( /[\n;]/ )
             .map( e => e.replace( /\/\/.*/g , '' )
                         .replace( / *([([{\}\]\)]) */g , '$1' )
                         .trim()
                 )
             .filter( (e) => { return não( évazia(e) ) } )
   }



   svl.nomes = function()
   // nomes de variáveis definidas
   {
     var TMP_nomes = [];
     for (var TML_n in window)
       if (
               typeof(window[TML_n]) !== "function"
            && typeof(window[TML_n]) !== "object"
            && typeof(window[TML_n]) !== "boolean"
            && typeof(window[TML_n]) !== "undefined"
          )
         TMP_nomes.push(TML_n);
     return TMP_nomes.sort();
   }

   svl.mons = function()
   // mostre nomes das variáveis definidas
   {
     for (var TML_p in svl.nomes())
       svl.esc(svl.nomes()[TML_p])
   }



   svl.na = function()
   // não aplicável
   {
     return( -1 );	// -1
   }



   svl.falso = function()
   // falso
   {
     return( false );	// 0
   }

   svl.verd = function()
   // verdadeiro
   {
     return( true );	// 1
   }



   svl.não = function(ARG_operação_lógica)
   // não é ARG_operação_lógica
   {
     return( ! ARG_operação_lógica );
   }



   svl.sãoiguais = function(ARG_valor1, ARG_valor2)
   // ARG_valor1 é igual a ARG_valor2
   {
     return( ARG_valor1 == ARG_valor2 );
   }



   svl.évazia = function(ARG_lista_ou_palavra)
   // ARG_lista_ou_palavra é vazia
   {
     return svl.nel(ARG_lista_ou_palavra) == 0;
   }



   svl.temvalor = function(ARG_palavra)
   // variável ARG_palavra tem valor definido (énome)
   {
     return typeof( typeof(ARG_palavra)=="string" ? window[ARG_palavra] : ARG_palavra ) != "undefined";
   }



   svl.éprocedimento = function(ARG_palavra)
   // ARG_palavra é procedimento (função)
   {
  // return( ( svl.épalavra(ARG_palavra) ? eval('typeof('+ARG_palavra+')') : typeof(ARG_palavra) ) == 'function' );
     if (svl.não(svl.épalavra(ARG_palavra))) ARG_palavra = ARG_palavra.name;
     return svl.procedimentos().indexOf(ARG_palavra) >= 0;
   }



   svl.épropriedade = function(ARG_objeto)
   // ARG_objeto é propriedade (objeto/hash/dicionário/array associativo/simbólico)
   {
     return ARG_objeto.constructor === Object;
   }



   svl.élista = function(ARG_objeto)
   // ARG_objeto é lista
   {
     return ARG_objeto.constructor === Array;
   }



   svl.épalavra = function(ARG_objeto)
   // ARG_objeto é palavra
   {
     return ARG_objeto.constructor === String;
   }



   svl.énúmero = function(ARG_objeto)
   // ARG_objeto é número
   {
     return ARG_objeto.constructor === Number;
   }



   svl.ébooleano = function(ARG_objeto)
   // ARG_objeto é booleano
   {
     return ARG_objeto.constructor === Boolean;
   }



   svl.repita = function(ARG_número_vezes, ARG_função_comandos)
   // repita
   {

     while(ARG_número_vezes>0)
     {
       ARG_função_comandos();
       ARG_número_vezes--;
     }

   }



   svl.espere = function(ARG_número_milisegundos)
   {
     return new Promise(resolve => setTimeout(resolve, ARG_número_milisegundos));
   }



/*
   svl.espere = function(ARG_número_segundos)
   {
     svl.timeout += ARG_número_segundos*1000;
   }
*/



/*
  _function svl.carregue(ARG_palavra_caminho)
   // bug
   {

//   ARG_palavra_caminho += ".js";

     var TMP_script   = document.createElementNS( aux.svg, "script" );
     TMP_script.setAttribute( "href" , ARG_palavra_caminho );
     TMP_script.setAttribute( "type" , "application/javascript" );
     svg.raiz.appendChild(TMP_script);
     esc("'"+ARG_palavra_caminho+"' carregado");

//   var e  = document.createElement("script");
//   e.src  = url;
//   e.type = "text/javascript";
//   document.getElementsByTagName("head")[0].appendChild(e);
//
//   onload = function(){ dhtmlLoadScript("dhtml_way.js") }

   }
*/



/*
  _function svl.carregue(ARG_palavra_caminho)
   // bug
   {
     var TMP_end_real = ARG_palavra_caminho;
//   if ( TMP_end_real.indexOf("http")<0 && TMP_end_real.indexOf(".svl")<0 ) TMP_end_real += ".svl";
     var TMP_script   = document.createElementNS( aux.svg, "script" );
     TMP_script.setAttribute( "href" , TMP_end_real );
     TMP_script.setAttribute( "type" , "application/javascript" );
     svg.raiz.appendChild(TMP_script);
//   esc("'"+ARG_palavra_caminho+"' carregado");
//   var TMP_value    = "alert(1)";
//   var TMP_textNode = document.createTextNode("<![CDATA["+TMP_value+"]]>");
//   svg.raiz.appendChild(TMP_textNode);
   }
*/



   svl.mo = function(ARG_lista_ou_palavra="", ARG_prefixo="")
   // SAÍDA  representação em forma de lista do Logo de ARG_lista_ou_palavra
   {
     var TMP_nível = (arguments[1]) ? arguments[1] : 0;
     var TMP_out   = "[ ";
     if ( svl.élista(ARG_lista_ou_palavra) )
     {
       for (var TMP_item in ARG_lista_ou_palavra)
       {
         var TMP_aspas = svl.épalavra(ARG_lista_ou_palavra[TMP_item]) ? '"' : '';
         TMP_out += svl.élista(ARG_lista_ou_palavra[TMP_item]) ? svl.mo(ARG_lista_ou_palavra[TMP_item],TMP_nível+1) : TMP_aspas + ARG_lista_ou_palavra[TMP_item] + TMP_aspas + " ";
       }
     }
     else
     {
       var TMP_aspas = svl.épalavra(ARG_lista_ou_palavra) ? '"' : '';
       TMP_out = TMP_aspas + ARG_lista_ou_palavra + TMP_aspas;
     }
     if (svl.élista(ARG_lista_ou_palavra)) TMP_out += "] ";
     console.log( svl.évazia(ARG_lista_ou_palavra) ? '' : ARG_prefixo+TMP_out );
   }



   svl.esc = function(ARG_lista_ou_palavra="")
   // SAÍDA  entrada enviada para o console JS
   {
     console.log( svl.évazia(ARG_lista_ou_palavra) ? '' : svl.épalavra(ARG_lista_ou_palavra) ? ARG_lista_ou_palavra : JSON.stringify( ARG_lista_ou_palavra, null, 2 ) );
   }



   svl._chame = function(ARG_número)
   // chama procedimento de baixo nivel (compatibilidade)
   {
      if ( ARG_número == 0 ) location.reload(); else return svl.falso();
   }



   svl.tat = function()
   // limpa tela do console web e tela gráfica
   {
     pc();
     ad();
     att();
   }



   svl.att = function()
   // limpa tela do console web
   // <http://developer.mozilla.org/docs/Tools/Web_Console/Helpers>
   // <http://developer.mozilla.org/docs/Web/API/Console>
   {
     console.clear();
   }



   // How do I make JavaScript beep? <http://stackoverflow.com/a/41077092>
   svl.toque = async function (ARG_frequencia = 440, ARG_duração = 50, ARG_volume = .5, ARG_onda_tipo = 'sine', ARG_pausa = 100)
   {
     var oscillator = aux.audioCtx.createOscillator();
     var gainNode   = aux.audioCtx.createGain();
     oscillator.connect(gainNode);
     gainNode  .connect(aux.audioCtx.destination);
     gainNode  .gain.value      = ARG_volume;
     oscillator.frequency.value = ARG_frequencia;
     oscillator.type            = ARG_onda_tipo;
     oscillator.start();
     await espere(ARG_duração);
     oscillator.stop()
     await espere(ARG_pausa);
  // setTimeout( ()=> oscillator.stop(), ARG_duração );
   }



   svl.mudeprojeto = function(ARG_palavra_nome='projeto_sem_nome')
   // mude nome projeto
   {
     svl.v_projeto = ARG_palavra_nome;
     document.title = svl.v_projeto + ' - SVL';
   }

   svl.projeto = function()
   // nome projeto
   {
     return( svl.v_projeto );
   }



   svl.hotlogo = function(ARG_palavra_caminho='', ARG_palavra_procedimento='')
   // abra webmsx com hot-logo carregando arquivo em prefixo_url+ARG_palavra_caminho e executando ARG_palavra_procedimento
   // ex: svl.hotlogo('https://gitlab.com/arkanon/svl/raw/dev/hot-logo/desenhos/paisagem', 'tudo')
   {
     var gl_branch   = 'master';
     var prefixo_url = 'http://gitlab.com/arkanon/svl/raw/' + gl_branch + '/hot-logo/';
     var wmsx        = 'http://webmsx.org';
     var parâmetros  =
         {
           // <https//github.com/ppeccin/webmsx#parameters-reference>
           'ROM'                    : 'http://msxblog.es/wp-content/uploads/2009/09/msxlogo_por.zip',
           'MACHINE'                : 'MSX1A',
           'SCREEN_DEFAULT_ASPECT'  : 1,
           'SCREEN_CONTROL_BAR'     : 0,
           'SCREEN_FILTER_MODE'     : 1,
           'SCREEN_CRT_SCANLINES'   : 5,
           'SCREEN_CRT_PHOSPHOR'    : 1,
           'SCREEN_FULLSCREEN_MODE' : 2,
           'SCREEN_DEFAULT_SCALE'   : 0.5,
           'CPU_TURBO_MODE'         : 8,
           'VDP_TURBO_MODE'         : 9,
           'SPEED'                  : 1000,
           'FAST_BOOT'              : 1,
         }
     if ( svl.não(svl.évazia(ARG_palavra_caminho)) )
     {
       parâmetros.DISK_FILES  = prefixo_url + ARG_palavra_caminho;
       parâmetros.BASIC_ENTER = 'tat dt mudecf 1 carregue "' + svl.nomearq(ARG_palavra_caminho);
       if ( svl.não(svl.évazia(ARG_palavra_procedimento)) ) parâmetros.BASIC_ENTER += ' ' + ARG_palavra_procedimento;
     }
     var chaves = Object.keys(parâmetros);
     var url    = wmsx + '?';
     for ( var chave in chaves ) url += chaves[chave] + '=' + encodeURI(parâmetros[chaves[chave]]) + '&';
     window.open(url.slice(0,-1))
   }



   svl.carregue = function(ARG_palavra_caminho)
   {
     var TMP_end_real, TMP_script;
     var TMP_diretório = new URL( svl.pastaarq(ARG_palavra_caminho), svl.pasta() ).href;
     svl.end_real = TMP_diretório + svl.nomearq(ARG_palavra_caminho);
     if ( svl.évazia(svl.extensãoarq(svl.end_real)) ) svl.end_real += '.svl';
// esc( "0 - " + svl.pasta() );
// esc( "1 - " + ARG_palavra_caminho );
// esc( "2 - " + svl.pastaarq(ARG_palavra_caminho) );
// esc( "3 - " + TMP_diretório );
// esc( "4 - " + svl.nomearq(ARG_palavra_caminho) );
// esc( "5 - " + svl.end_real );
     TMP_script = document.createElementNS( aux.svg, "script" );
     TMP_script.setAttribute( "href" , svl.end_real );
     TMP_script.setAttribute( "type" , "application/javascript" );
     TMP_script.onload = function()
     {
       svl.mudeprojeto(svl.prenomearq(svl.end_real))
       svl.esc('Arquivo ' + svl.nomearq(svl.end_real) + ' carregado.')
     }
     svg.raiz.appendChild(TMP_script);
   }



   svl.pastaarq   = function(ARG_palavra_caminho) { return ARG_palavra_caminho.match  ( /.*\//     ) ? ARG_palavra_caminho.match( /.*\// )[0] : './' } // <http://abeautifulsite.net/javascript-functions-for-basename-and-dirname>
   svl.nomearq    = function(ARG_palavra_caminho) { return ARG_palavra_caminho.replace( /.*\//, '' )    }
   svl.prenomearq = function(ARG_palavra_caminho) { return svl.nomearq(ARG_palavra_caminho).replace( '.' + svl.extensãoarq(ARG_palavra_caminho), '' ) }
// svl.nomearq    = function(ARG_palavra_caminho) { return ARG_palavra_caminho.split  ( /[\\/]/ ).pop() } // <http://stackoverflow.com/questions/3820381/need-a-basename-function-in-javascript#comment29942319_15270931>

   // <http://stackoverflow.com/questions/190852/how-can-i-get-file-extensions-with-javascript>
   svl.extensãoarq = function(ARG_palavra_caminho)
   {
     var a = svl.nomearq(ARG_palavra_caminho).split(".");
     return a.length === 1 || ( a[0] === "" && a.length === 2 ) ? "" : a.pop();
   }



   // Get list of filenames in folder with JavaScript <http://stackoverflow.com/a/58041770>
   // <http://developer.mozilla.org/docs/Archive/Misc_top_level/Same-origin_policy_for_file:_URIs>
   // <http://developer.mozilla.org/docs/Security/MixedContent>

   svl.listaarquivos = function(ARG_palavra_diretório='.')
   {
     var proxy = ARG_palavra_diretório.match(/^http/) ? 'https://cors-anywhere.herokuapp.com/' : '';
     var xhr = new XMLHttpRequest();
         xhr.open( "GET", proxy + ARG_palavra_diretório, false ); // false for synchronous request
         xhr.send( null );
     var ret   = xhr.responseText;
     var list  = ret.split('\n');
     var lista = [];
     for ( i=0; i<list.length; i++ )
     {
       var info = list[i].split(' ');
       if ( info[0] == "201:" )
       {
         var nome = decodeURI(info[1]);
         if ( nome[0] != '.' )
         {
           var tamanho = info[2];
           var data    = aux.timestamp(decodeURI(info[3]));
           var tipo    = info[4] == 'FILE' ? 'ARQ' : 'PAS';
               nome   += tipo == 'PAS' ? '/' : '';
           lista = svl.jf( [ tipo , nome , tamanho , data ] , lista );
         }
       }
     }
     return lista;
   }

   svl.arquivos = function(ARG_palavra_diretório=svl.pasta())
   {
     ARG_palavra_diretório = new URL( ARG_palavra_diretório, svl.pasta() ).href + '/';
     var proxy = ARG_palavra_diretório.match(/^http/) ? 'https://cors-anywhere.herokuapp.com/' : '';
     var xhr = new XMLHttpRequest();
         xhr.open( "GET", proxy + ARG_palavra_diretório, false ); // false for synchronous request
         xhr.send( null );
     var ret   = xhr.responseText;
     var list  = ret.split('\n');
     var total = 0;
     for ( i=0; i<list.length; i++ )
     {
       var info = list[i].split(' ');
       if ( info[0] == "201:" )
       {
         var nome = decodeURI(info[1]);
         if ( nome[0] != '.' )
         {
           var tamanho = aux.pad  (info[2],7,' ');
           var data    = aux.timestamp(decodeURI(info[3]));
           var tipo    =           info[4] == 'FILE' ? 'ARQ' : 'PAS';
           esc( tipo + '   ' + tamanho + '   ' + data + '   ' + nome + (tipo=='PAS'?'/':'') );
           total += parseInt(tamanho);
         }
       }
     }
     esc( 'TOTAL ' + aux.pad(total,7,' ') + ' caracteres' );
   }

   aux.curl = function(url)
   {
     var proxy = url.match(/^http/) ? 'https://cors-anywhere.herokuapp.com/' : '';
     var xhr   = new XMLHttpRequest();
         xhr.open( "GET", proxy + url, false ); // false for synchronous request
         xhr.send( null );
     var ret   = xhr.responseText;
     var list  = ret.split(/\n|<hr>/i).map(e=>e.trim()) // .map( e=>e.trim().replace(/^.*(< *a +href *=.+)/, '$1') ).filter( (elem,index,arr) => elem.match(/< *a +href *=/i) );
     return list;
   }



   svl.mudepasta = function(ARG_palavra_diretório)
   // mude pasta de arquivos
   {
  // svl.v_pasta = svl.pasta() + '/' + ARG_palavra_diretório;
     svl.v_pasta = new URL( ARG_palavra_diretório, svl.pasta() ).href + '/';
   }

   svl.pasta = function()
   // pasta de arquivos
   {
     return( svl.v_pasta );
   }



/*

  <!-- https://github.com/bahmutov/console-log-div -->

  <style>
    .console-log-div
    {
      border: 1px solid gray;
      padding: 5px 10px;
      border-radius: 5px;
      width: 95% !important;
      background-color: #efefef;
    }
  </style>

  <fieldset id="console-log-div"></fieldset>

  <script src="https://rawgit.com/bahmutov/console-log-div/master/console-log-div.js" ></script>

  <script>
    console.log('oi');
    throw new Error('this is a thrown error');
  </script>

*/



// EOF
