/*

  $modifed = 2014/02/26 (Wed) 03:46:18 (BRS)
  $created = 2009/06/15 (Seg) 21:14:16
  $counted = 2009/07/04 (Sáb) 14:56:39

*/

   svl.ad = function()
   // apague desenho
   {
     var TMP_desenho = document.getElementsByClassName("traço");
     Object.keys(TMP_desenho).forEach( function() { TMP_desenho[0].remove() } )
   }



   svl.pp = function(ARG_lista_coordenadas)
   // ponha ponto
   {

     for(var TAT_i in svl.v_quem)
     {

       var TMP_coorx = svl.elemento(1,ARG_lista_coordenadas);
       var TMP_coory = svl.elemento(2,ARG_lista_coordenadas);
       var TMP_coorz = svl.elemento(3,ARG_lista_coordenadas);

       var TMP_svg_x = TMP_coorx / ( 1 - TMP_coorz / svl.perpectiva );
       var TMP_svg_y = TMP_coory / ( 1 - TMP_coorz / svl.perpectiva );

       var TAT_ind   = svl.v_quem[TAT_i];
       var TMP_cl    = svl.v_cl[TAT_ind];
       var TMP_ol    = svl.v_ol[TAT_ind];

       svg.novo_ponto = document.createElementNS( aux.svg, "use" );
       svg.novo_ponto.setAttribute( "transform"      , "translate("+TMP_svg_x+","+TMP_svg_y+")" );
       svg.novo_ponto.setAttribute( "stroke"         , svl.elemento(TMP_cl+1,svl.paleta())      );
       svg.novo_ponto.setAttribute( "stroke-width"   , 0                                        );
       svg.novo_ponto.setAttribute( "stroke-opacity" , 1                                        );
       svg.novo_ponto.setAttribute( "fill"           , svl.elemento(TMP_cl+1,svl.paleta())      );
       svg.novo_ponto.setAttribute( "fill-opacity"   , TMP_ol                                   );
       svg.novo_ponto.setAttribute( "href"           , "#def_ponto"                             );
       svg.tela.appendChild(svg.novo_ponto);

     }

   }



   svl.mudecf = function(ARG_cor_número)
   // mude cor fundo
   {
     svl.v_cf = aux.cor( ARG_cor_número, svg.fundo );
   }

   svl.cf = function()
   // cor fundo
   {
     return( svl.v_cf );
   }



   svl.gravedes = function(ARG_palavra_nome)
   // grave desenho
   {
     var code = document.getElementsByTagName('svg')[0].outerHTML;
     var url  = 'data:image/svg+xml;base64,' + btoa(unescape(encodeURIComponent(code)));
     svg.save.download = ARG_palavra_nome + '.svg';
     svg.save.setAttribute( 'href' , url );
     aux.simule(svg.save, 'click');
   }



   // http://vanlanschot.ch/en/-/media/illustrations/a---inspirational-header-desktop---testimonial-desktop/a_vls_33_zonsopgang_dorp_natuur.ashx
   // http://static.vecteezy.com/system/resources/svgs/000/150/591/original/671c27b4-cbb4-4af7-9fa4-4f4b1ccd066d.svg
   // http://kwchu.com/assets/img/foundation/foundation-img3.svg
   // http://musescore.com/static/musescore/scoredata/gen/1/7/6/5471671/4b6ff09b8f72bf762fe50795c1ab28b147a9b5f9/score_0.svg
   // http://clker.com/cliparts/O/0/3/Z/P/m/beach-portrait.svg
   // http://iso.500px.com/wp-content/uploads/2014/07/big-one.jpg
   svl.carreguedes = function(ARG_palavra_nome='', ARG_palavra_caminho='')
   // carregue desenho
   {

     if (svl.évazia(ARG_palavra_nome)) ARG_palavra_nome = 'des[' + Object.keys(svl.desenhos()).length + ']';

     if (svl.évazia(ARG_palavra_caminho))
     {
       // Insert HTML code inside SVG Text element <http://stackoverflow.com/a/9946724>
       // How open select file dialog via js? <http://stackoverflow.com/a/40971885>
       // Get width of svg element from uploaded file <http://stackoverflow.com/a/28870777>
       // How to add a svg-object (created with DOMParser from a string) into a div element? <http://stackoverflow.com/a/35784344>
       svg.load.type     = 'file';
       svg.load.accept   = 'image/svg+xml';
       svg.load.onchange = fileEvent =>
       {
         var file = fileEvent.target.files[0];

/*
         esc( 'name: ' + file.name );
         esc( 'size: ' + file.size );
         esc( 'type: ' + file.type );
         esc( 'last: ' + aux.timestamp(file.lastModified) );
         esc( 'date: ' + file.lastModifiedDate );
         esc( 'path: ' + file.webkitRelativePath );
*/

         var reader = new FileReader();
         ARG_palavra_caminho = file.name;
         reader.onloadend = () => insert( ARG_palavra_nome, document.adoptNode( new DOMParser().parseFromString(reader.result, file.type).documentElement ) );
         reader.readAsText(file,'UTF-8');
       }
       svg.load.click();
     }
     else
     {
       var proxy = ARG_palavra_caminho.match(/^http/) ? 'https://cors-anywhere.herokuapp.com/' : '';
       var xhr   = new XMLHttpRequest();
       xhr.open( "GET", proxy + ARG_palavra_caminho, true ); // false for synchronous request
       xhr.overrideMimeType("image/svg+xml");
       xhr.onload = function(e)
       {
      // esc(xhr.responseText);
         if (xhr.readyState === 4)
            if (xhr.status === 200)
               insert( ARG_palavra_nome, xhr.responseXML.documentElement );
            else
               console.error(xhr.statusText);
       }
       xhr.onerror = e => console.error(xhr.statusText);
       xhr.send(null);
     }

     function insert(ARG_palavra_nome, fig)
     {

       var largura_fisica, altura_fisica, largura_tela_svl, altura_tela_svl, resolução, ampliação_svl, ampliação_fisica, largura_imagem, altura_imagem, f, conversão_unidade, escala, tx, ty, elemento;

       largura_fisica   = innerWidth;
       altura_fisica    = innerHeight;
       largura_tela_svl = svl.max_coorx() - svl.min_coorx();
       altura_tela_svl  = svl.max_coory() - svl.min_coory();
       resolução        = svl.resolução();
       ampliação_svl    = innerWidth/largura_tela_svl;
       ampliação_fisica = devicePixelRatio;

       if (svl.temvalor(fig.width))
       {
         largura_imagem = fig.width .baseVal.valueInSpecifiedUnits;
         altura_imagem  = fig.height.baseVal.valueInSpecifiedUnits;
       }
       else
       {
         largura_imagem = fig.viewBox.baseVal.width;
         altura_imagem  = fig.viewBox.baseVal.height;
       }

// mudelimites([-innerWidth/2,-innerWidth/2],[innerWidth/2,innerWidth/2])

       f = fig.width.baseVal.valueAsString;
            if (f.match(/pt/)) conversão_unidade = 3/4;            // 1 px = 3/4    pt
       else if (f.match(/mm/)) conversão_unidade = 25.4/resolução; // 1 px = 25.4/R mm
       else if (f.match(/%/ )) conversão_unidade = largura_imagem/largura_tela_svl;
       else                    conversão_unidade = 1;

       if ( largura_imagem > altura_imagem )
       {
         dimensão_fisica = largura_fisica;
         dimensão_imagem = largura_imagem;
       }
       else
       {
         dimensão_fisica = altura_fisica;
         dimensão_imagem = altura_imagem;
       }

       escala = dimensão_fisica / dimensão_imagem * conversão_unidade / ampliação_fisica / ampliação_svl;

       tx = svl.min_coorx();
       ty = svl.max_coory();

/*
       esc( 'resolução ----------------------------- ' + resolução         );
       esc();
       esc( 'largura_fisica ------------------------ ' + largura_fisica    );
       esc( 'largura_tela_svl  --------------------- ' + largura_tela_svl  );
       esc( 'largura_imagem ------------------------ ' + largura_imagem    );
       esc();
       esc( 'altura_fisica ------------------------- ' + altura_fisica     );
       esc( 'altura_tela_svl ----------------------- ' + altura_tela_svl   );
       esc( 'altura_imagem ------------------------- ' + altura_imagem     );
       esc();
       esc( 'ampliação_fisica ---------------------- ' + ampliação_fisica  );
       esc( 'ampliação_svl ------------------------- ' + ampliação_svl     );
       esc( 'conversão_unidade --------------------- ' + conversão_unidade );
       esc( 'escala -------------------------------- ' + escala            );
       esc();
       esc( 'min_coorx ----------------------------- ' + tx                );
       esc( 'max_coory ----------------------------- ' + ty                );
       esc();
       esc( 'viewBox.baseVal.width  ---------------- ' + ( fig.viewBox.baseVal === null ? '' : fig.viewBox.baseVal.width  ) );
       esc( 'viewBox.baseVal.height ---------------- ' + ( fig.viewBox.baseVal === null ? '' : fig.viewBox.baseVal.height ) );
       esc( 'width  .baseVal.valueInSpecifiedUnits - ' + fig.width  .baseVal.valueInSpecifiedUnits );
       esc( 'height .baseVal.valueInSpecifiedUnits - ' + fig.height .baseVal.valueInSpecifiedUnits );
       esc( 'width  .baseVal.valueAsString --------- ' + fig.width  .baseVal.valueAsString         );
       esc( 'height .baseVal.valueAsString --------- ' + fig.height .baseVal.valueAsString         );
       esc( 'x ---- .baseVal.valueInSpecifiedUnits - ' + fig.x      .baseVal.valueInSpecifiedUnits );
       esc( 'y ---- .baseVal.valueInSpecifiedUnits - ' + fig.y      .baseVal.valueInSpecifiedUnits );
       esc( 'viewport ------------------------------ ' + fig.viewport         );
       esc( 'currentView --------------------------- ' + fig.currentView      );
       esc( 'currentScale -------------------------- ' + fig.currentScale     );
       esc( 'currentTranslate ---------------------- ' + fig.currentTranslate );
       esc(fig);
*/

       fig.setAttribute( 'id'        , ARG_palavra_nome );
       fig.setAttribute( 'transform' , 'translate(' + tx + ' ' + ty + ') scale(' + escala + ' -' + escala + ')' ); // <http://developer.mozilla.org/docs/Web/SVG/Attribute/transform>
       elemento = aux.$(ARG_palavra_nome);
       if ( typeof(elemento) != 'undefined' && elemento != null ) elemento.remove();

       svg.tela.appendChild(fig);
    // svg.tela.appendChild(document.adoptNode(fig.documentElement));

       svl.v_desenhos[ARG_palavra_nome] = ARG_palavra_caminho;
       esc(ARG_palavra_nome);

     }

   }



   svl.desenhos = function()
   // desenhos criados e carregados
   {
  // return JSON.stringify(svl.v_desenhos, null, 2);
     return svl.v_desenhos;
   }



   svl.apaguedes = function(ARG_palavra_nome)
   // apague desenho
   {
     aux.$(ARG_palavra_nome).remove();
     delete svl.v_desenhos[ARG_palavra_nome];
   }



   svl.mostredes = async function(ARG_palavra_nome)
   // mostre desenho
   {
     var des = aux.$(ARG_palavra_nome);
     for ( i=0; i<3; i++ )
     {
       des.style.visibility = 'hidden';
       await espere(300);
       des.style.visibility = 'visible';
       await espere(300);
     }
   }



/*

  Primitivas Novas
  ---------- -----

*/



   svl.mudeof = function(ARG_opacidade_razão)
   // mude opacidade fundo
   {

       svl.v_of = ARG_opacidade_razão;
       svg.fundo.setAttribute( "fill-opacity", svl.of());

   }

   svl.of = function()
   // opacidade fundo
   {
     return( svl.v_of );
   }



   svl.mudelimites = function(ARG_lista_min_coor,ARG_lista_max_coor)
   // mude limites do fundo
   {

       svl.v_min_coorx = svl.pri(ARG_lista_min_coor); // *devicePixelRatio;
       svl.v_min_coory = svl.ult(ARG_lista_min_coor); // *devicePixelRatio;
       svl.v_max_coorx = svl.pri(ARG_lista_max_coor); // *devicePixelRatio;
       svl.v_max_coory = svl.ult(ARG_lista_max_coor); // *devicePixelRatio;
       svg.raiz   .setAttribute( "viewBox"   , "0 0 "+(svl.max_coorx()-svl.min_coorx())+" "+(svl.max_coory()-svl.min_coory()) );
       svg.fundo  .setAttribute( "x"         ,  svl.min_coorx() );
       svg.fundo  .setAttribute( "y"         ,  svl.min_coory() );
       svg.fundo  .setAttribute( "width"     , (svl.max_coorx()-svl.min_coorx()) );
       svg.fundo  .setAttribute( "height"    , (svl.max_coory()-svl.min_coory()) );
       svg.tela   .setAttribute( "transform" , "translate("+( -svl.min_coorx() )+","+( svl.max_coory() )+") scale(1,-1)" );
//     svg.externo.setAttribute( "transform" , "translate("+( -svl.min_coorx() )+","+( svl.max_coory() )+") scale(1,-1)" );
//     aux.logo();
//     aux.validate();

   }

   svl.limites = function()
   // limites do fundo
   {
     return( [ [ svl.min_coorx(), svl.min_coory() ] , [ svl.max_coorx(), svl.max_coory() ] ] );
   }

   svl.mudeborda = function(ARG_número_borda)
   // mude largura borda
   {
     svl.v_borda = ARG_número_borda;
   }

   svl.borda = function()
   // largura borda
   {
     return( svl.v_borda );
   }

   svl.travelimite = function()
   //
   {
     svl.dinâmico = svl.falso();
   }

   svl.destravelimite = function()
   //
   {
     svl.dinâmico = svl.verd();
   }

   svl.édinâmico = function()
   //
   {
     return svl.dinâmico;
   }



   svl.min_coorx = function()
   // menor coordenada x
   {
     return( svl.v_min_coorx );
   }



   svl.min_coory = function()
   // menor coordenada y
   {
     return( svl.v_min_coory );
   }



   svl.max_coorx = function()
   // maior coordenada x
   {
     return( svl.v_max_coorx );
   }



   svl.max_coory = function()
   // maior coordenada y
   {
     return( svl.v_max_coory );
   }



   svl.listafig = function(ARG_número_fig)
   {
     return svl.todasf()[ARG_número_fig];
   }

   svl.criafigl = function(ARG_número_fig, ARG_lista_fig)
   {

     var elemento = aux.$("F-" + ARG_número_fig);
     if ( typeof(elemento) != 'undefined' && elemento != null ) elemento.remove();

  // for (i=0; i<16; i++) esc( aux.d2b(ARG_lista_fig[i]) + aux.d2b(ARG_lista_fig[i+16]) );
     // <g id="def_fig" transform="translate(0 -8)">
     var g0 = document.createElementNS( aux.svg, "g" );
     g0.setAttribute( "id"        , "F-" + ARG_número_fig );
     g0.setAttribute( "transform" , "translate(0 -8)" );
     var text_fig = '';
     for (var i=0; i<16; i++)
     {
       var L = '';
       for (var c=0; c<2; c++)
       {
         var o = 16*c+i;
         var I = o.toString().padStart(2, 0);
         var B = aux.d2b(ARG_lista_fig[o]);
         L += B+'  ';
         // <g id="$I" transform="translate(0 $I) scale(-1 1)">
         var g1 = document.createElementNS( aux.svg, "g" );
         g1.setAttribute( "id"        , 'B-' + ARG_número_fig + '-' + o );
         g1.setAttribute( "transform" , "translate("+(c*8)+" "+i+") scale(-1 1)" );
         for (var j=0; j<8; j++)
         {
           var b = document.createElementNS( aux.svg, "rect" );
           b.setAttribute( "id"           , 'b-' + ARG_número_fig + '-' + o + '-' + (7-j) );
           b.setAttribute( "x"            , 7-j );
           b.setAttribute( "stroke"       , paleta()[cf()] );
           b.setAttribute( "stroke-width" , '0.15' );
           var rgba = cf()==1 ? 'rgba(255,255,255,.2)' : 'rgba(0,0,0,.2)';
           b.setAttribute( "style"        , 'filter: drop-shadow( .05px .01px .01px ' + rgba + ')' );
           b.setAttribute( "paint-order"  , 'fill stroke markers' );
           b.setAttribute( "width"        , '1' );
           b.setAttribute( "height"       , '1' );
           b.setAttribute( "fill"         , B[j]=='1' ? "white" : "transparent" );
           g1.appendChild(b);
        // aux.$( I + '-' + (7-j) ).setAttribute( "fill" , B[j]=='1' ? "white" : "transparent" );
         }
         // </g>
         g0.appendChild(g1);
       }
       if (i==8) text_fig += '\n';
       text_fig += ( L + '\n' ).replace(/0/g,'░').replace(/1/g,'█');
     }
     esc(text_fig);
     // </g>
     svg.defs.appendChild(g0);

     // <use id="fig" href="#def_fig" transform="scale(8 -8)" />
     var f = document.createElementNS( aux.svg, "use" );
     f.setAttribute( "id"        , "f-"  + ARG_número_fig );
     f.setAttribute( "href"      , "#F-" + ARG_número_fig );
     f.setAttribute( "transform" , "translate(-"+tt()/2+" "+tt()/2+") scale("+tt()+" -"+tt()+")" );
     svg.tela.appendChild(f);

  // return f;

   }

// (async ()=>{mudecf(1);dt();for(i=0;i<11;i++){esc(i);criafigl(0,listafig(i));await espere(100)}})()
// mudecf(1); mudect(5); mudett(16); criafigl(0,listafig(36))



   svl.edfig = function(ARG_número_fig)
   {

     var s  = 8;
     var ed = 'ED';
     var cursor = 999;

     tats = svl.quem();
     svl.atat(cursor);
     svl.mudect(15);
     svl.mudeot(1);
     svl.mudett(s);
     svl.criafigl(ed,svl.listafig(ARG_número_fig));
     svl.mudett(s/10);
     svl.un();
     svl.pc();
     svl.mudepos([-64,64]);
     svl.at();
     aux.paracima(svg.tat[svl.quem()]);

     var color = cf()==1 ? 'white' : 'black';
     var help  = document.createElementNS( aux.svg, "text" );
     help.setAttribute( 'id'          , 'help'        );
     help.setAttribute( 'transform'   , 'scale(1 -1)' );
     help.setAttribute( 'font-family' , 'Ubuntu'      );
     help.setAttribute( 'font-size'   , '4px'         );
     help.setAttribute( 'fill'        , color         );
     help.setAttribute( 'y'           , '-146px'      );
     help.innerHTML += '<tspan x="-145px" dy="5px" style="font-weight:bold">' + 'Barra de Espaço' + '</tspan>' + '<tspan x="-113px">' + 'Inverte o ponto abaixo da tartaruga'                    + '</tspan>';
     help.innerHTML += '<tspan x="-145px" dy="5px" style="font-weight:bold">' + 'Seta de Cursor'  + '</tspan>' + '<tspan x="-113px">' + 'Move a tartaruga'                                       + '</tspan>';
     help.innerHTML += '<tspan x="-145px" dy="5px" style="font-weight:bold">' + 'Control + Seta'  + '</tspan>' + '<tspan x="-113px">' + 'Move a figura'                                          + '</tspan>';
     help.innerHTML += '<tspan x="-145px" dy="5px" style="font-weight:bold">' + 'Control + I'     + '</tspan>' + '<tspan x="-113px">' + 'Inverte o estado da figura'                             + '</tspan>';
     help.innerHTML += '<tspan x="-145px" dy="5px" style="font-weight:bold">' + 'Control + K'     + '</tspan>' + '<tspan x="-113px">' + 'Apaga toda a figura'                                    + '</tspan>';
     help.innerHTML += '<tspan x="-145px" dy="5px" style="font-weight:bold">' + 'Control + Y'     + '</tspan>' + '<tspan x="-113px">' + 'Restaura a figura inicialmente editada'                 + '</tspan>';
     help.innerHTML += '<tspan x="-145px" dy="5px" style="font-weight:bold">' + 'Control + H'     + '</tspan>' + '<tspan x="-113px">' + 'Espelha a figura horizontalmente         — IMPLEMENTAR' + '</tspan>';
     help.innerHTML += '<tspan x="-145px" dy="5px" style="font-weight:bold">' + 'Control + V'     + '</tspan>' + '<tspan x="-113px">' + 'Espelha a figura verticalmente           — IMPLEMENTAR' + '</tspan>';
     help.innerHTML += '<tspan x="-145px" dy="5px" style="font-weight:bold">' + 'Control + R'     + '</tspan>' + '<tspan x="-113px">' + 'Rotaciona a figura 90º à direita         — IMPLEMENTAR' + '</tspan>';
     help.innerHTML += '<tspan x="-145px" dy="5px" style="font-weight:bold">' + 'Control + C'     + '</tspan>' + '<tspan x="-113px">' + 'Sai cancelando as modificações'                         + '</tspan>';
     help.innerHTML += '<tspan x="-145px" dy="5px" style="font-weight:bold">' + 'Escape'          + '</tspan>' + '<tspan x="-113px">' + 'Sai salvando   as modificações'                         + '</tspan>';
     help.innerHTML += '<tspan x="-145px" dy="5px" style="font-weight:bold">' + ' '               + '</tspan>' + '<tspan x="-113px">' + 'Mover a tartaruga com o mouse            — IMPLEMENTAR' + '</tspan>';
     help.innerHTML += '<tspan x="-145px" dy="5px" style="font-weight:bold">' + ' '               + '</tspan>' + '<tspan x="-113px">' + 'Inverter o ponto com um clique do mouse  — IMPLEMENTAR' + '</tspan>';
     svg.tela.appendChild(help);

     var blink_sty = document.createElementNS( aux.svg, "style" );
     blink_sty.setAttribute( "id"            , "blink-sty"  );
     blink_sty.textContent = '@keyframes blink { 0%, 50% { opacity: 0 } 51%,100% { opacity: 1 } }';
     svg.tela.appendChild(blink_sty);

     var sombra = 'filter: drop-shadow(.5px -.5px .5px rgba(0,0,0,1));';
     var blink  = 'animation: blink .5s linear infinite;';

     aux.$('tat-'+cursor).setAttribute( 'style' , sombra+blink );

     var state =
     {
       false       : 'transparent'
     , true        : 'white'
     , transparent : false
     , white       : true
     }

     document.onkeydown = ()=>
     {

       var byte = ( ( ( svl.arredonde( svl.coorx() ) >= 0 ? 192 : 64 ) - svl.arredonde( svl.coory() ) ) / 8 );
       var bit  = ( ( ( svl.arredonde( svl.coorx() ) >= 0 ?  56 : -8 ) - svl.arredonde( svl.coorx() ) ) / 8 );

       switch ( event.key )
       {

         case "ArrowLeft":
           if ( event.ctrlKey )
           {

             // as 3 variáveis abaixo >> NÃO PODEM << ser atribuidas simultaneamente (var Bo = Bn = lista = [];). Por algum motivo, seu valores parecem ficar atrelados entre si.
             var Bo = [];
             var Bn = [];
             var lista = [];
             for (var B=0; B<32; B++)
             {
               var bin = '';
               for (var b=0; b<8; b++) bin = (0+state[ aux.$('b-'+ed+'-'+B+'-'+b).getAttribute('fill') ]) + bin;
               lista = svl.jf( bin, lista );
             }
             var text_fig = '';
             for (var i=0; i<16; i++)
             {
               Bo[0] = lista[16*0+i];
               Bo[1] = lista[16*1+i];
               Bn[0] = sp(Bo[0])+pri(Bo[1]);
               Bn[1] = sp(Bo[1])+pri(Bo[0]);
               text_fig += ( '│' + Bn[0] + '│' + Bn[1] + '│\n' ).replace(/0/g,'░').replace(/1/g,'█');
               for (var c=0; c<2; c++)
               {
                 var B = 16*c+i;
                 for (var b=0; b<8; b++) aux.$('b-'+ed+'-'+B+'-'+b).setAttribute ( 'fill' , state[Bn[c][7-b]==1] );
               }
             }
          // esc(text_fig);

           }
           else
           {
             svl.mudedç(270);
             svl.pf( s*( byte<16 && bit>6 ? -15 : 1 ) );
           }
           break;

         case "ArrowRight":
           if ( event.ctrlKey )
           {

             // as 3 variáveis abaixo >> NÃO PODEM << ser atribuidas simultaneamente (var Bo = Bn = lista = [];). Por algum motivo, seu valores parecem ficar atrelados entre si.
             var Bo = [];
             var Bn = [];
             var lista = [];
             for (var B=0; B<32; B++)
             {
               var bin = '';
               for (var b=0; b<8; b++) bin = (0+state[ aux.$('b-'+ed+'-'+B+'-'+b).getAttribute('fill') ]) + bin;
               lista = svl.jf( bin, lista );
             }
             var text_fig = '';
             for (var i=0; i<16; i++)
             {
               Bo[0] = lista[16*0+i];
               Bo[1] = lista[16*1+i];
               Bn[0] = ult(Bo[1])+su(Bo[0]);
               Bn[1] = ult(Bo[0])+su(Bo[1]);
               text_fig += ( '│' + Bn[0] + '│' + Bn[1] + '│\n' ).replace(/0/g,'░').replace(/1/g,'█');
               for (var c=0; c<2; c++)
               {
                 var B = 16*c+i;
                 for (var b=0; b<8; b++) aux.$('b-'+ed+'-'+B+'-'+b).setAttribute ( 'fill' , state[Bn[c][7-b]==1] );
               }
             }
          // esc(text_fig);

           }
           else
           {
             svl.mudedç(90);
             svl.pf( s*( byte>15 && bit<1 ? -15 : 1 ) );
           }
           break;

         case "ArrowUp":
           if ( event.ctrlKey )
           {

             var lista = [];
             lista[0] = lista[1] = [];
             for (var i=0; i<16; i++)
             {
               for (var c=0; c<2; c++)
               {
                 var B   = 16*c+i;
                 var bin = '';
                 for (var b=0; b<8; b++) bin = (0+state[ aux.$('b-'+ed+'-'+B+'-'+b).getAttribute('fill') ]) + bin;
                 lista[c] = svl.jf( bin, lista[c] );
               }
             }
             lista[0] = jf( pri(lista[0]) , sp(lista[0]) );
             lista[1] = jf( pri(lista[1]) , sp(lista[1]) );
             var text_fig = '';
             for (var i=0; i<16; i++)
             {
               for (var c=0; c<2; c++)
               {
                 var B = 16*c+i;
                 for (var b=0; b<8; b++) aux.$('b-'+ed+'-'+B+'-'+b).setAttribute ( 'fill' , state[lista[c][i][7-b]==1] );
               }
               text_fig += ( lista[0][i] + lista[1][i] + '\n' ).replace(/0/g,'░').replace(/1/g,'█');
             }
          // esc(text_fig);

           }
           else
           {
             svl.mudedç(0);
             svl.pf( s*( byte> 0 && byte<=15 || byte> 16 && byte<=31 ? 1 : -15 ) );
           }
           break;

         case "ArrowDown":
           if ( event.ctrlKey )
           {

             var lista = [];
             lista[0] = lista[1] = [];
             for (var i=0; i<16; i++)
             {
               for (var c=0; c<2; c++)
               {
                 var B   = 16*c+i;
                 var bin = '';
                 for (var b=0; b<8; b++) bin = (0+state[ aux.$('b-'+ed+'-'+B+'-'+b).getAttribute('fill') ]) + bin;
                 lista[c] = svl.jf( bin, lista[c] );
               }
             }
             lista[0] = ji( ult(lista[0]) , su(lista[0]) );
             lista[1] = ji( ult(lista[1]) , su(lista[1]) );
             var text_fig = '';
             for (var i=0; i<16; i++)
             {
               for (var c=0; c<2; c++)
               {
                 var B = 16*c+i;
                 for (var b=0; b<8; b++) aux.$('b-'+ed+'-'+B+'-'+b).setAttribute ( 'fill' , state[lista[c][i][7-b]==1] );
               }
               text_fig += ( lista[0][i] + lista[1][i] + '\n' ).replace(/0/g,'░').replace(/1/g,'█');
             }
          // esc(text_fig);

           }
           else
           {
             svl.mudedç(180);
             svl.pf( s*( byte>=0 && byte< 15 || byte>=16 && byte< 31 ? 1 : -15 ) );
           }
           break;

         case "Escape":
           var lista = [];
           lista[0]  = [];
           lista[1]  = [];
           var text_fig = '';
           for (var i=0; i<16; i++)
           {
             var L = '';
             for (var c=0; c<2; c++)
             {
               var B   = 16*c+i;
               var bin = '';
               for (var b=0; b<8; b++) bin = (0+state[ aux.$('b-'+ed+'-'+B+'-'+b).getAttribute('fill') ]) + bin;
               lista[c] = svl.jf( parseInt(bin,2), lista[c] );
               L += bin+'  ';
             }
             if (i==8) text_fig += '\n';
             text_fig += ( L + '\n' ).replace(/0/g,'░').replace(/1/g,'█');
           }
           esc(text_fig);
           svl.mo(svl.sn(lista[0],lista[1]));
           svl.v_todasf[ARG_número_fig] = svl.sn(lista[0],lista[1]);
           aux.$('F-'+ed    ).remove();
           aux.$('f-'+ed    ).remove();
           aux.$('help'     ).remove();
           aux.$('blink-sty').remove();
           aux.$('tat-'+cursor).setAttribute( "style" , "" );
           document.onkeydown = null;
           svl.dt();
           svl.atat(tats);
           break;

         case " ":
        // esc( byte + ' , ' + bit );
           aux.$('b-'+ed+'-'+byte+'-'+bit).setAttribute
           (
             'fill' , state[ svl.não( state[ aux.$('b-'+ed+'-'+byte+'-'+bit).getAttribute('fill') ] ) ]
           );
           break;

         case "c":
           if ( event.ctrlKey )
           {
             aux.$('F-'+ed    ).remove();
             aux.$('f-'+ed    ).remove();
             aux.$('help'     ).remove();
             aux.$('blink-sty').remove();
             aux.$('tat-'+cursor).setAttribute( "style" , "" );
             document.onkeydown = null;
             svl.dt();
             svl.atat(tats);
           }
           break;

         case "k":
           if ( event.ctrlKey )
           {
             event.preventDefault();
             for (var B=0; B<32; B++) for (var b=0; b<8; b++) aux.$('b-'+ed+'-'+B+'-'+b).setAttribute ( 'fill' , 'white' );
           }
           break;

         case "i":
           if ( event.ctrlKey )
           {
             event.preventDefault();
             for (var B=0; B<32; B++)
             {
               for (var b=0; b<8; b++)
               {
                 aux.$('b-'+ed+'-'+B+'-'+b).setAttribute
                 (
                   'fill' , state[ svl.não( state[ aux.$('b-'+ed+'-'+B+'-'+b).getAttribute('fill') ] ) ]
                 );
               }
             }
           }
           break;

         case "y":
           if ( event.ctrlKey )
           {
             event.preventDefault();
             for (var B=0; B<32; B++)
             {
               var bin = aux.d2b(listafig(ARG_número_fig)[B]);
               for (var b=0; b<8; b++) aux.$('b-'+ed+'-'+B+'-'+b).setAttribute ( 'fill' , state[bin[7-b]==1] );
             }
           }
           break;

       }

    // svl.esc( [ svl.coorx(), svl.arredonde(svl.coorx()) , -8-svl.arredonde(svl.coorx()) , bit ] );
    // svl.mo( [ byte , bit ] );

       // [  1.7763568394002505e-15 , 0 , -8 ,  7 ]
       // [ -7.1054273576010020e-15 , 0 , -8 , -1 ]

     }

   }



// EOF
