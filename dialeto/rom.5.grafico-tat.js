/*

  $modifed = 2014/02/26 (Wed) 03:50:09 (BRS)
  $created = 2009/06/15 (Seg) 21:14:16
  $counted = 2009/07/04 (Sáb) 14:56:39

*/

   svl.rg = function()
   // restaura gráfico
   {
       dt();
       pc();
    // svl.timeout    = 0;
    // svl.f          = 300;
    // svl.digse      = 3;
       svl.perpectiva = 750;
       svl.mudelimites   ( [ def.min_coorx , def.min_coory ] , [ def.max_coorx , def.max_coory ] );
       svl.mudeborda     (def.borda);
       svl.mudecf        (def.cf);
       svl.mudeof        (def.of);
       svl.mudeqtat      (def.qtat);
       svl.mudeqfig      (def.qfig);
       svl.atat          (def.quem);
       svl.pc            ();
       svl.mudett        (def.tt);
       svl.mudeproporção (def.proporção);
       svl.muderesolução (def.resolução);
    // svl.mudevel       (def.vel);
       svl.mudect        (def.ct);
       svl.mudeot        (def.ot);
       svl.mudecl        (def.cl);
       svl.mudeborrão    (def.borrão);
       svl.mudeol        (def.ol);
       svl.mudeponta     (def.ponta);
       svl.mudejunta     (def.junta);
       svl.mudetraço     (def.traço);
       svl.mudeel        (def.el);
       if (def.dinâmico ) svl.destravelimite(); else svl.travelimite();
       if (def.lápis    ) svl.ul            (); else svl.un         ();
       if (def.évisível ) svl.at            (); else svl.dt         ();
       if (def.temsombra) svl.comsombra     (); else svl.semsombra  ();
    // svl.muderetardo   (def.retardo);
   }



   svl.at = function()
   // apareça tartaruga
   {
       for(var TAT_i in svl.v_quem)
       {
         svl.v_évisível[svl.v_quem[TAT_i]] = svl.verd();
         var style = "visibility:visible;";
         if (svl.v_temsombra[svl.v_quem[TAT_i]]) style += "filter: drop-shadow(.5px -.5px .5px rgba(0,0,0,1));";
         svg.tat       [svl.v_quem[TAT_i]].setAttribute( "style", style );
       }
   }

   svl.dt = function()
   // desapareça tartaruga
   {
       for(var TAT_i in svl.v_quem)
       {
         svl.v_évisível[svl.v_quem[TAT_i]] = svl.falso();
         var style = "visibility:hidden;";
         svg.tat       [svl.v_quem[TAT_i]].setAttribute( "style", style );
       }
   }



   svl.évisível = function()
   // tartaruga é visivel
   {
     return( svl.v_évisível[svl.pri(svl.v_quem)] );
   }



   svl.paleta = function(ARG_cor_número)
   // devolva paleta ou cor rgb da ARG_cor_número na paleta
   {
     svl.v_paleta =
     [

       // original msx1
       "transparent", //  0 transparente
       "#000000",     //  1 preto
       "#07ca28",     //  2 verde
       "#3de265",     //  3 verde claro
       "#4444f0",     //  4 azul escuro
       "#706df4",     //  5 azul
       "#d03013",     //  6 vermelho escuro
       "#40e8f0",     //  7 azul claro (ciano)
       "#f34242",     //  8 vermelho
       "#f47878",     //  9 vermelho claro
       "#d0ca30",     // 10 amarelo
       "#dcdc89",     // 11 amarelo claro
       "#06a920",     // 12 verde escuro
       "#da40c5",     // 13 rosa (magenta)
       "#bcbcbc",     // 14 cinza
       "#ffffff",     // 15 branco

     /*

       // original msx2
       "transparent", //  0 transparente
       "#000000",     //  1 preto
       "#24db24",     //  2 verde
       "#6dff6d",     //  3 verde claro
       "#2424ff",     //  4 azul escuro
       "#496dff",     //  5 azul
       "#b62424",     //  6 vermelho escuro
       "#49dbff",     //  7 azul claro (ciano)
       "#ff2424",     //  8 vermelho
       "#ff6d6d",     //  9 vermelho claro
       "#dbdb24",     // 10 amarelo
       "#dbdb92",     // 11 amarelo claro
       "#249224",     // 12 verde escuro
       "#db49b6",     // 13 rosa (magenta)
       "#b6b6b6",     // 14 cinza
       "#ffffff",     // 15 branco

       // rgb arredondado
       "transparent", //  0 transparente
       "#000000",     //  1 preto
       "#00ff00",     //  2 verde
       "#bfff7f",     //  3 verde claro
       "#00007f",     //  4 azul escuro
       "#0000ff",     //  5 azul
       "#7f0000",     //  6 vermelho escuro
       "#00ffff",     //  7 azul claro (ciano)
       "#ff0000",     //  8 vermelho
       "#ff7f7f",     //  9 vermelho claro
       "#ffff00",     // 10 amarelo
       "#ffffbf",     // 11 amarelo claro
       "#007f00",     // 12 verde escuro
       "#ff00ff",     // 13 rosa (magenta)
       "#7F7F7F",     // 14 cinza
       "#ffffff",     // 15 branco

     */

     ]
     return( svl.temvalor(ARG_cor_número) ? svl.v_paleta[ARG_cor_número] : svl.v_paleta );
   }



   svl.mudect = function(ARG_cor_número)
   // mude cor tartaruga
   {
     for(var TAT_i in svl.v_quem) svl.v_ct[svl.v_quem[TAT_i]] = aux.cor( ARG_cor_número, svg.tat[svl.v_quem[TAT_i]] );
   }



   svl.ct = function()
   // cor tartaruga
   {
     return( svl.v_ct[svl.pri(svl.v_quem)] );
   }



   svl.mudecl = function(ARG_cor_número)
   // mude cor lápis
   {
     for(var TAT_i in svl.v_quem) svl.v_cl[svl.v_quem[TAT_i]] = aux.cor( ARG_cor_número );
   }

   svl.cl = function()
   // cor lápis
   {
     return( svl.v_cl[svl.pri(svl.v_quem)] );
   }



   svl.mudeborrão = function(ARG_nivel_número)
   // mude borrão
   {
       for(var TAT_i in svl.v_quem)
       {
         svl.v_borrão[svl.v_quem[TAT_i]] = ARG_nivel_número;
       }
   }

   svl.cl = function()
   // borrão
   {
     return( svl.v_borrão[svl.pri(svl.v_quem)] );
   }



   svl.mudeproporção = function(ARG_proporção_yx_razão)
   // mude proporção
   {
       for(var TAT_i in svl.v_quem)
       {
         svl.v_proporção[svl.v_quem[TAT_i]] = ARG_proporção_yx_razão;
       }
   }

   svl.proporção = function()
   // proporção
   {
     return( svl.v_proporção[svl.pri(svl.v_quem)] );
   }



   svl.muderesolução = function(ARG_resolução_tela)
   // mude resolução
   {
     svl.v_resolução = ARG_resolução_tela;
   }

   svl.resolução = function()
   // resolução
   {
     return( svl.v_resolução );
   }



   svl.ul = function()
   // use lápis
   {
       for(var TAT_i in svl.v_quem)
       {
         svl.v_lápis[svl.v_quem[TAT_i]] = svl.verd();
       }
   }

   svl.un = function()
   // use nada
   {
       for(var TAT_i in svl.v_quem)
       {
         svl.v_lápis[svl.v_quem[TAT_i]] = svl.falso();
       }
   }



   svl.carimbe = function()
   // carimbe fundo com estampa da tat
   {
       for(var TAT_i in svl.v_quem)
       {
         var TAT_ind       = svl.v_quem[TAT_i];
         var TMP_dç_cx     = svl.elemento( 1, svl.elemento( 1, svl.v_dç[TAT_ind] ) );
         var TMP_dç_cy     = svl.elemento( 2, svl.elemento( 1, svl.v_dç[TAT_ind] ) );
         var TMP_dç        = TMP_dç_cx <= 90 ? TMP_dç_cy : (360-TMP_dç_cy);
         var TMP_svg_x     = svl.svg_x      [TAT_ind];
         var TMP_svg_y     = svl.svg_y      [TAT_ind];
         var TMP_tt        = svl.v_tt       [TAT_ind];
         var TMP_proporção = svl.v_proporção[TAT_ind];
         var TMP_lápis     = svl.v_lápis    [TAT_ind];
         var TMP_cl        = svl.v_cl       [TAT_ind];
         var TMP_el        = svl.v_el       [TAT_ind];
         var TMP_ol        = svl.v_ol       [TAT_ind];
         var TMP_tat       = svg.tat        [TAT_ind];
         if (TMP_lápis)
         {
           svg.novatat = def.tat.cloneNode(true);
           svg.novatat.id = "carimbo" + svl.sorteieaté(100000);
           svg.novatat.setAttribute
           (
             "transform", ""
           + "translate(" + (  TMP_svg_x  ) + "," + (  TMP_svg_y                ) + ")"
           + "rotate   (" + ( -TMP_dç     )                                       + ")"
           + "translate(" + ( -8.5*TMP_tt ) + "," + ( -7.5*TMP_tt               ) + ")"
           + "scale    (" + (      TMP_tt ) + "," + (      TMP_tt*TMP_proporção ) + ")"
           );
           svg.novatat.setAttribute( "class"          , "traço"                             );
           svg.novatat.setAttribute( "stroke"         , svl.elemento(TMP_cl+1,svl.paleta()) );
           svg.novatat.setAttribute( "stroke-width"   , 0                                   );
           svg.novatat.setAttribute( "stroke-opacity" , 1                                   );
           svg.novatat.setAttribute( "fill"           , svl.elemento(TMP_cl+1,svl.paleta()) );
           svg.novatat.setAttribute( "fill-opacity"   , TMP_ol                              );
           svg.novatat.setAttribute( "style"          , "visibility:visible"                );
           svg.novatat.setAttribute( "href"           , "#def-tat"                          );
           svg.tela.appendChild(svg.novatat);
           aux.paracima(TMP_tat);
         }
       }
   }



   svl.coorx = function()
   // coordenada x
   {
     return( svl.v_coorx[svl.pri(svl.v_quem)] );
   }

   svl.coory = function()
   // coordenada y
   {
     return( svl.v_coory[svl.pri(svl.v_quem)] );
   }

   svl.coorz = function()
   // coordenada z
   {
     return( svl.v_coorz[svl.pri(svl.v_quem)] );
   }

   svl.pos = function()
   // coordenadas x y z
   {
     return( [ svl.coorx(), svl.coory(), svl.coorz() ] );
   }



   svl.dç = function()
   // direção
   {
       var TMP_dç = svl.v_dç[svl.pri(svl.v_quem)];
       if (arguments.length!=0)
       {
         /*
                      cx   cy  cz       lx   ly  lz      ax  ay  az
                     ---  ---  --      ---  ---  --      --  --  --
              0: [ [  90,   0, 90 ], [   0,  90, 90 ], [ 90, 90,  0 ] ]
             15: [ [  75,  15, 90 ], [  15, 105, 90 ], [ 90, 90,  0 ] ]
             30: [ [  60,  30, 90 ], [  30, 120, 90 ], [ 90, 90,  0 ] ]
             45: [ [  45,  45, 90 ], [  45, 135, 90 ], [ 90, 90,  0 ] ]
             60: [ [  30,  60, 90 ], [  60, 150, 90 ], [ 90, 90,  0 ] ]
             75: [ [  15,  75, 90 ], [  75, 165, 90 ], [ 90, 90,  0 ] ]
             90: [ [   0,  90, 90 ], [  90, 180, 90 ], [ 90, 90,  0 ] ]
               :
            105: [ [  15, 105, 90 ], [ 105, 165, 90 ], [ 90, 90,  0 ] ]
            120: [ [  30, 120, 90 ], [ 120, 150, 90 ], [ 90, 90,  0 ] ]
            135: [ [  45, 135, 90 ], [ 135, 135, 90 ], [ 90, 90,  0 ] ]
            150: [ [  60, 150, 90 ], [ 150, 120, 90 ], [ 90, 90,  0 ] ]
            165: [ [  75, 165, 90 ], [ 165, 105, 90 ], [ 90, 90,  0 ] ]
            180: [ [  90, 180, 90 ], [ 180,  90, 90 ], [ 90, 90,  0 ] ]
               :
            195: [ [ 105, 165, 90 ], [ 165,  75, 90 ], [ 90, 90,  0 ] ]
            210: [ [ 120, 150, 90 ], [ 150,  60, 90 ], [ 90, 90,  0 ] ]
            225: [ [ 135, 135, 90 ], [ 135,  45, 90 ], [ 90, 90,  0 ] ]
            240: [ [ 150, 120, 90 ], [ 120,  30, 90 ], [ 90, 90,  0 ] ]
            255: [ [ 165, 105, 90 ], [ 105,  15, 90 ], [ 90, 90,  0 ] ]
            270: [ [ 180,  90, 90 ], [  90,   0, 90 ], [ 90, 90,  0 ] ]
               :
            285: [ [ 165,  75, 90 ], [  75,  15, 90 ], [ 90, 90,  0 ] ]
            300: [ [ 150,  60, 90 ], [  60,  30, 90 ], [ 90, 90,  0 ] ]
            315: [ [ 135,  45, 90 ], [  45,  45, 90 ], [ 90, 90,  0 ] ]
            330: [ [ 120,  30, 90 ], [  30,  60, 90 ], [ 90, 90,  0 ] ]
            345: [ [ 105,  15, 90 ], [  15,  75, 90 ], [ 90, 90,  0 ] ]
            359: [ [  91,   1, 90 ], [   1,  89, 90 ], [ 90, 90,  0 ] ]
         */
         var TMP_dç_cx = svl.elemento( 1, svl.elemento( 1, TMP_dç ) );
         var TMP_dç_cy = svl.elemento( 2, svl.elemento( 1, TMP_dç ) );
             TMP_dç    = TMP_dç_cx <= 90 ? TMP_dç_cy : (360-TMP_dç_cy);
       }
       return(TMP_dç);
   }



   svl.pc = function()
   // para o centro
   {

       svl.mudedç(def.dç);
       svl.mudepos([def.coorx,def.coory,def.coorz]);
//     svl.mudet(def.coort);

   }



   svl.pca = function()
   // para o centro apague
   {
       svl.pc();
       svl.ad();
   }



   svl.mudepos = function(ARG_lista_coordenadas)
   // mude posição
   {
       var TMP_coorx = svl.elemento(1,ARG_lista_coordenadas);
       var TMP_coory = svl.elemento(2,ARG_lista_coordenadas);
       if (svl.nel(ARG_lista_coordenadas)==2) ARG_lista_coordenadas = svl.sn(ARG_lista_coordenadas,0);
       var TMP_coorz = svl.elemento(3,ARG_lista_coordenadas);
       var TMP_svg_x_novo = TMP_coorx / ( 1 - TMP_coorz / svl.perpectiva );
       var TMP_svg_y_novo = TMP_coory / ( 1 - TMP_coorz / svl.perpectiva );
       for(var TAT_i in svl.v_quem)
       {
         var TAT_ind    = svl.v_quem   [TAT_i  ];
         var TMP_svg_x  = svl.svg_x    [TAT_ind];
         var TMP_svg_y  = svl.svg_y    [TAT_ind];
         var TMP_lápis  = svl.v_lápis  [TAT_ind];
         var TMP_cl     = svl.v_cl     [TAT_ind];
         var TMP_borrão = svl.v_borrão [TAT_ind];
         var TMP_el     = svl.v_el     [TAT_ind];
         var TMP_ol     = svl.v_ol     [TAT_ind];
         var TMP_ponta  = svl.pontas(svl.v_ponta[TAT_ind]);
         var TMP_junta  = svl.juntas(svl.v_junta[TAT_ind]);
         var TMP_traço  = svl.v_traço  [TAT_ind].join(" ");
         var TMP_tat    = svg.tat      [TAT_ind];
         if ( TMP_lápis && JSON.stringify( svl.pos() ) != JSON.stringify( [ TMP_coorx , TMP_coory , TMP_coorz ] ) )
         {

           var blur = 'blur' + TMP_borrão;
           var elem = aux.$(blur);
           if ( typeof(elem) == 'undefined' || elem == null )
           {
             var definições = aux.$ ("definições");
             var filtro     = document.createElementNS( aux.svg, "filter" );
             filtro.setAttribute( "id"          , blur            );
             filtro.setAttribute( "filterUnits" , "userSpaceOnUse" );
             filtro.innerHTML = '<feGaussianBlur stdDeviation="' + TMP_borrão + '" />' ;
             definições.appendChild(filtro);
           }

           svg.rastro = document.createElementNS( aux.svg, "path" );
           if (TMP_borrão>0) svg.rastro.setAttribute( "filter" , "url(#"+blur+")" );
           svg.rastro.setAttribute( "class"            , "traço" );
           svg.rastro.setAttribute( "d"                , "M "+TMP_svg_x+" "+TMP_svg_y+" L "+TMP_svg_x_novo+" "+TMP_svg_y_novo );
           svg.rastro.setAttribute( "stroke"           , svl.elemento(TMP_cl+1,svl.paleta()) );
           svg.rastro.setAttribute( "stroke-width"     , TMP_el                              );
           svg.rastro.setAttribute( "stroke-opacity"   , TMP_ol                              );
           svg.rastro.setAttribute( "stroke-linecap"   , TMP_ponta                           );
           svg.rastro.setAttribute( "stroke-linejoin"  , TMP_junta                           );
           svg.rastro.setAttribute( "stroke-dasharray" , TMP_traço                           );
           svg.tela.appendChild(svg.rastro);
           aux.paracima(TMP_tat);

         }
         svl.svg_x  [TAT_ind] = TMP_svg_x_novo;
         svl.svg_y  [TAT_ind] = TMP_svg_y_novo;
         svl.v_coorx[TAT_ind] = TMP_coorx;
         svl.v_coory[TAT_ind] = TMP_coory;
         svl.v_coorz[TAT_ind] = TMP_coorz;
       }
       aux.posicione();

       if ( svl.édinâmico() )
          svl.mudelimites
          (
            [
              svl.coorx()-svl.borda() < svl.min_coorx() ? svl.coorx()-svl.borda() : svl.min_coorx() ,
              svl.coory()-svl.borda() < svl.min_coory() ? svl.coory()-svl.borda() : svl.min_coory()
            ] ,
            [
              svl.coorx()+svl.borda() > svl.max_coorx() ? svl.coorx()+svl.borda() : svl.max_coorx() ,
              svl.coory()+svl.borda() > svl.max_coory() ? svl.coory()+svl.borda() : svl.max_coory()
            ]
          )

   }

   svl.mudex = function(ARG_coorx)
   // mude coordenada x
   {
     svl.mudepos([ARG_coorx,svl.coory(),svl.coorz()]);
   }

   svl.mudey = function(ARG_coory)
   // mude coordenada y
   {
     svl.mudepos([svl.coorx(),ARG_coory,svl.coorz()]);
   }

   svl.mudez = function(ARG_coorz)
   // mude coordenada z
   {
     svl.mudepos([svl.coorx(),svl.coory(),ARG_coorz]);
   }



   svl.mudedç = function(ARG_direção_2d_ou_3d)
   // mude direção
   //
   // A direção tridimensional da tartaruga é definida pela trinca das direções das dimensões da tartaruga.
   // A direção de uma dimensão da tartaruga e formada pela trinca dos três ângulos formados entre os eixos e o vetor suporte dessa dimensão.
   // Os ângulos associados ao comprimento, são cx, cy e cz. À largura, lx, ly e lz. E à altura, ax, ay e az.
   {
       for(var TAT_i in svl.v_quem)
       {
         if (svl.énúmero(ARG_direção_2d_ou_3d))
         {
           var TMP__cy = ARG_direção_2d_ou_3d      <= 180 ? ARG_direção_2d_ou_3d      :            360 - ( ARG_direção_2d_ou_3d      )         ;
           var TMP__cx = ARG_direção_2d_ou_3d - 90 <= 180 ? ARG_direção_2d_ou_3d - 90 : svl.resto( 360 - ( ARG_direção_2d_ou_3d - 90 ) , 360 ) ;
           var TMP__cz =  90;
           var TMP__lx =     TMP__cy;
           var TMP__ly = 180-TMP__cx;
           var TMP__lz =  90;
           var TMP__ax =  90;
           var TMP__ay =  90;
           var TMP__az =   0;
         }
         else
         {
           var TMP__cx = svl.resto( 360 + svl.elemento( 1, svl.elemento( 1, ARG_direção_2d_ou_3d ) ), 360 );
           var TMP__cy = svl.resto( 360 + svl.elemento( 2, svl.elemento( 1, ARG_direção_2d_ou_3d ) ), 360 );
           var TMP__cz = svl.resto( 360 + svl.elemento( 3, svl.elemento( 1, ARG_direção_2d_ou_3d ) ), 360 );
           var TMP__lx = svl.resto( 360 + svl.elemento( 1, svl.elemento( 2, ARG_direção_2d_ou_3d ) ), 360 );
           var TMP__ly = svl.resto( 360 + svl.elemento( 2, svl.elemento( 2, ARG_direção_2d_ou_3d ) ), 360 );
           var TMP__lz = svl.resto( 360 + svl.elemento( 3, svl.elemento( 2, ARG_direção_2d_ou_3d ) ), 360 );
           var TMP__ax = svl.resto( 360 + svl.elemento( 1, svl.elemento( 3, ARG_direção_2d_ou_3d ) ), 360 );
           var TMP__ay = svl.resto( 360 + svl.elemento( 2, svl.elemento( 3, ARG_direção_2d_ou_3d ) ), 360 );
           var TMP__az = svl.resto( 360 + svl.elemento( 3, svl.elemento( 3, ARG_direção_2d_ou_3d ) ), 360 );
         }
         svl.v_dç[svl.v_quem[TAT_i]] = svl.lista
         (
           svl.lista( TMP__cx, TMP__cy, TMP__cz ),
           svl.lista( TMP__lx, TMP__ly, TMP__lz ),
           svl.lista( TMP__ax, TMP__ay, TMP__az )
         );
         aux.posicione();
       }
   }



   svl.pf = async function(ARG_número)
   // ande para frente
   {
       svl.cada( ()=>
       {
         var TMP_coorx = ( svl.coorx() + ARG_número * aux.cosdç('c','x') );
         var TMP_coory = ( svl.coory() + ARG_número * aux.cosdç('c','y') );
         var TMP_coorz = ( svl.coorz() + ARG_número * aux.cosdç('c','z') );
         svl.mudepos( [ TMP_coorx, TMP_coory, TMP_coorz ] );
      // await svl.espere(1000);
       });
   }



   svl.pd = async function(ARG_ângulo)
   // vire para a direita
   {
       var TMP_ccx = aux.cosdç('c','x') * svl.cos(ARG_ângulo) + aux.cosdç('l','x') * svl.sen(ARG_ângulo);
       var TMP_ccy = aux.cosdç('c','y') * svl.cos(ARG_ângulo) + aux.cosdç('l','y') * svl.sen(ARG_ângulo);
       var TMP_ccz = aux.cosdç('c','z') * svl.cos(ARG_ângulo) + aux.cosdç('l','z') * svl.sen(ARG_ângulo);
       var TMP_clx = aux.cosdç('l','x') * svl.cos(ARG_ângulo) - aux.cosdç('c','x') * svl.sen(ARG_ângulo);
       var TMP_cly = aux.cosdç('l','y') * svl.cos(ARG_ângulo) - aux.cosdç('c','y') * svl.sen(ARG_ângulo);
       var TMP_clz = aux.cosdç('l','z') * svl.cos(ARG_ângulo) - aux.cosdç('c','z') * svl.sen(ARG_ângulo);
       var TMP__cx = aux.dçtan(TMP_ccx);
       var TMP__cy = aux.dçtan(TMP_ccy);
       var TMP__cz = aux.dçtan(TMP_ccz);
       var TMP__lx = aux.dçtan(TMP_clx);
       var TMP__ly = aux.dçtan(TMP_cly);
       var TMP__lz = aux.dçtan(TMP_clz);
       var TMP__ax = svl.elemento( 1, svl.elemento( 3, svl.dç() ) );
       var TMP__ay = svl.elemento( 2, svl.elemento( 3, svl.dç() ) );
       var TMP__az = svl.elemento( 3, svl.elemento( 3, svl.dç() ) );
       svl.mudedç
       (
         svl.lista
         (
           svl.lista( TMP__cx, TMP__cy, TMP__cz ),
           svl.lista( TMP__lx, TMP__ly, TMP__lz ),
           svl.lista( TMP__ax, TMP__ay, TMP__az )
         )
       );
    // await svl.espere(1000);
       // DEF
       //   [  0 1  0 ] [ 1  0  0 ] [  0  0 1 ]
       //   [ 90 0 90 ] [ 0 90 90 ] [ 90 90 0 ]
       //
       // pd(0)
       //   [  0 1  0 ] [ 1  0  0 ] [  0  0 1 ]
       //   [ 90 0 90 ] [ 0 90 90 ] [ 90 90 0 ]
       //
       // pd(30)
       //   [  0.5  0.866  0 ] [  0.866  -0.5  0 ] [  0  0 1 ]
       //   [ 60   30     90 ] [ 30     120   90 ] [ 90 90 0 ]
       //
       // pd(45)
       //   [  0.707  0.707  0 ] [  0.707  -0.707  0 ] [  0  0 1 ]
       //   [ 45     45     90 ] [ 45     135     90 ] [ 90 90 0 ]
//     aux.modç();
   }



   svl.cpc = function(ARG_ângulo)
   // cabeceie para cima
   {
       ARG_ângulo = - ARG_ângulo;
       var TMP_ccx = aux.cosdç('c','x') * svl.cos(ARG_ângulo) - aux.cosdç('a','x') * svl.sen(ARG_ângulo);
       var TMP_ccy = aux.cosdç('c','y') * svl.cos(ARG_ângulo) - aux.cosdç('a','y') * svl.sen(ARG_ângulo);
       var TMP_ccz = aux.cosdç('c','z') * svl.cos(ARG_ângulo) - aux.cosdç('a','z') * svl.sen(ARG_ângulo);
       var TMP_cax = aux.cosdç('a','x') * svl.cos(ARG_ângulo) + aux.cosdç('c','x') * svl.sen(ARG_ângulo);
       var TMP_cay = aux.cosdç('a','y') * svl.cos(ARG_ângulo) + aux.cosdç('c','y') * svl.sen(ARG_ângulo);
       var TMP_caz = aux.cosdç('a','z') * svl.cos(ARG_ângulo) + aux.cosdç('c','z') * svl.sen(ARG_ângulo);
       var TMP__cx = aux.dçtan(TMP_ccx);
       var TMP__cy = aux.dçtan(TMP_ccy);
       var TMP__cz = aux.dçtan(TMP_ccz);
       var TMP__lx = svl.elemento( 1, svl.elemento( 2, svl.dç() ) );
       var TMP__ly = svl.elemento( 2, svl.elemento( 2, svl.dç() ) );
       var TMP__lz = svl.elemento( 3, svl.elemento( 2, svl.dç() ) );
       var TMP__ax = aux.dçtan(TMP_cax);
       var TMP__ay = aux.dçtan(TMP_cay);
       var TMP__az = aux.dçtan(TMP_caz);
       svl.mudedç
       (
         svl.lista
         (
           svl.lista( TMP__cx, TMP__cy, TMP__cz ),
           svl.lista( TMP__lx, TMP__ly, TMP__lz ),
           svl.lista( TMP__ax, TMP__ay, TMP__az )
         )
       );
   }



   svl.rpd = function(ARG_ângulo)
   // role para a direita
   {
       var TMP_clx = aux.cosdç('l','x') * svl.cos(ARG_ângulo) + aux.cosdç('a','x') * svl.sen(ARG_ângulo);
       var TMP_cly = aux.cosdç('l','y') * svl.cos(ARG_ângulo) + aux.cosdç('a','y') * svl.sen(ARG_ângulo);
       var TMP_clz = aux.cosdç('l','z') * svl.cos(ARG_ângulo) + aux.cosdç('a','z') * svl.sen(ARG_ângulo);
       var TMP_cax = aux.cosdç('a','x') * svl.cos(ARG_ângulo) - aux.cosdç('l','x') * svl.sen(ARG_ângulo);
       var TMP_cay = aux.cosdç('a','y') * svl.cos(ARG_ângulo) - aux.cosdç('l','y') * svl.sen(ARG_ângulo);
       var TMP_caz = aux.cosdç('a','z') * svl.cos(ARG_ângulo) - aux.cosdç('l','z') * svl.sen(ARG_ângulo);
       var TMP__cx = svl.elemento( 1, svl.elemento( 1, svl.dç() ) );
       var TMP__cy = svl.elemento( 2, svl.elemento( 1, svl.dç() ) );
       var TMP__cz = svl.elemento( 3, svl.elemento( 1, svl.dç() ) );
       var TMP__lx = aux.dçtan(TMP_clx);
       var TMP__ly = aux.dçtan(TMP_cly);
       var TMP__lz = aux.dçtan(TMP_clz);
       var TMP__ax = aux.dçtan(TMP_cax);
       var TMP__ay = aux.dçtan(TMP_cay);
       var TMP__az = aux.dçtan(TMP_caz);
       svl.mudedç
       (
         svl.lista
         (
           svl.lista( TMP__cx, TMP__cy, TMP__cz ),
           svl.lista( TMP__lx, TMP__ly, TMP__lz ),
           svl.lista( TMP__ax, TMP__ay, TMP__az )
         )
       );
   }



   svl.pt  = function(ARG_número) { svl.pf ( - ARG_número ); } // ande para trás
   svl.pe  = function(ARG_ângulo) { svl.pd ( - ARG_ângulo ); } // vire para a esquerda
   svl.cpb = function(ARG_ângulo) { svl.cpc( - ARG_ângulo ); } // cabeceie para baixo
   svl.rpe = function(ARG_ângulo) { svl.rpd( - ARG_ângulo ); } // role para a esquerda



   svl.dçpara = function(ARG_lista_coordenadas)
   // direção para ponto ARG_lista_coordenadas
   {
     var TMP_coorx = svl.pri(ARG_lista_coordenadas);
     var TMP_coory = svl.ult(ARG_lista_coordenadas);
     return( svl.resto( 360 + svl.arctan( TMP_coorx-svl.coorx() , TMP_coory-svl.coory() ) , 360 ) );
   }



   svl.distância = function(ARG_lista_coordenadas)
   // distância até o ponto ARG_lista_coordenadas
   {
     var TMP_coorx = svl.elemento(1, ARG_lista_coordenadas);
     var TMP_coory = svl.elemento(2, ARG_lista_coordenadas);
     var TMP_coorz = svl.elemento(3, ARG_lista_coordenadas);
     return(
             svl.rq(
                     Math.pow( svl.coorx()-TMP_coorx, 2 ) +
                     Math.pow( svl.coory()-TMP_coory, 2 ) +
                     Math.pow( svl.coorz()-TMP_coorz, 2 )
                   )
           );
   }



   svl.escondida = function()
   {
     return( aux.cosdç('a','x')*svl.coorx() + aux.cosdç('a','y')*svl.coory() + aux.cosdç('a','z')*(svl.coorz()-svl.f) > 0 ? svl.verd() : svl.falso() );
   }



/*

  Primitivas para Múltiplas Tartarugas
  ---------- ---- --------- ----------

*/



   svl.atat = function(ARG_número_ou_lista_números)
   // atenção tartaruga
   {
       svl.v_quem = svl.sn([], ARG_número_ou_lista_números);
       for(var TAT_i in svl.v_quem)
       {
         if (svl.não(svl.temvalor( svg.tat[svl.v_quem[TAT_i]] )))
         {
           var TAT_ind = svl.v_quem[TAT_i];
           svl.svg_x       [TAT_ind] = def.svg_x     ;
           svl.svg_y       [TAT_ind] = def.svg_y     ;
           svl.v_coorx     [TAT_ind] = def.coorx     ;
           svl.v_coory     [TAT_ind] = def.coory     ;
           svl.v_coorz     [TAT_ind] = def.coorz     ;
//         svl.v_coort     [TAT_ind] = def.coort     ;
           svl.v_dç        [TAT_ind] = def.dç        ;
           svl.v_tt        [TAT_ind] = def.tt        ;
           svl.v_proporção [TAT_ind] = def.proporção ;
           svl.v_ct        [TAT_ind] = def.ct        ;
           svl.v_ot        [TAT_ind] = def.ot        ;
           svl.v_évisível  [TAT_ind] = svl.falso()   ;
           svl.v_temsombra [TAT_ind] = def.temsombra ;
           svl.v_cl        [TAT_ind] = def.cl        ;
           svl.v_borrão    [TAT_ind] = def.borrão    ;
           svl.v_el        [TAT_ind] = def.el        ;
           svl.v_ol        [TAT_ind] = def.ol        ;
           svl.v_ponta     [TAT_ind] = def.ponta     ;
           svl.v_junta     [TAT_ind] = def.junta     ;
           svl.v_traço     [TAT_ind] = def.traço     ;
           svl.v_lápis     [TAT_ind] = def.lápis     ;
//         svl.v_vel       [TAT_ind] = def.vel       ;
           svl.v_retardo   [TAT_ind] = def.retardo   ;
           svg.tat         [TAT_ind] = document.createElementNS( aux.svg, "use" );
           var TMP_ct      = svl.v_ct       [TAT_ind];
           var TMP_ot      = svl.v_ot       [TAT_ind];
           var TMP_visib   = svl.v_évisível [TAT_ind] ? "visible" : "hidden";
           svg.tat         [TAT_ind].setAttribute( "id"           , "tat-"+TAT_ind                      );
           svg.tat         [TAT_ind].setAttribute( "fill"         , svl.elemento(TMP_ct+1,svl.paleta()) ); // ct
           svg.tat         [TAT_ind].setAttribute( "fill-opacity" , TMP_ot                              ); // ot
           svg.tat         [TAT_ind].setAttribute( "style"        , "visibility:"+TMP_visib             ); // évisível
           svg.tat         [TAT_ind].setAttribute( "href"         , "#def-tat"                          );
           svg.tela.appendChild(svg.tat[TAT_ind]);
         }
       }
   }



   svl.quem = function()
   // quem foi chamado atenção
   {
     return( svl.nel(svl.v_quem)==1 ? svl.pri(svl.v_quem) : svl.v_quem );
   }



   svl.todas = function()
   // todas as tartarugas
   {
     return( svl.v_todas );
   }



   svl.cada = function(ARG_função_comandos)
   // cada tartaruga
   {
       for(var TAT_i in svl.v_quem)
       {
         svl.sótat( svl.v_quem[TAT_i], function(){ ARG_função_comandos(); } );
       }
   }



   svl.sótat = function(ARG_tat_número, ARG_função_comandos)
   // somente tartaruga
   {
       var TMP_quem_cópia = svl.v_quem;
       svl.atat(ARG_tat_número);
       ARG_função_comandos();
       svl.atat(TMP_quem_cópia );
   }



/*

  Primitivas Novas
  ---------- -----

*/



   svl.comsombra = function()
   // tartaruga faça sombra
   {
       for(var TAT_i in svl.v_quem)
       {
         svl.v_temsombra[svl.v_quem[TAT_i]] = svl.verd();
         var style = "visibility:" + ( svl.v_évisível[svl.v_quem[TAT_i]] ? "visible" : "hidden" ) + ";";
         style += "filter: drop-shadow(.5px -.5px .5px rgba(0,0,0,1));";
         svg.tat[svl.v_quem[TAT_i]].setAttribute( "style", style );
       }
   }

   svl.semsombra = function()
   // tartaruga faça sombra
   {
       for(var TAT_i in svl.v_quem)
       {
         svl.v_temsombra[svl.v_quem[TAT_i]] = svl.falso();
         var style = "visibility:" + ( svl.v_évisível[svl.v_quem[TAT_i]] ? "visible" : "hidden" ) + ";";
         svg.tat[svl.v_quem[TAT_i]].setAttribute( "style", style );
       }
   }

   svl.temsombra = function()
   // tartaruga tem sombra
   {
     return( svl.v_temsombra[svl.pri(svl.v_quem)] );
   }



   svl.lápis = function()
   // usa lápis
   {
     return( svl.v_lápis[svl.pri(svl.v_quem)] );
   }



   svl.mudeot = function(ARG_opacidade_razão)
   // mude opacidade tartaruga
   {
       for(var TAT_i in svl.v_quem)
       {
         svl.v_ot[svl.v_quem[TAT_i]] = ARG_opacidade_razão;
         svg.tat [svl.v_quem[TAT_i]].setAttribute( "fill-opacity", svl.ot() );
       }
   }

   svl.ot = function()
   // opacidade tartaruga
   {
     return( svl.v_ot[svl.pri(svl.v_quem)] );
   }



   svl.mudett = function(ARG_tamanho_razão)
   // mude tamanho tartaruga
   {
       for(var TAT_i in svl.v_quem)
       {
         svl.v_tt[svl.v_quem[TAT_i]] = ARG_tamanho_razão;
         aux.posicione();
       }
   }

   svl.tt = function()
   // tamanho tartaruga
   {
     return( svl.v_tt[svl.pri(svl.v_quem)] );
   }



   svl.mudeel = function(ARG_espessura_número)
   // mude espessura lápis
   {
       for(var TAT_i in svl.v_quem)
       {
         svl.v_el[svl.v_quem[TAT_i]] = ARG_espessura_número;
       }
   }

   svl.el = function()
   // espessura lápis
   // PROBLEMA: 'el' é a contração da primitiva 'elimine'
   {
     return( svl.v_el[svl.pri(svl.v_quem)] );
   }



   svl.mudeol = function(ARG_opacidade_razão)
   // mude opacidade lápis
   {
       for(var TAT_i in svl.v_quem)
       {
         svl.v_ol[svl.v_quem[TAT_i]] = ARG_opacidade_razão;
       }
   }

   svl.ol = function()
   // opacidade lápis
   {
     return( svl.v_ol[svl.pri(svl.v_quem)] );
   }



   svl.mudeponta = function(ARG_ponta_tipo)
   // mude tipo ponta
   {
       for(var TAT_i in svl.v_quem)
       {
         svl.v_ponta[svl.v_quem[TAT_i]] = ARG_ponta_tipo;
       }
   }

   svl.ponta = function()
   // tipo ponta
   {
     return( svl.v_ponta[svl.pri(svl.v_quem)] );
   }

   svl.pontas = function(ARG_ponta_número)
   // devolva os tipos de ponta
   {
     svl.v_pontas =
     [
       "butt",
       "round",
       "square",
     ]
     return( svl.temvalor(ARG_ponta_número) ? svl.v_pontas[ARG_ponta_número] : svl.v_pontas );
   }



   svl.mudejunta = function(ARG_junta_tipo)
   // mude tipo junta
   {
       for(var TAT_i in svl.v_quem)
       {
         svl.v_junta[svl.v_quem[TAT_i]] = ARG_junta_tipo;
       }
   }

   svl.junta = function()
   // tipo junta
   {
     return( svl.v_junta[svl.pri(svl.v_quem)] );
   }

   svl.juntas = function(ARG_junta_número)
   // devolva os tipos de junta
   {
     svl.v_juntas =
     [
       "miter",
       "round",
       "bevel",
     ]
     return( svl.temvalor(ARG_junta_número) ? svl.v_juntas[ARG_junta_número] : svl.v_juntas );
   }



   svl.mudetraço = function(ARG_traço_lista=[0])
   // mude tipo traço
   {
       for(var TAT_i in svl.v_quem)
       {
         svl.v_traço[svl.v_quem[TAT_i]] = ARG_traço_lista;
       }
   }

   svl.traço = function()
   // tipo traço
   {
     return( svl.v_traço[svl.pri(svl.v_quem)] );
   }



/*
   svl.muderetardo = function(ARG_número_segundos)
   {
       for(var TAT_i in svl.v_quem)
       {
         svl.v_retardo[svl.v_quem[TAT_i]] = ARG_número_segundos*1000;
       }
   }

   svl.retardo = function()
   {
     return( svl.v_retardo[svl.pri(svl.v_quem)] );
   }
*/



   svl.mudeqtat = function(ARG_quantidade_tartarugas)
   // mude quantidade de tartarugas disponíveis
   {
       svl.v_todas = [];
       for(var TAT_i=0; TAT_i<ARG_quantidade_tartarugas; TAT_i++) svl.v_todas[TAT_i] = TAT_i;
   }

   svl.qtat = function()
   // quantas tartarugas estão disponíveis
   {
       return svl.nel(svl.todas());
   }



   svl.mudeqfig = function(ARG_quantidade_figuras)
   // mude quantidade de figuras disponíveis
   {
    // svl.v_todasf = [];
    // for(var TAT_i=0; TAT_i<ARG_quantidade_figuras; TAT_i++) svl.v_todasf[TAT_i] = TAT_i;
   }

   svl.qfig = function()
   // quantas figuras estão disponíveis
   {
       return svl.nel(svl.todasf());
   }

   svl.todasf = function()
   // todas as figuras
   {
     return( svl.v_todasf );
   }



   svl.pde = function(ARG_número)
   // para direita espelhado
   {
   //  svl .atat( [ 0, 1 ] );
       svl.sótat( 0, function(){ svl.pd(ARG_número) } );
       svl.sótat( 1, function(){ svl.pe(ARG_número) } );
   }

   svl.pee = function(ARG_número)
   // para esquerda espelhado
   {
   //  svl .atat( [ 0, 1 ] );
       svl.sótat( 0, function(){ svl.pe(ARG_número) } );
       svl.sótat( 1, function(){ svl.pd(ARG_número) } );
   }



   // Embedding external SVG in HTML for JavaScript manipulation <http://stackoverflow.com/a/14070928>
   // <http://developer.mozilla.org/docs/Web/API/XMLHttpRequest/Requisicoes_sincronas_e_assincronas>
   // <http://developer.mozilla.org/docs/Archive/Misc_top_level/Same-origin_policy_for_file:_URIs>
   //   about:config
   //     security.fileuri.strict_origin_policy
   //       false
   svl.carreguefig = function(nome, origem, sxy=[1,0,0])
   {
     var xhr = new XMLHttpRequest();
     if (svl.não(origem.match(/\//))) origem = ( location.host == 'arkanon.gitlab.io' ? '' : '../' ) + 'figs/' + origem + '.svg';
     xhr.open( "GET", origem, true );
     xhr.overrideMimeType("image/svg+xml");
     xhr.onload = function(e)
     {
       if (xhr.readyState === 4)
          if (xhr.status === 200)
          {
         // console.log(xhr.responseText);

            var g0 = document.createElementNS( aux.svg, "g" );
            g0.setAttribute( "id"        , nome );

            var g1 = document.createElementNS( aux.svg, "g" );
            g1.setAttribute( "id"        , nome + "-scale" );
            g1.setAttribute( "transform" , "scale(" + sxy[0] + " " + -sxy[0] + ")" );

            var g2 = document.createElementNS( aux.svg, "g" );
            g2.setAttribute( "id"        , nome + "-translate" );
            g2.setAttribute( "transform" , "translate(" + sxy[1] + " " + sxy[2] + ")" );

         // var fig = xhr.responseXML.documentElement;
            var fig = document.adoptNode( new DOMParser().parseFromString( xhr.responseText.replace(/fill-opacity:1/g, ""), "image/svg+xml" ).documentElement );
            w = fig.width.baseVal.value;
            h = fig.height.baseVal.value;
            fig.setAttribute( "id"        , nome + "-ori" );
            fig.setAttribute( "transform" , "translate(" + -w/2 + " " + -h/2 + ")" ); // <http://developer.mozilla.org/docs/Web/SVG/Attribute/transform>

            var elemento = aux.$(nome);
            if ( typeof(elemento) != 'undefined' && elemento != null ) elemento.remove();

            g2.appendChild(fig);
            g1.appendChild(g2);
            g0.appendChild(g1);
            svg.defs.appendChild(g0);

            svg.tat[0].setAttribute( "href" , "#" + nome );

          }
          else console.error(xhr.statusText);
     }
     xhr.onerror = function(e)
     {
       console.error(xhr.statusText);
     }
     xhr.send(null);
   }

   // fig
   // copiafig
   // criafigl
   // mudefig
   // listafig
   // mostrearq



// EOF
