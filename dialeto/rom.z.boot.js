/*

  $modifed = 2014/02/26 (Wed) 03:46:05 (BRS)
  $created = 2009/06/15 (Seg) 21:14:16
  $counted = 2009/07/04 (Sáb) 14:56:39

  <http://vim.wikia.com/wiki/Folding>
  za - togle
  zR - open  all
  zM - close all

*/



     aux.emerja('svl');



     svl.número_versão = "0.19";



// ------------
//   DEFAULTS
// ------------

     def.tat        = aux.$("def-tat");

     def.min_coorx  = -150;
     def.min_coory  = -150;
     def.max_coorx  =  150;
     def.max_coory  =  150;

  // def.min_coorx  = - innerWidth  / 2;
  // def.min_coory  = - innerHeight / 2;
  // def.max_coorx  =   innerWidth  / 2;
  // def.max_coory  =   innerHeight / 2;

     def.borda      = 0;
     def.dinâmico   = svl.falso();

     def.cf         = 5; // 1=preto, 5=azul, 15=branco
     def.of         = 1;

     def.qtat       = 30;
     def.qfig       = 60;
     def.quem       = 0;

     def.svg_x      = 0;
     def.svg_y      = 0;
     def.coorx      = 0;
     def.coory      = 0;
     def.coorz      = 0;
     def.coort      = 0;
     def.dç         = [[90,0,90],[0,90,90],[90,90,0]];

     def.tt         = 1;
     def.proporção  = 1;
     def.resolução  = 96;

     def.ct         = 15;
     def.ot         = 1;
     def.évisível   = svl.verd();
     def.temsombra  = svl.falso();
     def.cl         = 15;
     def.borrão     = 0;
     def.el         = 1;
     def.ol         = 1;
     def.ponta      = 1
     def.junta      = 1
     def.traço      = [0];
     def.lápis      = svl.verd();
     def.lápis      = svl.verd();
     def.vel        = 0;
     def.retardo    = .3;
     def.useretardo = svl.falso();
     def.pasta      = location.href;



// -------------
//   CONSTANTS
// -------------

     aux.audioCtx            = new(window.AudioContext || window.webkitAudioContext)();

  // aux.logo_margin         = 3;
  // aux.logo_scale          = .08;
  // aux.logo_opacity        = 1;
  // aux.logo_visibility     = "visible";
//// aux.logo_visibility     = "hidden";
  // aux.logo_source         = "svl-logo.svg#layer1";

  // aux.validate_margin     = 3;
  // aux.validate_scale      = .5;
  // aux.validate_opacity    = 1;
  // aux.validate_visibility = "visible";
//// aux.validate_visibility = "hidden";
  // aux.validate_source     = "w3c-svg-1.1.svg#layer1";
  // aux.validate_href       = "http://validator.w3.org/check?uri=referer&group=1&verbose=1&No200=1";
  // aux.validate_title      = "SVG 1.1";
  // aux.validate_alt        = "SVG";

     aux.svg                 = "http://www.w3.org/2000/svg";

     svg.raiz                = aux.$("raiz");
     svg.defs                = aux.$("definições");
     svg.def_ponto           = aux.$("def-ponto");
     svg.def_tat             = aux.$("def-tat");
     svg.tela                = aux.$("tela");
     svg.fundo               = aux.$("fundo");
     svg.logo                = aux.$("logo");
     svg.validate            = aux.$("validate");
     svg.save                = aux.$("save");
     svg.load                = aux.$("load");



// -----------------
//   VARIABLE INIT
// -----------------

     svl.v_paleta    = [];
     svl.v_desenhos  = {};

     svl.v_todas     = [];
     svl.v_quem      = [];

     svl.svg_x       = [];
     svl.svg_y       = [];
     svl.v_coorx     = [];
     svl.v_coory     = [];
     svl.v_coorz     = [];
     svl.v_coort     = [];

     svl.v_dç        = [];
     svl.v_vel       = [];

     svl.v_ct        = [];
     svl.v_ot        = [];
     svl.v_tt        = [];

     svl.v_cl        = [];
     svl.v_borrão    = [];
     svl.v_el        = [];
     svl.v_ol        = [];
     svl.v_ponta     = [];
     svl.v_junta     = [];
     svl.v_traço     = [];

     svl.v_lápis     = [];
     svl.v_proporção = [];
     svl.v_retardo   = [];
     svl.v_évisível  = [];
     svl.v_temsombra = [];

     svg.tat         = [];

     svl.v_pasta     = def.pasta;

     svl.v_limpa =
       [ 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 , 255 ] // quadrado
     svl.v_todasf    =
     [
       [   7 ,  31 ,  63 , 127 , 127 , 255 , 255 , 255 , 255 , 255 , 255 , 127 , 127 ,  63 ,  31 ,   7 , 224 , 248 , 252 , 254 , 254 , 255 , 255 , 255 , 255 , 255 , 255 , 254 , 254 , 252 , 248 , 224 ] //  0  circulo
     , [  14 ,  31 ,  63 ,  63 , 127 , 127 , 127 , 127 ,  63 ,  63 ,  31 ,  15 ,   7 ,   3 ,   1 ,   0 ,  56 , 124 , 254 , 254 , 255 , 255 , 255 , 255 , 254 , 254 , 252 , 248 , 240 , 224 , 192 , 128 ] //  1  coração
     , [   0 ,  68 , 124 ,  84 , 108 ,  63 ,  63 ,  63 ,  63 ,  63 ,  63 ,  62 ,  44 ,  44 ,  72 ,   8 ,   0 ,   1 ,   2 ,   2 , 250 , 254 , 254 , 254 , 254 , 254 , 252 ,  60 ,  40 ,  88 ,  80 ,   0 ] //  2  gato
     , [   0 ,   0 ,   0 ,   0 , 128 , 128 ,  79 , 127 ,  31 ,  31 ,  28 ,  58 , 114 ,  68 ,  68 ,  66 ,   0 ,   0 ,  28 ,  59 ,  63 , 120 , 240 , 240 , 240 , 240 , 240 , 144 , 144 ,  72 ,  72 , 108 ] //  3  cachorro
     , [   0 ,   0 ,   0 ,   0 , 255 , 255 , 255 , 255 , 255 , 255 ,   0 , 192 , 192 ,   0 ,   0 ,   0 ,   0 ,   0 ,   0 ,   0 , 192 , 192 , 220 , 220 , 223 , 223 ,   0 , 195 , 195 ,   0 ,   0 ,   0 ] //  4  caminhão
     , [   0 ,   1 ,   1 ,   1 ,   1 ,   1 ,   7 ,  15 ,  15 ,  31 ,  31 ,  25 ,  25 ,  17 ,  17 ,  16 , 128 , 192 , 192 , 192 , 192 , 192 , 240 , 248 , 248 , 252 , 252 , 204 , 204 , 196 , 196 ,   4 ] //  5  foguete
     , [   8 ,   8 ,   8 , 255 , 128 , 128 , 128 , 255 ,   8 ,   8 ,   8 , 255 , 128 , 128 , 128 , 255 ,   8 ,   8 ,   8 , 255 , 128 , 128 , 128 , 255 ,   8 ,   8 ,   8 , 255 , 128 , 128 , 128 , 255 ] //  6  tijolo
     , [   0 ,   0 , 127 ,   0 , 112 , 223 , 168 , 222 , 115 ,   1 ,   0 ,   0 ,   0 ,   3 ,   0 ,   0 ,   0 ,   0 , 255 , 128 , 128 , 248 , 124 ,  62 ,  31 , 129 , 254 ,  80 ,  81 , 254 ,   0 ,   0 ] //  7  helicóptero
     , [   4 ,  16 , 130 ,  36 ,   0 ,   0 , 248 , 248 ,  41 ,  43 , 123 , 255 , 255 ,  27 ,  27 ,   0 ,   0 , 128 ,  96 , 144 ,   0 ,  56 ,  56 ,  16 ,  80 , 248 , 252 , 252 , 248 ,   6 ,   7 ,   0 ] //  8  locomotiva
     , [   0 ,   0 ,   0 ,   0 ,   0 ,   0 , 255 , 255 ,  85 ,  85 , 127 , 255 , 255 , 108 , 108 ,   0 ,   0 ,   0 ,   0 ,   0 ,   0 ,   0 , 254 , 254 ,  84 ,  84 , 252 , 254 , 255 , 108 , 108 ,   0 ] //  9  vagão
     , svl.v_limpa , svl.v_limpa , svl.v_limpa , svl.v_limpa , svl.v_limpa , svl.v_limpa , svl.v_limpa , svl.v_limpa , svl.v_limpa , svl.v_limpa , svl.v_limpa , svl.v_limpa , svl.v_limpa               // 10..22
     , svl.v_limpa , svl.v_limpa , svl.v_limpa , svl.v_limpa , svl.v_limpa , svl.v_limpa , svl.v_limpa , svl.v_limpa , svl.v_limpa , svl.v_limpa , svl.v_limpa , svl.v_limpa , svl.v_limpa               // 23..35
     , [   0 ,   1 ,   3 ,   3 ,   1 ,  51 ,  31 ,  15 ,  31 ,  31 ,  31 ,  31 ,  15 ,  23 ,  49 ,   0 ,   0 , 128 , 192 , 192 , 128 , 204 , 248 , 240 , 120 , 248 , 248 , 248 , 240 , 232 , 140 ,   0 ] // 36  tat   0° (353 -   7)
     , [   0 ,   0 ,   6 ,   2 ,   3 ,   7 ,  15 ,  15 ,  15 ,  47 ,  63 ,   7 ,   3 ,   0 ,   0 ,   0 ,   0 , 112 , 112 , 112 , 226 , 246 , 248 , 248 , 120 , 248 , 240 , 224 ,  32 ,  48 ,   0 ,   0 ] // 37  tat  15° (  8 -  22)
     , [   0 ,   0 ,  12 ,   4 ,   7 ,  15 ,  15 ,  31 ,  95 , 127 ,  15 ,   7 ,   0 ,   0 ,   0 ,   0 ,   0 , 112 , 112 , 112 , 224 , 224 , 240 , 252 , 116 , 224 , 224 , 192 , 128 , 192 ,   0 ,   0 ] // 38  tat  30° ( 23 -  37)
     , [   0 ,   0 ,   0 ,   0 ,   7 ,  15 ,  15 ,  47 ,  63 ,  15 ,  15 ,   7 ,   0 ,   0 ,   0 ,   0 ,   0 , 128 , 156 , 220 , 252 , 240 , 248 , 254 , 112 , 240 , 240 , 224 , 128 , 192 ,   0 ,   0 ] // 39  tat  45° ( 38 -  52)
     , [   0 ,   1 ,   0 ,   1 ,   7 ,  47 ,  63 ,  15 ,  15 ,  15 ,   7 ,   3 ,   2 ,   3 ,   0 ,   0 ,   0 , 128 , 128 , 206 , 254 , 254 , 240 , 240 , 112 , 252 , 228 , 128 ,   0 ,   0 ,   0 ,   0 ] // 40  tat  60° ( 53 -  67)
     , [   0 ,   0 ,   0 ,   3 ,  39 ,  63 ,  15 ,  15 ,  31 ,  31 ,  15 ,   7 ,   4 ,   6 ,   0 ,   0 ,   0 ,  48 ,  32 , 192 , 238 , 254 , 254 , 240 , 112 , 252 , 228 , 192 ,   0 ,   0 ,   0 ,   0 ] // 41  tat  75° ( 68 -  82)
     , [   0 ,   0 ,  64 , 111 ,  31 ,  63 ,  63 , 127 , 127 ,  63 ,  63 ,  31 , 111 ,  64 ,   0 ,   0 ,   0 ,   0 ,  32 ,  96 , 192 , 192 , 236 , 254 , 126 , 236 , 192 , 192 ,  96 ,  32 ,   0 ,   0 ] // 42  tat  90° ( 83 -  97)
     , [   0 ,   0 ,   6 ,   4 ,   7 ,  15 ,  31 ,  31 ,  15 ,  15 ,  63 ,  39 ,   3 ,   0 ,   0 ,   0 ,   0 ,   0 ,   0 ,   0 , 192 , 228 , 252 , 112 , 240 , 254 , 254 , 238 , 192 ,  32 ,  48 ,   0 ] // 43  tat 105° ( 98 - 112)
     , [   0 ,   0 ,   3 ,   2 ,   3 ,   7 ,  15 ,  15 ,  15 ,  63 ,  47 ,   7 ,   1 ,   0 ,   1 ,   0 ,   0 ,   0 ,   0 ,   0 , 128 , 228 , 252 , 112 , 240 , 240 , 254 , 254 , 206 , 128 , 128 ,   0 ] // 44  tat 120° (113 - 127)
     , [   0 ,   0 ,   0 ,   0 ,   7 ,  15 ,  15 ,  63 ,  47 ,  15 ,  15 ,   7 ,   0 ,   0 ,   0 ,   0 ,   0 ,   0 , 192 , 128 , 224 , 240 , 240 , 112 , 254 , 248 , 240 , 252 , 220 , 156 , 128 ,   0 ] // 45  tat 135° (128 - 142)
     , [   0 ,   0 ,   0 ,   0 ,   7 ,  15 , 127 ,  95 ,  31 ,  15 ,  15 ,   7 ,   4 ,  12 ,   0 ,   0 ,   0 ,   0 , 192 , 128 , 192 , 224 , 224 , 116 , 252 , 240 , 224 , 224 , 112 , 112 , 112 ,   0 ] // 46  tat 150° (143 - 157)
     , [   0 ,   0 ,   0 ,   3 ,   7 ,  63 ,  47 ,  15 ,  15 ,  15 ,   7 ,   3 ,   2 ,   6 ,   0 ,   0 ,   0 ,   0 ,  48 ,  32 , 224 , 240 , 248 , 120 , 248 , 248 , 246 , 226 , 112 , 112 , 112 ,   0 ] // 47  tat 165° (158 - 172)
     , [   0 ,  49 ,  23 ,  15 ,  31 ,  31 ,  31 ,  31 ,  15 ,  31 ,  51 ,   1 ,   3 ,   3 ,   1 ,   0 ,   0 , 140 , 232 , 240 , 248 , 248 , 248 , 120 , 240 , 248 , 204 , 128 , 192 , 192 , 128 ,   0 ] // 48  tat 180° (173 - 187)
     , [   0 ,   0 ,  12 ,   4 ,   7 ,  15 ,  31 ,  30 ,  31 ,  31 , 111 ,  71 ,  14 ,  14 ,  14 ,   0 ,   0 ,   0 ,   0 , 192 , 224 , 252 , 244 , 240 , 240 , 240 , 224 , 192 ,  64 ,  96 ,   0 ,   0 ] // 49  tat 195° (188 - 202)
     , [   0 ,   0 ,   3 ,   1 ,   3 ,   7 ,   7 ,  46 ,  63 ,  15 ,   7 ,   7 ,  14 ,  14 ,  14 ,   0 ,   0 ,   0 ,   0 ,   0 , 224 , 240 , 254 , 250 , 248 , 240 , 240 , 224 ,  32 ,  48 ,   0 ,   0 ] // 50  tat 210° (203 - 217)
     , [   0 ,   0 ,   3 ,   1 ,   7 ,  15 ,  15 ,  14 , 127 ,  31 ,  15 ,  63 ,  59 ,  57 ,   1 ,   0 ,   0 ,   0 ,   0 ,   0 , 224 , 240 , 240 , 252 , 244 , 240 , 240 , 224 ,   0 ,   0 ,   0 ,   0 ] // 51  tat 225° (218 - 232)
     , [   0 ,   0 ,   0 ,   0 ,   1 ,  39 ,  63 ,  14 ,  15 ,  15 , 127 , 127 , 115 ,   1 ,   1 ,   0 ,   0 ,   0 , 192 ,  64 , 192 , 224 , 240 , 240 , 240 , 252 , 244 , 224 , 128 ,   0 , 128 ,   0 ] // 52  tat 240° (233 - 247)
     , [   0 ,   0 ,   0 ,   0 ,   3 ,  39 ,  63 ,  14 ,  15 , 127 , 127 , 119 ,   3 ,   4 ,  12 ,   0 ,   0 ,   0 ,  96 ,  32 , 224 , 240 , 248 , 248 , 240 , 240 , 252 , 228 , 192 ,   0 ,   0 ,   0 ] // 53  tat 255° (248 - 262)
     , [   0 ,   0 ,   4 ,   6 ,   3 ,   3 ,  55 , 126 , 127 ,  55 ,   3 ,   3 ,   6 ,   4 ,   0 ,   0 ,   0 ,   0 ,   2 , 246 , 248 , 252 , 252 , 254 , 254 , 252 , 252 , 248 , 246 ,   2 ,   0 ,   0 ] // 54  tat 270° (263 - 277)
     , [   0 ,  12 ,   4 ,   3 , 119 , 127 , 127 ,  15 ,  14 ,  63 ,  39 ,   3 ,   0 ,   0 ,   0 ,   0 ,   0 ,   0 ,   0 , 192 , 228 , 252 , 240 , 240 , 248 , 248 , 240 , 224 ,  32 ,  96 ,   0 ,   0 ] // 55  tat 285° (278 - 292)
     , [   0 ,   1 ,   1 , 115 , 127 , 127 ,  15 ,  15 ,  14 ,  63 ,  39 ,   1 ,   0 ,   0 ,   0 ,   0 ,   0 , 128 ,   0 , 128 , 224 , 244 , 252 , 240 , 240 , 240 , 224 , 192 ,  64 , 192 ,   0 ,   0 ] // 56  tat 300° (293 - 307)
     , [   0 ,   1 ,  57 ,  59 ,  63 ,  15 ,  31 , 127 ,  14 ,  15 ,  15 ,   7 ,   1 ,   3 ,   0 ,   0 ,   0 ,   0 ,   0 ,   0 , 224 , 240 , 240 , 244 , 252 , 240 , 240 , 224 ,   0 ,   0 ,   0 ,   0 ] // 57  tat 315° (308 - 322)
     , [   0 ,  14 ,  14 ,  14 ,   7 ,   7 ,  15 ,  63 ,  46 ,   7 ,   7 ,   3 ,   1 ,   3 ,   0 ,   0 ,   0 ,   0 ,  48 ,  32 , 224 , 240 , 240 , 248 , 250 , 254 , 240 , 224 ,   0 ,   0 ,   0 ,   0 ] // 58  tat 330° (323 - 337)
     , [   0 ,  14 ,  14 ,  14 ,  71 , 111 ,  31 ,  31 ,  30 ,  31 ,  15 ,   7 ,   4 ,  12 ,   0 ,   0 ,   0 ,   0 ,  96 ,  64 , 192 , 224 , 240 , 240 , 240 , 244 , 252 , 224 , 192 ,   0 ,   0 ,   0 ] // 59  tat 345° (338 - 352)
     ];



// ---------------
//   STATUS INIT
// ---------------

/*
     svl.timeout;
     svl.f;
     svl.digse;
     svl.perpectiva;

     svl.v_cf;
     svl.v_of;
     svl.v_min_coorx;
     svl.v_min_coory;
     svl.v_max_coorx;
     svl.v_max_coory;
*/

   svl.rg();
   svl.mudepasta( ( location.host == 'arkanon.gitlab.io' ? '' : '../' ) + 'teste/featured');

/*
   var pta = window.parent.document.getElementById('entrada');
   pta.onblur = function()
   {
     aux.log(this.value);
     eval(this.value);
   }
*/

   svl.argumentos = svl.sp(location.search).split(/[&+]/);
   svl.endereço   = svl.nomearq(location.pathname) == 'tela.svg' ? svl.argumentos.shift() : location.pathname;

// svl.argumentos = svl.sp(location.hash.split(/[#,]/));
// svl.endereço   = svl.nomearq(location.pathname) == 'tela.svg' ? svl.sp(location.search).split(/[&+]/).shift() : location.pathname;

// svl.endereço   = svl.endereço ? svl.endereço : './ram.js';
// svl.prefixo    = svl.nomearq(svl.endereço).split(/\.[^.]*$/).shift();

/*
   esc
   (
     aux.dump
     (
       {
         'location.search'    : location.search,
         'location.pathname'  : location.pathname,
         'location.diretório' : aux.diretório (location.pathname),
         'location.arquivo'   : aux.arquivo(location.pathname),
         'svl.endereço'       : svl.endereço,
         'svl.diretório'      : aux.diretório (svl.endereço),
         'svl.arquivo'        : aux.arquivo(svl.endereço),
         'svl.argumentos'     : svl.argumentos,
         'svl.prefixo'        : svl.prefixo
       }
     )
   );
*/

   if (svl.endereço!='') svl.carregue(svl.endereço);

// carregue("http://www.google-analytics.com/ga.js");
// _gat._getTracker("UA-9593978-1")._trackPageview(); // desde 2009/07/02 Qui 00:00:00



   svl.esc(
            "SVL–" + svl.versão() + "\n"
          + "Arkanon 2008..2019"  + "\n"
          + "Bem-vindo ao Logo"
          );
   svl.esc();
   svl.esc(
            "As funções de acesso a arquivos externos do SVL fazem uso da API XMLHttpRequest. "
          + "Em uma cópia local do SVL, ela necessita que a política de Mesma-origem (Same-origin policy) para a URI 'file:' seja desativada." + "\n"
          + "Se você quiser fazer uso dessas funções e estiver em uma cópia local no Firefox, acesse as opções ocultas do navegador (about:config) e coloque o valor 'false' no parâmetro 'security.fileuri.strict_origin_policy'." + "\n\n"
          + "As funções de upload de arquivos precisam de permissão para abrir a janela de diálogo para a URI 'file:'" + "\n"
          + "O uso dessas funções exigirá uma exceção no bloqueio de janelas popup para sites do tipo 'file:'." + "\n"
          );

   // dom.dialog_element.enabled = true
   // browser.tabs.remote.separateFileUriProcess = true

   svl.esc();

// EOF
