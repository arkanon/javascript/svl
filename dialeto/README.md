# SVL–Scalable Vector Logo

## [Hot-Logo](http://bit.ly/2mfDiZp) para verificação/comparação

## HISTÓRICO

```
   _
  | 0.19   2019/09/15 Sun 18:20:35 -03 - tentativa de implementação da interrupção a cada passo da tartaruga com base em <<https://stackoverflow.com/a/39914235>>
  |        2019/09/15 Sun 10:35:51 -03 - | RESTART VI
  | 0.18   2019/01/20 Sun 13:23:23 -02 - correção da primitiva carimbe e carga de arquivos .svl a partir da url
  |        2019/01/19 Sat 16:02:54 -02 - | RESTART V
  | 0.17   2015/05/24 Sun 19:05:46 -03 - tentativa de adicionar botões svg, svl e interrupção a cada passo da tartaruga
  |        2015/05/24 Sun 18:53:55 -03 - | RESTART IV
  |        2014/03/03 Mon 06:45:38 -03 - readequados os testes, generalizado o teste da Curva de Koch, implementada a primitiva gravedes
  |        2014/03/02 Sun 09:56:51 -03 - reformulada a primitiva primitivas e implementada a primitiva títulos
  |        2014/03/02 Sun 09:20:43 -03 - FINALIZADA A TRANSFERENCIA PARA OS NAMESPACES
  | 0.16 - 2014/02/26 Wed 03:29:27 -03 - iniciado o confinamento do SVL em namespaces próprios
  | 0.15 - 2014/02/24 Mon 06:34:46 -03 - listagem das primitivas através da verificação de todas as funções do namespace global 'window'
  |        2014/02/24 Mon 05:57:13 -03 - correção da referência ao logo svl e ao botão svg para uso em protocolo file:
  |        2014/02/24 Mon 04:37:30 -03 - determinação da forma de uso da função carregue e dos comandos/procedimentos em svl (onload)
  |        2014/02/18 Tue 15:48:35 -03 - REMOÇÃO DA DEPENDÊNCIA FRACA DE PHP, para tornar o SVL uma aplicação web standalone
  |        2014/02/15 Sat 04:00:00 -02 - início da implementação da área de entrada interativa
  | 0.14 - 2014/02/10 Mon 01:54:20 -02 - implantada a interrupção por 'setInterval'
  | 0.13 - 2014/02/09 Sun 17:11:49 -02 - removido o código da tentativa antiga de execução pausada
  |                                      executados com sucesso todos os teste
  |        2014/02/09 Sun 05:07:47 -02 - adapatado às novas configurações e características da instalação do LUbuntu 13.04
  |                                      reativado o simple_couter e zerada a ocorrência de erros PHP
  |                                      | RESTART III
  | 0.12 - 2012/12/24 Mon 00:05:19 -02 - detectado e resolvido o bug clip-path⇐⇒clip no Firefox 17
  |                                      trocado o charset dos arquivos de Latin1 para UTF-8
  |        2012/11/29 Thu 13:54:43 -02   | RESTART II - encontrado um meio promissor para implementção do retardo. Ex: <<http://svl.lsd.org.br:8080/playground/wait/wait.html>>
  | 0.11 - 2009/10/01 Thu 08:25:21 -03 - tentativa falha de implementação do retardo
  |        2010/09/12 Sun 20:30:59 -03 - botão de validação SVG, ajustes no logotipo, análise do problema da associação da extensão .svl usando mod_rewrite
  |        2010/09/09 Thu 22:18:47 -03 - configuração do mod_rewrite, acerto dos ícones, inclusão hierárquica do CONFIG.php
  |        2010/09/07 Tue 23:52:41 -03 - configuração do mod_autoindex
  |        2010/09/07 Tue 08:35:37 -03   | retomada do projeto e qualificação como "pet project" <<http://en.wiktionary.org/wiki/pet_project>>
  |        2009/09/28 Mon 21:07:06 -03   | interrupção para pensar na
  |        2009/07/30 Thu 20:16:51 -03   |   implementação do retardo
  | 0.10 - 2009/07/30 Thu 18:24:03 -03 - INTEGRAÇÃO DO PARADIGMA DE MÚLTIPLAS TARTARUGAS AO PARADIGMA 3D E DO PARADIGMA 3D AO 2D
  | 0.9  - 2009/07/20 Mon 21:43:25 -03 - divisão da biblioteca SVL por área, implementação do paradigma de múltiplas tartarugas
  | 0.8  - 2009/07/12 Sun 23:42:29 -03 - implementação do paradigma 3D
  | 0.7  - 2009/07/02 Thu 19:19:36 -03 - primeira tentativa de implementação do retardo
  | 0.6  - 2009/07/07 Tue 19:58:12 -03 - implementação das primitivas de desenho, matemática, listas e novas primitivas
  |
   } JS não interativo
   _
  |        2009/06/15 Mon 21:14:16 -03   | RESTART
  | 0.5  - 2008/12/27 Sat 14:10:08 -02 - toolkit CSS/JS
  | 0.4  - 2008/08/01 Fri 19:06:00 -03 - layout ambiente SVL
  | 0.3  - 2008/07/31 Thu 18:42:24 -03 - layout ambiente SVL
  | 0.2  - 2008/07/28 Mon 09:58:04 -03 - tartaruga SVG
  | 0.1  - 2008/07/25 Fri 00:47:41 -03 - elementos SVG
  |        2008/07/19 Sat 15:00:00 -03   | STARTUP :-p
  |
   } Testes

```

## TODO

### Etapa 1: sintaxe JS não interativo | ram.js

#### CARACTERÍSTICAS
- tirar a atenção de tartarugas especificas
- un/ul interrompe/reinicia linha continua
- mostrar apenas variáveis definidas pelo usuário
- nomear conjunto de traços
- configurar a sombra
- mudança de cor aceitar nomes em português ou nomes/códigos html
- mudança de ponta e canto aceitar nomes em português
<!-- -->
- efeito visual para carimbe
- efeito sonoro para pf/pt/mudepos
- balão para fala/pensamento
- sintetização de voz
- matemática com precisão arbitrária
- tradução das primitivas
- tradução de texto
- implementar o kernel em JS
- arquivos do projeto em PHP apenas para registrar localmente os acessos
- utilizar a imagem de uma tartaruga como cursor
- documentar o SVL no código e num Blog <<http://www.lsd.org.br/>>
- utilizar o Google Analytics <<http://www.google.com/analytics/reporting/?id=19307880>>
- código do tela.svg com referência absoluta ao local original do kernel SVL, tornando necessário ter localmente apenas os arquivos tela.svg e ram.js

#### PRONTO
- produzir resultado das primitivas de desenho em SVG
- utilizar caracteres iso-latinos nas primitivas
- .chame 0
- mensagem de boot na console
- sombra
- carregar arquivos .svl por parâmetro na URL do tela.svg
- criar funções que aceitem funções e comandos como argumento
- não depender de PHP para interpretação dos comandos em Logo
- desnecessário usar a extensão .php nos arquivos e auto-remoção dela caso seja utilizada
- logotipo do SVL em SVG como marca d'água no fundo da tela com código mantido em arquivo separado
- mapear sistema de coordenadas do svg para o sistema cartesiano-polar do Hot Logo
- implementar as primitivas de desenho
- implementar as primitivas de matemática
- implementar as primitivas de listas
- conferir coerência da sintaxe lógica das primitivas SVL com as do Hot Logo
- implementar a curva de koch n-agonal
- implementar a primitiva 'carregue'
- implementar o paradigma de 3D
- configurar o mod_autoindex do Apache para o site do svl
- ajustar a posição dos ícones .php e .phps no layout do mod_autoindex
- implementar o paradigma de múltiplas tartarugas
- integrar o paradigma de múltiplas tartarugas às novas primitivas implementadas
- integrar o paradigma de múltiplas tartarugas às primitivas 3D
- integrar o paradigma de 3D às primitivas 2D
- utilizar namespace para as primitivas e variáveis internas
  - <<http://www.standardista.com/javascript/15-common-javascript-gotchas/#overload>>
  - <<http://developer.mozilla.org/en-US/docs/Web/JavaScript/Same_origin_policy_for_JavaScript>>
  - <<http://sparecycles.wordpress.com/2008/06/29/advanced-javascript/>>
  - <<http://stackoverflow.com/questions/10344388/javascript-difference-between-namespace-vs-closure/10344420#10344420>>
- resolver o problema da primitiva carregue só funcionar com a presença do comando alert() (resolvido automaticamente em versões novas do firefox)
- implementar a primitiva 'gravedes'

#### BUGS
- ao mudar os limites, os logos ficam escalados
- gravedes não está 100% ok

#### EM CONSTRUÇÃO
- implementar retardo entre execução de comandos
- usar a primitiva 'carregue' para registrar o acesso no GA
- implementar as primitivas de movimento espelhado
- implementar primitivas para características específicas dos elementos svg
- implementar união dos vértices dos segmentos
- tornar transparente o uso do elemento circle na primitiva repita
<!-- -->
- implementar o paradigma de 4D
- integrar o paradigma de 4D às primitivas 2/3D
- configurar o mod_rewrite para arquivos .svl serem redirecionados como entrada de tela.svg
- resolver bug do layout do mod_autoindex em relacao ao xhtml

#### POR FAZER
- calcular onde deve aparecer o pontilhado em traços ocultos na geometria 3D
- suavizar a mudança de estado da tat (pf, pd, mudect, mudett)
- conversor/interpretador de arquivos originais do Hot Logo
- implementar as primitivas construtíves usando apenas comandos do Logo
- guardar as primitivas implementadas em namespaces separados por área
- criar uma primitiva para habilitar/desabilitar o namespace da área desejada e montar a lista de primitivas automaticamente
- modificar a primitiva 'primitivas' para devolver um array simbólico bi-dimensional com os nomes das áreas apontando para as primitivas correspondentes
- implementar primitiva para inserção e uso de elementos em na seção 'defs'
- implementar primitiva para agrupamento de elementos
- implementar primitivas de sprites
- implementar primitivas de velocidade
- implementar a primitiva 'pinte'



### Etapa 2: sintaxe JS interativo não integrado | texto != svg

#### CARACTERÍSTICAS
- XHTML
- CSS
- PHP



### Etapa 3: sintaxe JS interativo integrado | texto == svg



### Etapa 4: sintaxe HL interativo integrado | texto == svg



### Etapa 5: extender à outras tecnologias e integrar com API's

#### CARACTERÍSTICAS
- Google Maps  <<http://code.google.com/apis/maps/>>
- Google Earth <<http://code.google.com/apis/earth/>>
- implementar comandos de som
  - HTML5      <<http://developer.mozilla.org/En/Using_audio_and_video_in_Firefox>>
  - JAI    - Javascript Audio Interface <<http://hyper-metrix.com/misc/jai/>>
- MathML   - Mathematical Markup Language
- MusicXML - Music Extensible Markup Language
- CML      - Chemical Markup Language
- X3D      - Extensible 3D Graphics

## Lista de Primitivas

```

    [O]    8  operadores
    [E]    5  equivalente
    [H]  100  HL implementadas
     |   ---  -------------------------------
     |    92  total HL disponível
    [ ]   56  ainda não re-implementadas
     |   ---  -------------------------------
     |   168  total a serem re-implementadas
    [-]   15  não serão re-implementadas
     |   ---  -------------------------------
     |   183  total HL
     |
     |
     |   170  total a serem re-implementadas
    [D]    7  3/4D  implementadas
    [N]   20  novas implementadas
     |   ---  -------------------------------
     |   196  total SVL disponível
    [ ]    4  3/4D  ainda não implementadas
    [ ]   16  novas ainda não implementadas
     |   ---  -------------------------------
     |   216  total SVL a estarem disponíveis
    [A]   18  auxiliares
     |
     |
    [K]  155  OK: implementadas



  ARQUIVOS
  |		K H arquivos
  |		K H carregue		[bug]
  |		- H carreguec
  |		K H carreguedes
  |		  H eliminearq
  |		K H gravedes
  |		  H gravetudo
  |		- H gravetudoc
  |		K H mostrearq

  DESENHOS
  |		K H ad
  |		  H al
  |		K H carimbe
  |		  H carimbetudo		[difícil]
  |		  H cb
  |		K H cf
  |		K H cl
  |		K H mudecf
  |		K H mudecl
  |		K H mudeproporção
  |		K H pd
  |		K H pe
  |		K H pf
  |		  H pinte		[difícil]
  |		K H pp
  |		K H proporção
  |		K H pt
  |		K H rg
  |		  H tl
  |		  H ub
  |		  H ui
  |		K H ul
  |		K H un

  IMPRESSORA
  |		- H comimpressora
  |		- H semimpressora

  JOYSTICK
  |		- H joy
  |		- H ébotão
  |		- H éjoy

  LISTAS
  |		K H elemento
  |		K H jf
  |		K H ji
  |		K H lista
  |		K H nel
  |		K H pal
  |		K H pri
  |		K H sn
  |		K H sp
  |		K H su
  |		K H ult
  |		K H élista
  |		K H épalavra
  |		K H évazia

  LÓGICA
  |		  H algum
  |		  H e
  |		K H falso
  |		K H não
  |		E H se			if
  |		K H sãoiguais
  |		K H verd

  MATEMÁTICA
  |		O H (
  |		O H )
  |		O H *
  |		O H +
  |		O H -
  |		O H /
  |		O H (sinal de menor)
  |		E H =			==
  |		O H (sinal de maior)
  |		K H arctan
  |		K H arredonde
  |		K H cos
  |		K H diferença
  |		K H int
  |		K H produto
  |		K H quociente
  |		  H reproduza
  |		K H resto
  |		K H rq
  |		K H sen
  |		K H soma
  |		K H sorteieaté
  |		K H troquesinal
  |		K H énúmero

  ÁUDIO
  |		K H toque
  |		  H som

  OBSERVADORES
  |		  H elobs
  |		  H em.colisão
  |		  H moobs
  |		  H quando

  PROCEDIMENTOS
  |		E H ap			function
  |		  H copie
  |		  H defina
  |		  H ed
  |		  H el			[CONFLITANDO COM 'espessura lápis']
  |		  H elps
  |		E H envie		return
  |		  H faça
  |		- H fim
  |		K H mop
  |		K H mops		procedimentos
  |		K H mots		títulos
  |		  H nívelinicial
  |		  H pare
  |		K H texto
  |		K H éprocedimento

  PROPRIEDADES
  |		  H cp
  |		  H moprop
  |		  H prop
  |		  H rp
  |		  H rtp
  |		  H todasprop

  SPRITES
  |		K H copiafig		[fácil]
  |		K H cortat		ct
  |		K H criafigl		[fácil]
  |		K H edfig
  |		K H fig			[fácil]
  |		  H inverta
  |		K H listafig		[fácil]
  |		K H mudect
  |		K H mudefig		[fácil]

  TARTARUGAS
  |		K H at
  |		K H atat
  |		K H cada
  |		  H congele		[médio]
  |		K H coorx
  |		K H coory
  |		  H descongele		[médio]
  |		K H direçãopara		dçpara
  |		K H distância
  |		K H dt
  |		K H dç
  |		K H mudedç
  |		K H mudepos
  |		  H mudevel		[médio]
  |		  H mudevelx		[médio]
  |		  H mudevely		[médio]
  |		K H mudex
  |		K H mudey
  |		K H pc
  |		K H pca
  |		K H pos
  |		K H quem
  |		K H sótat
  |		K H todas
  |		  H vel			[médio]
  |		  H velx		[médio]
  |		  H vely		[médio]
  |		K H évisível

  TEXTO
  |		K H att
  |		  H care
  |		  H cursor
  |		K H esc
  |		  H line
  |		K H mo
  |		  H mudecursor
  |		  H mudeteto
  |		  H ponha

  VARIÁVEIS
  |		E H atr			var ... = ...
  |		  H col
  |		  H conteúdo
  |		  H edns
  |		  H eln
  |		  H elns
  |		  H mons		nomes
  |		K H temvalor

  USO GERAL
  |		  H eltudo
  |		K H espere		[idéia]
  |		  H motudo
  |		  H mudecor
  |		K H repita
  |		K H tat

  INFORMÁTICA
  |		K H .chame
  |		- H .deposite
  |		- H .entra
  |		- H .examine
  |		- H .sai
  |		K H ascii
  |		K H car
  |		- H liberemem
  |		- H memlivre
  |		K H primitivas
  |		  H temcar
  |		K H versão
  |		K H éprimitiva



  3/4D
  |		K D cpb
  |		K D cpc
  |		K D rpd
  |		K D rpe
  |		K D coorz
  |		  D coort
  |		K D mudez
  |		  D mudet
  |		  D velz
  |		  D mudevelz
  |		K D escondida



  NOVAS
  |		K N paleta
  |		K N lápis
  |		K N mudeqt		[quantidade tartarugas]
  |		K N retardo
  |		K N muderetardo
  |             K N procedimentos

  |		K N max_coorx
  |		K N max_coory
  |		K N min_coorx
  |		K N min_coory
  |                 -------------------
  |		K N mudelimites
  |		K N limites

  |		K N of			[opacidade fundo]
  |		K N ot			[opacidade tartaruga]
  |		K N tt			[tamanho tartaruga]
  |                 -------------------
  |		K N mudeof
  |		K N mudeot
  |		K N mudett

  |		K N el			[espessura lápis]
  |		K N ol			[opacidade lápis]
  |		  N mi			[marcador inicial]
  |		  N mc			[marcador central]
  |		  N mf			[marcador final]
  |		  N la			[limite aguçamento]
  |		K N traço
  |		K N junta
  |		K N ponta
  |		K N borrão
  |                 -------------------
  |		K N mudeel
  |		K N mudeol
  |		  N mudemi
  |		  N mudemc
  |		  N mudemf
  |		  N mudela
  |		K N mudetraço
  |		K N mudejunta
  |		K N mudeponta
  |		K N mudeborrão



  AUXILIARES
  |		K A AUX_dump
  |		K A AUX_logo
  |		K A AUX_parabaixo
  |		K A AUX_paracima
  |		K A AUX_posicione
  |		K A AUX_timestamp
  |		K A AUX_EndTiming
  |		K A AUX_GetCurrentTime
  |		K A AUX_StartTiming
  |		K A AUX_compare
  |		K A AUX_cosdç
  |		K A AUX_dçtan
  |		K A AUX_modç
  |                 -------------------
  |		K A AUX_ltrim
  |		K A AUX_rtrim
  |		K A AUX_itrim
  |		K A AUX_trim
  |		K A AUX_gtrim



  CONSTRUTÍVEIS
		C   algum
		C   ap
		C   arctan
		C   arredonde
		C   att

		C   care
		C   carreguec
		C   col
		C   comimpressora
		C   congele
		C   coorx
		C   coory
		C   copiafig
		C   copie
		C   cos

		C   descongele
		C   diferença
		C   direçãopara
		C   distância

		C   ed
		C   edfig
		C   elemento
		C   elobs
		C   eltudo
		C   esc
		C   espere

		C   int

		C   jf
		C   ji

		C   gravetudoc

		C   line
		C   lista

		C   moobs
		C   motudo
		C   mudeproporção
		C   mudevelx
		C   mudevely
		C   mudex
		C   mudey

		C   nel

		C   pc
		C   pca
		C   pd
		C   pe
		C   pf
		C   pinte
		C   pp
		C   pri
		C   primitivas
		C   produto
		C   proporção
		C   pt

		C   quociente

		C   repita
		C   reproduza
		C   resto
		C   rg
		C   rq

		C   semimpressora
		C   sen
		C   soma
		C   sãoiguais

		C   tat
		C   temcar
		C   todas
		C   troquesinal

		C   ult

		C   velx
		C   vely
		C   versão

		C   ébotão
		C   élista
		C   énúmero
		C   épalavra
		C   éprimitiva
		C   évazia

```

```
# EOF
```
