# SVL–Scalable Vector Logo

Dialeto em JS+SVG da Linguagem Logo da Geometria da Tartaruga baseado no antigo (Hot|MSX)-Logo dos anos 80.

As versões originais do Logo para MSX em [português](http://bit.ly/2lbTjzv), [inglês](http://bit.ly/2mR9liD), [espanhol](http://bit.ly/2nmKRyd) e [holandês](http://bit.ly/2lQMymZ) podem ser acessadas online e interativamente graças ao [WebMSX](http://webmsx.org).

<!--

MSX Logo pt_BR debug
MSX Logo en_US debug
MSX Logo es_ES debug
MSX Logo nl_NL debug

http://webmsx.org/?ROM=http://msxblog.es/wp-content/uploads/2009/09/msxlogo_por.zip&MACHINE=MSX1A&SCREEN_DEFAULT_ASPECT=1&SCREEN_CONTROL_BAR=0&SCREEN_FILTER_MODE=1&SCREEN_CRT_SCANLINES=5&SCREEN_CRT_PHOSPHOR=1&SCREEN_FULLSCREEN_MODE=2&SCREEN_DEFAULT_SCALE=0.5&CPU_TURBO_MODE=8&VDP_TURBO_MODE=9&SPEED=1000
http://webmsx.org/?ROM=http://msxblog.es/wp-content/uploads/2009/09/msxlogo_ing.zip&MACHINE=MSX1A&SCREEN_DEFAULT_ASPECT=1&SCREEN_CONTROL_BAR=0&SCREEN_FILTER_MODE=1&SCREEN_CRT_SCANLINES=5&SCREEN_CRT_PHOSPHOR=1&SCREEN_FULLSCREEN_MODE=2&SCREEN_DEFAULT_SCALE=0.5&CPU_TURBO_MODE=8&VDP_TURBO_MODE=9&SPEED=1000
http://webmsx.org/?ROM=http://msxblog.es/wp-content/uploads/2009/09/msxlogo_esp.zip&MACHINE=MSX1A&SCREEN_DEFAULT_ASPECT=1&SCREEN_CONTROL_BAR=0&SCREEN_FILTER_MODE=1&SCREEN_CRT_SCANLINES=5&SCREEN_CRT_PHOSPHOR=1&SCREEN_FULLSCREEN_MODE=2&SCREEN_DEFAULT_SCALE=0.5&CPU_TURBO_MODE=8&VDP_TURBO_MODE=9&SPEED=1000
http://webmsx.org/?ROM=http://msxblog.es/wp-content/uploads/2009/09/msxlogo_hol.zip&MACHINE=MSX1A&SCREEN_DEFAULT_ASPECT=1&SCREEN_CONTROL_BAR=0&SCREEN_FILTER_MODE=1&SCREEN_CRT_SCANLINES=5&SCREEN_CRT_PHOSPHOR=1&SCREEN_FULLSCREEN_MODE=2&SCREEN_DEFAULT_SCALE=0.5&CPU_TURBO_MODE=8&VDP_TURBO_MODE=9&SPEED=1000

-->

A implementação do dialeto propriamente dito está armazenada no diretório [dialeto/](http://gitlab.com/arkanon/svl/tree/master/dialeto/) e pode ser experimentado interativamente em [tela.svg](http://arkanon.gitlab.io/svl/tela.svg) através da console web do navegador (F12).

Exemplos:

- [teste1](https://arkanon.gitlab.io/svl/tela.svg?../teste1)
- [teste2](https://arkanon.gitlab.io/svl/tela.svg?../teste2)
- [teste3](https://arkanon.gitlab.io/svl/tela.svg?../teste3)
- [teste4](https://arkanon.gitlab.io/svl/tela.svg?../teste4)
- [teste5](https://arkanon.gitlab.io/svl/tela.svg?../teste5)
- [teste7](https://arkanon.gitlab.io/svl/tela.svg?../teste7)
<!-- -->
- fig1:
   [ex1](https://arkanon.gitlab.io/svl/tela.svg?fig1#5,140,2*360/n,500),
   [ex2](https://arkanon.gitlab.io/svl/tela.svg?fig1),
   [ex3](https://arkanon.gitlab.io/svl/tela.svg?fig1#37,140,17*360/n,50)
- [fig2](https://arkanon.gitlab.io/svl/tela.svg?fig2)
<!-- -->
- atat2:
   [6 graus](https://arkanon.gitlab.io/svl/tela.svg?atat2#6),
   [1 grau ](https://arkanon.gitlab.io/svl/tela.svg?atat2#1)
- [atat3](https://arkanon.gitlab.io/svl/tela.svg?atat3)
<!-- -->
- listas - acompanhar pela console web (F12):
   [listas1](https://arkanon.gitlab.io/svl/tela.svg?../listas1),
   [listas2](https://arkanon.gitlab.io/svl/tela.svg?../listas2),
   [listas3](https://arkanon.gitlab.io/svl/tela.svg?../listas3),
   [listas4](https://arkanon.gitlab.io/svl/tela.svg?../listas4),
   [listas5](https://arkanon.gitlab.io/svl/tela.svg?../listas5)
- curva de koch:
   [ex1](https://arkanon.gitlab.io/svl/tela.svg?koch#3,3),
   [ex2](https://arkanon.gitlab.io/svl/tela.svg?koch#3,5),
   [ex3](https://arkanon.gitlab.io/svl/tela.svg?koch#4,6),
   [ex4](https://arkanon.gitlab.io/svl/tela.svg?koch#5,5),
   [ex5](https://arkanon.gitlab.io/svl/tela.svg?koch#6,5),
   [ex6](https://arkanon.gitlab.io/svl/tela.svg?koch#7,5),
   [ex7](https://arkanon.gitlab.io/svl/tela.svg?koch#8,5)
- 3d:
   [esfera      ](https://arkanon.gitlab.io/svl/tela.svg?tri1-esfera),
   [prismas     ](https://arkanon.gitlab.io/svl/tela.svg?tri2-prismas),
   [casa        ](https://arkanon.gitlab.io/svl/tela.svg?tri3-casa),
   [atat_prismas](https://arkanon.gitlab.io/svl/tela.svg?tri6-atat_prismas),
   [piramides   ](https://arkanon.gitlab.io/svl/tela.svg?tri7-piramides)
- [spin](https://arkanon.gitlab.io/svl/tela.svg?spin)
- para esquerda/direita espelhado:
   [rb     ](https://arkanon.gitlab.io/svl/tela.svg?rb),
   [monster](https://arkanon.gitlab.io/svl/tela.svg?monster)
- anel:
   [3 100%](https://arkanon.gitlab.io/svl/tela.svg?anel#3,0,100),
   [3  50%](https://arkanon.gitlab.io/svl/tela.svg?anel#3,0,50),
   [3  10%](https://arkanon.gitlab.io/svl/tela.svg?anel#3,0,10),
   [3   0%](https://arkanon.gitlab.io/svl/tela.svg?anel#3,0,0),
   [3 120%](https://arkanon.gitlab.io/svl/tela.svg?anel#3,0,120),
   [3 150%](https://arkanon.gitlab.io/svl/tela.svg?anel#3,0,150),
   [3 300%](https://arkanon.gitlab.io/svl/tela.svg?anel#3,120,300),
   [3 600%](https://arkanon.gitlab.io/svl/tela.svg?anel#3,70,600),
   [4 100%](https://arkanon.gitlab.io/svl/tela.svg?anel#4,0,100),
   [5 100%](https://arkanon.gitlab.io/svl/tela.svg?anel#5,0,100),
   [6 100%](https://arkanon.gitlab.io/svl/tela.svg?anel#6,0,100),
   [7 100%](https://arkanon.gitlab.io/svl/tela.svg?anel#7,0,100)
- [space ship](https://arkanon.gitlab.io/svl/tela.svg?spcship)
- polígono radial:
   [  default](https://arkanon.gitlab.io/svl/tela.svg?polir),
   [  3 lados](https://arkanon.gitlab.io/svl/tela.svg?polir#3;140;1;10),
   [  4 lados](https://arkanon.gitlab.io/svl/tela.svg?polir#4;240;1;10),
   [  5 lados](https://arkanon.gitlab.io/svl/tela.svg?polir#5;230;1;10),
   [  6 lados](https://arkanon.gitlab.io/svl/tela.svg?polir#6;240;1;10),
   [  7 lados](https://arkanon.gitlab.io/svl/tela.svg?polir#7;250;6/7;10),
   [  7 lados dinâmico](https://arkanon.gitlab.io/svl/tela.svg?polir#7;500;180/eleve(2,6)/n;0),
   [  8 lados](https://arkanon.gitlab.io/svl/tela.svg?polir#8;250;9/10;10),
   [  9 lados](https://arkanon.gitlab.io/svl/tela.svg?polir#9;250;1;10),
   [ 10 lados](https://arkanon.gitlab.io/svl/tela.svg?polir#10;250;1;10),
   [ 11 lados](https://arkanon.gitlab.io/svl/tela.svg?polir#11;250;10/11;10),
   [360 lados](https://arkanon.gitlab.io/svl/tela.svg?polir#360;250;1/2;0),
   [720 lados](https://arkanon.gitlab.io/svl/tela.svg?polir#720;250;1/4;0)
- [La Bamba @ Hot-Logo @ WebMSX](https://arkanon.gitlab.io/svl/tela.svg?hotlogo)
- progressões:
   [triângulo            ](https://arkanon.gitlab.io/svl/tela.svg?peges#1),
   [triângulo+círculo    ](https://arkanon.gitlab.io/svl/tela.svg?peges#2),
   [quadrado             ](https://arkanon.gitlab.io/svl/tela.svg?peges#3),
   [quadrado+círculo     ](https://arkanon.gitlab.io/svl/tela.svg?peges#4),
   [retângulo áureo      ](https://arkanon.gitlab.io/svl/tela.svg?peges#5),
   [pentagrama           ](https://arkanon.gitlab.io/svl/tela.svg?peges#6),
   [espiral de arquimedes](https://arkanon.gitlab.io/svl/tela.svg?peges#7)
- caos:
   [tat        ](https://arkanon.gitlab.io/svl/tela.svg?caos),
   [tat rápida ](https://arkanon.gitlab.io/svl/tela.svg?caos#,0),
   [borboleta4 ](https://arkanon.gitlab.io/svl/tela.svg?caos#0),
   [borboleta2 ](https://arkanon.gitlab.io/svl/tela.svg?caos#1),
   [serpente2  ](https://arkanon.gitlab.io/svl/tela.svg?caos#2),
   [caranguejo1](https://arkanon.gitlab.io/svl/tela.svg?caos#3),
   [formiga1   ](https://arkanon.gitlab.io/svl/tela.svg?caos#4),
   [joaninha1  ](https://arkanon.gitlab.io/svl/tela.svg?caos#5),
   [abelha2    ](https://arkanon.gitlab.io/svl/tela.svg?caos#6),
   [camundongo1](https://arkanon.gitlab.io/svl/tela.svg?caos#7),
   [tartaruga7 ](https://arkanon.gitlab.io/svl/tela.svg?caos#8),
   [foguete1   ](https://arkanon.gitlab.io/svl/tela.svg?caos#9),
   [foguete2   ](https://arkanon.gitlab.io/svl/tela.svg?caos#10),
   [aviao1     ](https://arkanon.gitlab.io/svl/tela.svg?caos#11),
   [carro1     ](https://arkanon.gitlab.io/svl/tela.svg?caos#12),
   [enterprise1](https://arkanon.gitlab.io/svl/tela.svg?caos#13)
- [criafigl](https://arkanon.gitlab.io/svl/tela.svg?criafigl)
- [interação pelo teclado](https://arkanon.gitlab.io/svl/tela.svg?teclado)
- [edfig](https://arkanon.gitlab.io/svl/tela.svg?edfig)

<!--

     http://webmsx.org/?ROM=http://msxblog.es/wp-content/uploads/2009/09/msxlogo_por.zip&MACHINE=MSX2A&SCREEN_DEFAULT_ASPECT=1&SCREEN_CONTROL_BAR=0&SCREEN_FILTER_MODE=1&SCREEN_CRT_SCANLINES=5&SCREEN_CRT_PHOSPHOR=1&SCREEN_FULLSCREEN_MODE=2&SCREEN_DEFAULT_SCALE=0.5&CPU_TURBO_MODE=8&VDP_TURBO_MODE=9&SPEED=1000&FAST_BOOT=1&DISK_FILES=https://gitlab.com/arkanon/svl/raw/dev/hot-logo/desenhos/paisagem&BASIC_ENTER=carregue%20%22paisagem%20mudecf%201%20tudo

-->

